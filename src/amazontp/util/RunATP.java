package amazontp.util;

import java.io.*;
import java.util.Vector;

/**
 * Title:        Spock
 * Description:  Speech recognition
 * Copyright:    Copyright (c) 2001
 * Company:      UCSD
 * @author Aldebaro Klautau
 * @version 4.0
 */

/**
 * This class exists because when reading both stdin and stderr, we need to
 * avoid having one blocking the thread, such that it will stay eternally
 * waiting for some line of information. So, create another thread for reading
 * stderr.
 */
public class RunATP {

	private final boolean m_oexitIfWritesToStderr;

	private final boolean m_oprintOutputToConsole;

	// assume stderr is always echoed to console
	private final boolean m_oprintAllErrorsToConsoleInTheEnd;

	private Vector m_stderrBuffer = new Vector();

	private final boolean m_osaveOutputToFile;

	private final boolean m_oappendToFileInsteadOfOverwriting;

	private final boolean m_oalsoSaveErrToFile;

	// private final boolean m_orunEfficiently = false;
	private final String m_fileName;

	private BufferedWriter m_bufferedWriter = null;

	public RunATP(String command) {
		this(command, true, true);
	}

	/**
	 * Use this constructor for faster processing.
	 */
	public RunATP(String command, boolean oprintOutputToConsole) {
		this(command, false, oprintOutputToConsole, false, null, false, true,
				false);
	}

	public RunATP(String command, boolean oexitIfWritesToStderr,
			boolean oprintOutputToConsole) {
		this(command, oexitIfWritesToStderr, oprintOutputToConsole, false,
				null, false, false, false);
	}

	/**
	 * If both oprintOutputToConsole and osaveOutputToFile are false, this
	 * method will return after starting the external program (that may stay
	 * running on background).
	 * 
	 * If oalsoSaveErrToFile is false, echo stderr.
	 */
	public RunATP(String command, boolean oexitIfWritesToStderr,
			boolean oprintOutputToConsole, boolean osaveOutputToFile,
			String fileName, boolean oappendToFileInsteadOfOverwriting,
			boolean oprintAllErrorsToConsoleInTheEnd, boolean oalsoSaveErrToFile) {
		m_oprintOutputToConsole = oprintOutputToConsole;
		m_oexitIfWritesToStderr = oexitIfWritesToStderr;
		m_osaveOutputToFile = osaveOutputToFile;
		m_oappendToFileInsteadOfOverwriting = oappendToFileInsteadOfOverwriting;
		m_fileName = fileName;
		m_oprintAllErrorsToConsoleInTheEnd = oprintAllErrorsToConsoleInTheEnd;
		m_oalsoSaveErrToFile = oalsoSaveErrToFile;

		if (!oexitIfWritesToStderr && !osaveOutputToFile
				&& oprintAllErrorsToConsoleInTheEnd && !oalsoSaveErrToFile) {
			runCommandEfficiently(command);
		} else {
			runCommand(command);
		}
	}

	private void printStderrBuffer() {
		int n = m_stderrBuffer.size();
		if (n < 1) {
			return;
		}
		for (int i = 0; i < n; i++) {
			System.out.println((String) m_stderrBuffer.elementAt(i));
			System.out.flush();
		}
	}

	/**
	 * Example: runDOSCommand("command.com /C dir");
	 * 
	 * @param command
	 *            to be passed to shell
	 */
	public void runCommand(String command) {
		Process p = null;
		try {
			Runtime r = Runtime.getRuntime();
			p = r.exec(command);

			// if I keep the lines below, Java will not wait until the end of
			// the external program
			// if (!m_oprintOutputToConsole && !m_osaveOutputToFile) {
			// //keep external program running in background and return
			// return;
			// }

			if (m_osaveOutputToFile) {
				if (m_fileName == null) {
					// End.throwError("User passed a null file name!");
				}
				m_bufferedWriter = new BufferedWriter(new FileWriter(
						m_fileName, m_oappendToFileInsteadOfOverwriting));
			}

			MyThread myThread = new MyThread(p);

			BufferedReader bf1 = new BufferedReader(new InputStreamReader(p
					.getInputStream()));
			String t;
			if (m_bufferedWriter == null) {
				while ((t = bf1.readLine()) != null) {
					if (m_oprintOutputToConsole) {
						System.out.println(t);
						System.out.flush();
					}
				}
			} else {
				while ((t = bf1.readLine()) != null) {
					if (m_oprintOutputToConsole) {
						System.out.println(t);
						System.out.flush();
					}
					m_bufferedWriter.write(t);
					m_bufferedWriter.newLine();
					m_bufferedWriter.flush();
				}
			}
			bf1.close();
			if (myThread != null) {
				myThread.join();
			}
			if (m_bufferedWriter != null) {
				m_bufferedWriter.close();
			}
			System.out.flush();
			if (m_oprintAllErrorsToConsoleInTheEnd) {
				printStderrBuffer();
			}
			if (m_oexitIfWritesToStderr && myThread.getIfProgramWroteToStderr()) {
				// End.throwError("Exiting because external program wrote to
				// stderr");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * Example: runDOSCommand("command.com /C dir");
	 * 
	 * I am trying to minimize stealing CPU cycles here.
	 * 
	 * @param command
	 *            to be passed to shell
	 */
	// ak maybe, not sure, if there is an error, the program may not close
	// the input stream ? (only the error stream) In this case, I could stay
	// in the loop eternally...
	public void runCommandEfficiently(String command) {
		Process p = null;
		try {
			Runtime r = Runtime.getRuntime();
			p = r.exec(command);

			// MyThread myThread = new MyThread(p);

			BufferedReader bf1 = new BufferedReader(new InputStreamReader(p
					.getInputStream()));

			String t;

			if (m_oprintOutputToConsole) {
				while ((t = bf1.readLine()) != null) {
					System.out.println(t);
				}
			} else {
				// do nothing
				while ((t = bf1.readLine()) != null)
					;
			}
			System.out.flush();
			bf1.close();

			// check if there's something written to stderr
			try {
				BufferedReader bf2 = new BufferedReader(new InputStreamReader(p
						.getErrorStream()));
				while ((t = bf2.readLine()) != null) {
					System.err.println(t);
				}
				System.err.flush();
				bf2.close();
			} catch (IOException ex) {
				// if bf2 is not available for reading, avoid printing Exception
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class MyThread extends Thread {
		boolean m_okeepDoing = true;

		boolean m_oprogramWroteToStderr = false;

		Process m_process;

		public MyThread(Process process) {
			super("RunATP");
			m_process = process;
			start();
		}

		public void run() {
			BufferedReader bf2 = new BufferedReader(new InputStreamReader(
					m_process.getErrorStream()));
			try {
				while (m_okeepDoing) {
					String t = bf2.readLine();
					if (t != null) {
						m_oprogramWroteToStderr = true;
						// save the if because it's true
						if (m_oprintAllErrorsToConsoleInTheEnd) {
							m_stderrBuffer.addElement(t);
						} else if (m_oprintOutputToConsole) {
							System.err.println(t);
							System.err.flush();
						}
						if (m_bufferedWriter != null) {
							if (m_oalsoSaveErrToFile) {
								m_bufferedWriter.write(t);
								m_bufferedWriter.newLine();
								m_bufferedWriter.flush();
							} else {
								// if stderr is not saved to a file, it goes to
								// console stderr
								System.err.println(t);
								System.err.flush();
							}
						}
					} else {
						stopIt();
					}
				} // end of loop
				bf2.close();
			} catch (Exception e) {
				e.printStackTrace();
				// End.exit();
			}
		}

		public boolean getIfProgramWroteToStderr() {
			return m_oprogramWroteToStderr;
		}

		public void stopIt() {
			m_okeepDoing = false;
		}
	}

}
