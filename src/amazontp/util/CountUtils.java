package amazontp.util;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author Gustavo Cunha Guedes
 * @version 1.0
 * 
 */
public abstract class CountUtils {
	/**
	 * 
	 * @param string
	 *            The string you want to analyse
	 * @return The number of words in the string
	 */
	public static int countNumberOfWordsInString(String string) {
		return new StringTokenizer(string).countTokens();
	}

	/**
	 * 
	 * @param fileName
	 *            A string containing the path to the text file you want to
	 *            analyse
	 * @return The number of words in the file
	 */
	public static int countNumberOfWordsInFile(String fileName) {
		int nnumberOfWords = 0;
		String line;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(
					fileName));
			while ((line = bufferedReader.readLine()) != null) {
				nnumberOfWords += countNumberOfWordsInString(line);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Sorry! File not found.");
		} catch (IOException e) {
			System.err.println("Sorry! An input/output error has ocurred.");
		}
		return nnumberOfWords;
	}

	/**
	 * 
	 * @param fileName
	 *            A string containing the path to the text file you want to
	 *            analyse
	 * @return The number of lines in the file
	 */
	public static int countNumberOfLinesInFile(String fileName) {
		int nnumberOfLines = 0;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(
					fileName));
			while (bufferedReader.readLine() != null) {
				nnumberOfLines++;
			}
		} catch (FileNotFoundException e) {
			System.err.println("Sorry! File not found.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Sorry! An input/output error has ocurred.");
			System.exit(1);
		}
		return nnumberOfLines;
	}

	/**
	 * 
	 * @param string
	 *            The string you want to analyse
	 * @return The number of chars in the string
	 */
	public static int countNumberOfCharsWithoutSpacesInString(String string) {
		int nnumberOfChars = 0;
		StringTokenizer st = new StringTokenizer(string);
		for (int i = 0; st.hasMoreTokens(); i++) {
			nnumberOfChars += st.nextToken().length();
		}
		return nnumberOfChars;
	}

	/**
	 * 
	 * @param fileName
	 *            A string containing the path to the text file you want to
	 *            analyse
	 * @return The number of chars in the file, without counting blank spaces or
	 *         tabs
	 */
	public static int countNumberOfCharsWithoutSpacesInFile(String fileName) {
		int nnumberOfChars = 0;
		String line;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(
					fileName));
			while ((line = bufferedReader.readLine()) != null) {
				nnumberOfChars += countNumberOfCharsWithoutSpacesInString(line);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Sorry! File not found.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Sorry! An input/output error has ocurred.");
			System.exit(1);
		}
		return nnumberOfChars;
	}

	public static void main(String[] args) {
		String fileName = args[0];
		System.out.println("Number of lines in the file: "
				+ countNumberOfLinesInFile(fileName));
		System.out.println("Number of words in the file: "
				+ countNumberOfWordsInFile(fileName));
		System.out.println("Number of chars in the file: "
				+ countNumberOfCharsWithoutSpacesInFile(fileName));
	}
}
