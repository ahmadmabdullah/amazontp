package amazontp.util;

/**Print error and warning messages.
 * Provides a consistent way to inform the user,
 * that can be through messages printed to
 * a console, dialog window, etc.
 *
 * If GUI is enabled, show dialog window if
 * an error but not if warning (wants to avoid
 * program flow interruption waiting for user input).
 *
 * @author Aldebaro Klautau
 * @version 2.0 - September 26, 2000.
 */

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import java.io.PrintStream;
import javax.swing.JOptionPane;

public final class Print {

	public static PrintStream m_printStream;
	public static JLabel m_jlabel;
	public static JProgressBar m_jprogressBar;
	public static boolean m_oisConsoleOutputActive;
	public static boolean m_oisGUIOutputActive;
	public static int m_nprogressBarCurrentValue;

	//default is output to System.out
	static {
		setConsoleOutput(System.out);
	}

	public static void setJProgressBar(JProgressBar	jprogressBar) {
		if (jprogressBar != null) {
			m_jprogressBar = jprogressBar;
		}
	}

	public static void setJProgressBarRange(int nminimum, int nmaximum) {
		if (m_jprogressBar != null) {
			 m_jprogressBar.setMinimum(nminimum);
			 m_jprogressBar.setMaximum(nmaximum);
			 m_jprogressBar.setValue(nminimum);
		}
	}

	public static void updateJProgressBar(int nvalue) {
		if (m_jprogressBar != null) {
			m_jprogressBar.setValue(nvalue);
			m_nprogressBarCurrentValue = nvalue;
		}
	}

	public static void incrementJProgressBar() {
		if (m_jprogressBar != null) {
			m_nprogressBarCurrentValue++;
			m_jprogressBar.setValue(m_nprogressBarCurrentValue);
		}
	}

	public static void setConsoleOutput(PrintStream printStream) {
		if (printStream != null) {
			m_printStream = printStream;
			m_oisConsoleOutputActive = true;
		} else {
			m_oisConsoleOutputActive = false;
		}
	}

	public static void setGUIOutput(JLabel jlabel) {
		if (m_jlabel != null) {
			return;
		}
		//nobody used it, so allow initializing
		if (jlabel != null) {
			m_jlabel = jlabel;
			m_oisGUIOutputActive = true;
		} else {
			m_oisGUIOutputActive = false;
		}
	}

	public static void dialogOnlyToConsole(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.println(message);
		}
	}

	public static void dialog(Object[] array) {
		for (int i = 0; i < array.length; i++) {
			dialogWithoutNewLine(array[i].toString() + " ");
		}
		dialog("");
	}

	public static void dialog(Object obj) {
		//below is never executed because the method is overloaded
		//with an option where the argument is an array.
		if (obj.getClass().isArray()) {
			dialog((Object[]) obj);
		} else {
			dialog(obj.toString());
		}
	}

	public static void dialog(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.println(message);
		}
		if (m_oisGUIOutputActive) {
			m_jlabel.setText(message);
		}
	}

	public static void dialogWithoutNewLine(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.print(message);
		}
		if (m_oisGUIOutputActive) {
			m_jlabel.setText(message);
		}
	}

	public static void dialogWithoutNewLineOnlyToConsole(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.print(message);
		}
	}

	public static void error(String message) {
		if (m_oisConsoleOutputActive) {
			//m_printStream.println("ERROR: " + message);
			System.err.println("ERROR: " + message);
		}
		if (m_oisGUIOutputActive) {
			//do not show same message in two different places
			//also, avoids the error message staying at status bar after user clicks ok
			//m_jlabel.setText("ERROR: " + message);
			showErrorMessageWindow(message);
		}
	}

	public static void warning(String message) {
		if (m_oisConsoleOutputActive) {
			//m_printStream.println("WARNING: " + message);
			System.err.println("WARNING: " + message);
		}
		if (m_oisGUIOutputActive) {
			m_jlabel.setText("WARNING: " + message);
		}
	}

	public static void putStringOnlyToConsole(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.print(message);
		}
	}

	public static void overwritingCurrentLine(String message) {
		if (m_oisConsoleOutputActive) {
			String backspaces = "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
			int nlength = message.length();
			int nendIndex = (nlength > 60) ? 60:nlength;
			String cutMessage = message.substring(0,nendIndex);
			m_printStream.print(backspaces);
			m_printStream.print(message);
		}
	}

	public static void putString(String message) {
		if (m_oisConsoleOutputActive) {
			m_printStream.print(message);
		}
		if (m_oisGUIOutputActive) {
			String currentText = m_jlabel.getText();
			m_jlabel.setText(currentText + message);
		}
	}

	public static boolean isGUIEnabled() {
		return m_oisGUIOutputActive;
	}

	public static void showErrorMessageWindow(String errorDescription) {
				JOptionPane.showMessageDialog(null,
										errorDescription,
										"Error", JOptionPane.ERROR_MESSAGE);
	}

	public static void showWarningMessageWindow(String message) {
				JOptionPane.showMessageDialog(null,
										message,
										"Warning", JOptionPane.WARNING_MESSAGE);
	}

}
