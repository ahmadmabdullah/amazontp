package amazontp.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

public abstract class FormatValues {
	private static DecimalFormat m_rlcScientificNumberFormat100;

	private static DecimalFormat m_rlcScientificNumberFormat10;

	private static DecimalFormat m_rlcScientificNumberFormat1;

	private static DecimalFormat m_rlcScientificNumberFormat0;

	private static DecimalFormat m_rlcScientificNumberFormatMinus10;

	private static DecimalFormat m_rlcScientificNumberFormatMinus1;

	private static DecimalFormat m_switchTimeScientificNumberFormat;

	private static final double m_dfactorToConvertToLogBase10;

	private static Random m_random = new Random(System.currentTimeMillis());

	static {
		m_dfactorToConvertToLogBase10 = 1.0 / Math.log(10);
		Locale.setDefault(Locale.US);
		System.getProperties().getProperty("file.separator");
		// This locale will be used, otherwise can get confused with "," instead
		// of "."
		NumberFormat.getInstance(Locale.US);
		m_switchTimeScientificNumberFormat = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormat0 = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormat1 = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormat10 = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormat100 = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormatMinus10 = (DecimalFormat) NumberFormat
				.getInstance();
		m_rlcScientificNumberFormatMinus1 = (DecimalFormat) NumberFormat
				.getInstance();

		// Each range of numbers require a special format:
		// Maybe there's some smarter way... but this way works! :)
		m_rlcScientificNumberFormat100.applyPattern("0.E000");
		m_rlcScientificNumberFormat100.setMaximumFractionDigits(0);
		m_rlcScientificNumberFormat10.applyPattern("0.#E00");
		m_rlcScientificNumberFormat1.applyPattern("0.##E0");
		m_rlcScientificNumberFormat0.applyPattern("0.#E0");
		m_rlcScientificNumberFormatMinus10.applyPattern("0.E00");
		// ak not sure about the one below:
		m_rlcScientificNumberFormatMinus1.applyPattern("0.#E0");
		m_switchTimeScientificNumberFormat.applyPattern("0.###E0##");

	}

	// If I pass to ATP this value (without the dot): 1E-10
	// it interprets as 1.000E-12 Stupid ATP!
	// and 2 (no dot) as 2.000E-02
	// s is a string that in NOT in scientific notation
	public static String formatRLC(double x) {
		if (x < 0) {
			System.err.println("Error " + x + " is negative!");
			return "Error " + x + " is negative!";
		}
		String s = Double.toString(x);
		// ATP restricts the RLC values to be represented by 6 characteres and
		// requires a dot .
		int n = s.length();
		boolean oisDotThere = (s.indexOf(".") == -1) ? false : true;
		if (oisDotThere && n <= 6) {
			// string is ok
			return forceNCharacters(s, x, 6);
		}
		if (n <= 5) { // and oisDotThere = false otherwise the if above would
			// be true
			// it must be an integer number, so add . in the end
			return forceNCharacters(s + ".", x, 6);
		}
		// Otherwise we need to use scientific notation positive number
		if (x < 1.0E-99) {
			System.err.println("ERROR parsing x=" + x + " too small");
			return "ERROR parsing x=" + x + " too small";
		}
		// Find the exponent:
		int exponent = (int) (Math.log(x) * m_dfactorToConvertToLogBase10);

		// This way is elegant but slower:
		// int exponentDigits = Integer.toString(exponent).length();
		if (exponent < -10) {
			s = m_rlcScientificNumberFormatMinus10.format(x);
		} else if (exponent < 0) {
			s = m_rlcScientificNumberFormatMinus1.format(x);
		} else if (exponent < 1) {
			s = m_rlcScientificNumberFormat0.format(x);
		} else if (exponent < 10) {
			s = m_rlcScientificNumberFormat1.format(x);
		} else if (exponent < 100) {
			s = m_rlcScientificNumberFormat10.format(x);
		} else if (exponent < 1000) {
			s = m_rlcScientificNumberFormat100.format(x);
			if (s.indexOf(".") == -1) {
				if (s.length() < 6) {
					n = s.indexOf("E");
					s = s.substring(0, n) + "." + s.substring(n);
				}
			}
		} else {
			// Should never happen because the maximum double is around 1e300
			System.err.println("ERROR: " + x + " is too large!");
			return "ERROR: " + x + " is too large!";
		}
		return forceNCharacters(s, x, 6);
	}

	public static String formatSwitchTime(double x) {
		// Here time CAN be negative
		String s = Double.toString(x);
		// ATP restricts the switch time values to be represented by 10
		// characteres and requires a dot .
		int n = s.length();
		boolean oisDotThere = (s.indexOf(".") == -1) ? false : true;
		if (oisDotThere && n <= 10) {
			// string is ok
			return forceNCharacters(s, x, 10);
		}
		if (n <= 9) { // and oisDotThere = false otherwise the if above would
			// be true
			// it must be an integer number, so add . in the end
			return forceNCharacters(s + ".", x, 10);
		}
		// AK: I'm too tired to do something general and robust,
		// I will simply assume 10 chars are enough to represent time
		s = m_switchTimeScientificNumberFormat.format(x);
		return forceNCharacters(s, x, 10);
	}

	private static String forceNCharacters(String s, double x, int N) {
		int n = s.length();
		if (n > N) {
			System.err.println("ERROR: " + x + " was wrongly parsed as " + s);
			return "ERROR: " + x + " was wrongly parsed as " + s;
		}
		if (n == N) {
			return s;
		}
		String fillIn = "                                                                                ";
		return fillIn.substring(0, N - n) + s;
	}
}
