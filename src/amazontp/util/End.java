package amazontp.util;

/**Called when application or applet wants to report an error or exit.
 *
 * @author Aldebaro Klautau
 * @version 2.0 - September 26, 2000.
 */

//maybe this class should perform the necessary operations
//to stop running all threads...

public final class End {

	//whatever number...
	private final static int m_ndefaultErrorCode = 1;

	public static void exit(int nerrorCode) {
		System.exit(nerrorCode);
	}

	public static void exit(String errorMessage) {
		Print.error(errorMessage);
		System.exit(m_ndefaultErrorCode);
	}

	public static void exit() {
		System.exit(m_ndefaultErrorCode);
	}

	public static void throwError(String errorDescription) {
		Print.error(errorDescription);
		throw new Error(errorDescription);
	}

	public static void throwError() {
		Print.error("ERROR !");
		throw new Error();
	}

}
