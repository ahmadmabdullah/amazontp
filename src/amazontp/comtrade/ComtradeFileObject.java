package amazontp.comtrade;
/**
 * <p>Title: comtrade file reader</p>
 * <p>Description: Object that contain COMTRADE data and information</p>
 * <p>Copyright: Copyright (c) 2001</p>
 * <p>Company: UFPA</p>
 * @author Rafael Marinho
 * @version 0.1 (beta)
 */

public class ComtradeFileObject {
	// Station where data was obtainded identification
	public String m_stationName;

	// Device where data was obtained identification
	public String m_recDeviceId;

	// Contrade Revision date
	public int m_contradeRevisionYear = 0000;

	// total # of channels
	public int m_tt;

	// # of analogic channels
	public int m_nOfAnalogicChannels ;

	// # of digital channels
	public int m_nOfDigitalChannels ;

	// analog data information
	public AnalogChannelInformation[] m_analogicChannelInformation;

	// digital data information
	public DigitalChannelInformation[] m_digitalChannelInformation;

	// number of rates in the data file
	public int m_nRates = 0;

	// sampling rate in Hz
	public double[] m_samp;

	// the last samples in the same indexed m_samp variable
	public int[] m_endSamp;

	// line frequency
	public double m_lf = 0.0;

	// data type (ft = file type)
	public String m_ft = "BINARY";

	// time multplier, it'd the deltaX of samples
	public double m_timemult = 0.0;

	// information readed from DAT File
	public double[][] m_data;
	
	public ComtradeFileObject(int nOfAnalogicChannels, int nOfDigitalChannels){
		this.m_nOfAnalogicChannels=nOfAnalogicChannels;
		this.m_nOfDigitalChannels=nOfDigitalChannels;
	}

	public void setStationName(String stationName) {
		m_stationName = stationName;
	}

	public void setRecDeviceId(String recDeviceId) {
		m_recDeviceId = recDeviceId;
	}

	public void setContradeRevisionYear(int contradeRevisionYear) {
		m_contradeRevisionYear = contradeRevisionYear;
	}

	public void setTT(int tt) {
		m_tt = tt;
	}

	public void setNOfAnalogicChannels(int nOfAnalogicChannels) {
		m_nOfAnalogicChannels = nOfAnalogicChannels;
	}

	public void setNRates(int nRates) {
		m_nRates = nRates;
	}

	public void setSamp(double[] samp) {
		for (int i = 0; i < samp.length; i++) {
			m_samp[i] = samp[i];
		}
	}

	public void setEndSamp(int[] endSamp) {
		for (int i = 0; i < endSamp.length; i++) {
			m_endSamp[i] = endSamp[i];
		}
	}

	public void setLF(double lf) {
		m_lf = lf;
	}

	public void setFT(boolean ft) {
		if (ft == true) {
			m_ft = "BYNARY";
		} else {
			m_ft = "ASCII";
		}
	}

	public double getTimeMult() {
		return m_timemult;
	}

	public String getStationName() {
		return m_stationName;
	}

	public String getRecDeviceId() {
		return m_recDeviceId;
	}

	public int getContradeRevisionYear() {
		return m_contradeRevisionYear;
	}

	public int getTT() {
		return m_tt;
	}

	public int getNOfAnalogicChannels() {
		return m_nOfAnalogicChannels;
	}

	public int getNRates() {
		return m_nRates;
	}

	public double[] getSamp() {
		return m_samp;
	}

	public int[] getEndSamp() {
		return m_endSamp;
	}

	public double getLF() {
		return m_lf;
	}

	public String getFT() {
		return m_ft;
	}

	public void getTimeMult(double timemult) {
		m_timemult = timemult;
	}

	public ComtradeFileObject() {
	}

	public AnalogChannelInformation[] getAnalogicChannelInformation() {
		return m_analogicChannelInformation;
	}

	public void setAnalogicChannelInformation(
			AnalogChannelInformation[] dataInformation) {
		m_analogicChannelInformation = dataInformation;
	}

	public DigitalChannelInformation[] getDigitalChannelInformation() {
		return m_digitalChannelInformation;
	}

	public void setDigitalChannelInformation(
			DigitalChannelInformation[] dataInformation) {
		m_digitalChannelInformation = dataInformation;
	}

	public int getNOfDigitalChannels() {
		return m_nOfDigitalChannels;
	}

	public void setNOfDigitalChannels(int ofDigitalChannels) {
		m_nOfDigitalChannels = ofDigitalChannels;
	}

	
}
