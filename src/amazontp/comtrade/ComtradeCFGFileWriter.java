package amazontp.comtrade;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;
import amazontp.io.SaveFile;

public class ComtradeCFGFileWriter {

	private static boolean isThereAllDigitalLines = false;

	private static boolean isThereAllAnalogicLines = false;

	private static int lastDigitalIndex = 0;

	private static int lastAnalogicIndex = 0;

	private static int dateLineNumber = 0;

	private static int TT;

	private static int A;

	private static int D;

	private static String firstLine(String station_name, String rec_dev_id,
			int rev_year) {

		if (station_name.length() > 64) {
			error("Station name maximum length must be 64 characters.");
		}

		if (rec_dev_id.length() > 64) {
			error("Recording device name maximum length must be 64 characters.");
		}

		if (rev_year != 1991 && rev_year != 1999) {
			error("Revision year must be 1991 or 1999.");
		}

		String line = station_name + "," + rec_dev_id + "," + rev_year + "\r\n";

		return line;
	}

	private static String secondLine(int total, int analogic, int digital) {

		TT = total;
		A = analogic;
		D = digital;

		if (analogic == 0) {
			isThereAllAnalogicLines = true;
		}

		if (digital == 0) {
			isThereAllDigitalLines = true;
		}

		if (total < 1) {
			error("There must be at least 1 channel.");
		}

		if (total > 999999) {
			error("Number of channels must be lower than 999999.");
		}

		if (analogic > 999999) {
			error("Number of analogic channels must be lower than 999999.");
		}

		if (digital > 999999) {
			error("Number of digital channels must be lower than 999999.");
		}

		if (total != (analogic + digital)) {
			error("Number of analogic and digital channels must be equal to number of channels");
		}

		String line = total + "," + analogic + "A," + digital + "D" + "\r\n";

		return line;
	}

	// Não terminado ainda.
	private static String analogicLines(int index, String ch_id, String ph,
			String ccbm, String uu, float a, float b, float skew, int min,
			int max, float primary, float secondary, String PS) {

		if (index <= 0) {
			error("Index of an analogic line cannot be zero or negative");
		}

		if (index > A) {
			error("Index "
					+ index
					+ "of analogic line out of bounds: bigger than the number of analogic lines (A).");
		}

		if (index == A) {
			isThereAllAnalogicLines = true;
		}

		if (index != lastAnalogicIndex + 1) {
			error("ERROR: Index " + index + " cannot come after "
					+ lastAnalogicIndex + " in analogic lines");
		}

		lastAnalogicIndex = index;

		if (ch_id.length() > 32) {
			error("Channel ID field must have less than 32 characters.");
		}

		if (ph.length() > 2) {
			error("Channel phase ID field must have less than 2 characters.");
		}

		if (ccbm.length() > 64) {
			error("Circuit component being monitored (ccbm) field must have less than 64 characters.");
		}

		if (uu.length() > 32) {
			error("Channel Units field must have less than 32 characters.");
		}

		if (hasNumber(uu)) {
			error("Channel Units field cannot contain numbers.");
		}

		if (("" + a).length() > 32) {
			error("Channel multiplier field cannot have more than 32 characters.");
		}

		if (("" + b).length() > 32) {
			error("Channel offset adder field cannot have more than 32 characters.");
		}

		if (min < -99999) {
			error("Range minimum data value cannot be lower than -99999");
		}

		if (min > 99999) {
			error("Range minimum data value cannot be bigger than 99999");
		}

		if (max < -99999) {
			error("Range maximum data value cannot be lower than -99999");
		}

		if (max > 99999) {
			error("Range maximum data value cannot be bigger than 99999");
		}

		if (("" + primary).length() > 32) {
			error("Ratio primary factor field cannot have more than 32 characters.");
		}

		if (("" + secondary).length() > 32) {
			error("Ratio secondary factor field cannot have more than 32 characters.");
		}

		if (!PS.equals("p") && !PS.equals("P") && !PS.equals("s")
				&& !PS.equals("S")) {
			error("PS field can only assume the values p, P, s, S.");
		}

		String line = index + "," + ch_id + "," + ph + "," + ccbm + "," + uu
				+ "," + a + "," + b + "," + min + "," + max + "," + primary
				+ "," + secondary + "," + PS + "\r\n";

		return line;
	}

	private static String digitalLine(int index, String ch_id, String ph,
			String ccbm, int y) {

		if (index <= 0) {
			error("Index of a digital line cannot be zero or negative");
		}

		if (index > D) {
			error("Index "
					+ index
					+ " of digital line out of bounds: bigger than the number of digital lines (D).");
		}

		if (index == D) {
			isThereAllDigitalLines = true;
		}

		if (index != lastDigitalIndex + 1) {
			error("ERROR: Index " + index + " cannot come after "
					+ lastDigitalIndex + " in digital lines");
		}

		lastDigitalIndex = index;

		if (ch_id.length() > 32) {
			error("Channel ID field must have less than 32 characters.");
		}

		if (ph.length() > 2) {
			error("Channel phase ID field must have less than 2 characters.");
		}

		if (ccbm.length() > 64) {
			error("Circuit component being monitored (ccbm) field must have less than 64 characters.");
		}

		if (y != 1 && y != 0) {
			error("Normal state of channel field must be 0 or 1");
		}

		String line = index + "," + ch_id + "," + ph + "," + ccbm + "," + y
				+ "\r\n";

		return line;

	}

	private static String lineFrequency(float lf) {

		String line = "" + lf;

		if (line.length() > 32) {
			error("Line frequency maximum length must be 32 characters.");
		}

		return line + "\r\n";
	}

	private static String nrates(int nrates) {

		if (nrates > 999) {
			error("Number of sample rates must be lower then 999.");
		}

		return "" + nrates + "\r\n";
	}

	private static String sampLine(float samp, int endsamp) {

		String line = "" + samp;

		if (line.length() > 32) {
			error("Sample rate length must be lower then 32 characters.");
		}

		line = "" + endsamp;

		if (line.length() > 10) {
			error("Last sample number (endsamp) length must be lower then 10 characters.");
		}

		line = samp + "," + endsamp;

		return line + "\r\n";
	}

	private static String dateLine(int day, int month, int year, int hour,
			int minutes, float seconds) {

		dateLineNumber++;

		if (dateLineNumber > 2) {
			error("Only 2 date lines are supported by COMTRADE.");
		}

		String dayStr = "" + day;
		String monthStr = "" + month;
		String yearStr = "" + year;
		String hourStr = "" + hour;
		String minutesStr = "" + minutes;
		String secondsStr = "" + seconds;

		if (day < 0) {
			error("Day of the month field can't be negative.");
		}

		if (day > 31) {
			error("Day of the month field must be lower than or equal to 31.");
		}

		if (dayStr.length() == 1) {
			dayStr = "0" + day;
		}

		if (month < 0) {
			error("Month field can't be negative.");
		}

		if (month > 12) {
			error("Month field must be lower than or equal to 12.");
		}

		if (monthStr.length() == 1) {
			monthStr = "0" + month;
		}

		if (year < 0) {
			error("Year field can't be negative.");
		}

		if (year > 9999 || yearStr.length() != 4) {
			error("Year field must have 4 characters.");
		}

		if (hour < 0) {
			error("Hour field can't be negative.");
		}

		if (hour > 23) {
			error("Hour field must be lower than or equal to 23.");
		}

		if (hourStr.length() == 1) {
			hourStr = "0" + hour;
		}

		if (minutes < 0) {
			error("Minutes field can't be negative.");
		}

		if (minutes > 59) {
			error("Minutes field must be lower than or equal to 59.");
		}

		if (minutesStr.length() == 1) {
			minutesStr = "0" + minutes;
		}

		if (seconds < 0) {
			error("Seconds field can't be negative.");
		}

		if (seconds > 60) {
			error("Seconds field must be lower than 60.");
		}

		// Preciso de um bom tratamento para deixar o seconds field exatamente
		// no formato ss.ssssss! Por enquanto, vamos partir do principio de que
		// a função sempre receberá um numero nesse formato e nao convertera
		// para a notacao logaritmica.

		String line = dayStr + "/" + monthStr + "/" + yearStr + "," + hourStr
				+ ":" + minutesStr + ":" + secondsStr + "\r\n";

		return line;
	}

	private static String fileType(String ft) {

		if (!ft.equals("binary") && !ft.equals("BINARY") && !ft.equals("ASCII")
				&& ft.equals("ascii")) {
			error("File type field must be BINARY (or binary) or ASCII (or ascii)");
		}

		return ft + "\r\n";
	}

	private static String timeMult(float mult) {

		String line = "" + mult;

		if (line.length() > 32) {
			error("Multiplication factor for timestamp field length must be slower than 32.");
		}

		return line + "\r\n";
	}

	private static void error(String message) {
		System.err.println(message);
		System.exit(1);
	}

	private static boolean hasNumber(String s) {
		Character ca = null;
		boolean hasNumber = false;
		for (int i = 0; i < s.length(); i++) {
			if (ca.isDigit(s.charAt(i)))
				hasNumber = true;
		}
		return hasNumber;
	}

	private static String CFGFileName(String index, String dir) {
		String cfgfile = null;
		String dateformat = "yyyy_MM_dd";
		Date da = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(dateformat);
		cfgfile = dir + formatter.format(da) + "_" + index + ".cgf";
		return cfgfile;
	}

	public static void CFGFileWriter(String contentcfgfile, String dir,
			String indexfile) throws IOException {
		String cfgfilename = CFGFileName(indexfile, dir);
		SaveFile sf = new SaveFile();
		sf.deleteFile(cfgfilename);
		sf.createFile(cfgfilename);
		sf.saveFile(cfgfilename, contentcfgfile);
	}

	public static void main(String[] args) {

		String teste = "", comtradedir;
		comtradedir = "C:\\";
		teste = teste+firstLine(
				"Arquivo de testes do CFGFileWriter - Jose Borges - LaPS UFPA",
				"999", 1999)
				;
		teste = teste +secondLine(6, 6, 0) ;
		teste = teste +analogicLines(1, "MB500A", "", "v", "V", (float) 2.727170e+01,
				(float) -4.468060e+05, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		teste = teste +analogicLines(2, "MB500B", "", "v", "V", (float) 2.727172e+01,
				(float) -4.468061e+05, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		teste = teste +analogicLines(3, "MB500B", "", "v", "V", (float) 2.727172e+01,
				(float) -4.468063e+05, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		teste = teste +analogicLines(4, "TU131A", "", "v", "V", (float) 7.016816e-01,
				(float) -1.149600e+04, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		teste = teste +analogicLines(5, "TU131B", "", "v", "V", (float) 7.016811e-01,
				(float) -1.149599e+04, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		teste = teste +analogicLines(6, "TU131C", "", "v", "V", (float) 7.016811e-01,
				(float) -1.149599e+04, (float) 0, 0, 32767, (float) 1,
				(float) 1, "p");

		// teste = digitalLine(1, "aaa", "A", "", 1);
		// System.out.println(teste);
		// teste = digitalLine(2, "bb", "B", "", 0);
		// System.out.println(teste);
		// teste = digitalLine(3, "c", "C", "", 1);

		teste = teste+lineFrequency(50) ;

		teste = teste+nrates(1) ;

		teste = teste+sampLine((float) 40000, 40001);

		teste = teste+dateLine(2, 5, 2006, 13, 40, (float) 47.000000) ;

		//teste = teste+dateLine(2, 5, 2006, 13, 40, (float) 47.000000);

		teste = teste+fileType("ASCII") ;

		teste = teste+timeMult((float) 4.656613e-10);
		System.out.println(teste);

		if (isThereAllDigitalLines == false) {
			error("ERROR: The generated file is wrong because not all digital lines were written.");
		} else {
			if (isThereAllAnalogicLines == false) {
				error("ERROR: The generated file is wrong because not all analogic lines were written.");
			} else {
				System.err.println("File successfuly generated!");
				try {

					CFGFileWriter(teste, comtradedir, "1");
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	}
}
