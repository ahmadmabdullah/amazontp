package amazontp.comtrade;

import java.util.*;

public class AnalogChannelInformation {
	/** The analog channel index number */
	private int m_An=0;

	/** The channel indentifier */
	private String m_chid=null;

	/** The channel phase identification */
	private String m_ph=null;

	/** The circuit component being monitored */
	private String m_ccbm=null;

	/** the channel units */
	private String m_uu=null;

	/** the channel multiplier */
	private double m_a=0.0;

	/** the channel offset adder */
	private double m_b=0.0;

	/** the channel time skew (in ms) from start of sample period */
	private double m_skew=0.0;

	/**
	 * The range minimum data value (lower limit of possible data value range)
	 * for data values of this channel
	 */
	private int m_min=0;

	/**
	 * the range maximum data value (upper limit of possible data value range)
	 * for data values of this channel
	 */
	private int m_max = 0;

	/** the channel voltage or current trannsformer ratio primary factor */
	private double m_primary=0.0;

	/** the channel voltage or current trannsformer ratio secondary factor */
	private double m_secondary=0.0;

	/**
	 * the primary or secondary data scaling identifier. The character specifies
	 * whether the value received from the channel conversion factor will
	 * represent a primary(P) or secondary(S)value
	 */
	private String m_ps=null;

	
	
	
	public double getA() {
		return m_a;
	}

	public void setAa(double m_a) {
		this.m_a = m_a;
	}

	public int getAn() {
		return m_An;
	}

	public void setAn(int an) {
		m_An = an;
	}

	public double getB() {
		return m_b;
	}

	public void setB(double m_b) {
		this.m_b = m_b;
	}

	public String getCcbm() {
		return m_ccbm;
	}

	public void setCcbm(String m_ccbm) {
		this.m_ccbm = m_ccbm;
	}

	public String getChid() {
		return m_chid;
	}

	public void setChid(String m_chid) {
		this.m_chid = m_chid;
	}

	public int getMax() {
		return m_max;
	}

	public void setMax(int m_max) {
		this.m_max = m_max;
	}

	public int getMin() {
		return m_min;
	}

	public void setMin(int m_min) {
		this.m_min = m_min;
	}

	public String getPh() {
		return m_ph;
	}

	public void setPh(String m_ph) {
		this.m_ph = m_ph;
	}

	public double getPrimary() {
		return m_primary;
	}

	public void setPrimary(double m_primary) {
		this.m_primary = m_primary;
	}

	public String getPs() {
		return m_ps;
	}

	public void setPs(String m_ps) {
		this.m_ps = m_ps;
	}

	public double getSecondary() {
		return m_secondary;
	}

	public void setSecondary(double m_secondary) {
		this.m_secondary = m_secondary;
	}

	public double getSkew() {
		return m_skew;
	}

	public void setSkew(double m_skew) {
		this.m_skew = m_skew;
	}

	public String getUu() {
		return m_uu;
	}

	public void setUu(String m_uu) {
		this.m_uu = m_uu;
	}

	public static void main(String[] args) {
		System.out.println(Double.MAX_VALUE);

	}

	public String toString() {
		return " AN: " + getAn() + "\n Ch_ID : " + getChid() + "\n ph: "
				+ getPh() + "\n ccbm: " + getCcbm() + "\n uu: " + getUu()
				+ "\n a: " + getA() + "\n b: " + getB() + "\n skew: "
				+ getSkew() + "\n min: " + getMin() + "\n max: " + getMax()
				+ "\n Primary: " + getPrimary() + "\n Secondary: "
				+ getSecondary() + "\n PS: " + getPs();
	}
	

}
