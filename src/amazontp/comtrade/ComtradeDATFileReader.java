package amazontp.comtrade;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class ComtradeDATFileReader extends ComtradeFileObject {

	ComtradeCFGFileReader cfg; // read file configuration
	// static byte[] m_w;

	public static short readShort(DataInputStream bf) throws EOFException,
			IOException {
		byte[] w = { 0, 0 };
		bf.readFully(w, 0, 2);
		// w[0] = bf.readByte();
		// w[1] = bf.readByte();
		return (short) ((w[1] & 0xff) << 8 | (w[0] & 0xff));
	}

	public static int readInt(DataInputStream bf) throws EOFException,
			IOException {
		byte[] w = { 0, 0, 0, 0 };
		bf.readFully(w);
		return (int) (w[3]) << 24 | (w[2] & 0xff) << 16 | (w[1] & 0xff) << 8
				| (w[0] & 0xff);
	}

	public static int[] readShortAsBits(DataInputStream bf)
			throws EOFException, IOException {
		byte[] w = { 0, 0 };
		int[] ret_BITS = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		bf.readFully(w, 0, 2);

		ret_BITS[7] = (w[1] & 0x01);
		ret_BITS[6] = ((w[1] >> 1) & 0x01);
		ret_BITS[5] = ((w[1] >> 2) & 0x01);
		ret_BITS[4] = ((w[1] >> 3) & 0x01);
		ret_BITS[3] = ((w[1] >> 4) & 0x01);
		ret_BITS[2] = ((w[1] >> 5) & 0x01);
		ret_BITS[1] = ((w[1] >> 6) & 0x01);
		ret_BITS[0] = ((w[1] >> 7) & 0x01);
		ret_BITS[15] = (w[0] & 0x01);
		ret_BITS[14] = ((w[0] >> 1) & 0x01);
		ret_BITS[13] = ((w[0] >> 2) & 0x01);
		ret_BITS[12] = ((w[0] >> 3) & 0x01);
		ret_BITS[11] = ((w[0] >> 4) & 0x01);
		ret_BITS[10] = ((w[0] >> 5) & 0x01);
		ret_BITS[9] = ((w[0] >> 6) & 0x01);
		ret_BITS[8] = ((w[0] >> 7) & 0x01);

		// for (int i = 0; i < 16; i++) {
		// System.out.println(ret_BITS[i]);
		// }

		/*
		 * byte currentBit; byte one = 0x01;
		 * 
		 * for (int i = 0; i < 8; i++) { currentBit = (byte) (w[0] & 0x01); if
		 * (currentBit == one) { ret_BITS[i] = 1; } else { ret_BITS[i] = 0; }
		 * w[0] = (byte) (w[0] >> 1); } for (int i = 8; i < 16; i++) {
		 * currentBit = (byte) (w[1] & 0x01); if (currentBit == one) {
		 * ret_BITS[i] = 1; } else { ret_BITS[i] = 0; } w[1] = (byte) (w[1] >>
		 * 1); }
		 */

		// for (int i = 0; i < 16; i++) {
		// System.out.print(ret_BITS[i]);
		// }
		return ret_BITS;

	}

	public ComtradeDATFileReader(String bf) throws Exception {
		FileInputStream fstream = new FileInputStream(bf);
		DataInputStream bf_file = new DataInputStream(fstream);

		cfg = new ComtradeCFGFileReader(bf.replace("dat", "cfg"));

		Vector sampleNumber = new Vector();
		Vector timeStamp = new Vector();

		int[] analogicChannels = new int[cfg.getNOfAnalogicChannels()];
		int[] digitalChannels = new int[cfg.getNOfDigitalChannels()];
		this.m_nOfAnalogicChannels = cfg.getNOfAnalogicChannels();
		this.m_nOfDigitalChannels = cfg.getNOfDigitalChannels();

		//Vector analogicSignalChannelss = new Vector();
		ArrayList  analogicSignalChannels  = new ArrayList();
		Vector digitalSignalChannels = new Vector();

		double nDigitalShortChannel = 0;

		nDigitalShortChannel = cfg.getNOfDigitalChannels() / 16;
		if (nDigitalShortChannel != (int) nDigitalShortChannel) {
			nDigitalShortChannel = Math.ceil(nDigitalShortChannel);
		}
		BufferedWriter bufferedwriter = new BufferedWriter(new FileWriter(
		"C://simulacao.txt"));
		BufferedReader bufferedreader = new BufferedReader(new FileReader(
				"C://simulacao2.txt"));
		int[] aux;
		try {
			String line=null;
			
			while (true) {

				// sampleNumber.add(Integer.valueOf(readInt(bf_file)));
				// timeStamp.add(Integer.valueOf(readInt(bf_file)));

				for (int k = 0; k < m_nOfAnalogicChannels; k++) {
					analogicChannels[k] = readShort(bf_file);
				}
				
				for (int k = 0; k < m_nOfAnalogicChannels; k++) {
					System.out.print(analogicChannels[k]+" ") ;
				}
				System.out.println();
				
				
				line = bufferedreader.readLine();
				analogicSignalChannels.add(analogicChannels);
				//System.out.println(line);
				if((line!= null)){
				bufferedwriter.write(line+", "+analogicChannels[0]+"\r\n");
				}
				else{
					//bufferedwriter.write("xxx"+", "+analogicChannels[0]+"\r\n");
				}
				
				for (int k = 0; k < (int) nDigitalShortChannel; k++) {
					aux = readShortAsBits(bf_file);
					for (int i = 0; i < aux.length; i++) {
						// digitalChannels[i] += aux[i];
					}
				}

				// digitalSignalChannels.add(digitalChannels);

			}
			
			
		} // fim do try
		catch (EOFException e) {
		} finally {
			try {
				bufferedwriter.close();
				bufferedreader.close();
				// sampleNumber.trimToSize();
				// timeStamp.trimToSize();
			//	analogicSignalChannels.trimToSize();
				System.out.println(analogicSignalChannels.size());
				
				// digitalSignalChannels.trimToSize();
			} catch (Exception e) {
			}
		}
	}

	public static void main(String[] args) throws Exception {
		String s = "E:/claudomir/contrade/2005_04_19_11_19_58/2005_04_19_11_19_58.dat";
		new ComtradeDATFileReader(s);
	}
}
