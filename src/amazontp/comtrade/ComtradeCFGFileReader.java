package amazontp.comtrade;
/**
 * <p>Title: comtrade file reader</p>
 * <p>Description: CFG File reader (atomic read)</p>
 * <p>Copyright: Copyright (c) 2001</p>
 * <p>Company: UFPA</p>
 * @author Rafael Marinho
 * @version 0.1 (beta)
 */

import java.io.*;
import java.util.*;

// import comtrade.*;

public class ComtradeCFGFileReader extends ComtradeFileObject {

	private String[] m_startAcq;

	private String[] m_endsAcq;

	private void getID(String line) {
		StringTokenizer st_line = null;

		st_line = new StringTokenizer(line, ",");

		int n_st = st_line.countTokens();
		m_stationName = st_line.nextToken();
		m_recDeviceId = st_line.nextToken();
		if (n_st == 3) {
			m_contradeRevisionYear = Integer.parseInt(st_line.nextToken());
		}
	}

	private void getChanelsID(String line) {
		StringTokenizer st_line = null;

		st_line = new StringTokenizer(line, ",");

		m_tt = Integer.parseInt(st_line.nextToken());
		m_nOfAnalogicChannels = Integer.parseInt((new StringTokenizer(st_line
				.nextToken(), "A")).nextToken());
		m_nOfDigitalChannels = Integer.parseInt((new StringTokenizer(st_line
				.nextToken(), "D")).nextToken());
	}

	/*
	 * private AnalogChannelInformation[] getAnalogicChannelInformation(String
	 * line) { /* According with IEEE COMTRADE Documentation the Analogic data
	 * Information // is stored following the identifiers below. I will not
	 * explain the identifiers // one by one, just give the ordenation of them. // //
	 * An,ch_id,ph,ccbm,uu,a,b,skew,min,max,primary,secondary,PS <CR/LF> // //
	 * for the Digital data Information we have // // Dn,ch_id,ph,ccbm,y <CR/LF>
	 */
	// StringTokenizer st_line = null;
	/*
	 * String[] st_line2 = new String[m_nOfAnalogicChannels];
	 * AnalogChannelInformation[] anci = new
	 * AnalogChannelInformation[m_nOfAnalogicChannels]; if
	 * (m_nOfAnalogicChannels != 0) { for (int i = 0; i < m_nOfAnalogicChannels;
	 * i++) { // st_line = new StringTokenizer(line, ","); st_line2 =
	 * line.split(","); // int size = st_line.countTokens(); /*anci[i]=
	 * getAnalogicChannelInformation( getAnalogicChannelInformation()[i],
	 * st_line2);
	 */

	/*
	 * } } setAnalogicChannelInformation(anci); return
	 * getAnalogicChannelInformation(); }
	 */

	private void getComponentsAnalogicChannelInformation(
			AnalogChannelInformation analogicDataInformation, String line) {
		String[] st_line2 = new String[line.length()];
		st_line2 = line.split(",");
		// System.out.println(line);
		if (st_line2.length > 1)
			analogicDataInformation.setAn(Integer.parseInt(st_line2[0]));
		if (st_line2.length > 2)
			analogicDataInformation.setChid(st_line2[1]);
		if (st_line2.length > 3)
			analogicDataInformation.setPh(st_line2[2]);
		if (st_line2.length > 4)
			analogicDataInformation.setCcbm(st_line2[3]);
		if (st_line2.length > 5)
			analogicDataInformation.setUu(st_line2[4]);
		if (st_line2.length > 6)
			analogicDataInformation.setAa(Double.parseDouble(st_line2[5]));
		if (st_line2.length > 7)
			analogicDataInformation.setB(Double.parseDouble(st_line2[6]));
		if (st_line2.length > 8)
			analogicDataInformation.setSkew(Double.parseDouble(st_line2[7]));
		if (st_line2.length > 9)
			analogicDataInformation.setMin(Integer.parseInt(st_line2[8]));
		if (st_line2.length > 10)
			analogicDataInformation.setMax(Integer.parseInt(st_line2[9]));
		if (st_line2.length > 11)
			analogicDataInformation
					.setPrimary(Double.parseDouble(st_line2[10]));
		if (st_line2.length > 12)
			analogicDataInformation.setSecondary(Double
					.parseDouble(st_line2[11]));
		if (st_line2.length >= 13)
			analogicDataInformation.setPs(st_line2[12]);

	}

	/*
	 * private DigitalChannelInformation[] getDigitalChannelInformation(String
	 * line) { StringTokenizer st_line = null; String[] st_line2; //
	 * m_digitalDataInformation = new //
	 * DigitalChannelInformation[m_nOfDigitalChannels];
	 * 
	 * if (m_nOfDigitalChannels != 0) { for (int i = 0; i <
	 * m_nOfDigitalChannels; i++) { st_line = new StringTokenizer(line, ",");
	 * st_line2 = line.split(","); int size = st_line.countTokens();
	 * setComponentsDigitalChannelInformaton( getDigitalChannelInformation()[i],
	 * st_line2); } } return getDigitalChannelInformation(); }
	 */

	private void getComponentsDigitalChannelInformaton(
			DigitalChannelInformation digitalchannelinformation, String line) {
		String[] st_line;
		st_line = line.split(",");
		if (st_line.length > 0)
			digitalchannelinformation.setDn(Integer.parseInt(st_line[0]));
		if (st_line.length > 1)
			digitalchannelinformation.setChid(st_line[1]);
		if (st_line.length > 2)
			digitalchannelinformation.setPh(st_line[2]);
		if (st_line.length > 3)
			digitalchannelinformation.setCcbm(st_line[3]);
		if (st_line.length > 9)
			digitalchannelinformation.setY(Integer.parseInt(st_line[4]));
	}

	private void getLF(String line) {
		StringTokenizer st_line = null;

		// Here the line-frequency value ir readed
		st_line = new StringTokenizer(line, ",");
		m_lf = Double.parseDouble(st_line.nextToken());
	}

	private void getNRate(String line) {
		/*
		 * // I found it important to say that the m_samp and m_endSamp should //
		 * have here the same lenght. In the CFG file they are in the same //
		 * line but here we decided separate them because they are not for //
		 * the same porpouse. // m_samp -> is the saple rate, double (floating
		 * point) // m_endSamp -> the last sample, int
		 */
		StringTokenizer st_line = null;

		st_line = new StringTokenizer(line, ",");
		m_nRates = Integer.parseInt(st_line.nextToken());
		if (m_nRates == 0) {
			System.err
					.println("There is no sample rate for this file, we very");
			System.err
					.println("encourage you to make a new data acquisition in");
			System.err.println("order to obtain a precise timing");
		}

	}

	private void getSamples(String line) {
		StringTokenizer st_line = null;

		m_samp = new double[m_nRates];
		m_endSamp = new int[m_nRates];
		for (int i = 0; i < m_nRates; i++) {
			st_line = new StringTokenizer(line, ",");
			m_samp[i] = Double.parseDouble(st_line.nextToken());
			m_endSamp[i] = Integer.parseInt(st_line.nextToken());
		}
	}

	private void getStartAcq(String line) {
		// it's not according with comtrade configuration file
		// in comtrade file it is not separated
		StringTokenizer st_line = null;
		m_startAcq = new String[2];

		st_line = new StringTokenizer(line, ",");
		m_startAcq[0] = st_line.nextToken();
		m_startAcq[1] = st_line.nextToken();
	}

	private void getEndsAcq(String line) {
		StringTokenizer st_line = null;
		m_endsAcq = new String[2];

		st_line = new StringTokenizer(line, ",");
		m_endsAcq[0] = st_line.nextToken();
		m_endsAcq[1] = st_line.nextToken();
	}

	private void getDataType(String line) {
		StringTokenizer st_line = null;

		// data type (file type) default value is "BINARY"
		st_line = new StringTokenizer(line, ",");
		m_ft = st_line.nextToken();
	}

	/*
	 * Here we have the constructor method. My idea was make various // *get*
	 * methods in order to make the code readable. It is still // not possible
	 * because we should first of all do an atomic read // of file besides an
	 * aleatory read, and the *get* methods get // the values of variables and
	 * not directly from comtrade file.
	 */

	public ComtradeCFGFileReader(String bf) throws Exception {
		BufferedReader bf_file = new BufferedReader(new FileReader(bf));

		getID(bf_file.readLine());

		getChanelsID(bf_file.readLine());

		m_analogicChannelInformation = new AnalogChannelInformation[m_nOfAnalogicChannels];
		for (int i = 0; i < getNOfAnalogicChannels(); i++) {
			m_analogicChannelInformation[i] = new AnalogChannelInformation();
			getComponentsAnalogicChannelInformation(
					m_analogicChannelInformation[i], bf_file.readLine());
		}
		m_digitalChannelInformation = new DigitalChannelInformation[m_nOfDigitalChannels];
		for (int i = 0; i < m_nOfDigitalChannels; i++) {
			m_digitalChannelInformation[i] = new DigitalChannelInformation();
			getComponentsDigitalChannelInformaton(
					m_digitalChannelInformation[i], bf_file.readLine());
		}
		getLF(bf_file.readLine());

		getNRate(bf_file.readLine());

		getSamples(bf_file.readLine());

		getStartAcq(bf_file.readLine());

		getEndsAcq(bf_file.readLine());

		getDataType(bf_file.readLine());

		// if((String s = bf_file.readLine()) != null){
		// st_line = new StringTokenizer(s, ",");
		// m_timemult = Double.parseDouble(st_line.nextToken());
		// }
	}

	public String toString() {
		String analogicdatainformation = " ";
		for (int i = 0; i < m_analogicChannelInformation.length; i++) {
			analogicdatainformation += "\n "
					+ m_analogicChannelInformation[i].toString() + "\n  ";
		}
		String digitaldatainformation = " ";
		for (int i = 0; i < getAnalogicChannelInformation().length; i++) {
			digitaldatainformation += getDigitalChannelInformation()[i]
					.toString()
					+ "\n  ";
		}
		String samp ="";
		for (int i = 0; i < getSamp().length; i++) {
			samp+=getSamp()[i]+" ";
		}
		String endsamp ="";
		for (int i = 0; i < getEndSamp().length; i++) {
			endsamp+=getEndSamp()[i]+" ";
		}
		
		return "\n Station_Name: " + getStationName() + "\n rec_dev_id: "
				+ getRecDeviceId() + "\n rev_year: "
				+ getContradeRevisionYear() + "\n TT: " + getTT()
				+ "\n NOfAnalogicChannels " + getNOfAnalogicChannels()
				+ "\n NOfDigitalChannels: " + getNOfDigitalChannels()
				+ "\n AnalogicChannelInformation: " + analogicdatainformation
				+ "\n DigitalDataInformation " + digitaldatainformation
				+ "\n Line Frequency: " + getLF() + "\n nrates: " + getNRates()
				+ "\n Samp: " + samp +"\n EndSamp: "+endsamp+ "\n startAcq: " + m_startAcq[0] + ", "
				+ m_startAcq[1] + "\n endsAcq: " + m_endsAcq[0] + ", "
				+ m_endsAcq[1] + "\n Data file type: " + m_ft;
	}

	/* Main method for debug purpose */

	public static void main(String[] args) throws Exception {
		String s = "E://claudomir//contrade//2005_03_30_16_00_18//2005_03_30_16_00_18.cfg";
		// String s = "/root/2004_04_09_08_04_39.CFG";
		ComtradeCFGFileReader cfg = new ComtradeCFGFileReader(s);
		System.out.println(cfg.toString());

		// Object ob = new comtradeCFGFileReader(args[0]);
	}
}
