package amazontp.comtrade;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.StringTokenizer;

import amazontp.io.ConvertAtpOutputToBinaryFile;

import amazontp.io.FileNamesAndDirectories;
import amazontp.io.IO;
import amazontp.io.LeastSignifcantByte;
import amazontp.io.LoadATPOutputFile;
import amazontp.matlab.Cloner;
import amazontp.matlab.MatlabInterfacer;

public class ComtradeDATFileWriter {

	private static String[] m_tempBuffer = new String[52];

	private static boolean m_splitWaveforms = false;

	private static boolean m_onlyEvent = true;

	private static boolean m_odebug = false;

	/**
	 * @param n
	 *            the sample number
	 * @param timestamp
	 *            Non-critical if nrates and samp variables in .CFG �le are
	 *            nonzero, critical if nrates and samp variables in .CFG �le are
	 *            zero
	 * @param A
	 *            The analog channel data values
	 * @param D
	 *            The status channel data values
	 */

	private static String WriteASCIIDatFileFormat(long n, long timestamp,
			int[] A, int[] D) {
		if (n < 1 || n > 999999999)
			error("The sample number n must be between 1 and 9999999999 ");
		String c = null;
		if (c.valueOf(timestamp).length() < 1
				|| c.valueOf(timestamp).length() > 10)
			error("The timestamp must be 1 character or 10 characters ");
		String line = n + ", " + timestamp + ", ";
		for (int i = 0; i < A.length; i++) {
			if (A[i] < -99999 || A[i] > 99998)
				error("The analog channel data values must be between -99999 and 99998  ");
			line = line + A[i] + ", ";
		}
		for (int i = 0; i < D.length - 1; i++) {
			if (D[i] != 1 && D[i] != 0)
				error("The status channel data values must be 0 or 1"
						+ " D[i] " + D[i]);
			line = line + D[i] + ", ";
		}
		if (D[D.length - 1] != 1 && D[D.length - 1] != 0)
			error("The status channel data values must be 0 or 1 ");
		line = line + D[D.length - 1] + "\r\n";
		return line;
	}

	private static void WriteBinaryDatFileFormat(String filename, int[][] signal) {
		try {
			File file = new File(filename);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			DataOutputStream dataOutputStream = new DataOutputStream(
					fileOutputStream);
			LeastSignifcantByte lsb = new LeastSignifcantByte(dataOutputStream);
			for (int i = 0; i < signal.length; i++)
				for (int j = 0; j < signal[0].length; j++) {
					lsb.writeInt(signal[i][j]);
				}			
			fileOutputStream.close();
		} catch (Exception e) {
			System.out.println("IO exception = " + e);
		}

	}

	private static void error(String message) {
		System.err.println(message);
		System.exit(1);
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {

			System.out

			.println("Usage: <input directory>");

			System.exit(1);

		}
		String directory = args[0];
		// force string to use "/" and end with /
		directory = FileNamesAndDirectories
				.replaceAndForceEndingWithSlash(directory);
		String labelsFile = directory + "labels.txt";
		if (!new File(labelsFile).exists()) {

			System.out.println(labelsFile + " does not exist!");

			System.exit(1);

		}

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				labelsFile));

		int nfileCounter = 0;
		String line = null;
		SimulationInfo previousSimulationInfo = new SimulationInfo();
		previousSimulationInfo.m_fileName = "";

		while ((line = bufferedReader.readLine()) != null) {

			if (line.equals("")) {

				break;

			}

			System.out.println(line);

			SimulationInfo simulationInfo = parseLine(line);

			if (simulationInfo.m_fileName

			.equals(previousSimulationInfo.m_fileName)) {

				// deal with the bug: delete the previous files and decrement

				// the counter

				nfileCounter--;

				String temp = directory + simulationInfo.toString() + "_"

				+ nfileCounter + ".dat";

				if (new File(temp).delete()) {

					System.out.println("Deleted " + temp);

				}

			}

			writeEventAsOneFile(directory, simulationInfo, nfileCounter);

			// prepare for next iteration

			nfileCounter++;

			previousSimulationInfo = simulationInfo;

		}

		bufferedReader.close();

		System.out.println(nfileCounter + " files were saved as dat.");

	}

	public static SimulationInfo parseLine(String line) throws Exception {

		StringTokenizer stringTokenizer = new StringTokenizer(line);

		int n = stringTokenizer.countTokens();

		if (n != 52) {

			System.err.println("Error in logic while parsing line: \n" + line);

			throw new Exception();

		}

		// store in temporary variable

		for (int i = 0; i < n; i++) {

			String token = stringTokenizer.nextToken();

			// System.out.println(i + " " + token);

			m_tempBuffer[i] = token;

		}

		SimulationInfo simulationInfo = new SimulationInfo();

		// take out the ":" from fileName:

		simulationInfo.m_fileName = m_tempBuffer[0].substring(0,

		m_tempBuffer[0].length() - 1);

		simulationInfo.m_network = m_tempBuffer[4];

		simulationInfo.m_faultType = m_tempBuffer[9];

		simulationInfo.m_faultLineIndex = m_tempBuffer[14];

		simulationInfo.m_startTime = m_tempBuffer[19];

		simulationInfo.m_finalTime = m_tempBuffer[24];

		simulationInfo.m_faultPosition = m_tempBuffer[31];

		simulationInfo.m_linesResistance = m_tempBuffer[41];

		simulationInfo.m_lineGroundResistance = m_tempBuffer[50];

		return simulationInfo;

	}

	private static class SimulationInfo {

		protected String m_fileName;

		protected String m_network;

		protected String m_faultType;

		protected String m_faultLineIndex;

		protected String m_startTime;

		protected String m_finalTime;

		protected String m_faultPosition;

		protected String m_linesResistance;

		protected String m_lineGroundResistance;

		public String toString() {

			return (m_network + "_" + m_faultLineIndex + "_" + m_faultType)

			.toLowerCase();

		}

	}

	private static double[][] getWholeWaveforms(String fileName)

	throws Exception {

		// read the whole waveform

		float[][] signals = LoadATPOutputFile.parseATPOutputFile(fileName);

		int noutputSamples = signals[0].length - 1;

		// System.out.println(startTime + " " + T + " " + endTime + " " +

		// noutputSamples);

		// assume there are 6 signals of interest

		double[][] output = new double[6][noutputSamples];

		// take the samples at the begin of the line

		for (int i = 0; i < noutputSamples; i++) {

			// will not consider the "time" waveform

			output[0][i] = signals[1][i];

			output[1][i] = signals[2][i];

			output[2][i] = signals[3][i];

			output[3][i] = signals[4][i];

			output[4][i] = signals[5][i];

			output[5][i] = signals[6][i];

		}

		return output;

	}

	private static void writeEventAsOneFile(String directory,

	SimulationInfo simulationInfo, int nfileCounter) throws Exception {

		String inputFileName = directory + simulationInfo.m_fileName + ".txt";

		double startTime = Double.parseDouble(simulationInfo.m_startTime);

		double endTime = Double.parseDouble(simulationInfo.m_finalTime);

		double[][] signals = null;
		// whole signal

		signals = getWholeWaveforms(inputFileName);

		// dump matrix into file

		// 3 voltages from begin of line and 3 voltages from end of line

		String outputFileName = directory + simulationInfo.toString() + "_"

		+ nfileCounter + ".txt";
        int[][] s = new int [signals.length][signals[0].length];
		for (int i = 0; i < signals.length; i++) {
			for (int j = 0; j < signals[0].length; j++) {
				s [i][j] = (int)signals[i][j];
			}
		} 
		//WriteBinaryDatFileFormat(outputFileName, s);
        IO.writeMatrixtoASCIIFile(outputFileName,signals); 
		//IO.DisplayMatrix(signals);

		System.out.println("Wrote " + outputFileName);

	}

}
