package amazontp.comtrade;
public class DigitalChannelInformation {

	/** The status channel index number */
	private int m_Dn;

	/** channel name */
	private String m_chid;

	/** channel phase identification */
	private String m_ph;

	/** circuit component being monitored */
	private String m_ccbm;

	/**
	 * the state of input when the primary apparatus is in the steady state "in
	 * service" condition values 0 or 1
	 */
	private int m_y;

	public String getCcbm() {
		return m_ccbm;
	}

	public void setCcbm(String m_ccbm) {
		this.m_ccbm = m_ccbm;
	}

	public String getChid() {
		return m_chid;
	}

	public void setChid(String m_chaid) {
		this.m_chid = m_chaid;
	}

	public int getDn() {
		return m_Dn;
	}

	public void setDn(int dn) {
		m_Dn = dn;
	}

	public String getPh() {
		return m_ph;
	}

	public void setPh(String m_ph) {
		this.m_ph = m_ph;
	}

	public int getY() {
		return m_y;
	}

	public void setY(int m_y) {
		this.m_y = m_y;
	}

	public String toString() {
		return " DN: " + getDn() + "\n Ch_ID : " + getChid() + "\n ph: "
				+ getPh() + "\n ccbm: " + getCcbm() + "\n y: " + getY();
	}

}
