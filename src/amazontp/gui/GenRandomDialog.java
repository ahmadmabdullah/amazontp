package amazontp.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.LineZT;
import amazontp.io.PrepareMasterFile;
import amazontp.io.SaveFile;
import amazontp.io.masterFile;
import amazontp.util.RunATP;

public class GenRandomDialog extends JFrame {
	
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ProbabilitySettingsDialog psd;
	String fileName;
	
	String amazontpDir = System.getenv("AMAZONTP");
	static String oS = System.getProperty("os.name");
	String atpDir;
	String genRandomScriptName;
	Runtime r = Runtime.getRuntime();
	
	int selectLines[];
	
	private JPanel jContentPane = null;
	private JPanel jPanel1 = null;
	
	private ArrayList<LineZTPropertiesPanel> h;
	
	private JTextField windowLengthText = null;
	private JTextField saplingFrequencyText = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JButton jButton = null;
	private JButton jButton1 = null;
	private JButton probabilitiesButton = null;
	private JPanel jPanel21 = null;
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JTextField numberOfFaultsTextField = null;
	private LineZTGroupFrame LineFrame=null;
//	private LineZTPropertiesPanel Teste = null;
	private JScrollPane jScrollPane = null;
	ArrayList<Component> list;
	
	/**
	 * This is the default constructor
	 * @param fileName2 
	 */
	public GenRandomDialog(ArrayList<Component> list, String fileName2) {
		super();
		this.list=list;
		this.fileName=fileName2;
		initialize();
	}

	public void setFileName(String name) {
		fileName = name;
	}
	
	public String getFileName() {
		return this.fileName; 
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(690, 337);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getJPanel1(), null);
			jContentPane.add(getJButton(), null);
			jContentPane.add(getJButton1(), null);
			jContentPane.add(getProbabilitiesButton(), null);
			jContentPane.add(getJPanel21(), null);
			jContentPane.add(getJScrollPane(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setBounds(new java.awt.Rectangle(14,58,123,16));
			jLabel1.setText("Sampling Frequency");
			jLabel = new JLabel();
			jLabel.setBounds(new java.awt.Rectangle(23,29,97,16));
			jLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 12));
			jLabel.setText("Window Length");
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.setBounds(new java.awt.Rectangle(422,9,248,102));
			jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ARFF Settings", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, java.awt.Color.black));
			jPanel1.add(getWindowLengthText(), null);
			jPanel1.add(getSaplingFrequencyText(), null);
			jPanel1.add(jLabel, null);
			jPanel1.add(jLabel1, null);
			
		}
		return jPanel1;
	}

	/**
	 * This method initializes jPanel2	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	

	/**
	 * This method initializes jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getWindowLengthText() {
		if (windowLengthText== null) {
			windowLengthText = new JTextField();
			windowLengthText.setBounds(new java.awt.Rectangle(140,28,99,20));
			windowLengthText.setText("5");
		}
		return windowLengthText;
	}

	/**
	 * This method initializes jTextField1	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSaplingFrequencyText() {
		if (saplingFrequencyText == null) {
			saplingFrequencyText = new JTextField();
			saplingFrequencyText.setBounds(new java.awt.Rectangle(140,58,99,20));
			saplingFrequencyText.setText("5000");
		}
		return saplingFrequencyText;
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setBounds(new java.awt.Rectangle(129,268,94,25));
			jButton.setText("Run");
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ArrayList<LineZT> listSelectedLines=new ArrayList<LineZT>();
					selectLines =LineFrame.getIndexesOfSelectedLines();
					HashMap<Integer,LineZT>lines=(HashMap<Integer, LineZT>)LineFrame.getLineZt();
					
					for(int x=0;x<selectLines.length;x++){
						listSelectedLines.add(lines.get(selectLines[x]));
					}
					
					
					if (oS.indexOf("Windows") != -1) {
						atpDir = System.getenv("WATDIR");
						if (atpDir == null) {
							atpDir = "c:\\ATP";
							System.err.println("Couldn't find Enviroment Variable WATDIR, setting c:\\ATP as ATP Folder.");
						} else {
							System.out.println("TPBIG located in: " + atpDir);		
						}
					} else {
						atpDir = System.getenv("GNUDIR");
						if (atpDir == null) {
							atpDir = "/home/atplinux";
							System.err.println("Couldn't find Enviroment Variable GNUDIR, setting /home/linux as ATP Folder.");
						} else {
							System.out.println("TPBIG located in: " + atpDir);
						}
					}

					

					try {

						GenBatFile(listSelectedLines);

					} catch (Exception ex) {

						ex.printStackTrace();
						dispose();

					}

					new RunATP(atpDir + genRandomScriptName);
										
					
					if (oS.indexOf("Windows") != -1) {
						System.out.println("Fault Files: " + atpDir + "amazonatps");	
					} else {
						System.out.println("Fault Files: " + atpDir);
					}
					
					System.out.println("ARFF File: " + atpDir + "ATPWatcom");

					dispose();
				}
			});
		}
		return jButton;
	}

	/**
	 * This method initializes jButton1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setBounds(new java.awt.Rectangle(491,269,94,25));
			jButton1.setText("Cancel");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); // TODO Auto-generated Event stub actionPerformed()
				}
			});
		}
		return jButton1;
	}

	/**
	 * This method initializes jButton2	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getProbabilitiesButton() {
		if (probabilitiesButton == null) {
			probabilitiesButton = new JButton();
			probabilitiesButton.setBounds(new java.awt.Rectangle(486,208,157,28));
			probabilitiesButton.setText("Probabilities Settings");
		
		probabilitiesButton
		.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				
				psd = new ProbabilitySettingsDialog();

				Dimension screenSize = Toolkit.getDefaultToolkit()
						.getScreenSize();

				Dimension frameSize = psd.getSize();

				if (frameSize.height > screenSize.height) {

					frameSize.height = screenSize.height;

				}

				if (frameSize.width > screenSize.width) {

					frameSize.width = screenSize.width;

				}

				psd.setLocation(
						(screenSize.width - frameSize.width) / 2,

						(screenSize.height - frameSize.height) / 2);

				psd.setVisible(true);
			}
		});
        }
		return probabilitiesButton;
	}

	/**
	 * This method initializes jPanel21	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel21() {
		if (jPanel21 == null) {
			jLabel3 = new JLabel();
			jLabel3.setBounds(new java.awt.Rectangle(27,29,101,16));
			jLabel3.setText("Number of faults:");
			jLabel2 = new JLabel();
			jLabel2.setText("");
			jLabel2.setBounds(new java.awt.Rectangle(124,26,0,0));
			jPanel21 = new JPanel();
			jPanel21.setLayout(null);
			jPanel21.setBounds(new java.awt.Rectangle(420,130,255,61));
			jPanel21.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Faults", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, java.awt.Color.black));
			jPanel21.add(jLabel2, null);
			jPanel21.add(jLabel3, null);
			jPanel21.add(getNumberOfFaultsTextField(), null);
		}
		return jPanel21;
	}

	/**
	 * This method initializes jTextField2	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getNumberOfFaultsTextField() {
		if (numberOfFaultsTextField == null) {
			numberOfFaultsTextField = new JTextField();
			numberOfFaultsTextField.setBounds(new java.awt.Rectangle(133,28,102,20));
			//numberOfFaultsTextField.setText("");
		}
		return numberOfFaultsTextField;
	}

	/**
	 * This method initializes Teste	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private LineZTGroupFrame getTeste() {
		if (LineFrame == null) {
			LineFrame = new LineZTGroupFrame();
			LineZT x;
			
			for(Iterator i=this.list.iterator();i.hasNext();){
				
				try{
					x=(LineZT)i.next();
					LineFrame.addLineZTPropertiesPanel(x);	
					
				}
				catch(Exception e){
					System.err.println("Selected device is not a line");
				}
				
			}
	
		}
		return LineFrame;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(new java.awt.Rectangle(6,9,413,219));
			jScrollPane.setViewportView(getTeste());
		}
		return jScrollPane;
	}

	public void GenBatFile(ArrayList<LineZT> LineZTSelected) throws Exception {

		if (oS.indexOf("Windows") != -1) {
			genRandomScriptName = "GenRandomicFaults.bat";
		} else {
			genRandomScriptName = "GenRandomicFaults.sh";
		}
		
		
		
		masterFile master=new masterFile(this.getFileName(), atpDir + "master.atp",LineZTSelected);
		// cria o arquivo master.atp
		int numberLinesZT = master.prepareMasterFile();
		System.out.println("numberLinesZT: "+numberLinesZT);
		int n = Integer.parseInt(numberOfFaultsTextField.getText());
		System.out.println("numberOfFaultsTextField "+numberOfFaultsTextField.getText());
		
		System.err.println(amazontpDir);
		SaveFile sb = new SaveFile();
		sb.deleteFile(atpDir + genRandomScriptName);
		sb.createFile(atpDir + genRandomScriptName);
		sb.saveFile(atpDir + genRandomScriptName, "cd " + atpDir);
		
	
		
		String expr="java -cp " + amazontpDir + " amazontp.genfaults.GenRandomicFaults "
		+ this.getFileName() + " " + n + " " + psd.location_start
		+ " " + psd.location_end + " " + psd.time_start + " "
		+ psd.time_end + " " + psd.duration_start + " "
		+ psd.duration_end + " " + psd.resistance_start + " "
		+ psd.resistance_end + " " + psd.isLocationUniform + " "
		+ psd.isTimeUniform + " " + psd.isDurationUniform + " "
		+ psd.isResistanceUniform + " " + windowLengthText.getText() + " " + saplingFrequencyText.getText() + " " +Integer.toString(numberLinesZT);

		for(int k=0;k<selectLines.length;k++){
			expr=expr+" "+Integer.toString(selectLines[k]);
		}
		
		sb.saveFile(atpDir + genRandomScriptName,expr);
		
		if (oS.indexOf("Windows") == -1) {
			
			r.exec("chmod a+x " + atpDir + genRandomScriptName);
			Thread.sleep(100);
		}
	}
	
}  //  @jve:decl-index=0:visual-constraint="-57,7"
