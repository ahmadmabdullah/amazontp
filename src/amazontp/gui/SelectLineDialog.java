package amazontp.gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class SelectLineDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String fileName;

	public void setFileName(String name) {
		fileName = name;
	}
	
	private JPanel jContentPane = null;
	private JButton jButton = null;
	private JButton jButton1 = null;
	private JScrollPane linesScrollPane = null;
	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setText("OK");
			jButton.setSize(new java.awt.Dimension(81,30));
			jButton.setLocation(new java.awt.Point(180,220));
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					GenSingleFrame f = new GenSingleFrame();

					// f.setSize(new Dimension(400, 450));

					Dimension screenSize = Toolkit.getDefaultToolkit()
							.getScreenSize();

					Dimension frameSize = f.getSize();

					if (frameSize.height > screenSize.height) {

						frameSize.height = screenSize.height;

					}

					if (frameSize.width > screenSize.width) {

						frameSize.width = screenSize.width;

					}

					f.setLocation(
							(screenSize.width - frameSize.width) / 2,

							(screenSize.height - frameSize.height) / 2);

					f.setFileName(fileName);
					
					f.setVisible(true);
				}
			});
			
		}
		return jButton;
	}

	/**
	 * This method initializes jButton1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Cancel");
			jButton1.setSize(new java.awt.Dimension(81,30));
			jButton1.setLocation(new java.awt.Point(270,220));
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose();
				}
			});
		}
		return jButton1;
	}

	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getLinesScrollPane() {
		if (linesScrollPane == null) {
			linesScrollPane = new JScrollPane();
			linesScrollPane.setBounds(new java.awt.Rectangle(10,10,341,201));
			linesScrollPane.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			linesScrollPane.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			linesScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Lines", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
		}
		return linesScrollPane;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * This is the default constructor
	 */
	public SelectLineDialog() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(367, 285);
		this.setResizable(false);
		this.setContentPane(getJContentPane());
		this.setTitle("Select Line Z-T");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getJButton(), null);
			jContentPane.add(getJButton1(), null);
			jContentPane.add(getLinesScrollPane(), null);
		}
		return jContentPane;
	}
	
	public void addLineZTPropertiesPanel(LineZTPropertiesPanel lztp) {
		this.getLinesScrollPane().add(lztp);
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
