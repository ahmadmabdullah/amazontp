package amazontp.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * @author Jefferson
 */
public class ProbabilitySettingsDialog extends JDialog {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		double location_start = 0;
		double location_end = 0;
		
		double time_start = 0;
		double time_end = 0;
		
		double duration_start = 0;
		double duration_end = 0;
		
		double resistance_start = 0;
		double resistance_end = 0;
		
		boolean isLocationUniform = true;
		boolean isTimeUniform = true;
		boolean isDurationUniform = true;
		boolean isResistanceUniform = true;

	/**
	 * @uml.property name="jContentPane"
	 * @uml.associationEnd
	 */
	private JPanel jContentPane = null;

	/**
	 * @uml.property name="jPanel"
	 * @uml.associationEnd
	 */
	private JPanel jPanel = null;

	/**
	 * @uml.property name="resistancesPanel"
	 * @uml.associationEnd
	 */
	private JPanel resistancesPanel = null;

	/**
	 * @uml.property name="resistancesUniformRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton resistancesUniformRadioButton = null;

	/**
	 * @uml.property name="resistancesGaussianRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton resistancesGaussianRadioButton = null;

	/**
	 * @uml.property name="resistancesLabel1"
	 * @uml.associationEnd
	 */
	private JLabel resistancesLabel1 = null;

	/**
	 * @uml.property name="resistancesLabel2"
	 * @uml.associationEnd
	 */
	private JLabel resistancesLabel2 = null;

	/**
	 * @uml.property name="resistancesLabel4"
	 * @uml.associationEnd
	 */
	private JLabel resistancesLabel4 = null;

	/**
	 * @uml.property name="resistancesLabel3"
	 * @uml.associationEnd
	 */
	private JLabel resistancesLabel3 = null;

	/**
	 * @uml.property name="startResistancesTextField"
	 * @uml.associationEnd
	 */
	private JTextField startResistanceTextField = null;

	/**
	 * @uml.property name="endResistancesTextField"
	 * @uml.associationEnd
	 */
	private JTextField endResistanceTextField = null;

	/**
	 * @uml.property name="locationPanel"
	 * @uml.associationEnd
	 */
	private JPanel locationPanel = null;

	/**
	 * @uml.property name="locationUniformRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton locationUniformRadioButton = null;

	/**
	 * @uml.property name="locationGaussianRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton locationGaussianRadioButton = null;

	/**
	 * @uml.property name="locationLabel1"
	 * @uml.associationEnd
	 */
	private JLabel locationLabel1 = null;

	/**
	 * @uml.property name="locationLabel2"
	 * @uml.associationEnd
	 */
	private JLabel locationLabel2 = null;

	/**
	 * @uml.property name="timeLabel3"
	 * @uml.associationEnd
	 */
	private JLabel timeLabel3 = null;

	/**
	 * @uml.property name="timeLabel4"
	 * @uml.associationEnd
	 */
	private JLabel timeLabel4 = null;

	/**
	 * @uml.property name="startLocationTextField"
	 * @uml.associationEnd
	 */
	private JTextField startLocationTextField = null;

	/**
	 * @uml.property name="endLocationTextField"
	 * @uml.associationEnd
	 */
	private JTextField endLocationTextField = null;

	/**
	 * @uml.property name="timePanel"
	 * @uml.associationEnd
	 */
	private JPanel timePanel = null;

	/**
	 * @uml.property name="timeUniformRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton timeUniformRadioButton = null;

	/**
	 * @uml.property name="timeGaussianRadioButton"
	 * @uml.associationEnd
	 */
	private JRadioButton timeGaussianRadioButton = null;

	/**
	 * @uml.property name="timeLabel1"
	 * @uml.associationEnd
	 */
	private JLabel timeLabel1 = null;

	/**
	 * @uml.property name="timeLabel2"
	 * @uml.associationEnd
	 */
	private JLabel timeLabel2 = null;

	/**
	 * @uml.property name="locationLabel3"
	 * @uml.associationEnd
	 */
	private JLabel locationLabel3 = null;

	/**
	 * @uml.property name="locationLabel4"
	 * @uml.associationEnd
	 */
	private JLabel locationLabel4 = null;

	/**
	 * @uml.property name="startTimeTextField"
	 * @uml.associationEnd
	 */
	private JTextField startTimeTextField = null;

	/**
	 * @uml.property name="endTimeTextField"
	 * @uml.associationEnd
	 */
	private JTextField endTimeTextField = null;

	private JPanel jPanel1 = null;

	private JRadioButton durationUniformRadioButton = null;

	private JRadioButton durationGaussianRadioButton = null;

	private JLabel durationLabel1 = null;

	private JLabel durationLabel2 = null;

	private JLabel jLabel2 = null;

	private JLabel jLabel3 = null;

	private JTextField startDurationTextField = null;

	private JTextField endDurationTextField = null;

	private JTextField fixedDurationTextField = null;

	private JLabel jLabel5 = null;

	private JCheckBox fixDurationCheckBox = null;

	private JCheckBox setZeroToTMaxCheckBox = null;

	private JButton cancelButton = null;

	private JButton okButton = null;

	/**
	 * This is the default constructor
	 */
	public ProbabilitySettingsDialog() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(469, 385);
		this.setResizable(false);
		this.setModal(true);
		this.setTitle("Probability Settings");
		this.setContentPane(getJContentPane());
		this.setDefaultInitialValues();
	}

	private void setDefaultInitialValues() {
		startResistanceTextField.setText("0.1");
		endResistanceTextField.setText("2");
		resistance_start = 0.1;
		resistance_end = 2;
		isResistanceUniform = true;

		startLocationTextField.setText("20");
		endLocationTextField.setText("80");
		location_start = 20;
		location_end = 80;
		isLocationUniform = true;

		startTimeTextField.setText("0");
		endTimeTextField.setText("1");
		time_start = 0;
		time_end = 1;
		isTimeUniform = true;

		startDurationTextField.setText("0");
		endDurationTextField.setText("0.1");
		duration_start = 0;
		duration_end = 0.1;
		isDurationUniform = true;
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jContentPane"
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel(), java.awt.BorderLayout.CENTER);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jPanel"
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.add(getResistancesPanel(), null);
			jPanel.add(getLocationPanel(), null);
			jPanel.add(getTimePanel(), null);
			jPanel.add(getJPanel1(), null);
			jPanel.add(getCancelButton(), null);
			jPanel.add(getOkButton(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel1
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="resistancesPanel"
	 */
	private JPanel getResistancesPanel() {
		if (resistancesPanel == null) {
			resistancesLabel3 = new JLabel();
			resistancesLabel3
					.setBounds(new java.awt.Rectangle(80, 110, 38, 16));
			resistancesLabel3.setText("Ohms");
			resistancesLabel4 = new JLabel();
			resistancesLabel4
					.setBounds(new java.awt.Rectangle(80, 160, 38, 16));
			resistancesLabel4.setText("Ohms");
			resistancesLabel2 = new JLabel();
			resistancesLabel2
					.setBounds(new java.awt.Rectangle(10, 140, 106, 16));
			resistancesLabel2.setText("End");
			resistancesLabel1 = new JLabel();
			resistancesLabel1
					.setBounds(new java.awt.Rectangle(10, 90, 106, 16));
			resistancesLabel1.setText("Start");
			resistancesPanel = new JPanel();
			resistancesPanel.setLayout(null);
			resistancesPanel
					.setBounds(new java.awt.Rectangle(319, 10, 132, 191));
			resistancesPanel.setBorder(BorderFactory.createTitledBorder(null,
					"Resistances", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, new Font("Dialog",
							Font.BOLD, 12), new Color(51, 51, 51)));
			resistancesPanel.add(getResistancesUniformRadioButton(), null);
			resistancesPanel.add(getResistancesGaussianRadioButton(), null);
			resistancesPanel.add(resistancesLabel1, null);
			resistancesPanel.add(resistancesLabel2, null);
			resistancesPanel.add(resistancesLabel4, null);
			resistancesPanel.add(resistancesLabel3, null);
			resistancesPanel.add(getStartResistancesTextField(), null);
			resistancesPanel.add(getEndResistancesTextField(), null);
		}
		return resistancesPanel;
	}

	/**
	 * This method initializes jRadioButton
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="resistancesUniformRadioButton"
	 */
	private JRadioButton getResistancesUniformRadioButton() {
		if (resistancesUniformRadioButton == null) {
			resistancesUniformRadioButton = new JRadioButton();
			resistancesUniformRadioButton.setBounds(new java.awt.Rectangle(10,
					30, 106, 21));
			resistancesUniformRadioButton.setText("Uniform");
			resistancesUniformRadioButton.setSelected(true);
			resistancesUniformRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (resistancesGaussianRadioButton.isSelected() == true)
								resistancesGaussianRadioButton
										.setSelected(false);
							resistancesUniformRadioButton.setSelected(true);
							resistancesLabel1.setText("Start");
							resistancesLabel2.setText("End");
							startResistanceTextField.setText("0.1");
							endResistanceTextField.setText("2");

							isResistanceUniform = true;
						}
					});
		}
		return resistancesUniformRadioButton;
	}

	/**
	 * This method initializes jRadioButton1
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="resistancesGaussianRadioButton"
	 */
	private JRadioButton getResistancesGaussianRadioButton() {
		if (resistancesGaussianRadioButton == null) {
			resistancesGaussianRadioButton = new JRadioButton();
			resistancesGaussianRadioButton.setBounds(new java.awt.Rectangle(10,
					50, 106, 21));
			resistancesGaussianRadioButton.setText("Gaussian");
			resistancesGaussianRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (resistancesUniformRadioButton.isSelected() == true)
								resistancesUniformRadioButton
										.setSelected(false);
							resistancesGaussianRadioButton.setSelected(true);
							resistancesLabel1.setText("Mean");
							resistancesLabel2.setText("Std. Deviation");
							startResistanceTextField.setText("0.5");
							endResistanceTextField.setText("0.3");

							isResistanceUniform = false;

						}
					});
		}
		return resistancesGaussianRadioButton;
	}

	/**
	 * This method initializes jTextField
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="startResistancesTextField"
	 */
	private JTextField getStartResistancesTextField() {
		if (startResistanceTextField == null) {
			startResistanceTextField = new JTextField();
			startResistanceTextField.setBounds(new java.awt.Rectangle(10, 110,
					61, 20));
		}
		return startResistanceTextField;
	}

	/**
	 * This method initializes jTextField1
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="endResistancesTextField"
	 */
	private JTextField getEndResistancesTextField() {
		if (endResistanceTextField == null) {
			endResistanceTextField = new JTextField();
			endResistanceTextField.setBounds(new java.awt.Rectangle(10, 160,
					61, 20));
		}
		return endResistanceTextField;
	}

	/**
	 * This method initializes jPanel2
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="locationPanel"
	 */
	private JPanel getLocationPanel() {
		if (locationPanel == null) {
			timeLabel4 = new JLabel();
			timeLabel4.setBounds(new Rectangle(255, 60, 38, 16));
			timeLabel4.setText("%");
			timeLabel3 = new JLabel();
			timeLabel3.setBounds(new Rectangle(255, 30, 38, 16));
			timeLabel3.setText("%");
			locationLabel2 = new JLabel();
			locationLabel2.setBounds(new Rectangle(120, 60, 76, 16));
			locationLabel2.setText("End");
			locationLabel1 = new JLabel();
			locationLabel1.setBounds(new Rectangle(120, 30, 76, 16));
			locationLabel1.setText("Start");
			locationPanel = new JPanel();
			locationPanel.setLayout(null);
			locationPanel.setBounds(new java.awt.Rectangle(10, 10, 301, 91));
			locationPanel.setBorder(BorderFactory.createTitledBorder(null,
					"Location", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, new Font("Dialog",
							Font.BOLD, 12), new Color(51, 51, 51)));
			locationPanel.add(getLocationUniformRadioButton(), null);
			locationPanel.add(getLocationGaussianRadioButton(), null);
			locationPanel.add(locationLabel1, null);
			locationPanel.add(locationLabel2, null);
			locationPanel.add(timeLabel3, null);
			locationPanel.add(timeLabel4, null);
			locationPanel.add(getStartLocationTextField(), null);
			locationPanel.add(getEndLocationTextField(), null);
		}
		return locationPanel;
	}

	/**
	 * This method initializes jRadioButton2
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="locationUniformRadioButton"
	 */
	private JRadioButton getLocationUniformRadioButton() {
		if (locationUniformRadioButton == null) {
			locationUniformRadioButton = new JRadioButton();
			locationUniformRadioButton.setBounds(new java.awt.Rectangle(15, 30,
					91, 21));
			locationUniformRadioButton.setText("Uniform");
			locationUniformRadioButton.setSelected(true);
			locationUniformRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (locationGaussianRadioButton.isSelected() == true)
								locationGaussianRadioButton.setSelected(false);
							locationUniformRadioButton.setSelected(true);
							locationLabel1.setText("Start");
							locationLabel2.setText("End");
							startLocationTextField.setText("20");
							endLocationTextField.setText("80");

							isLocationUniform = true;

						}
					});
		}
		return locationUniformRadioButton;
	}

	/**
	 * This method initializes jRadioButton3
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="locationGaussianRadioButton"
	 */
	private JRadioButton getLocationGaussianRadioButton() {
		if (locationGaussianRadioButton == null) {
			locationGaussianRadioButton = new JRadioButton();
			locationGaussianRadioButton.setBounds(new java.awt.Rectangle(15,
					60, 91, 21));
			locationGaussianRadioButton.setText("Gaussian");
			locationGaussianRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (locationUniformRadioButton.isSelected() == true)
								locationUniformRadioButton.setSelected(false);
							locationGaussianRadioButton.setSelected(true);
							locationLabel1.setText("Mean");
							locationLabel2.setText("Std. Deviation");
							startLocationTextField.setText("0.5");
							endLocationTextField.setText("0.3");

							isLocationUniform = false;
						}
					});
		}
		return locationGaussianRadioButton;
	}

	/**
	 * This method initializes jTextField2
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="startLocationTextField"
	 */
	private JTextField getStartLocationTextField() {
		if (startLocationTextField == null) {
			startLocationTextField = new JTextField();
			startLocationTextField.setBounds(new Rectangle(195, 30, 46, 20));
		}
		return startLocationTextField;
	}

	/**
	 * This method initializes jTextField3
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="endLocationTextField"
	 */
	private JTextField getEndLocationTextField() {
		if (endLocationTextField == null) {
			endLocationTextField = new JTextField();
			endLocationTextField.setBounds(new Rectangle(195, 60, 46, 20));
		}
		return endLocationTextField;
	}

	/**
	 * This method initializes jPanel3
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="timePanel"
	 */
	private JPanel getTimePanel() {
		if (timePanel == null) {
			locationLabel4 = new JLabel();
			locationLabel4.setBounds(new java.awt.Rectangle(255, 40, 38, 16));
			locationLabel4.setText("sec");
			locationLabel3 = new JLabel();
			locationLabel3.setBounds(new java.awt.Rectangle(255, 20, 38, 16));
			locationLabel3.setText("sec");
			timeLabel2 = new JLabel();
			timeLabel2.setBounds(new java.awt.Rectangle(120, 40, 76, 16));
			timeLabel2.setText("End");
			timeLabel1 = new JLabel();
			timeLabel1.setBounds(new java.awt.Rectangle(120, 20, 76, 16));
			timeLabel1.setText("Start");
			timePanel = new JPanel();
			timePanel.setLayout(null);
			timePanel.setBounds(new java.awt.Rectangle(10, 110, 301, 91));
			timePanel.setBorder(BorderFactory.createTitledBorder(null, "Time",
					TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, new Font("Dialog",
							Font.BOLD, 12), new Color(51, 51, 51)));
			timePanel.add(getTimeGaussianRadioButton(), null);
			timePanel.add(timeLabel1, null);
			timePanel.add(timeLabel2, null);
			timePanel.add(locationLabel3, null);
			timePanel.add(locationLabel4, null);
			timePanel.add(getStartTimeTextField(), null);
			timePanel.add(getEndTimeTextField(), null);
			timePanel.add(getTimeUniformRadioButton(), null);
			timePanel.add(getSetZeroToTMaxCheckBox(), null);
		}
		return timePanel;
	}

	/**
	 * This method initializes jRadioButton4
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="timeUniformRadioButton"
	 */
	private JRadioButton getTimeUniformRadioButton() {
		if (timeUniformRadioButton == null) {
			timeUniformRadioButton = new JRadioButton();
			timeUniformRadioButton.setText("Uniform");
			timeUniformRadioButton.setBounds(new java.awt.Rectangle(17, 27, 91,
					21));
			timeUniformRadioButton.setSelected(true);
			timeUniformRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (timeGaussianRadioButton.isSelected() == true)
								timeGaussianRadioButton.setSelected(false);
							setZeroToTMaxCheckBox.setEnabled(true);
							timeUniformRadioButton.setSelected(true);
							timeLabel1.setText("Start");
							timeLabel2.setText("End");
							startTimeTextField.setText("0");
							endTimeTextField.setText("1");

							isTimeUniform = true;
						}
					});
		}
		return timeUniformRadioButton;
	}

	/**
	 * This method initializes jRadioButton5
	 * 
	 * @return javax.swing.JRadioButton
	 * @uml.property name="timeGaussianRadioButton"
	 */
	private JRadioButton getTimeGaussianRadioButton() {
		if (timeGaussianRadioButton == null) {
			timeGaussianRadioButton = new JRadioButton();
			timeGaussianRadioButton.setBounds(new java.awt.Rectangle(15, 60,
					91, 21));
			timeGaussianRadioButton.setText("Gaussian");
			timeGaussianRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (timeUniformRadioButton.isSelected() == true)
								timeUniformRadioButton.setSelected(false);

							setZeroToTMaxCheckBox.setSelected(false);
							setZeroToTMaxCheckBox.setEnabled(false);
							timeLabel1.setEnabled(true);
							timeLabel2.setEnabled(true);
							startTimeTextField.setEnabled(true);
							endTimeTextField.setEnabled(true);

							timeGaussianRadioButton.setSelected(true);
							timeLabel1.setText("Mean");
							timeLabel2.setText("Std. Deviation");
							startTimeTextField.setText("0.5");
							endTimeTextField.setText("0.3");

							isTimeUniform = false;
						}
					});
		}
		return timeGaussianRadioButton;
	}

	/**
	 * This method initializes jTextField4
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="startTimeTextField"
	 */
	private JTextField getStartTimeTextField() {
		if (startTimeTextField == null) {
			startTimeTextField = new JTextField();
			startTimeTextField
					.setBounds(new java.awt.Rectangle(195, 20, 46, 20));
		}
		return startTimeTextField;
	}

	/**
	 * This method initializes jTextField5
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="endTimeTextField"
	 */
	private JTextField getEndTimeTextField() {
		if (endTimeTextField == null) {
			endTimeTextField = new JTextField();
			endTimeTextField.setBounds(new java.awt.Rectangle(195, 40, 46, 20));
		}
		return endTimeTextField;
	}

	/**
	 * This method initializes jPanel1
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jLabel5 = new JLabel();
			jLabel5.setBounds(new java.awt.Rectangle(380, 50, 31, 21));
			jLabel5.setText("sec");
			jLabel5.setEnabled(false);
			jLabel3 = new JLabel();
			jLabel3.setBounds(new Rectangle(255, 60, 38, 16));
			jLabel3.setText("sec");
			jLabel2 = new JLabel();
			jLabel2.setBounds(new Rectangle(255, 30, 38, 16));
			jLabel2.setText("sec");
			durationLabel2 = new JLabel();
			durationLabel2.setBounds(new Rectangle(120, 60, 76, 16));
			durationLabel2.setText("End");
			durationLabel1 = new JLabel();
			durationLabel1.setBounds(new Rectangle(120, 30, 76, 16));
			durationLabel1.setText("Start");
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.setBounds(new java.awt.Rectangle(10, 210, 441, 89));
			jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
					null, "Duration",
					javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
					javax.swing.border.TitledBorder.DEFAULT_POSITION,
					new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
					new java.awt.Color(51, 51, 51)));
			jPanel1.add(getJRadioButton(), null);
			jPanel1.add(getJRadioButton1(), null);
			jPanel1.add(durationLabel1, null);
			jPanel1.add(durationLabel2, null);
			jPanel1.add(jLabel2, null);
			jPanel1.add(jLabel3, null);
			jPanel1.add(getJTextField(), null);
			jPanel1.add(getJTextField1(), null);
			jPanel1.add(getJTextField2(), null);
			jPanel1.add(jLabel5, null);
			jPanel1.add(getFixDurationCheckBox(), null);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jRadioButton
	 * 
	 * @return javax.swing.JRadioButton
	 */

	private JRadioButton getJRadioButton() {
		if (durationUniformRadioButton == null) {
			durationUniformRadioButton = new JRadioButton();
			durationUniformRadioButton.setBounds(new java.awt.Rectangle(15, 30,
					91, 21));
			durationUniformRadioButton.setText("Uniform");
			durationUniformRadioButton.setSelected(true);
			durationUniformRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (durationGaussianRadioButton.isSelected() == true)
								durationGaussianRadioButton.setSelected(false);
							durationUniformRadioButton.setSelected(true);
							durationLabel1.setText("Start");
							durationLabel2.setText("End");
							startDurationTextField.setText("0");
							endDurationTextField.setText("0.1");

							isDurationUniform = true;
						}
					});
		}
		return durationUniformRadioButton;
	}

	/**
	 * This method initializes jRadioButton1
	 * 
	 * @return javax.swing.JRadioButton
	 */
	private JRadioButton getJRadioButton1() {
		if (durationGaussianRadioButton == null) {
			durationGaussianRadioButton = new JRadioButton();
			durationGaussianRadioButton
					.setBounds(new Rectangle(15, 60, 91, 21));
			durationGaussianRadioButton.setText("Gaussian");
			durationGaussianRadioButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (durationUniformRadioButton.isSelected() == true)
								durationUniformRadioButton.setSelected(false);
							durationGaussianRadioButton.setSelected(true);
							durationLabel1.setText("Mean");
							durationLabel2.setText("Std. Deviation");
							startDurationTextField.setText("0.5");
							endDurationTextField.setText("0.3");

							isDurationUniform = false;
						}
					});
		}
		return durationGaussianRadioButton;
	}

	/**
	 * This method initializes jTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField() {
		if (startDurationTextField == null) {
			startDurationTextField = new JTextField();
			startDurationTextField.setBounds(new Rectangle(195, 30, 46, 20));
			startDurationTextField.setEditable(true);
			startDurationTextField.setText("");
		}
		return startDurationTextField;
	}

	/**
	 * This method initializes jTextField1
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField1() {
		if (endDurationTextField == null) {
			endDurationTextField = new JTextField();
			endDurationTextField.setBounds(new Rectangle(195, 60, 46, 20));
		}
		return endDurationTextField;
	}

	/**
	 * This method initializes jTextField2
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField2() {
		if (fixedDurationTextField == null) {
			fixedDurationTextField = new JTextField();
			fixedDurationTextField.setLocation(new java.awt.Point(330, 50));
			fixedDurationTextField.setSize(new java.awt.Dimension(46, 20));
			fixedDurationTextField.setEnabled(false);
		}
		return fixedDurationTextField;
	}

	/**
	 * This method initializes fixDurationCheckBox
	 * 
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getFixDurationCheckBox() {
		if (fixDurationCheckBox == null) {
			fixDurationCheckBox = new JCheckBox();
			fixDurationCheckBox.setBounds(new java.awt.Rectangle(314, 18, 114,
					23));
			fixDurationCheckBox.setText("Fix duration in:");
			fixDurationCheckBox
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (fixDurationCheckBox.isSelected()) {
								durationLabel1.setEnabled(false);
								durationLabel2.setEnabled(false);
								startDurationTextField.setEnabled(false);
								endDurationTextField.setEnabled(false);
								durationUniformRadioButton.setEnabled(false);
								durationGaussianRadioButton.setEnabled(false);
								fixedDurationTextField.setEnabled(true);
							} else {
								durationLabel1.setEnabled(true);
								durationLabel2.setEnabled(true);
								startDurationTextField.setEnabled(true);
								endDurationTextField.setEnabled(true);
								durationUniformRadioButton.setEnabled(true);
								durationGaussianRadioButton.setEnabled(true);
								fixedDurationTextField.setEnabled(false);

							}

						}
					});
		}
		return fixDurationCheckBox;
	}

	/**
	 * This method initializes setZeroToTMaxCheckBox
	 * 
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getSetZeroToTMaxCheckBox() {
		if (setZeroToTMaxCheckBox == null) {
			setZeroToTMaxCheckBox = new JCheckBox();
			setZeroToTMaxCheckBox.setBounds(new java.awt.Rectangle(117, 63,
					173, 18));
			setZeroToTMaxCheckBox.setText("Set range from 0 to TMax");
			setZeroToTMaxCheckBox
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							if (setZeroToTMaxCheckBox.isSelected()) {
								timeLabel1.setEnabled(false);
								timeLabel2.setEnabled(false);
								startTimeTextField.setEnabled(false);
								endTimeTextField.setEnabled(false);
							} else {
								timeLabel1.setEnabled(true);
								timeLabel2.setEnabled(true);
								startTimeTextField.setEnabled(true);
								endTimeTextField.setEnabled(true);

							}

						}
					});
		}
		return setZeroToTMaxCheckBox;
	}

	/**
	 * This method initializes cancelButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getCancelButton() {
		if (cancelButton == null) {
			cancelButton = new JButton();
			cancelButton.setBounds(new java.awt.Rectangle(260, 310, 101, 31));
			cancelButton.setText("Cancel");
			cancelButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					setDefaultInitialValues();
					dispose();
				}
			});
		}
		return cancelButton;
	}

	/**
	 * This method initializes okButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton();
			okButton.setText("OK");
			okButton.setSize(new java.awt.Dimension(101, 31));
			okButton.setLocation(new java.awt.Point(100, 310));
			okButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {

					location_start = Double.parseDouble(startLocationTextField
							.getText());
					location_end = Double.parseDouble(endLocationTextField
							.getText());

					resistance_start = Double
							.parseDouble(startResistanceTextField.getText());
					resistance_end = Double.parseDouble(endResistanceTextField
							.getText());

					if (setZeroToTMaxCheckBox.isSelected()) {
						time_start = 0;
						time_end = -1; // -1 indicates GenFaults class that
						// Tmax will be used.

					} else {
						time_start = Double.parseDouble(startTimeTextField
								.getText());
						time_end = Double.parseDouble(endTimeTextField
								.getText());
					}

					if (fixDurationCheckBox.isSelected()) {
						duration_start = Double
								.parseDouble(fixedDurationTextField.getText());
						duration_end = duration_start;
						isDurationUniform = true;
					} else {
						duration_start = Double
								.parseDouble(startDurationTextField.getText());
						duration_end = Double.parseDouble(endDurationTextField
								.getText());
					}					
					
					dispose();
				}
			});
		}
		return okButton;
	}

} // @jve:decl-index=0:visual-constraint="10,10"
