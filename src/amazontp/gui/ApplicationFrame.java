package amazontp.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.LineZT;
import amazontp.parsing.Parse;
import amazontp.util.End;
import amazontp.util.ExampleFileFilter;

public class ApplicationFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	File fileSelected;

	String fileName;
	
	private Parse parse;
	
	public static String fileNameWithoutPath;

	private JPanel jContentPane = null;

	private JMenuBar jJMenuBar = null;

	private JMenu fileMenu = null;

	private JMenu simulationMenu = null;

	private JMenu helpMenu = null;

	private JMenuItem exitMenuItem = null;

	private JMenuItem openMenuItem = null;

	private JMenuItem singleFaultSettingsMenuItem = null;

	private JMenuItem randomFaultsMenuItem = null;

	private JMenuItem outputMenuItem = null;

	private JMenuItem aboutMenuItem = null;

	private JPanel jPanel = null;

	private JLabel jLabel = null;

	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getFileMenu());
			jJMenuBar.add(getSimulationMenu());
			jJMenuBar.add(getHelpMenu());
		}
		return jJMenuBar;
	}

	private JMenu getFileMenu() {
		if (fileMenu == null) {
			fileMenu = new JMenu();
			fileMenu.setText("File");
			fileMenu.add(getOpenMenuItem());
			fileMenu.add(getExitMenuItem());
		}
		return fileMenu;
	}

	private JMenu getSimulationMenu() {
		if (simulationMenu == null) {
			simulationMenu = new JMenu();
			simulationMenu.setText("Simulate");
			simulationMenu.setEnabled(false);
			simulationMenu.add(getSingleFaultSettingsMenuItem());
			simulationMenu.add(getRandomFaultsMenuItem());
			simulationMenu.add(getOutputMenuItem());
		}
		return simulationMenu;
	}


	private JMenu getHelpMenu() {
		if (helpMenu == null) {
			helpMenu = new JMenu();
			helpMenu.setText("Help");
			helpMenu.add(getAboutMenuItem());
		}
		return helpMenu;
	}

	private JMenuItem getOpenMenuItem() {
		if (openMenuItem == null) {
			openMenuItem = new JMenuItem();
			openMenuItem.setText("Open");
			openMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					JFileChooser fileChooser = new JFileChooser();

					ExampleFileFilter atpFilter;
					atpFilter = new ExampleFileFilter("atp", "Alternative Transient Program Files");
					fileChooser.addChoosableFileFilter(atpFilter);

					fileChooser.setAcceptAllFileFilterUsed(false);
					

					int ReturnSelect = fileChooser.showOpenDialog(openMenuItem);
					if (ReturnSelect == 0) {
						fileSelected = fileChooser.getSelectedFile();
						fileName = fileSelected.getPath();
						fileNameWithoutPath = fileSelected.getName();
						System.out.println("File: " + fileName);
						simulationMenu.setEnabled(true);
					}				
					
				}
			});
		}
		return openMenuItem;
	}

	private JMenuItem getExitMenuItem() {
		if (exitMenuItem == null) {
			exitMenuItem = new JMenuItem();
			exitMenuItem.setText("Exit");
			exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					End.exit();
				}
			});
		}
		return exitMenuItem;
	}

	private JMenuItem getSingleFaultSettingsMenuItem() {
		if (singleFaultSettingsMenuItem == null) {
			singleFaultSettingsMenuItem = new JMenuItem();
			singleFaultSettingsMenuItem.setText("Single Fault");
			singleFaultSettingsMenuItem
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							SelectLineDialog f = new SelectLineDialog();

							// f.setSize(new Dimension(400, 450));

							Dimension screenSize = Toolkit.getDefaultToolkit()
									.getScreenSize();

							Dimension frameSize = f.getSize();

							if (frameSize.height > screenSize.height) {

								frameSize.height = screenSize.height;

							}

							if (frameSize.width > screenSize.width) {

								frameSize.width = screenSize.width;

							}

							f.setLocation(
									(screenSize.width - frameSize.width) / 2,

									(screenSize.height - frameSize.height) / 2);

							f.setFileName(fileName);

							f.setVisible(true);

						}
					});
		}
		return singleFaultSettingsMenuItem;
	}

	private JMenuItem getRandomFaultsMenuItem() {
		if (randomFaultsMenuItem == null) {
			randomFaultsMenuItem = new JMenuItem();
			randomFaultsMenuItem.setText("Random Faults");
			randomFaultsMenuItem
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// faz o parse e manda o resultado para gui
							parse=new Parse(fileName);
							
							
							
							GenRandomDialog p = new GenRandomDialog(parse.getBranch(),fileName);

							// p.setSize(new Dimension(300, 200));

							Dimension screenSize = Toolkit.getDefaultToolkit()
									.getScreenSize();

							Dimension frameSize = p.getSize();

							if (frameSize.height > screenSize.height) {

								frameSize.height = screenSize.height;

							}

							if (frameSize.width > screenSize.width) {

								frameSize.width = screenSize.width;

							}

							p.setLocation(
									(screenSize.width - frameSize.width) / 2,

									(screenSize.height - frameSize.height) / 2);

							p.setFileName(fileName);

							p.setVisible(true);

						}
					});
		}
		return randomFaultsMenuItem;
	}

	private JMenuItem getOutputMenuItem() {
		if (outputMenuItem == null) {
			outputMenuItem = new JMenuItem();
			outputMenuItem.setText("Output Selection");
			outputMenuItem
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
						}
					});
		}
		return outputMenuItem;
	}

	private JMenuItem getAboutMenuItem() {
		if (aboutMenuItem == null) {
			aboutMenuItem = new JMenuItem();
			aboutMenuItem.setText("About");
			aboutMenuItem
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							About dlg = new About();

							Dimension screenSize = Toolkit.getDefaultToolkit()
									.getScreenSize();

							Dimension frameSize = dlg.getSize();

							if (frameSize.height > screenSize.height) {

								frameSize.height = screenSize.height;

							}

							if (frameSize.width > screenSize.width) {

								frameSize.width = screenSize.width;

							}

							dlg.setLocation(
									(screenSize.width - frameSize.width) / 2,

									(screenSize.height - frameSize.height) / 2);

							dlg.setVisible(true);

						}
					});
		}
		return aboutMenuItem;
	}

	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel = new JLabel();
			jLabel.setText("");
			jLabel.setSize(new java.awt.Dimension(201, 122));
			jLabel.setLocation(new java.awt.Point(2, 0));
			jLabel.setIcon(new ImageIcon(getClass().getResource(
					"/amazontp/gui/amazontp.png")));
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.add(jLabel, null);
		}
		return jPanel;
	}

	public ApplicationFrame() {
		super();
		initialize();
	}

	private void initialize() {
		this.setSize(210, 174);
		this.setResizable(false);
		this.setJMenuBar(getJJMenuBar());
		this.setContentPane(getJContentPane());
		this.setTitle("AmazonTP 1.0");
		Dimension frameSize = getPreferredSize();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);

		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				End.exit();
			}
		});
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJPanel(), java.awt.BorderLayout.CENTER);
		}
		return jContentPane;
	}

	public String getFileName() {
		return fileName;
	}

} // @jve:decl-index=0:visual-constraint="10,10"
