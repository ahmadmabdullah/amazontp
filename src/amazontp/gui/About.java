package amazontp.gui;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author  Jefferson
 */
public class About extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel jPanel = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JLabel jLabel4 = null;
	private JLabel jLabel5 = null;
	private JLabel jLabel6 = null;
	private JLabel jLabel7 = null;
	private JLabel jLabel8 = null;
	private JLabel jLabel9 = null;
	private JLabel jLabel10 = null;
	private JLabel jLabel11 = null;
	/**
	 * This is the default constructor
	 */
	public About() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(252, 534);
		this.setContentPane(getJPanel());
		this.setModal(true);
		this.setTitle("About AmazonTP 1.0");
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel11 = new JLabel();
			jLabel11.setBounds(new java.awt.Rectangle(21,459,210,25));
			jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel11.setText("Surya Santoso  (Univ. of Texas, USA)");
			jLabel10 = new JLabel();
			jLabel10.setBounds(new java.awt.Rectangle(20,403,211,29));
			jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel10.setText("Ant�nio Carvalho (GSEI - UFPA)");
			jLabel9 = new JLabel();
			jLabel9.setBounds(new java.awt.Rectangle(20,430,211,29));
			jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel9.setText("Marcus Vinicius (GSEI - UFPA)");
			jLabel8 = new JLabel();
			jLabel8.setBounds(new java.awt.Rectangle(20,380,211,24));
			jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel8.setText("With aids of:");
			jLabel7 = new JLabel();
			jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel7.setLocation(new java.awt.Point(70,330));
			jLabel7.setSize(new java.awt.Dimension(111,28));
			jLabel7.setText("Yomara Pires");
			jLabel6 = new JLabel();
			jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel6.setLocation(new java.awt.Point(70,300));
			jLabel6.setSize(new java.awt.Dimension(111,28));
			jLabel6.setText("Jos� Borges");
			jLabel5 = new JLabel();
			jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel5.setLocation(new java.awt.Point(70,270));
			jLabel5.setSize(new java.awt.Dimension(111,28));
			jLabel5.setText("Jefferson Morais");
			jLabel4 = new JLabel();
			jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel4.setLocation(new java.awt.Point(70,240));
			jLabel4.setSize(new java.awt.Dimension(111,28));
			jLabel4.setText("Gustavo Guedes");
			jLabel3 = new JLabel();
			jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel3.setLocation(new java.awt.Point(70,210));
			jLabel3.setSize(new java.awt.Dimension(111,28));
			jLabel3.setText("Andr�ia Santos");
			jLabel2 = new JLabel();
			jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel2.setLocation(new java.awt.Point(70,180));
			jLabel2.setSize(new java.awt.Dimension(111,28));
			jLabel2.setText("Aldebaro Klautau");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new java.awt.Rectangle(20,150,211,27));
			jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			jLabel1.setText("Developed in LaPS - UFPA, Brazil by:");
			jLabel = new JLabel();
			jLabel.setBounds(new java.awt.Rectangle(20,20,201,123));
			jLabel.setDisabledIcon(new ImageIcon(getClass().getResource("/amazontp/gui/amazontp.png")));
			jLabel.setIcon(new ImageIcon(getClass().getResource("/amazontp/gui/amazontp.png")));
			jLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
			jLabel.setText("JLabel");
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.add(jLabel, null);
			jPanel.add(jLabel1, null);
			jPanel.add(jLabel2, null);
			jPanel.add(jLabel3, null);
			jPanel.add(jLabel4, null);
			jPanel.add(jLabel5, null);
			jPanel.add(jLabel6, null);
			jPanel.add(jLabel7, null);
			jPanel.add(jLabel8, null);
			jPanel.add(jLabel9, null);
			jPanel.add(jLabel10, null);
			jPanel.add(jLabel11, null);
		}
		return jPanel;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
