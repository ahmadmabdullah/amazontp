package amazontp.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JTabbedPane;

import amazontp.atpcomponents.LineZT;

import java.awt.Dimension;
import java.awt.BorderLayout;

public class LineZTPropertiesPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8328577738511201418L;
	private JTabbedPane jTabbedPane = null;
	private JPanel jPanel = null;
	private JCheckBox selectThisLineCheckBox = null;
	private JPanel genNodesPanel = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JPanel endNodesPanel = null;
	private JTextField genNodeATextField = null;
	private JTextField genNodeBTextField = null;
	private JTextField genNodeCTextField = null;
	private JLabel jLabel3 = null;
	private JLabel jLabel4 = null;
	private JLabel jLabel5 = null;
	private JTextField endNodeATextField = null;
	private JTextField endNodeBTextField = null;
	private JTextField endNodeCTextField = null;
	private JPanel ValuesPanel1 = null;
	private JTextField lengthTextField = null;
	private JLabel zeroSeqResLabel = null;
	private JLabel jLabel7 = null;
	private JLabel jLabel8 = null;
	private JTextField zeroSeqResTextField = null;
	private JTextField zeroSeqIndTextField = null;
	private JTextField zeroSeqCapTextField = null;
	private JLabel lengthLabel = null;
	private JPanel jPanel1 = null;
	private JLabel jLabel6 = null;
	private JLabel jLabel9 = null;
	private JLabel jLabel10 = null;
	private JTextField posSeqResTextField = null;
	private JTextField posSeqIndTextField = null;
	private JTextField posSeqCapTextField = null;
	private LineZT lineZT;
	private static Dimension d = new Dimension(361, 196);
	/**
	 * This is the default constructor
	 */
	public LineZTPropertiesPanel(LineZT linezt) {
		super();
		initialize();
		this.lineZT = linezt;
	}
	
	public LineZTPropertiesPanel() {
		super();
		initialize();
		this.lineZT = null;
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		lengthLabel = new JLabel();
		lengthLabel.setBounds(new java.awt.Rectangle(180,15,91,16));
		lengthLabel.setText("Line Length:");
		this.setLayout(null);
		this.setSize(d);
		this.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.gray,1));
		this.add(getJTabbedPane(), null);
		this.add(getSelectThisLineCheckBox(), null);
		this.add(getLengthTextField(), null);
		this.add(lengthLabel, null);
	}

	/**
	 * This method initializes jTabbedPane	
	 * 	
	 * @return javax.swing.JTabbedPane	
	 */
	private JTabbedPane getJTabbedPane() {
		if (jTabbedPane == null) {
			jTabbedPane = new JTabbedPane();
			jTabbedPane.setBounds(new java.awt.Rectangle(15,45,331,136));
			jTabbedPane.addTab("Gen Nodes", null, getJPanel(), null);
			jTabbedPane.addTab("End Nodes", null, getEndNodesPanel(), null);
			jTabbedPane.addTab("Zero Seq", null, getValuesPanel1(), null);
			jTabbedPane.addTab("Pos Seq", null, getJPanel1(), null);
		}
		return jTabbedPane;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(new BorderLayout());
			jPanel.add(getGenNodesPanel(), java.awt.BorderLayout.CENTER);
		}
		return jPanel;
	}

	/**
	 * This method initializes selectThisLineCheckBox	
	 * 	
	 * @return javax.swing.JCheckBox	
	 */
	public JCheckBox getSelectThisLineCheckBox() {
		if (selectThisLineCheckBox == null) {
			selectThisLineCheckBox = new JCheckBox();
			selectThisLineCheckBox.setText("Select this line");
			selectThisLineCheckBox.setBounds(new java.awt.Rectangle(15,15,121,16));
		}
		return selectThisLineCheckBox;
	}

	/**
	 * This method initializes nodesPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getGenNodesPanel() {
		if (genNodesPanel == null) {
			jLabel2 = new JLabel();
			jLabel2.setBounds(new java.awt.Rectangle(15,75,106,16));
			jLabel2.setText("Gen Node C:");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new java.awt.Rectangle(15,45,106,16));
			jLabel1.setText("Gen Node B:");
			jLabel = new JLabel();
			jLabel.setBounds(new java.awt.Rectangle(15,15,106,16));
			jLabel.setText("Gen Node A:");
			genNodesPanel = new JPanel();
			genNodesPanel.setLayout(null);
			genNodesPanel.add(jLabel, null);
			genNodesPanel.add(jLabel1, null);
			genNodesPanel.add(jLabel2, null);
			genNodesPanel.add(getGenNodeATextField(), null);
			genNodesPanel.add(getGenNodeBTextField(), null);
			genNodesPanel.add(getGenNodeCTextField(), null);
		}
		return genNodesPanel;
	}

	/**
	 * This method initializes endNodesPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getEndNodesPanel() {
		if (endNodesPanel == null) {
			jLabel5 = new JLabel();
			jLabel5.setBounds(new java.awt.Rectangle(15,75,106,16));
			jLabel5.setText("End Node C:");
			jLabel4 = new JLabel();
			jLabel4.setBounds(new java.awt.Rectangle(15,45,106,16));
			jLabel4.setText("End Node B:");
			jLabel3 = new JLabel();
			jLabel3.setBounds(new java.awt.Rectangle(15,15,106,16));
			jLabel3.setText("End Node A:");
			endNodesPanel = new JPanel();
			endNodesPanel.setLayout(null);
			endNodesPanel.add(jLabel3, null);
			endNodesPanel.add(jLabel4, null);
			endNodesPanel.add(jLabel5, null);
			endNodesPanel.add(getEndNodeATextField(), null);
			endNodesPanel.add(getEndNodeBTextField(), null);
			endNodesPanel.add(getEndNodeCTextField(), null);
		}
		return endNodesPanel;
	}

	/**
	 * This method initializes genNodeATextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getGenNodeATextField() {
		if (genNodeATextField == null) {
			genNodeATextField = new JTextField();
			genNodeATextField.setBounds(new java.awt.Rectangle(120,15,91,20));
		}
		return genNodeATextField;
	}

	/**
	 * This method initializes genNodeBTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getGenNodeBTextField() {
		if (genNodeBTextField == null) {
			genNodeBTextField = new JTextField();
			genNodeBTextField.setBounds(new java.awt.Rectangle(120,45,91,20));
		}
		return genNodeBTextField;
	}

	/**
	 * This method initializes genNodeCTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getGenNodeCTextField() {
		if (genNodeCTextField == null) {
			genNodeCTextField = new JTextField();
			genNodeCTextField.setBounds(new java.awt.Rectangle(120,75,91,20));
		}
		return genNodeCTextField;
	}

	/**
	 * This method initializes jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getEndNodeATextField() {
		if (endNodeATextField == null) {
			endNodeATextField = new JTextField();
			endNodeATextField.setBounds(new java.awt.Rectangle(120,15,91,20));
		}
		return endNodeATextField;
	}

	/**
	 * This method initializes jTextField1	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getEndNodeBTextField() {
		if (endNodeBTextField == null) {
			endNodeBTextField = new JTextField();
			endNodeBTextField.setBounds(new java.awt.Rectangle(120,45,91,20));
		}
		return endNodeBTextField;
	}

	/**
	 * This method initializes jTextField2	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getEndNodeCTextField() {
		if (endNodeCTextField == null) {
			endNodeCTextField = new JTextField();
			endNodeCTextField.setBounds(new java.awt.Rectangle(120,75,91,20));
		}
		return endNodeCTextField;
	}

	/**
	 * This method initializes ValuesPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getValuesPanel1() {
		if (ValuesPanel1 == null) {
			jLabel8 = new JLabel();
			jLabel8.setBounds(new java.awt.Rectangle(15,75,151,16));
			jLabel8.setText("Zero Seq Capacitance:");
			jLabel7 = new JLabel();
			jLabel7.setBounds(new java.awt.Rectangle(15,45,151,16));
			jLabel7.setText("Zero Seq Inductance:");
			zeroSeqResLabel = new JLabel();
			zeroSeqResLabel.setBounds(new java.awt.Rectangle(15,15,151,16));
			zeroSeqResLabel.setText("Zero Seq Resistance:");
			ValuesPanel1 = new JPanel();
			ValuesPanel1.setLayout(null);
			ValuesPanel1.add(zeroSeqResLabel, null);
			ValuesPanel1.add(jLabel7, null);
			ValuesPanel1.add(jLabel8, null);
			ValuesPanel1.add(getZeroSeqResTextField(), null);
			ValuesPanel1.add(getZeroSeqIndTextField(), null);
			ValuesPanel1.add(getZeroSeqCapTextField(), null);
		}
		return ValuesPanel1;
	}

	/**
	 * This method initializes lengthTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getLengthTextField() {
		if (lengthTextField == null) {
			lengthTextField = new JTextField();
			lengthTextField.setBounds(new java.awt.Rectangle(270,15,76,20));
		}
		return lengthTextField;
	}

	/**
	 * This method initializes zeroSeqResTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getZeroSeqResTextField() {
		if (zeroSeqResTextField == null) {
			zeroSeqResTextField = new JTextField();
			zeroSeqResTextField.setBounds(new java.awt.Rectangle(165,15,76,20));
		}
		return zeroSeqResTextField;
	}

	/**
	 * This method initializes zeroSeqIndTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getZeroSeqIndTextField() {
		if (zeroSeqIndTextField == null) {
			zeroSeqIndTextField = new JTextField();
			zeroSeqIndTextField.setBounds(new java.awt.Rectangle(165,45,76,20));
		}
		return zeroSeqIndTextField;
	}

	/**
	 * This method initializes zeroSeqCapTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getZeroSeqCapTextField() {
		if (zeroSeqCapTextField == null) {
			zeroSeqCapTextField = new JTextField();
			zeroSeqCapTextField.setBounds(new java.awt.Rectangle(165,75,76,20));
		}
		return zeroSeqCapTextField;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jLabel10 = new JLabel();
			jLabel10.setBounds(new java.awt.Rectangle(15,75,151,16));
			jLabel10.setText("Pos Seq Capacitance:");
			jLabel9 = new JLabel();
			jLabel9.setBounds(new java.awt.Rectangle(15,45,151,16));
			jLabel9.setText("Pos Seq Inductance:");
			jLabel6 = new JLabel();
			jLabel6.setBounds(new java.awt.Rectangle(15,15,151,16));
			jLabel6.setText("Pos Seq Resistance:");
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.add(jLabel6, null);
			jPanel1.add(jLabel9, null);
			jPanel1.add(jLabel10, null);
			jPanel1.add(getPosSeqResTextField(), null);
			jPanel1.add(getPosSeqIndTextField(), null);
			jPanel1.add(getPosSeqCapTextField(), null);
		}
		return jPanel1;
	}

	/**
	 * This method initializes jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getPosSeqResTextField() {
		if (posSeqResTextField == null) {
			posSeqResTextField = new JTextField();
			posSeqResTextField.setBounds(new java.awt.Rectangle(165,15,76,20));
		}
		return posSeqResTextField;
	}

	/**
	 * This method initializes jTextField1	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getPosSeqIndTextField() {
		if (posSeqIndTextField == null) {
			posSeqIndTextField = new JTextField();
			posSeqIndTextField.setBounds(new java.awt.Rectangle(165,45,76,20));
		}
		return posSeqIndTextField;
	}

	/**
	 * This method initializes jTextField2	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	public JTextField getPosSeqCapTextField() {
		if (posSeqCapTextField == null) {
			posSeqCapTextField = new JTextField();
			posSeqCapTextField.setBounds(new java.awt.Rectangle(165,75,76,20));
		}
		return posSeqCapTextField;
	}
	
	public static Dimension getDimension() {
		return d;
	}

	public LineZT getLineZT() {
		return lineZT;
	}

	public void setLineZT(LineZT lineZT) {
		this.lineZT = lineZT;
	}

}  //  @jve:decl-index=0:visual-constraint="23,14"
