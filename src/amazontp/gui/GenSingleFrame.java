package amazontp.gui;

import java.awt.Choice;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import amazontp.io.SaveFile;
import amazontp.util.RunATP;

/**
 * @author Jefferson
 */
public class GenSingleFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String atpDir;
	
	String amazontpDir = System.getenv("AMAZONTP");
	
	static String oS = System.getProperty("os.name");
	
	String genSingleScriptName;

	int numberOfFaultType;

	double initiation_time, cleaning_time, location_from_begin, line_to_line_R,
			line_to_ground_R;

	String fileName;
	
	Runtime r = Runtime.getRuntime();

	public void setFileName(String name) {
		fileName = name;
	}

	/**
	 * @uml.property name="jContentPane"
	 */
	private JPanel jContentPane = null;

	/**
	 * @uml.property name="jPanel"
	 */
	private JPanel jPanel = null;

	/**
	 * @uml.property name="jPanel1"
	 */
	private JPanel jPanel1 = null;

	private JLabel jLabel = null;

	/**
	 * @uml.property name="choice"
	 */
	private Choice choice = null;

	/**
	 * @uml.property name="jButton"
	 */
	private JButton jButton = null;

	/**
	 * @uml.property name="jTextField"
	 */
	private JTextField locationTextField = null;

	/**
	 * @uml.property name="jPanel4"
	 */
	private JPanel jPanel4 = null;

	/**
	 * @uml.property name="jTextField9"
	 */
	private JTextField line_to_lineTextField = null;

	private JLabel jLabel24 = null;

	private JLabel jLabel1 = null;

	private JLabel jLabel4 = null;

	private JTextField initiationTextField = null;

	private JLabel jLabel5 = null;

	private JLabel jLabel2 = null;

	private JTextField cleaningTextField = null;

	private JLabel jLabel3 = null;

	private JLabel jLabel6 = null;

	private JLabel jLabel7 = null;

	private JTextField line_to_groundTextField = null;

	private JLabel jLabel8 = null;

	/**
	 * This is the default constructor
	 */
	public GenSingleFrame() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(427, 261);
		this.setResizable(false);
		this.setContentPane(getJContentPane());
		this.setTitle("Single Fault Settings ");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jContentPane"
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getJPanel(), null);
			jContentPane.add(getJPanel1(), null);
			jContentPane.add(getJPanel4(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes jPanel
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jPanel"
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel = new JLabel();
			jLabel.setBounds(new java.awt.Rectangle(38, 21, 100, 27));
			jLabel
					.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
							12));
			jLabel.setText("Select Fault Type");
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
					"Fault Type",
					javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
					javax.swing.border.TitledBorder.DEFAULT_POSITION,
					new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12),
					java.awt.Color.blue));
			jPanel.setSize(new java.awt.Dimension(410, 60));
			jPanel.setLocation(new java.awt.Point(4, 4));
			jPanel.add(jLabel, null);
			jPanel.add(getChoice(), null);
			jPanel.add(getJButton(), null);
		}
		return jPanel;
	}

	/**
	 * This method initializes jPanel1
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jPanel1"
	 */
	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jLabel3 = new JLabel();
			jLabel3.setText("sec");
			jLabel3.setLocation(new java.awt.Point(187, 55));
			jLabel3.setSize(new java.awt.Dimension(20, 20));
			jLabel3.setFont(new Font("Dialog", Font.PLAIN, 12));
			jLabel2 = new JLabel();
			jLabel2.setText("Fault Cleaning Time:");
			jLabel2.setLocation(new java.awt.Point(31, 57));
			jLabel2.setSize(new java.awt.Dimension(119, 20));
			jLabel2
					.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
							12));
			jLabel5 = new JLabel();
			jLabel5.setText("sec");
			jLabel5.setLocation(new java.awt.Point(187, 32));
			jLabel5.setSize(new java.awt.Dimension(20, 20));
			jLabel5.setFont(new Font("Dialog", Font.PLAIN, 12));
			jLabel4 = new JLabel();
			jLabel4.setText("Fault Initiation Time:");
			jLabel4.setSize(new java.awt.Dimension(110, 20));
			jLabel4.setLocation(new java.awt.Point(31, 34));
			jLabel4.setFont(new Font("Dialog", Font.PLAIN, 12));
			jLabel1 = new JLabel();
			jLabel1
					.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
							12));
			jLabel1.setLocation(new java.awt.Point(244, 46));
			jLabel1.setSize(new java.awt.Dimension(88, 20));
			jLabel1.setText("% of line length:");
			jPanel1 = new JPanel();
			jPanel1.setLayout(null);
			jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
					null, "Fault Location and Time",
					javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
					javax.swing.border.TitledBorder.DEFAULT_POSITION,
					new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12),
					java.awt.Color.blue));
			jPanel1.setSize(new java.awt.Dimension(410, 88));
			jPanel1.setLocation(new java.awt.Point(4, 69));
			jPanel1.add(getJTextField(), null);
			jPanel1.add(jLabel1, null);
			jPanel1.add(jLabel4, null);
			jPanel1.add(getJTextField2(), null);
			jPanel1.add(jLabel5, null);
			jPanel1.add(jLabel2, null);
			jPanel1.add(getJTextField1(), null);
			jPanel1.add(jLabel3, null);
		}
		return jPanel1;
	}

	/**
	 * This method initializes choice
	 * 
	 * @return java.awt.Choice
	 * @uml.property name="choice"
	 */
	private Choice getChoice() {
		if (choice == null) {
			choice = new Choice();
			choice.setBounds(new java.awt.Rectangle(172, 24, 97, 31));
		}

		choice.addItem("AG Fault");
		choice.addItem("BG Fault");
		choice.addItem("CG Fault");
		choice.addItem("AB Fault");
		choice.addItem("AC Fault");
		choice.addItem("BC Fault");
		choice.addItem("ABC Fault");
		choice.addItem("ABG Fault");
		choice.addItem("ACG Fault");
		choice.addItem("BCG Fault");
		choice.addItem("ABCG Fault");

		return choice;
	}

	/**
	 * This method initializes jButton
	 * 
	 * @return javax.swing.JButton
	 * @uml.property name="jButton"
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setBounds(new java.awt.Rectangle(293, 21, 96, 27));
			jButton.setText("Run");
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {

					numberOfFaultType = choice.getSelectedIndex();
					if (numberOfFaultType == -1) {
						System.err.println("Please choose a fault type!");
						System.exit(1);
					}

					initiation_time = Double.parseDouble(initiationTextField
							.getText());
					cleaning_time = Double.parseDouble(cleaningTextField
							.getText());
					location_from_begin = Double.parseDouble(locationTextField
							.getText());
					line_to_line_R = Double.parseDouble(line_to_lineTextField
							.getText());
					line_to_ground_R = Double
							.parseDouble(line_to_groundTextField.getText());

					if (oS.indexOf("Windows") != -1) {
						atpDir = System.getenv("WATDIR");
						if (atpDir == null) {
							atpDir = "c:\\ATP";
							System.err.println("Couldn't find Enviroment Variable WATDIR, setting c:\\ATP as ATP Folder.");
						} else {
							System.out.println("TPBIG located in: " + atpDir);		
						}
					} else {
						atpDir = System.getenv("GNUDIR");
						if (atpDir == null) {
							atpDir = "/home/atplinux";
							System.err.println("Couldn't find Enviroment Variable GNUDIR, setting /home/linux as ATP Folder.");
						} else {
							System.out.println("TPBIG located in: " + atpDir);		
						}
					}

					try {

						GenBatFile();

					} catch (Exception ex) {

						ex.printStackTrace();
						dispose();

					}

					new RunATP(atpDir + genSingleScriptName);
				
					dispose();
				}
			});
		}
		return jButton;
	}

	public void GenBatFile() throws Exception {

		if (oS.indexOf("Windows") != -1) {
			genSingleScriptName = "GenSingleFault.bat";
		} else {
			genSingleScriptName = "GenSingleFault.sh";
		}
		
		SaveFile sb = new SaveFile();
		sb.deleteFile(atpDir + genSingleScriptName);
		sb.createFile(atpDir + genSingleScriptName);
		sb.saveFile(atpDir + genSingleScriptName, "cd " + atpDir);
		sb.saveFile(atpDir + genSingleScriptName,
				"java -cp " + amazontpDir + " amazontp.genfaults.GenSingleFault "
						+ fileName + " " + 0 + " " + numberOfFaultType + " " +initiation_time + " "
						+ cleaning_time + " " + location_from_begin + " "
						+ line_to_line_R + " " + line_to_ground_R); // O zero � s� enquanto nao fazemos
											// a escolha das linhas.
		
		if (oS.indexOf("Windows") == -1) {
			r.exec("chmod a+x " + atpDir + genSingleScriptName);
			Thread.sleep(100);
		}

	}

	/**
	 * This method initializes jTextField
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="jTextField"
	 */
	private JTextField getJTextField() {
		if (locationTextField == null) {
			locationTextField = new JTextField();
			locationTextField.setLocation(new java.awt.Point(338, 47));
			locationTextField.setSize(new java.awt.Dimension(30, 20));
		}
		return locationTextField;
	}

	/**
	 * This method initializes jPanel4
	 * 
	 * @return javax.swing.JPanel
	 * @uml.property name="jPanel4"
	 */
	private JPanel getJPanel4() {
		if (jPanel4 == null) {
			jLabel8 = new JLabel();
			jLabel8.setText("Ohm");
			jLabel8.setSize(new java.awt.Dimension(31, 20));
			jLabel8.setLocation(new java.awt.Point(340, 25));
			jLabel8.setFont(new Font("Dialog", Font.PLAIN, 12));
			jLabel7 = new JLabel();
			jLabel7
					.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
							12));
			jLabel7.setLocation(new java.awt.Point(216, 25));
			jLabel7.setSize(new java.awt.Dimension(85, 20));
			jLabel7.setText("Line-to-ground");
			jLabel6 = new JLabel();
			jLabel6
					.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
							12));
			jLabel6.setLocation(new java.awt.Point(40, 25));
			jLabel6.setSize(new java.awt.Dimension(68, 20));
			jLabel6.setText("Line-to-line");
			jLabel24 = new JLabel();
			jLabel24.setText("Ohm");
			jLabel24.setLocation(new java.awt.Point(145, 25));
			jLabel24.setSize(new java.awt.Dimension(30, 20));
			jLabel24.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN,
					12));
			jPanel4 = new JPanel();
			jPanel4.setLayout(null);
			jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(
					null, "Resistance",
					javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
					javax.swing.border.TitledBorder.DEFAULT_POSITION,
					new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12),
					java.awt.Color.blue));
			jPanel4.setLocation(new java.awt.Point(4, 162));
			jPanel4.setSize(new java.awt.Dimension(410, 59));
			jPanel4.add(getJTextField9(), null);
			jPanel4.add(jLabel24, null);
			jPanel4.add(jLabel6, null);
			jPanel4.add(jLabel7, null);
			jPanel4.add(getJTextField3(), null);
			jPanel4.add(jLabel8, null);
		}
		return jPanel4;
	}

	/**
	 * This method initializes jTextField9
	 * 
	 * @return javax.swing.JTextField
	 * @uml.property name="jTextField9"
	 */
	private JTextField getJTextField9() {
		if (line_to_lineTextField == null) {
			line_to_lineTextField = new JTextField();
			line_to_lineTextField.setLocation(new java.awt.Point(110, 25));
			line_to_lineTextField.setSize(new java.awt.Dimension(30, 20));
		}
		return line_to_lineTextField;
	}

	/**
	 * This method initializes jTextField2
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField2() {
		if (initiationTextField == null) {
			initiationTextField = new JTextField();
			initiationTextField.setSize(new java.awt.Dimension(30, 20));
			initiationTextField.setLocation(new java.awt.Point(151, 33));
		}
		return initiationTextField;
	}

	/**
	 * This method initializes jTextField1
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField1() {
		if (cleaningTextField == null) {
			cleaningTextField = new JTextField();
			cleaningTextField.setLocation(new java.awt.Point(151, 56));
			cleaningTextField.setSize(new java.awt.Dimension(30, 20));
		}
		return cleaningTextField;
	}

	/**
	 * This method initializes jTextField3
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField3() {
		if (line_to_groundTextField == null) {
			line_to_groundTextField = new JTextField();
			line_to_groundTextField.setLocation(new java.awt.Point(306, 25));
			line_to_groundTextField.setSize(new java.awt.Dimension(30, 20));
		}
		return line_to_groundTextField;
	}

	public static void main(String[] args) throws Exception {
		GenSingleFrame sF = new GenSingleFrame();
		sF.setVisible(true);
	}

} // @jve:decl-index=0:visual-constraint="10,10"
