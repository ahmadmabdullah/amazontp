package amazontp.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import amazontp.atpcomponents.LineZT;


public class LineZTGroupFrame extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269649135119810187L;
	
	private Map <Integer,LineZT>lineZt=new HashMap<Integer, LineZT>();

	private JPanel linesPanel;

	private Vector<LineZTPropertiesPanel> linesGroup;

	private JScrollPane jscrollpane;
	
	private int cont=0; //numero de linhas ZT

	public LineZTGroupFrame(Vector<LineZTPropertiesPanel> linesGroup) {
	//uper("LineZT's of the circuit");

		this.linesGroup = linesGroup;
		initialize();
	}
	
	public LineZTGroupFrame() {
	//	super("LineZT's of the circuit");
		
		linesGroup = new Vector<LineZTPropertiesPanel>();
		initialize();
	}
	
	private void initialize() {
		linesPanel = new JPanel();
		jscrollpane = new JScrollPane();

		//this.add(new JScrollPane(linesPanel));
	}

	public void addLineZTPropertiesPanel(LineZT x){
		linesGroup.addElement(x.getLineZTPropertiesPanel());
		lineZt.put(cont,x);
		cont++;
		update();
	}

	public LineZTPropertiesPanel getLineZTPropertiesPanel(int index) {
		return linesGroup.elementAt(index);
	}
	
	public LineZT getLineZT(int index) {
		return linesGroup.elementAt(index).getLineZT();
	}

	private void update() {
		int numberOfPanels = linesGroup.size();

		linesPanel.setLayout(new GridLayout(numberOfPanels, 1));
		linesPanel.setPreferredSize(new Dimension(LineZTPropertiesPanel
				.getDimension().width, LineZTPropertiesPanel
				.getDimension().height
				* numberOfPanels));

		linesPanel.removeAll();
		for (int i = 0; i < numberOfPanels; i++) {
			linesPanel.add(linesGroup.elementAt(i));
		}

		jscrollpane = new JScrollPane(linesPanel);
        this.add(jscrollpane);

		if (numberOfPanels > 3) {

			setSize(new Dimension(
					LineZTPropertiesPanel.getDimension().width + 30,
					LineZTPropertiesPanel.getDimension().height * 3 + 30));
		} else {
			setSize(new Dimension(
					LineZTPropertiesPanel.getDimension().width + 30,
					LineZTPropertiesPanel.getDimension().height
							* numberOfPanels + 30));
		}

		System.out.print(this.getPreferredSize().toString());

	}
	
	public  int[] getIndexesOfSelectedLines() {
		int[] temp = new int[linesGroup.size()];
		int effectiveLength = 0;
		int j = 0;
		for (int i = 0; i < temp.length; i++) {
			if (linesGroup.get(i).getSelectThisLineCheckBox().isSelected()) {
				temp[j] = i;
				effectiveLength++;
				j++;
			}
		}
		
		int[] indexes = new int[effectiveLength];
		for (int i = 0; i < indexes.length; i++) {
			indexes[i] = temp[i];			
		}
		return indexes;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LineZTGroupFrame f = new LineZTGroupFrame();
		LineZTPropertiesPanel p = new LineZTPropertiesPanel();
	//	LineZTPropertiesPanel p1 = new LineZTPropertiesPanel();
	//	LineZTPropertiesPanel p2 = new LineZTPropertiesPanel();
	//	LineZTPropertiesPanel p3 = new LineZTPropertiesPanel();
	//	LineZTPropertiesPanel p4 = new LineZTPropertiesPanel();
	//	LineZTPropertiesPanel p5 = new LineZTPropertiesPanel();

	//	f.addLineZTPropertiesPanel(p);
	//	f.addLineZTPropertiesPanel(p1);
	//	f.addLineZTPropertiesPanel(p2);
	//	f.addLineZTPropertiesPanel(p3);
	//	f.addLineZTPropertiesPanel(p4);
	//	f.addLineZTPropertiesPanel(p5);
	   	f.setVisible(true);
	}

	/**
	 * @return Retorna o lineZt.
	 */
	public Map<Integer, LineZT> getLineZt() {
		return lineZt;
	}

	
}
