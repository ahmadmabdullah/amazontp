package amazontp.matlab;

import amazontp.io.*;

/**
 * 
 * @author Aldebaro
 *
 */
public class WaveletPower {
	
	private static int m_ndecompositionLevel = 4;
	private static String m_wavelet = "db4";
	private static double m_windowDuration = 10e-3;
	private static double m_samplingFrequency = 40000.0;
	private static int m_samplesPerWindow;
	
	private static boolean m_odebug = false;
	
	static {
		double outputFs = m_samplingFrequency / Math.pow(2, m_ndecompositionLevel);
		m_samplesPerWindow = (int) (m_windowDuration * outputFs);
		if (m_samplesPerWindow < 1) {
			m_samplesPerWindow = 1;
		}		
	}

	/**
	 * @param args
	 * java -cp c:\cvs\laps\amazontp\bin;c:\cvs\laps\\ufpajava\\bin amazontp.util.WaveletPower . bin
	 */
	public static void main(String[] args) {		
		if (args.length != 2) {
			System.out.println("Usage: <input directory> <input files extension>");
			System.exit(1);
		}		
		String dir = args[0];
		String extension = args[1];
		
		DirectoryTree directoryTree = new DirectoryTree(dir, extension);
		
		String[] fileNames = directoryTree.getFilesAsStrings();
		if (fileNames == null) {
			System.err.println("Couldn't find files with extension " + extension);
			System.exit(1);
		} else {
			System.out.println("Found " + fileNames.length + " files under " + dir);
		}
		
		
		for (int i = 0; i < fileNames.length; i++) {			
			double[][] x = IO.readMatrixFromBinFile(fileNames[i]);
			double[][] allEnergies = null;
			for (int j = 0; j < 6; j++) {
				MatlabInterfacer.sendArray(x[j], "x");			
				double[][] energies = getOutputEnergy();
				if (allEnergies == null) {
					//initialize. Need to do this after knowing its dimension
					//all rows will have energies[0].length columns
					allEnergies = new double[6 * energies.length][];
				}
				for (int k = 0; k < energies.length; k++) {
					allEnergies[energies.length*j + k] = energies[k]; 
				}
			}
			String outputFileName = FileNamesAndDirectories.substituteExtension(fileNames[i], ".ene");
			IO.writeMatrixtoBinFile(outputFileName, allEnergies);
			
			//ak
			System.out.println(fileNames[i] + "\t => " + outputFileName);
			
			if (m_odebug) {
				MatlabInterfacer.sendArray(allEnergies, "y");
				MatlabInterfacer.sendCommand("plot(y');");
				IO.pause();
			}
		}
		System.exit(1);
	}

	//[C,L] = WAVEDEC(X,N,'wname') returns the wavelet
    //decomposition of the signal X at level N, using 'wname'.
	//		  	'haar'   : Haar wavelet.
	//		    'db'     : Daubechies wavelets.
	//		    'sym'    : Symlets.
	//		    'coif'   : Coiflets. 	 		
	private static double[][] getOutputEnergy() {
		//assume the vector x is in Matlab's workspace
		String command = "[C,L] = wavedec(x," + m_ndecompositionLevel + ",'" + m_wavelet + "');";
		//MatlabInterfacer.sendCommandAndPrint(command);
		MatlabInterfacer.sendCommand(command);
		
		double[] coefficients = MatlabInterfacer.getVector("C");
		double[] lengths = MatlabInterfacer.getVector("L");
		double[][] matrix = organizeAsMatrix(coefficients, lengths);
		
		//check if all decompositions can contribute with the same number of frames
		int maximumPossibleNumberOfFrames = Integer.MAX_VALUE;
		int decimationFactor = 1;
		//includes approximation and all details:
		int numberOfSignals = lengths.length - 1;
		for (int i = 1; i < numberOfSignals; i++) {
			int nframes = (int) (lengths[i] / (m_samplesPerWindow * decimationFactor));			
			//System.out.println(nframes + " " + lengths[i] + " " + decimationFactor + " " + m_samplesPerWindow);			
			if (nframes < maximumPossibleNumberOfFrames) {
				maximumPossibleNumberOfFrames = nframes;
			}
			decimationFactor *= 2;
		}
		
		double[][] output = new double[numberOfSignals][maximumPossibleNumberOfFrames];
		decimationFactor = 1;
		for (int i = 0; i < output.length; i++) {
			int windowLength = m_samplesPerWindow * decimationFactor;
			//System.out.println(lengths[i] + " " + decimationFactor + " " + m_samplesPerWindow + " " + output[i].length);			
			output[i] = calculateWindowedEnergy(matrix[i], windowLength, maximumPossibleNumberOfFrames);
			if (i != 0) {
				//don't update after processing the approximation, only for the details
				decimationFactor *= 2;
			}
		}
		return output;
	}
	
	private static double[] calculateWindowedEnergy(double[] x, int windowLength,
			int numberOfWindows) {
		double[] output = new double[numberOfWindows];
		int ncurrentSample = 0;
		for (int i = 0; i < numberOfWindows; i++) {
			double energy = 0;
			for (int j = 0; j < windowLength; j++) {
				double temp = x[ncurrentSample + j];
				energy += temp * temp;
			}
			output[i] = energy / windowLength;
			ncurrentSample += windowLength;			
		}
		return output;
	}

	//The structure is organized as:
	//	    C      = [app. coef.(N)|det. coef.(N)|... |det. coef.(1)]
	//	    L(1)   = length of app. coef.(N)
	//	    L(i)   = length of det. coef.(N-i+2) for i = 2,...,N+1
	//	    L(N+2) = length(X).
	private static double[][] organizeAsMatrix(double[] coefficients, double[] lengths) {
		int N = lengths.length - 1;
		double[][] output = new double[N][];
		int ncurrent = 0;
		for (int i = 0; i < N; i++) {
			output[i] = new double[(int) lengths[i]];
			for (int j = 0; j < output[i].length; j++) {
				output[i][j] = coefficients[ncurrent + j]; 
			}
			ncurrent += output[i].length;
		}
		return output;
	}

}
