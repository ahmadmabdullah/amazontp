package amazontp.matlab;

import amazontp.io.*;


/**
 * Decimate a signal by simply throwing out samples. It does not take care of
 * aliasing. Should use an algorithm similar to resample in Matlab, which uses a
 * FIR filter to avoid aliasing.
 * 
 * @author Aldebaro
 * 
 */
public class Resample {

	// java -cp c:\cvs\laps\amazontp\bin;c:\cvs\laps\\ufpajava\\bin
	// amazontp.util.Resample . bin 5khz 8
	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("Usage: <input directory> <input files extension> " +
							"<output files extension> <decimation factor>");
			System.exit(1);
		}
		String dir = args[0];
		String inputExtension = args[1];
		String outputExtension = args[2];
		int ndecimationFactor = Integer.parseInt(args[3]);

		DirectoryTree directoryTree = new DirectoryTree(dir, inputExtension);

		String[] fileNames = directoryTree.getFilesAsStrings();
		if (fileNames == null) {
			System.err.println("Couldn't find files with extension "
					+ inputExtension);
			System.exit(1);
		}

		for (int i = 0; i < fileNames.length; i++) {
			double[][] x = IO.readMatrixFromBinFile(fileNames[i]);
			// IO.displayPartOfMatrix(x, 6);
			// using Matlab
			// MatlabInterfacer.sendArray(x,"x");
			// MatlabInterfacer.sendCommand("[r,c]=size(x); z=x(:,1:" +
			// ndecimationFactor + ":c);");
			int newNumberOfColumns = x[0].length / ndecimationFactor;
			double[][] y = new double[x.length][newNumberOfColumns];
			for (int j = 0; j < y.length; j++) {
				for (int k = 0; k < y[0].length; k++) {
					y[j][k] = x[j][k * ndecimationFactor];
				}
			}
			String outputFileName = FileNamesAndDirectories
					.substituteExtension(fileNames[i], outputExtension);
			IO.writeMatrixtoBinFile(outputFileName, y);
			System.out.println(fileNames[i] + " => " + outputFileName);
			// IO.DisplayMatrix(y);
			// IO.pause();
		}

		System.out.println(fileNames.length + " files processed.");

		System.exit(1);
	}

}
