package amazontp.matlab;



//import JMatLink;

//the message WARNING: Connection with Matlab engine already open
//comes from the C code I think

//There was a reasonable solution, such that we don't need to change
//anything if we want to run jbuilder4. All calls to Matlab are now through
//the class MatlabInterfacer. I created an inner class, inside
//MatlabInterfacer, with the name JMatLink, with empty methods and took out
//the command
//import JMatLink;
//that JBuilder 8 doesn't like. So, you can even keep JMatLink in your jpx
//project. If we want to use Matlab, we should uncomment the import above in
//edu.ucsd.asr.MatlabInterfacer and rename its inner class JMatLink to
//something else, otherwise the code compiles but the inner class is called
//instead of the actual JMatLink.
public class MatlabInterfacer {

	private static JMatLink m_jMatLink;

	static {
		m_jMatLink = new JMatLink();
		m_jMatLink.engOpen();
	}

	public static void close() {
		m_jMatLink.engClose();
	}

	public static void destroy() {
		m_jMatLink.destroy();
	}

	public static void sendCommandAndPrint(String command) {
		m_jMatLink.engEvalString(command);
		System.out.println(" " + command);
	}

	public static void sendCommand(String command) {
		m_jMatLink.engEvalString(command);
	}

	public static void sendArray(double[] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, array);
	}

	public static void sendArray(double[][] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, array);
	}

	public static void sendArray(float[] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
	}

	public static void sendArray(int[] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
	}

	public static void sendArray(float[][] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
	}

	public static void sendArray(int[][] array, String arrayName) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
	}

	public static void plotArray(int[] array, String arrayName, int nfigureIndex) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
		sendCommandAndPrint("figure(" + nfigureIndex + ");plot(" + arrayName
				+ ");");
	}

	public static void plotArray(float[] array, String arrayName,
			int nfigureIndex) {
		m_jMatLink.engPutArray(arrayName, Cloner.cloneAsDouble(array));
		sendCommandAndPrint("figure(" + nfigureIndex + ");plot(" + arrayName
				+ ");");
	}

	public static void plotArray(double[] array, String arrayName,
			int nfigureIndex) {
		m_jMatLink.engPutArray(arrayName, array);
		sendCommandAndPrint("figure(" + nfigureIndex + ");plot(" + arrayName
				+ ");");
	}

	public static double[][] getMatrix(String matrixName) {
		return m_jMatLink.engGetArray(matrixName);
	}

	public static double[] getVector(String vectorName) {
		// engGetVector is not working according to
		// http://groups.yahoo.com/group/jmatlinkmail/message/90
		// return m_jMatLink.engGetVector(vectorName);
		double[][] x = m_jMatLink.engGetArray(vectorName);
		if (x == null) {
			return null;
		}
		if (x[0].length >= x.length) {
			// row vector or scalar
			return x[0];
		} else {
			// column vector
			double[] y = new double[x.length];
			for (int i = 0; i < y.length; i++) {
				y[i] = x[i][0];
			}
			return y;
		}
	}

	public static String getOutputBuffer() {
		return m_jMatLink.engOutputBuffer();
	}

	public static double getScalar(String scalarName) {
		// engGetScalar is probably not working according to
		// http://groups.yahoo.com/group/jmatlinkmail/message/90
		// double x = m_jMatLink.engGetScalar(scalarName);
		double[][] x = m_jMatLink.engGetArray(scalarName);
		if (x == null) {
			System.err.println("Could not find " + scalarName
					+ " in Matlab workspace! I'm returning NaN");
			return Double.NaN;
		}
		return x[0][0];
	}

	public static void main(String[] args) {
		System.out.println("Start Matlab:");
		MatlabInterfacer.sendCommand("plot(1:10)");
		MatlabInterfacer.sendCommand("x=1:100;m=sum(x);");
		double y = MatlabInterfacer.getScalar("m");
		System.out.println(y + "");
		System.out.println("Finish Matlab:");
		System.exit(1);
	}

}
