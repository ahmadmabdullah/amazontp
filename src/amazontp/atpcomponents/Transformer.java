package amazontp.atpcomponents;

import java.util.ArrayList;

public  class Transformer  implements Component
{
	
	//Type of Transformer: Undefined or Already-Defined
	private String type;
	//(current, flux) pair used to define linear inductance
	private ArrayList<Double> flux, current; //flux	
	
	//A name for the internal starpoint for the winding 
	private String bustop;
	
	//Rmag: Constant and linear resistence(in ohms)	
	private double Rmag;
	
	//Flag: Output specification flag for the magnetizing-reactance branch.
	// = 1: branch current output
	// = 2: branch voltage output
	// = 3: both branch current and branch  voltage output 
	private int flag;
	
	//k: Winding reference number (1, 2,...,N)
	private ArrayList<Winding> kwinding;	
	
	public Transformer()
	{
		flux = new ArrayList<Double>();
		current = new ArrayList<Double>();
		kwinding = new ArrayList<Winding>();
		setBustop(" ");
		setFlag(0);
		setRmag(0);
		
	}
	
	
	public String getBustop()
	{
		return bustop;
	}

	public void setBustop(String bustop)
	{
		this.bustop = bustop;
	}

	public ArrayList<Double> getCurrent()
	{
		return current;
	}	

	public int getFlag()
	{
		return flag;
	}

	public void setFlag(int flag)
	{
		this.flag = flag;
	}

	public ArrayList<Double> getFlux()
	{
		return flux;
	}

	public double getRmag()
	{
		return Rmag;
	}

	public void setRmag(double rmag)
	{
		Rmag = rmag;
	}

	public ArrayList<Winding> getKwinding() {
		return kwinding;
	}
	
	public void setKwinding(Winding w)
	{
	  kwinding.add(w);	
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public void setCurrent(ArrayList<Double> current) {
		this.current = current;
	}


	public void setFlux(ArrayList<Double> flux) {
		this.flux = flux;
	}
	
	public String toString() {
		String x;
		
			
	            x= " TYPE:" + this.getType() 
	            + "\n busStop: " + this.getBustop()
				+ "\n rmag: " + this.getRmag()
				+ "\n Flag " + this.getFlag()
				+ "\n flux: " + this.getFlux().toString()
				+ "\n Current: " + this.getCurrent().toString()
				+ "\n Kwinding: " + this.getKwinding().toString()

				
				+ "\n END OF THE DESCRIPTION OF THE TRANSFORMER" ;
	            
	            return x;
	}
		
	
	

}
