package amazontp.atpcomponents;

import amazontp.gui.LineZTPropertiesPanel;
import amazontp.util.FormatValues;

public class LineZT {
	
	private String fase;
	
	private String faseA;
	
	private String faseB;
	
	private String faseC;

	private String genNodeA;

	private String endNodeA;

	private String genNodeB;

	private String endNodeB;

	private String genNodeC;

	private String endNodeC;

	private double length;

	private double zeroSeqResistance;

	private double zeroSeqInductance;

	private double zeroSeqCapacitance;

	private double posSeqResistance;

	private double posSeqInductance;

	private double posSeqCapacitance;
	
    private String output;
	
	private String iline;

	// defalut constructor
	public LineZT() {
		this.length = 0;

		this.zeroSeqResistance = 0;
		this.zeroSeqInductance = 0;
		this.zeroSeqCapacitance = 0;
		this.posSeqResistance = 0;
		this.posSeqInductance = 0;
		this.posSeqCapacitance = 0;
		this.genNodeA = null;
		this.endNodeA = null;
		this.genNodeB = null;
		this.endNodeB = null;
		this.genNodeC = null;
		this.endNodeC = null;
	}

	public LineZT(String genNodeA, String endNodeA, String genNodeB,
			String endNodeB, String genNodeC, String endNodeC, double length,
			double zeroSeqResistance, double zeroSeqInductance,
			double zeroSeqCapacitance, double posSeqResistance,
			double posSeqInductance, double posSeqCapacitance) {

		this.length = length;

		this.zeroSeqResistance = zeroSeqResistance;
		this.zeroSeqInductance = zeroSeqInductance;
		this.zeroSeqCapacitance = zeroSeqCapacitance;
		this.posSeqResistance = posSeqResistance;
		this.posSeqInductance = posSeqInductance;
		this.posSeqCapacitance = posSeqCapacitance;
		this.genNodeA = genNodeA;
		this.endNodeA = endNodeA;
		this.genNodeB = genNodeB;
		this.endNodeB = endNodeB;
		this.genNodeC = genNodeC;
		this.endNodeC = endNodeC;
	}

	public String getEndNodeA() {
		return endNodeA;
	}

	public String getEndNodeB() {
		return endNodeB;
	}

	public String getEndNodeC() {
		return endNodeC;
	}

	public String getGenNodeA() {
		return genNodeA;
	}

	public String getGenNodeB() {
		return genNodeB;
	}

	public String getGenNodeC() {
		return genNodeC;
	}

	public double getLength() {
		return length;
	}

	public double getPosSeqCapacitance() {
		return posSeqCapacitance;
	}

	public double getPosSeqInductance() {
		return posSeqInductance;
	}

	public double getPosSeqResistance() {
		return posSeqResistance;
	}

	public double getZeroSeqCapacitance() {
		return zeroSeqCapacitance;
	}

	public double getZeroSeqInductance() {
		return zeroSeqInductance;
	}

	public double getZeroSeqResistance() {
		return zeroSeqResistance;
	}

	public void setEndNodeA(String endNodeA) {
		this.endNodeA = endNodeA;
	}

	public void setEndNodeB(String endNodeB) {
		this.endNodeB = endNodeB;
	}

	public void setEndNodeC(String endNodeC) {
		this.endNodeC = endNodeC;
	}

	public void setGenNodeA(String genNodeA) {
		this.genNodeA = genNodeA;
	}

	public void setGenNodeB(String genNodeB) {
		this.genNodeB = genNodeB;
	}

	public void setGenNodeC(String genNodeC) {
		this.genNodeC = genNodeC;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public void setPosSecCapacitance(double posSecCapacitance) {
		this.posSeqCapacitance = posSecCapacitance;
	}

	public void setPosSecInductance(double posSecInductance) {
		this.posSeqInductance = posSecInductance;
	}

	public void setPosSecResistance(double posSecResistance) {
		this.posSeqResistance = posSecResistance;
	}

	public void setZeroSecCapacitance(double zeroSecCapacitance) {
		this.zeroSeqCapacitance = zeroSecCapacitance;
	}

	public void setZeroSecInductance(double zeroSecInductance) {
		this.zeroSeqInductance = zeroSecInductance;
	}

	public void setZeroSecResistance(double zeroSecResistance) {
		this.zeroSeqResistance = zeroSecResistance;
	}
	
	public String getIline() {
		return iline;
	}

	public void setIline(String iline) {
		if(iline.equals("0"))
		this.iline = "L',C'";
		else if(iline.equals("1"))
		this.iline="Z,v";
		else if(iline.equals("2"))
		this.iline="Z,tau";	
	}

	public String getOutput() {
		return output;
	}

public void setOutput(String output) {
		
		if(output.equals("0"))
		this.output = "no";
		else if (output.equals("1"))
			this.output="Current";
		else if (output.equals("2"))
			this.output="voltage";
		else if (output.equals("3"))
			this.output="Curr&Volt";
		else if (output.equals("4"))
			this.output="Pow&Energy";
	}

	// we have to format the double values to have 6 characters
	// each before we put then into the branch
	public String getBranchCard() {
		String s = "";
		s += "-1";
		s += this.getGenNodeA();
		s += this.getEndNodeA();
		s += "            ";
		s += FormatValues.formatRLC(this.getZeroSeqResistance());
		s += FormatValues.formatRLC(this.getZeroSeqInductance());
		s += FormatValues.formatRLC(this.getZeroSeqCapacitance());
		s += FormatValues.formatRLC(this.getLength());
		s += " 0 0                         0\r\n";
		s += "-2";
		s += this.getGenNodeB();
		s += this.getEndNodeB();
		s += "            ";
		s += FormatValues.formatRLC(this.getPosSeqResistance());
		s += FormatValues.formatRLC(this.getPosSeqInductance());
		s += FormatValues.formatRLC(this.getPosSeqCapacitance());
		s += FormatValues.formatRLC(this.getLength());
		s += " 0 0                         0\r\n";
		s += "-3";
		s += this.getGenNodeC();
		s += this.getEndNodeC();
		s += "                                  "
				+ "                               0\r\n";

		return s;
	}

	public String toString() {
		String s = "";
		s = " TYPE:" + "Line Z-T" + "\r\n";
		s += "Lenght: " + length + "\n";
		s += "zeroSecResistance: " + zeroSeqResistance + "\r\n";
		s += "zeroSecInductance: " + zeroSeqInductance + "\r\n";
		s += "zeroSecCapacitance: " + zeroSeqCapacitance + "\r\n";
		s += "posSecResistance: " + posSeqResistance + "\r\n";
		s += "posSecInductance: " + posSeqInductance + "\r\n";
		s += "posSecCapacitance: " + posSeqCapacitance + "\r\n";
		s += "genNodeA: " + genNodeA + "\r\n";
		s += "endNodeA: " + endNodeA + "\r\n";
		s += "genNodeB: " + genNodeB + "\r\n";
		s += "endNodeB: " + endNodeB + "\r\n";
		s += "genNodeC: " + genNodeC + "\r\n";
		s += "endNodeC: " + endNodeC + "\r\n";

		return s;
	}

	public LineZTPropertiesPanel getLineZTPropertiesPanel() {
		LineZTPropertiesPanel lztp = new LineZTPropertiesPanel();
		lztp.getLengthTextField().setText(String.valueOf(this.getLength()));
		lztp.getGenNodeATextField().setText(this.genNodeA);
		lztp.getGenNodeBTextField().setText(this.genNodeB);
		lztp.getGenNodeCTextField().setText(this.genNodeC);
		lztp.getEndNodeATextField().setText(this.endNodeA);
		lztp.getEndNodeBTextField().setText(this.endNodeB);
		lztp.getEndNodeCTextField().setText(this.endNodeC);
		lztp.getZeroSeqResTextField().setText(
				String.valueOf(this.getZeroSeqResistance()));
		lztp.getZeroSeqIndTextField().setText(
				String.valueOf(this.getZeroSeqInductance()));
		lztp.getZeroSeqCapTextField().setText(
				String.valueOf(this.getZeroSeqCapacitance()));
		lztp.getPosSeqResTextField().setText(
				String.valueOf(this.getPosSeqResistance()));
		lztp.getPosSeqIndTextField().setText(
				String.valueOf(this.getPosSeqInductance()));
		lztp.getPosSeqCapTextField().setText(
				String.valueOf(this.getPosSeqCapacitance()));

		return lztp;
	}

	/**
	 * @return Retorna o faseA.
	 */
	public String getFaseA() {
		return faseA;
	}

	/**
	 * @param faseA O faseA a ser definido.
	 */
	public void setFaseA(String faseA) {
		this.faseA = faseA;
	}

	/**
	 * @return Retorna o faseB.
	 */
	public String getFaseB() {
		return faseB;
	}

	/**
	 * @param faseB O faseB a ser definido.
	 */
	public void setFaseB(String faseB) {
		this.faseB = faseB;
	}

	/**
	 * @return Retorna o faseC.
	 */
	public String getFaseC() {
		return faseC;
	}

	/**
	 * @param faseC O faseC a ser definido.
	 */
	public void setFaseC(String faseC) {
		this.faseC = faseC;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	
}
