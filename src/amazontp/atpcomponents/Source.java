/*
 * Created on 26/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package amazontp.atpcomponents;

// 345678901234567890123456789012345678901234567890123456789012345678901234567890
// C < n 1><>< Ampl. >< Freq. ><Phase/T0>< A1 >< T1 >< TSTART >< TSTOP >

/**
 * @author  
 */
public class Source {
	/**
	 * @uml.property  name="type"
	 */
	private String type = null;

	/**
	 * @uml.property  name="node1"
	 */
	private String node1 = null;

	/**
	 * @uml.property  name="initial"
	 */
	private String initial = null;

	/**
	 * @uml.property  name="amplitude"
	 */
	private String amplitude;

	/**
	 * @uml.property  name="frequency"
	 */
	private String frequency;

	/**
	 * @uml.property  name="phaseT0"
	 */
	private String phaseT0;

	/**
	 * @uml.property  name="a1"
	 */
	private String a1;

	/**
	 * @uml.property  name="t1"
	 */
	private String t1;

	/**
	 * @uml.property  name="tStart"
	 */
	private String tStart;

	/**
	 * @uml.property  name="tStop"
	 */
	private String tStop;

	public Source() {
		a1 = "";
		amplitude = "";
		frequency = "";
		initial = "";
		node1 = "";
		phaseT0 = "";
		t1 = "";
		tStart = "";
		tStop = "";
		type = "";
	}

	/**
	 * @return  Returns the a1.
	 * @uml.property  name="a1"
	 */
	public String getA1() {
		return a1;
	}

	/**
	 * @return  Returns the amplitude.
	 * @uml.property  name="amplitude"
	 */
	public String getAmplitude() {
		return amplitude;
	}

	/**
	 * @return  Returns the frequency.
	 * @uml.property  name="frequency"
	 */
	public String getFrequency() {
		return frequency;
	}

	/**
	 * @return  Returns the initial.
	 * @uml.property  name="initial"
	 */
	public String getInitial() {
		return initial;
	}

	/**
	 * @return  Returns the node1.
	 * @uml.property  name="node1"
	 */
	public String getNode1() {
		return node1;
	}

	/**
	 * @return  Returns the phaseT0.
	 * @uml.property  name="phaseT0"
	 */
	public String getPhaseT0() {
		return phaseT0;
	}

	/**
	 * @return  Returns the t1.
	 * @uml.property  name="t1"
	 */
	public String getT1() {
		return t1;
	}

	/**
	 * @return  Returns the tStart.
	 * @uml.property  name="tStart"
	 */
	public String getTStart() {
		return tStart;
	}

	/**
	 * @return  Returns the tStop.
	 * @uml.property  name="tStop"
	 */
	public String getTStop() {
		return tStop;
	}

	/**
	 * @return  Returns the type.
	 * @uml.property  name="type"
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param a1  The a1 to set.
	 * @uml.property  name="a1"
	 */
	public void setA1(String a1) {
		this.a1 = a1;
	}

	/**
	 * @param amplitude  The amplitude to set.
	 * @uml.property  name="amplitude"
	 */
	public void setAmplitude(String amplitude) {
		this.amplitude = amplitude;
	}

	/**
	 * @param frequency  The frequency to set.
	 * @uml.property  name="frequency"
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	/**
	 * @param initial  The initial to set.
	 * @uml.property  name="initial"
	 */
	public void setInitial(String initial) {
		this.initial = initial;
	}

	/**
	 * @param node1  The node1 to set.
	 * @uml.property  name="node1"
	 */
	public void setNode1(String node1) {
		this.node1 = node1;
	}

	/**
	 * @param phaseT0  The phaseT0 to set.
	 * @uml.property  name="phaseT0"
	 */
	public void setPhaseT0(String phaseT0) {
		this.phaseT0 = phaseT0;
	}

	/**
	 * @param t1  The t1 to set.
	 * @uml.property  name="t1"
	 */
	public void setT1(String t1) {
		this.t1 = t1;
	}

	/**
	 * @param tStart  The tStart to set.
	 * @uml.property  name="tStart"
	 */
	public void setTStart(String start) {
		tStart = start;
	}

	/**
	 * @param tStop  The tStop to set.
	 * @uml.property  name="tStop"
	 */
	public void setTStop(String stop) {
		tStop = stop;
	}

	/**
	 * @param type  The type to set.
	 * @uml.property  name="type"
	 */
	public void setType(String type) {
		this.type = type;
	}

	public String toString() {
		return " Type: " + getType() + "\n Node 1: " + getNode1()
				+ "\n Initial: " + getInitial() + "\n Amplitude: "
				+ getAmplitude() + "\n Frequency: " + getFrequency()
				+ "\n Phase/T0: " + getPhaseT0() + "\n A1: " + getA1()
				+ "\n T1: " + getT1() + "\n TSTART: " + getTStart()
				+ "\n TSTOP: " + getTStop();
	}

}
