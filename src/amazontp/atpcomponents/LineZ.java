package amazontp.atpcomponents;

/**
 * 
 * @author Claudomir Junior
 *
 */

public class LineZ implements Component{
	
	private String genNodeA;

	private String endNodeA;
	
	private String iline;
	
	private String resistance;
	
	private String z;
	
	private String v;
	
	private String length;
	
	private String output;

	public String getEndNodeA() {
		return endNodeA;
	}

	public void setEndNodeA(String endNodeA) {
		this.endNodeA = endNodeA;
	}

	public String getGenNodeA() {
		return genNodeA;
	}

	public void setGenNodeA(String genNodeA) {
		this.genNodeA = genNodeA;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		
		if(output.equals("0"))
		this.output = "no";
		else if (output.equals("1"))
			this.output="Current";
		else if (output.equals("2"))
			this.output="voltage";
		else if (output.equals("3"))
			this.output="Curr&Volt";
		else if (output.equals("4"))
			this.output="Pow&Energy";
	}

	public String getResistance() {
		return resistance;
	}

	public void setResistance(String resistance) {
		if(resistance.equals("    "))
		this.resistance = "0";
		else
		this.resistance=resistance;	
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getZ() {
		return z;
	}

	public void setZ(String z) {
		this.z = z;
	}
	
	public String getIline() {
		return iline;
	}

	public void setIline(String iline) {
		if(iline.equals("0"))
		this.iline = "L',C'";
		else if(iline.equals("1"))
		this.iline="Z,v";
		else if(iline.equals("2"))
		this.iline="Z,tau";	
	}
	
	public String toString() {
		String x; 
		
		if(this.getIline().equals("1")){
		
		x= " TYPE:" + "LineZ" 
		+ "\n Node 1: " + this.getGenNodeA()
		+ "\n Initial: " + this.genNodeA + "\n Length: "
		+ this.getLength() + "\n Output: " + this.getOutput()
		+ "\n Resistance: " + this.getResistance() 
		+ "\n V: " + this.getV()
		+ "\n Z: " + this.getZ() 
		+ "\n Iline: " + this.getIline()
		+ "\n END OF THE DESCRIPTION OF THE LineZ " ;			
	}
		else if(this.getIline().equals("0")){
			
			x= " TYPE:" + "LineZ" 
			+ "\n Node 1: " + this.getGenNodeA()
			+ "\n Initial: " + this.genNodeA + "\n Length: "
			+ this.getLength() + "\n Output: " + this.getOutput()
			+ "\n Resistance: " + this.getResistance() 
			+ "\n C: " + this.getV()
			+ "\n L: " + this.getZ() 
			+ "\n Iline: " + this.getIline()
			+ "\n END OF THE DESCRIPTION OF THE LineZ " ;
			
		}
		
		
    else{
			
			x= " TYPE:" + "LineZ" 
			+ "\n Node 1: " + this.getGenNodeA()
			+ "\n Initial: " + this.genNodeA + "\n Length: "
			+ this.getLength() + "\n Output: " + this.getOutput()
			+ "\n Resistance: " + this.getResistance() 
			+ "\n tau: " + this.getV()
			+ "\n Z: " + this.getZ() 
			+ "\n Iline: " + this.getIline()
			+ "\n END OF THE DESCRIPTION OF THE LineZ " ;
			
		}
		
		return x;
		
	}// end of method


	

}
