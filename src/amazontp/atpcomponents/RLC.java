package amazontp.atpcomponents;

/**
 * @author  Jefferson
 */
public class RLC {

	/**
	 * @uml.property  name="capacitance"
	 */
	private String capacitance;

	/**
	 * @uml.property  name="inductance"
	 */
	private String inductance;

	/**
	 * @uml.property  name="iType"
	 */
	private String iType;

	/**
	 * @uml.property  name="length"
	 */
	private String length;

	/**
	 * @uml.property  name="node1"
	 */
	private String node1;

	/**
	 * @uml.property  name="node2"
	 */
	private String node2;

	/**
	 * @uml.property  name="output"
	 */
	private String output;

	/**
	 * @uml.property  name="resistance"
	 */
	private String resistance;

	// C < n 1>< n 2><ref1><ref2>< R >< L >< C >
	// C < n 1>< n 2><ref1><ref2>< R >< A >< B ><Leng><><>0

	public RLC() {
		capacitance = "";
		inductance = "";
		iType = "";
		length = "";
		node1 = "";
		node2 = "";
		output = "";
		resistance = "";
	}

	/**
	 * @return  Returns the capacitance.
	 * @uml.property  name="capacitance"
	 */
	public String getCapacitance() {
		return capacitance;
	}

	/**
	 * @return  Returns the inductance.
	 * @uml.property  name="inductance"
	 */
	public String getInductance() {
		return inductance;
	}

	/**
	 * @return  Returns the iType.
	 * @uml.property  name="iType"
	 */
	public String getIType() {
		return iType;
	}

	/**
	 * @return  Returns the length.
	 * @uml.property  name="length"
	 */
	public String getLength() {
		return length;
	}

	/**
	 * @return  Returns the node1.
	 * @uml.property  name="node1"
	 */
	public String getNode1() {
		return node1;
	}

	/**
	 * @return  Returns the node2.
	 * @uml.property  name="node2"
	 */
	public String getNode2() {
		return node2;
	}

	/**
	 * @return  Returns the output.
	 * @uml.property  name="output"
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * @return  Returns the resistance.
	 * @uml.property  name="resistance"
	 */
	public String getResistance() {
		return resistance;
	}

	/**
	 * @param capacitance  The capacitance to set.
	 * @uml.property  name="capacitance"
	 */
	public void setCapacitance(String capacitance) {
		this.capacitance = capacitance;
	}

	/**
	 * @param inductance  The inductance to set.
	 * @uml.property  name="inductance"
	 */
	public void setInductance(String inductance) {
		this.inductance = inductance;
	}

	/**
	 * @param iType  The iType to set.
	 * @uml.property  name="iType"
	 */
	public void setIType(String type) {
		iType = type;
	}

	/**
	 * @param length  The length to set.
	 * @uml.property  name="length"
	 */
	public void setLength(String length) {
		this.length = length;
	}

	/**
	 * @param node1  The node1 to set.
	 * @uml.property  name="node1"
	 */
	public void setNode1(String node1) {
		this.node1 = node1;
	}

	/**
	 * @param node2  The node2 to set.
	 * @uml.property  name="node2"
	 */
	public void setNode2(String node2) {
		this.node2 = node2;
	}

	/**
	 * @param output  The output to set.
	 * @uml.property  name="output"
	 */
	public void setOutput(String output) {
		this.output = output;
	}

	/**
	 * @param resistance  The resistance to set.
	 * @uml.property  name="resistance"
	 */
	public void setResistance(String resistance) {
		this.resistance = resistance;
	}

	public String toString() {
		return " iType: " + getIType() + "\n Node 1: " + getNode1()
				+ "\n Node 2: " + getNode2() + "\n Resistance: "
				+ getResistance() + "\n Inductance: " + getInductance()
				+ "\n Capacitance: " + getCapacitance() + "\n Length: "
				+ getLength() + "\n Output: " + getOutput();
	}
}
