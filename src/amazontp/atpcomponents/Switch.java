/*
 * Created on 26/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package amazontp.atpcomponents;

/**
 * @author  Jefferson
 */
public class Switch {

	// C < n 1>< n 2>< Tclose ><Top/Tde >< Ie ><Vf/CLOP >< type >

	/**
	 * @uml.property  name="iType"
	 */
	private String iType;

	/**
	 * @uml.property  name="node1"
	 */
	private String node1;

	/**
	 * @uml.property  name="node2"
	 */
	private String node2;

	/**
	 * @uml.property  name="tClose"
	 */
	private String tClose;

	/**
	 * @uml.property  name="topTde"
	 */
	private String topTde;

	/**
	 * @uml.property  name="ie"
	 */
	private String ie;

	/**
	 * @uml.property  name="vfCLOP"
	 */
	private String vfCLOP;

	/**
	 * @uml.property  name="type"
	 */
	private String type;

	/**
	 * @uml.property  name="output"
	 */
	private String output;

	public Switch() {
		ie = "";
		iType = "";
		node1 = "";
		node2 = "";
		output = "";
		tClose = "";
		topTde = "";
		type = "";
		vfCLOP = "";
	}



	/**
	 * @return  Returns the ie.
	 * @uml.property  name="ie"
	 */
	public String getIe() {
		return ie;
	}



	/**
	 * @return  Returns the iType.
	 * @uml.property  name="iType"
	 */
	public String getIType() {
		return iType;
	}



	/**
	 * @return  Returns the node1.
	 * @uml.property  name="node1"
	 */
	public String getNode1() {
		return node1;
	}



	/**
	 * @return  Returns the node2.
	 * @uml.property  name="node2"
	 */
	public String getNode2() {
		return node2;
	}



	/**
	 * @return  Returns the output.
	 * @uml.property  name="output"
	 */
	public String getOutput() {
		return output;
	}



	/**
	 * @return  Returns the tClose.
	 * @uml.property  name="tClose"
	 */
	public String getTClose() {
		return tClose;
	}



	/**
	 * @return  Returns the topTde.
	 * @uml.property  name="topTde"
	 */
	public String getTopTde() {
		return topTde;
	}



	/**
	 * @return  Returns the type.
	 * @uml.property  name="type"
	 */
	public String getType() {
		return type;
	}



	/**
	 * @return  Returns the vfCLOP.
	 * @uml.property  name="vfCLOP"
	 */
	public String getVfCLOP() {
		return vfCLOP;
	}



	/**
	 * @param ie  The ie to set.
	 * @uml.property  name="ie"
	 */
	public void setIe(String ie) {
		this.ie = ie;
	}



	/**
	 * @param iType  The iType to set.
	 * @uml.property  name="iType"
	 */
	public void setIType(String type) {
		iType = type;
	}



	/**
	 * @param node1  The node1 to set.
	 * @uml.property  name="node1"
	 */
	public void setNode1(String node1) {
		this.node1 = node1;
	}



	/**
	 * @param node2  The node2 to set.
	 * @uml.property  name="node2"
	 */
	public void setNode2(String node2) {
		this.node2 = node2;
	}



	/**
	 * @param output  The output to set.
	 * @uml.property  name="output"
	 */
	public void setOutput(String output) {
		this.output = output;
	}



	/**
	 * @param tClose  The tClose to set.
	 * @uml.property  name="tClose"
	 */
	public void setTClose(String close) {
		tClose = close;
	}



	/**
	 * @param topTde  The topTde to set.
	 * @uml.property  name="topTde"
	 */
	public void setTopTde(String topTde) {
		this.topTde = topTde;
	}



	/**
	 * @param type  The type to set.
	 * @uml.property  name="type"
	 */
	public void setType(String type) {
		this.type = type;
	}



	/**
	 * @param vfCLOP  The vfCLOP to set.
	 * @uml.property  name="vfCLOP"
	 */
	public void setVfCLOP(String vfCLOP) {
		this.vfCLOP = vfCLOP;
	}



	public String toString() {
		return " iType: " + getIType() + "\n Node 1: " + getNode1() + "\n Node 2: "
				+ getNode2() + "\n Tclose: " + getTClose() + "\n Top/Tde: "
				+ getTopTde() + "\n Ie: " + getIe() + "\n Vf / Clop: "
				+ getVfCLOP() + "\n Type: " + getType() + "\n Output: "
				+ getOutput();
	}
}
