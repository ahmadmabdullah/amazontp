package amazontp.atpcomponents;

public class Winding 
{
    //	R, L: Resistence and inductance associatd with the winding k in question.
	private double R;
    //	R, L: Resistence and inductance associatd with the winding k in question.
	private double L;
	
	// VRat: Rated voltage of winding
	private double vrat;
	
//	BUS1, BUS2: Terminal nodes of the transformer winding k
	private String Bus1;

//	BUS1, BUS2: Terminal nodes of the transformer winding k
	private String Bus2;

	public String getBus1() {
		return Bus1;
	}

	public void setBus1(String bus1) {
		Bus1 = bus1;
	}

	public String getBus2() {
		return Bus2;
	}

	public void setBus2(String bus2) {
		Bus2 = bus2;
	}

	public double getL()
	{
		return L;
	}

	public void setL(double l)
	{
		L = l;
	}

	public double getR()
	{
		return R;
	}

	public void setR(double r)
	{
		R = r;
	}

	public double getVrat()
	{
		return vrat;
	}

	public void setVrat(double vrat)
	{
		this.vrat = vrat;
	}
	

}
