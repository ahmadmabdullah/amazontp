package amazontp.atpcomponents;

/**
 * 
 * @author Claudomir Junior
 *
 */

public interface Component {
	
	public String toString();

}
