package amazontp.atpcomponents;

/**
 * 
 * @author Claudomir Junior
 *
 */

public class LineZ_T implements Component{
	
	private String genNodeA;

	private String endNodeA;

	private String genNodeB;

	private String endNodeB;
	
	private String genResisstance;
	
	private String endResistance;
	
	private String genZ;
	
	private String endZ;
	
	private String genV;
	
	private String endV;
	
	private String length;
	
	private String output;
	
	private String iline;

	public String getEndNodeA() {
		return endNodeA;
	}

	public void setEndNodeA(String endNodeA) {
		this.endNodeA = endNodeA;
	}

	public String getEndNodeB() {
		return endNodeB;
	}

	public void setEndNodeB(String endNodeB) {
		this.endNodeB = endNodeB;
	}

	public String getEndResistance() {
		return endResistance;
	}

	public void setEndResistance(String endResistance) {
		if(endResistance.equals(" "))
		this.endResistance = "0";
		else
			this.endResistance = endResistance;
	}

	public String getEndV() {
		return endV;
	}

	public void setEndV(String endV) {
		if(endV.equals(""))
		this.endV = "0";
		else
			this.endV = endV;
	}

	public String getEndZ() {
		return endZ;
	}

	public void setEndZ(String endZ) {
		this.endZ = endZ;
	}

	public String getGenNodeA() {
		return genNodeA;
	}

	public void setGenNodeA(String genNodeA) {
		this.genNodeA = genNodeA;
	}

	public String getGenNodeB() {
		return genNodeB;
	}

	public void setGenNodeB(String genNodeB) {
		this.genNodeB = genNodeB;
	}

	public String getGenResisstance() {
		return genResisstance;
	}

	public void setGenResisstance(String genResisstance) {
		if(genResisstance.equals("    "))
		this.genResisstance = "0";
		else
		this.genResisstance = genResisstance;
	}

	public String getGenV() {
		
		return genV;
	}

	public void setGenV(String genV) {
		if(genV.equals("    "))
		this.genV = "0";
		else 
		this.genV = genV;
	}

	public String getGenZ() {
		return genZ;
	}

	public void setGenZ(String genZ) {
		this.genZ = genZ;
	}

	public String getIline() {
		return iline;
	}

	public void setIline(String iline) {
		if(iline.equals("0"))
		this.iline = "L',C'";
		else if(iline.equals("1"))
		this.iline="Z,v";
		else if(iline.equals("2"))
		this.iline="Z,tau";	
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getOutput() {
		return output;
	}

public void setOutput(String output) {
		
		if(output.equals("0"))
		this.output = "no";
		else if (output.equals("1"))
			this.output="Current";
		else if (output.equals("2"))
			this.output="voltage";
		else if (output.equals("3"))
			this.output="Curr&Volt";
		else if (output.equals("4"))
			this.output="Pow&Energy";
	}

public String toString() {
	String x;
	if(this.getIline().equals("1")){
		
            x= " TYPE:" + "Line Z-T" + "\n Start Node A: " + this.getGenNodeA()
			+ "\n Out Node A: " + this.getEndNodeA()
			+ "\n Start Node B: " + this.getGenNodeB()
			+ "\n Out Node B: " + this.getEndNodeB()
			+ "\n Start Node B: " + this.getGenNodeB()
		
			+ "\n Start Resistance: "+ this.getGenResisstance()
			+ "\n End Resistance: "+ this.getEndResistance()
			+ "\n  Z+: "+ this.getGenZ()
			+ "\n  Z0: "+ this.getEndZ()
			+ "\n  V+: "+ this.getGenV()
			+ "\n  V0: "+ this.getEndV()
			+ "\n  Length: "+ this.getLength()
		    + "\n  Iline: "+ this.getIline()
			+ "\n Output: " + this.getOutput()
			
			+ "\n END OF THE DESCRIPTION OF THE Line Z-T " ;
	}
	
	else if(this.getIline().equals("0")){
		
        x= " TYPE:" + "Line Z-T" + "\n Start Node A: " + this.getGenNodeA()
		+ "\n Out Node A: " + this.getEndNodeA()
		+ "\n Start Node B: " + this.getGenNodeB()
		+ "\n Out Node B: " + this.getEndNodeB()
		+ "\n Start Node B: " + this.getGenNodeB()
		
		+ "\n Start Resistance: "+ this.getGenResisstance()
		+ "\n End Resistance: "+ this.getEndResistance()
		+ "\n  L'+: "+ this.getGenZ()
		+ "\n  L'0: "+ this.getEndZ()
		+ "\n  C'+: "+ this.getGenV()
		+ "\n  C'0: "+ this.getEndV()
		+ "\n  Length: "+ this.getLength()
	    + "\n  Iline: "+ this.getIline()
		+ "\n Output: " + this.getOutput()
		
		+ "\n END OF THE DESCRIPTION OF THE Line Z-T " ;
	}
	else{
		x= " TYPE:" + "Line Z-T" + "\n Start Node A: " + this.getGenNodeA()
		+ "\n Out Node A: " + this.getEndNodeA()
		+ "\n Start Node B: " + this.getGenNodeB()
		+ "\n Out Node B: " + this.getEndNodeB()
		+ "\n Start Node B: " + this.getGenNodeB()
		
		+ "\n Start Resistance: "+ this.getGenResisstance()
		+ "\n End Resistance: "+ this.getEndResistance()
		+ "\n  L'+: "+ this.getGenZ()
		+ "\n  L'0: "+ this.getEndZ()
		+ "\n  tau +: "+ this.getGenV()
		+ "\n  tau 0: "+ this.getEndV()
		+ "\n  Length: "+ this.getLength()
	    + "\n  Iline: "+ this.getIline()
		+ "\n Output: " + this.getOutput()
		
		+ "\n END OF THE DESCRIPTION OF THE Line Z-T " ;
	}
	return x;
			
}




}
