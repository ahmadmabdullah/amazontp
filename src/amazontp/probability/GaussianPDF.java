package amazontp.probability;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class GaussianPDF {

	private Random r;

	private double stdDev;

	private double mean;

	private	double minValue;

	private	double maxValue;

	private	Object[] obj;

	private	double[] dArray;

	private	int[] iArray;
	
	public GaussianPDF(double minValue, double maxValue, double mean, double stdDev) {
		r = new Random(System.currentTimeMillis());
		this.mean = mean;
		this.stdDev = stdDev;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public GaussianPDF(Object[] obj, int mean, int stdDev) {
		r = new Random(System.currentTimeMillis());
		this.obj = obj;
		this.mean = mean;
		this.stdDev = stdDev;
		this.minValue = 0;
		this.maxValue = obj.length - 1;
	}
	
	public GaussianPDF(double[] dArray, int mean, int stdDev) {
		r = new Random(System.currentTimeMillis());
		this.dArray = dArray;
		this.mean = mean;
		this.stdDev = stdDev;
		this.minValue = 0;
		this.maxValue = dArray.length - 1;
	}

	public GaussianPDF(int[] iArray, int mean, int stdDev) {
		r = new Random(System.currentTimeMillis());
		this.iArray = iArray;
		this.mean = mean;
		this.stdDev = stdDev;
		this.minValue = 0;
		this.maxValue = iArray.length - 1;
	}
	
	// returns a double value between minValue and maxValue
	// note: this method can't be used for returning array's indexes
	public double getNextDouble() {

		double value;

		while(true) {
			value = r.nextGaussian() * stdDev + mean;
			
			// Is 'value' between limits? If yes, returns,
			// otherwise tries again.
			if (value >= minValue && value <= maxValue)
				break;
		}
		
		return value;
	}
	
	// returns an int value between minValue and maxValue
	public int getNextInt() {
		
		int value;

		while(true) {
			value = (int) Math.floor(r.nextGaussian() * stdDev + mean);
			
			// Is 'value' between limits? If yes, returns,
			// otherwise tries again.
			if (value >= (int) Math.floor(minValue) && value <= (int) Math.floor(maxValue))
				break;
		}
		
		return value;
	}

	// returns a double array with random values
	// between minValue and maxValue 
	private double[] getNextDoubleArray(int arrayLength) {
		
		double[] x = new double[arrayLength];
		
		for (int i = 0; i < x.length; i++) {
			x[i] = getNextDouble();
		}
		
		return x;
	}

	// returns an int array with random values
	// between minValue and maxValue 
	private int[] getNextIntArray(int arrayLength) {
		
		int[] x = new int[arrayLength];
		
		for (int i = 0; i < x.length; i++) {
			x[i] = getNextInt();
		}
		
		return x;
	}
		
	public Object getNextObjectElement() {
		int i = this.getNextInt();
		return obj[i];
	}
	
	public int getNextIntElement() {
		int i = this.getNextInt();
		return iArray[i];
	}
	
	public double getNextDoubleElement() {
		int i = this.getNextInt();
		return dArray[i];	
	}
		
	// just for example...
	public static void main(String[] args) throws IOException {

		int[] x = new int[1000];
		for (int i = 0; i < x.length; i++) {
			x[i] = i;
		}
		
		File f = new File("c://x.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));
				
		GaussianPDF gpdf = new GaussianPDF(x, 500, 100);
		int v;
		for (int i = 0; i < 100000; i++) {
			v = gpdf.getNextIntElement();			
			bw.write(v + "\n");
		}
		bw.close();
	}
}
