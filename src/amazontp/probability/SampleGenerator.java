package amazontp.probability;

import java.util.Random;

public abstract class SampleGenerator {

	protected Random m_random;

	// sets the seed of the random number generator object
	public void setSeedOfRandomNumberGenerator(long lseed) {
		m_random = new Random(lseed);
	}
	
	//no seed of the random number generator object
	public void setRandomNumberGenerator() {
		m_random = new Random();
	}
	// sets the seed using "currentTimeMillis"
	public void generateRandomSeedOfRandomNumberGenerator() {
		long lseed = System.currentTimeMillis();
		setSeedOfRandomNumberGenerator(lseed);
	}
	
	public void generateRandomRandomNumberGenerator() {
		setRandomNumberGenerator();
	}

	// checks if the input double vectors are valid
	protected void checkInputArguments(double[] dprobabilities, double[] dvalues) {

		if (dvalues == null || dprobabilities == null) {
			System.out.println("DiscretePDFNumberGenerator can't be "
					+ " constructed with null vectors.");
			System.exit(1);
		}

		if (dprobabilities.length != dvalues.length) {
			System.out.println("DiscretePDFNumberGenerator can't be "
					+ " constructed with vectors of different lengths.");
			System.exit(1);
		}
	}

	// checks if the input float vectors are valid
	protected void checkInputArguments(float[] fprobabilities, float[] fvalues) {
		checkInputArguments(fprobabilities, fvalues);
	}

	
	protected double[] convertFloatVectorToDouble(float[] x) {
		double[] y = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = x[i];
		}
		return y;
	}

	protected float[] convertDoubleVectorToFloat(double[] x) {
		float[] y = new float[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = (float) x[i];
		}
		return y;
	}
}
