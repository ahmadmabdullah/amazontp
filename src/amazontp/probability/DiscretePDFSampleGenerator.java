package amazontp.probability;


/**
 * Generates samples draw from a discrete pdf.
 * 
 * @see HMMToy
 * @author Aldebaro Klautau
 * @version 2.0 - August 25, 2000
 */
public class DiscretePDFSampleGenerator extends SampleGenerator {

	private double[] m_dprobabilities;

	private double[] m_dvalues;

	private double[] m_dupLimits;

	public DiscretePDFSampleGenerator(double[] dprobabilities, double[] dvalues, boolean noseed) {

		checkInputArguments(dprobabilities, dvalues);
		m_dprobabilities = dprobabilities;
		m_dvalues = dvalues;
		// sets m_random using "currentTimeMillis"
		if (noseed)
			generateRandomRandomNumberGenerator();
		else generateRandomSeedOfRandomNumberGenerator();
		calculateUpLimits();
	}

	public DiscretePDFSampleGenerator(double[] dprobabilities, boolean noseed) {
		// create vector with values 0, 1, ...
		double[] dvalues = new double[dprobabilities.length];
		for (int i = 0; i < dvalues.length; i++) {
			dvalues[i] = i;
		}
		checkInputArguments(dprobabilities, dvalues);
		m_dprobabilities = dprobabilities;
		m_dvalues = dvalues;
		// sets m_random using "currentTimeMillis"
		if (noseed)
			generateRandomRandomNumberGenerator();
		else generateRandomSeedOfRandomNumberGenerator();
		calculateUpLimits();
	}

	public DiscretePDFSampleGenerator(float[] fprobabilities, float[] fvalues, boolean noseed) {

		checkInputArguments(fprobabilities, fvalues);
		m_dprobabilities = convertFloatVectorToDouble(fprobabilities);
		m_dvalues = convertFloatVectorToDouble(fvalues);
		// sets m_random using "currentTimeMillis"
		if (noseed)
			generateRandomRandomNumberGenerator();
		else generateRandomSeedOfRandomNumberGenerator();
		calculateUpLimits();
	}

	public DiscretePDFSampleGenerator(float[] fprobabilities, boolean noseed) {
		// create vector with values 0, 1, ...
		float[] fvalues = new float[fprobabilities.length];
		for (int i = 0; i < fvalues.length; i++) {
			fvalues[i] = (float) i;
		}

		checkInputArguments(fprobabilities, fvalues);
		m_dprobabilities = convertFloatVectorToDouble(fprobabilities);
		m_dvalues = convertFloatVectorToDouble(fvalues);
		// sets m_random using "currentTimeMillis"
		if (noseed)
			generateRandomRandomNumberGenerator();
		else generateRandomSeedOfRandomNumberGenerator();
		
		calculateUpLimits();
	}

	private void calculateUpLimits() {
		m_dupLimits = new double[m_dprobabilities.length];
		double sum = 0.0;
		for (int i = 0; i < m_dupLimits.length; i++) {
			sum += m_dprobabilities[i];
			m_dupLimits[i] = sum;
		}
	}

	// gets the index of the random element
	public int getSampleIndex() {
		// from [0, 1] ?
		// loop considering the possibility of x = 1
		// i could assume it is part of last interval too...
		// i could do binary search too...
		int nchosen = -1;
		do {
			double x = m_random.nextDouble();
			for (int i = 0; i < m_dprobabilities.length; i++) {
				if (x < m_dupLimits[i]) {
					nchosen = i;
					break;
				}
			}
		} while (nchosen == -1);
		return nchosen;
	}

	// gets the random element in double type
	public double getDoubleSample() {
		return m_dvalues[getSampleIndex()];
	}

	// gets the random element in float type
	public float getFloatSample() {
		return (float) getDoubleSample();
	}

	// gets a sequence of nnumberOfSamples random indices
	public int[] getSequenceOfSampleIndices(int nnumberOfSamples) {
		if (nnumberOfSamples < 1) {
			return null;
		}
		int[] y = new int[nnumberOfSamples];
		for (int i = 0; i < nnumberOfSamples; i++) {
			y[i] = getSampleIndex();
		}
		return y;
	}

	// gets a sequence of nnumberOfSamples random elements as doubles
	public double[] getSequenceOfDoubleSamples(int nnumberOfSamples) {
		if (nnumberOfSamples < 1) {
			return null;
		}
		double[] y = new double[nnumberOfSamples];
		for (int i = 0; i < nnumberOfSamples; i++) {
			y[i] = getDoubleSample();
		}
		return y;
	}

	// gets a sequence of nnumberOfSamples random elements as floats
	public float[] getSequenceOfFloatSamples(int nnumberOfSamples) {
		double[] y = new double[nnumberOfSamples];
		y = getSequenceOfDoubleSamples(nnumberOfSamples);
		return super.convertDoubleVectorToFloat(y);
		
	}
}
