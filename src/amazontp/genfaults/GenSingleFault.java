package amazontp.genfaults;

import java.io.BufferedReader;
import java.io.FileReader;

import amazontp.io.PrepareMasterFile;
import amazontp.io.SaveFile;
import amazontp.util.CountUtils;
import amazontp.util.RunATP;

public class GenSingleFault {

	static String oS = System.getProperty("os.name");
	
	String batFileName;

	static String atpDir;
	
	Runtime r = Runtime.getRuntime();

	private void GenBatFile() throws Exception {

		SaveFile sb = new SaveFile();

		if (oS.indexOf("Windows") != -1) {
			atpDir = System.getenv("WATDIR");
			if (atpDir == null) {
				atpDir = "c:\\ATP";
				System.err
						.println("Couldn't find Enviroment Variable WATDIR, setting c:\\ATP as ATP Folder.");
			} else {
				System.out.println("TPBIG located in: " + atpDir);
			}
		} else {
			atpDir = System.getenv("GNUDIR");
			if (atpDir == null) {
				atpDir = "/home/atplinux";
				System.err
						.println("Couldn't find Enviroment Variable GNUDIR, setting /home/linux as ATP Folder.");
			} else {
				System.out.println("TPBIG located in: " + atpDir);
			}
		}

		if (oS.indexOf("Windows") != -1) 
			batFileName = "amazontp.bat";
		else batFileName = "amazontp.sh";

		sb.deleteFile(atpDir + batFileName);
		sb.createFile(atpDir + batFileName);
		sb.saveFile(atpDir + batFileName, "cd " + atpDir);

		if (oS.indexOf("Windows") != -1)
			sb.saveFile(atpDir + batFileName, "tpbig.exe disk %1 * -r");
		else {
			sb.saveFile(atpDir + batFileName, "tpbig DISK $1 $2 -R");

			Runtime r = Runtime.getRuntime();
			r.exec("chmod a+x " + atpDir + batFileName);
			Thread.sleep(200);
		}
	}

	private void genSingleFaults(String atpFile, int indexOfWantedLineZT,
			int typeOfFault, double tclose, double topen,
			double fractionFromBegin, double line_to_line_R,
			double line_to_ground_R) throws Exception {

		int numberOfATPFileLines = CountUtils.countNumberOfLinesInFile(atpFile);

		double TStep = 0;
		double TMax = 0;

		String numLineZTInString = "00";

		if (indexOfWantedLineZT < 10) {
			numLineZTInString = "0" + indexOfWantedLineZT;
		} else {
			numLineZTInString = "" + indexOfWantedLineZT;
		}

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				atpFile));
		String atpFileLine = bufferedReader.readLine();

		for (int i = 0; i < numberOfATPFileLines; i++) {

			if (atpFileLine.indexOf("BEGIN") != -1
					|| atpFileLine.substring(0, 1).equals("C") == true
					|| atpFileLine.substring(0, 1).equals("$") == true) {
				// Do nothing, BEGIN NEW DATA CASE, comment ou special cards
				// lines.
			} else {
				TStep = Double.parseDouble(atpFileLine.substring(0, 8));
				TMax = Double.parseDouble(atpFileLine.substring(8, 16));
				System.out.println("Time Step: " + TStep);
				System.out.println("Simulation Time (TMax): " + TMax);
				break;
			}

			atpFileLine = bufferedReader.readLine();

		}

		if (tclose > topen) {
			System.err
					.println("Cleaning time must be smaller than Initiation time.");
			System.exit(1);
		}
		if (tclose < 0 || topen < 0) {
			System.err.println("Time can't be negative");
			System.exit(1);
		}
		if (tclose > TMax || topen > TMax) {
			System.err.println("Time set is bigger than TMax (TMax = " + TMax
					+ ")");
			System.exit(1);
		}

		String fileName;

		if (oS.indexOf("Windows") != -1) {
			fileName = ".\\amazonatps\\SINGLESIMULATION";
		} else {
			fileName = "SINGLESIMULATION";
		}

		// Number of faults types (AT,BT,CT,AB,AC,BC,ABC,ABT,ACT,BCT,ABCT)

		FaultConfiguration.faultConfiguration(fileName, typeOfFault,
				numberOfATPFileLines, atpFile, topen, tclose, line_to_line_R,
				line_to_ground_R, fractionFromBegin, numLineZTInString);

		try {
			GenBatFile();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		 invoke ATP
		String command;
		
		if (oS.indexOf("Windows") != -1) {
			command = atpDir + batFileName + " " + fileName + ".ATP";
		} else {
			command = atpDir + batFileName + " " + fileName + ".ATP " + fileName + ".lis -R";
		}
		
		new RunATP(command);
		
		if (oS.indexOf("Windows") == -1) {
			r.exec("mv SINGLESIMULATION.ATP SINGLESIMULATION.LIS SINGLESIMULATION.txt ./AMAZONATPS");
		}

		Thread.sleep(100);

	}

	public static void main(String[] args) throws Exception {

		if (args.length != 8) {
			System.err
					.println("Usage: <file name> <number of simulations><numLines>");
			System.exit(1);
		}

		if (oS.indexOf("Windows") != -1) {
			atpDir = System.getenv("WATDIR");
		} else {
			atpDir = System.getenv("GNUDIR");
		}

		String fileName = args[0];
	//	PrepareMasterFile.prepareMasterFile(fileName, atpDir + "master.atp");
		String preparedMasterFile = atpDir + "master.atp";

		int indexOfWantedLineZT = Integer.parseInt(args[1]);
		int typeOfFault = Integer.parseInt(args[2]);
		double initiation_time = Double.parseDouble(args[3]);
		double cleaning_time = Double.parseDouble(args[4]);
		double location_from_begin = Double.parseDouble(args[5]);
		double line_to_line_R = Double.parseDouble(args[6]);
		double line_to_ground_R = Double.parseDouble(args[7]);

		GenSingleFault gsf = new GenSingleFault();

		gsf.genSingleFaults(preparedMasterFile, indexOfWantedLineZT,
				typeOfFault, initiation_time, cleaning_time,
				(location_from_begin / 100), line_to_line_R, line_to_ground_R);

	}

}
