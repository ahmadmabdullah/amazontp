package amazontp.genfaults;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class FaultConfiguration {

	static int m_counter = 0;

	static double faultPosition = 0;

	static double length = 0;

	public static String faultConfiguration(String fileName, int nfaultType,
			int numberOfATPFileLines, String atpFile, double topen,
			double tclose, double line_to_line_R, double line_to_ground_R,
			double fractionFromBegin, String numLineZTInString)
			throws Exception {
		System.out.println("fileName: "+fileName);
		// For the 11 fault types, nfaultType will represent its
		// respectively indexes:

		// AT = 0 , BT = 1 , CT = 2 , AB = 3 , AC = 4 , BC = 5,
		// ABC = 6 , ABT = 7 , ACT = 8 , BCT = 9 and ABCT = 10.

		// Passar para ingl�s mais adiante, quando for finalizar a
		// vers�o 1.0.

		// Como simular as faltas AT, BT e CT:
		//				 
		// Achar as linhas das chaves abaixo e mudar os valores Topen
		// (Top/Tde) e Tclose no momento da falta.
		// X � a fase (A,B ou C) e ## � o n�mero da linha Z-T.
		// NEW##XMIDX##
		// END_##STG_##
		//				
		// Tamb�m mudar os valores das resist�ncias abaixo:
		// MIDX##END_##
		// STG_##

		// Para simular as faltas AB, AC, BC:
		//		 
		// Chaves: (X e Y s�o as fases em quest�o)
		// NEW##XMIDX##
		// NEW##YMIDY##
		//
		// Resistores:
		// MIDX##END_##
		// MIDY##END_##

		// Para simular as faltas ABT, ACT, BCT:
		//		 
		// Chaves:
		// NEW##XMIDX##
		// NEW##YMIDY##
		// END_##STG_##
		//		
		// Resistores:
		// MIDX##END_##
		// MIDY##END_##
		// STG_##

		// Para simular a falta ABC:
		//		 
		// Chaves:
		// NEW##AMIDA##
		// NEW##BMIDB##
		// NEW##CMIDC##
		// END_##STG_##
		//		
		// Resistores:
		// MIDA##END_##
		// MIDB##END_##
		// MIDC##END_##
		// STG_##

		// Para simular a falta ABCT:
		//		
		// Chaves:
		// NEW##AMIDA##
		// NEW##BMIDB##
		// NEW##CMIDC##
		// END_##STG_##
		//		
		// Resistores:
		// MIDA##END_##
		// MIDB##END_##
		// MIDC##END_##
		// STG_##

		String label = "";
		String[] phases = { "A", "B", "C" };
		String atpFileLine = "C 345678901234567890123456789012345678901234567890123456789012345678901234567890";

		// Method insertSwitchLines: (numLineZTInString, flag)
		// flag = 0 : NEW##AMIDA##
		// flag = 1 : NEW##BMIDB##
		// flag = 2 : NEW##CMIDC##
		// flag = 3 : END_##STG_##

		// Method GenFaultsUtils.insertResistorLines: (numLineZTInString, flag)
		// flag = 0 : MIDA##END_##
		// flag = 1 : MIDB##END_##
		// flag = 2 : MIDC##END_##
		// flag = 3 : STG_##

		if (nfaultType < 3) { // AT, BT and CT fault

			// Finds the needed lines to change in ATP File and changes it
			// according to the values.

			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = phases[nfaultType] + "T";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
							

						} else {

							String[] switchLines = new String[2];
							String[] resistorLines = new String[2];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString,
											nfaultType);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 3);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString,
											nfaultType);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 3);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, true);
						}
					}

				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}
		if (nfaultType == 3) { // AB fault

			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "AB";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[2];
							String[] resistorLines = new String[2];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, false);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 4) { // AC fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "AC";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[2];
							String[] resistorLines = new String[2];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, false);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 5) { // BC fault
			
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "BC";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[2];
							String[] resistorLines = new String[2];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, false);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 6) { // ABC fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "ABC";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[3];
							String[] resistorLines = new String[3];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							switchLines[2] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);
							resistorLines[2] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, false);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 7) { // ABT fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "ABT";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[3];
							String[] resistorLines = new String[3];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							switchLines[2] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 3);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);
							resistorLines[2] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 3);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, true);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 8) { // ACT fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "ACT";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[3];
							String[] resistorLines = new String[3];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							switchLines[2] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 3);
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);
							resistorLines[2] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 3);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, true);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 9) { // BCT fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "BCT";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[3];
							String[] resistorLines = new String[3];

							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							switchLines[2] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 3);

							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);
							resistorLines[2] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 3);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, true);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

		}

		if (nfaultType == 10) { // ABCT fault
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					fileName + ".ATP"));

			label = "ABCT";

			FaultGenerationUtils.writeTitleInATPFile(label, numLineZTInString,
					tclose, topen, fractionFromBegin, line_to_line_R,
					bufferedWriter);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					atpFile));

			for (m_counter = 0; m_counter < numberOfATPFileLines; m_counter++) {

				if ((atpFileLine = bufferedReader1.readLine()).equals(null) == false) {

					if (atpFileLine
							.indexOf("C AMAZONTP - Line "
									+ Integer.parseInt(numLineZTInString)
									+ " - Part 1") != -1) {

						FaultGenerationUtils.foundLine1stPartAndUpdateLength(
								atpFileLine, fractionFromBegin,
								bufferedReader1, bufferedWriter);

					} else {
						if (atpFileLine.indexOf("C AMAZONTP - Line "
								+ Integer.parseInt(numLineZTInString)
								+ " - Part 2") != -1) {

							FaultGenerationUtils
									.foundLine2ndPartAndUpdateLength(
											atpFileLine, bufferedReader1,
											bufferedWriter);
						} else {

							String[] switchLines = new String[4];
							switchLines[0] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 0);
							switchLines[1] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 1);
							switchLines[2] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 2);
							switchLines[3] = FaultGenerationUtils
									.insertSwitchLines(numLineZTInString, 3);

							String[] resistorLines = new String[4];
							resistorLines[0] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 0);
							resistorLines[1] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 1);
							resistorLines[2] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 2);
							resistorLines[3] = FaultGenerationUtils
									.insertResistorLines(numLineZTInString, 3);

							FaultGenerationUtils.foundAndChangeLines(
									atpFileLine, switchLines, resistorLines,
									topen, tclose, line_to_line_R,
									line_to_ground_R, bufferedWriter,
									bufferedReader1, true);

						}
					}
				}
				bufferedWriter.newLine();
			}

			bufferedReader1.close();
			bufferedWriter.close();

			label = "ABCT";

		}
		return (label);
	}
}