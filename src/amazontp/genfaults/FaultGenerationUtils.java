package amazontp.genfaults;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import amazontp.util.FormatValues;


public class FaultGenerationUtils {

	static String m_StartNode;

	static String m_EndNode;

	private static String formatToChange(boolean isSwitch, String atpFileLine,
			double topen, double tclose, double R) throws Exception {
		// isSwitch = true --> Switch
		// isSwitch = false --> Resistor

		String formatToChangeSwitchLines = atpFileLine.substring(0, 14)
				+ FormatValues.formatSwitchTime(tclose)
				+ FormatValues.formatSwitchTime(topen)
				+ atpFileLine.substring(34, 80);

		String formatToChangeResistorLines = atpFileLine.substring(0, 26)
				+ FormatValues.formatRLC(R) + atpFileLine.substring(32, 80);

		if (isSwitch == true) {
			return formatToChangeSwitchLines;
		} else {
			return formatToChangeResistorLines;
		}

	}

	public static String insertSwitchLines(String numLineZTInString, int flag) {

		// flag = 0 : NEW##AMIDA##
		// flag = 1 : NEW##BMIDB##
		// flag = 2 : NEW##CMIDC##
		// flag = 3 : END_##STG_##

		String switchLine = null;

		if (flag == 0) {
			switchLine = "NEW" + numLineZTInString + "AMIDA"
					+ numLineZTInString;
		}
		if (flag == 1) {
			switchLine = "NEW" + numLineZTInString + "BMIDB"
					+ numLineZTInString;
		}
		if (flag == 2) {
			switchLine = "NEW" + numLineZTInString + "CMIDC"
					+ numLineZTInString;
		}
		if (flag == 3) {
			switchLine = "END_" + numLineZTInString + "STG_"
					+ numLineZTInString;
		}

		return switchLine;
	}

	public static String insertResistorLines(String numLineZTInString, int flag) {

		// flag = 0 : MIDA##END_##
		// flag = 1 : MIDB##END_##
		// flag = 2 : MIDC##END_##
		// flag = 3 : STG_##

		String resistorLine = null;

		if (flag == 0) {
			resistorLine = "MIDA" + numLineZTInString + "END_"
					+ numLineZTInString;
		}
		if (flag == 1) {
			resistorLine = "MIDB" + numLineZTInString + "END_"
					+ numLineZTInString;
		}
		if (flag == 2) {
			resistorLine = "MIDC" + numLineZTInString + "END_"
					+ numLineZTInString;
		}
		if (flag == 3) {
			// The blank (" ") before STG is needed, otherwise, node
			// END_##STG_## will be considerated a resistorLine
			// and also a switchLine.
			resistorLine = " STG_" + numLineZTInString;
		}

		return resistorLine;
	}

	public static void foundAndChangeLines(String atpFileLine,
			String[] switchLines, String[] resistorLines, double topen,
			double tclose, double line_to_line_R, double line_to_ground_R,
			BufferedWriter bufferedWriter, BufferedReader bufferedReader1,
			boolean doesFaultGoesGround) throws Exception {

		final String newCommentLine = "C AMAZONTP: Line changed from original master file!";
		String lastModifiedLine = "";

		if (switchLines.length == 2) {
			if (atpFileLine.indexOf(switchLines[0]) != -1
					|| atpFileLine.indexOf(switchLines[1]) != -1
					|| atpFileLine.indexOf(resistorLines[0]) != -1
					|| atpFileLine.indexOf(resistorLines[1]) != -1) {

				if (atpFileLine.indexOf(switchLines[0]) != -1) {
					System.out.println("Found: " + switchLines[0]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);

				}
				if (atpFileLine.indexOf(switchLines[1]) != -1) {
					System.out.println("Found: " + switchLines[1]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}

				if (atpFileLine.indexOf(resistorLines[0]) != -1) {
					System.out.println("Found: " + resistorLines[0]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[1]) != -1) {
					System.out.println("Found: " + resistorLines[1]);
					if (doesFaultGoesGround == true) {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_ground_R);
					} else {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_line_R);
					}
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
			} else {

				findOutputCardOrWritesInFile(atpFileLine, newCommentLine,
						bufferedReader1, bufferedWriter);
			}
		}

		if (switchLines.length == 3) {

			if (atpFileLine.indexOf(switchLines[0]) != -1
					|| atpFileLine.indexOf(switchLines[1]) != -1
					|| atpFileLine.indexOf(switchLines[2]) != -1
					|| atpFileLine.indexOf(resistorLines[0]) != -1
					|| atpFileLine.indexOf(resistorLines[1]) != -1
					|| atpFileLine.indexOf(resistorLines[2]) != -1) {

				if (atpFileLine.indexOf(switchLines[0]) != -1) {
					System.out.println("Found: " + switchLines[0]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);

				}
				if (atpFileLine.indexOf(switchLines[1]) != -1) {
					System.out.println("Found: " + switchLines[1]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(switchLines[2]) != -1) {
					System.out.println("Found: " + switchLines[2]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}

				if (atpFileLine.indexOf(resistorLines[0]) != -1) {
					System.out.println("Found: " + resistorLines[0]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[1]) != -1) {
					System.out.println("Found: " + resistorLines[1]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[2]) != -1) {
					System.out.println("Found: " + resistorLines[2]);
					if (doesFaultGoesGround == true) {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_ground_R);
					} else {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_line_R);
					}
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}

			} else {

				findOutputCardOrWritesInFile(atpFileLine, newCommentLine,
						bufferedReader1, bufferedWriter);

			}
		}

		if (switchLines.length == 4) {

			if (atpFileLine.indexOf(switchLines[0]) != -1
					|| atpFileLine.indexOf(switchLines[1]) != -1
					|| atpFileLine.indexOf(switchLines[2]) != -1
					|| atpFileLine.indexOf(switchLines[3]) != -1
					|| atpFileLine.indexOf(resistorLines[0]) != -1
					|| atpFileLine.indexOf(resistorLines[1]) != -1
					|| atpFileLine.indexOf(resistorLines[2]) != -1
					|| atpFileLine.indexOf(resistorLines[3]) != -1) {

				if (atpFileLine.indexOf(switchLines[0]) != -1) {
					System.out.println("Found: " + switchLines[0]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);

				}
				if (atpFileLine.indexOf(switchLines[1]) != -1) {
					System.out.println("Found: " + switchLines[1]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(switchLines[2]) != -1) {
					System.out.println("Found: " + switchLines[2]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(switchLines[3]) != -1) {
					System.out.println("Found: " + switchLines[3]);
					lastModifiedLine = formatToChange(true, atpFileLine, topen,
							tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}

				if (atpFileLine.indexOf(resistorLines[0]) != -1) {
					System.out.println("Found: " + resistorLines[0]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[1]) != -1) {
					System.out.println("Found: " + resistorLines[1]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[2]) != -1) {
					System.out.println("Found: " + resistorLines[2]);
					lastModifiedLine = formatToChange(false, atpFileLine,
							topen, tclose, line_to_line_R);
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}
				if (atpFileLine.indexOf(resistorLines[3]) != -1) {
					System.out.println("Found: " + resistorLines[3]);
					if (doesFaultGoesGround == true) {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_ground_R);
					} else {
						lastModifiedLine = formatToChange(false, atpFileLine,
								topen, tclose, line_to_line_R);
					}
					bufferedWriter.write(newCommentLine);
					bufferedWriter.newLine();
					bufferedWriter.write(lastModifiedLine);
					System.out.println(lastModifiedLine);
				}

			} else {

				findOutputCardOrWritesInFile(atpFileLine, newCommentLine,
						bufferedReader1, bufferedWriter);

			}
		}

	}

	public static void findOutputCardOrWritesInFile(String atpFileLine,
			String newCommentLine, BufferedReader bufferedReader1,
			BufferedWriter bufferedWriter) throws Exception {

		// This "if" finds the OUTPUT. Here the selected line ZT origin
		// and end nodes are selected to output.

		if (atpFileLine.indexOf("/OUTPUT") != -1) {

			bufferedWriter.write("" + atpFileLine);
			bufferedWriter.newLine();
			bufferedWriter.write(newCommentLine);
			bufferedWriter.newLine();

			atpFileLine = bufferedReader1.readLine();
			FaultConfiguration.m_counter++;

			bufferedWriter.write("  " + m_StartNode + "A" + m_StartNode + "B"
					+ m_StartNode + "C"); // + m_EndNode + "A" + m_EndNode + "B"
					//+ m_EndNode + "C");

		} else {

			// Inseri-se os medidores nas linhas que sofreram faltas !!!
			if(m_StartNode!=null && atpFileLine.contains(m_StartNode) && atpFileLine.contains("MEASURING"))
			{
				//bufferedWriter.write(atpFileLine.replace("0","1"));
				bufferedWriter.write(atpFileLine.replace("MEASURING                0","MEASURING                1"));
			}
             // Here the master file lines are simply copied to the new
			//  ATPFile (non modified lines).
			else{
			bufferedWriter.write("" + atpFileLine);
			}
		}

	}

	public static void writeTitleInATPFile(String label,
			String numLineZTInString, double tclose, double topen,
			double fractionFromBegin, double R, BufferedWriter bufferedWriter)
			throws Exception {

		String title = label + " fault in line #" + numLineZTInString
				+ ". Started at " + FormatValues.formatSwitchTime(tclose)
				+ ", ended at " + FormatValues.formatSwitchTime(topen) + ".";

		String titlePart2 = "The fault occurs in "
				+ FormatValues.formatSwitchTime(fractionFromBegin)
				+ " of line length and the resistances are: "
				+ FormatValues.formatRLC(R);

		bufferedWriter
				.write("C -----------------------------------------------------------------------------");
		bufferedWriter.newLine();
		bufferedWriter
				.write("C Auto Generated by AmazonTP 1.0 - Universidade Federal do Par� (UFPA)");
		bufferedWriter.newLine();
		bufferedWriter.write("C " + title);
		bufferedWriter.newLine();
		bufferedWriter.write("C " + titlePart2);
		bufferedWriter.newLine();
		bufferedWriter
				.write("C -----------------------------------------------------------------------------");
		bufferedWriter.newLine();

	}

	public static void foundLine1stPartAndUpdateLength(String atpFileLine,
			double fractionFromBegin, BufferedReader bufferedReader1,
			BufferedWriter bufferedWriter) throws Exception {

		// Found the Line ZT Part 1 in order to change the length.
		// The length format is the same as RLC, so, we will use
		// GenFaults.formatRLC.

		atpFileLine = bufferedReader1.readLine();

		m_StartNode = atpFileLine.substring(2, 7);
		
		System.out.println("m_StartNode="+m_StartNode+"*");

		FaultConfiguration.m_counter++;
		FaultConfiguration.length = Double.parseDouble(atpFileLine.substring(
				45, 51)) + 10;
		FaultConfiguration.faultPosition = fractionFromBegin
				* FaultConfiguration.length;

		System.out.println("Found 1st part of the Line. Length = "
				+ FormatValues.formatRLC(FaultConfiguration.faultPosition));

		String newModifiedLine = atpFileLine.substring(0, 44)
				+ FormatValues.formatRLC(FaultConfiguration.faultPosition)
				+ atpFileLine.substring(50, 80);
		bufferedWriter.write(newModifiedLine);
		bufferedWriter.newLine();

		System.out.println(newModifiedLine);

		atpFileLine = bufferedReader1.readLine();
		FaultConfiguration.m_counter++;
		newModifiedLine = atpFileLine.substring(0, 44)
				+ FormatValues.formatRLC(FaultConfiguration.faultPosition)
				+ atpFileLine.substring(50, 80);
		bufferedWriter.write(newModifiedLine);

		System.out.println(newModifiedLine);
	}

	public static void foundLine2ndPartAndUpdateLength(String atpFileLine,
			BufferedReader bufferedReader1, BufferedWriter bufferedWriter)
			throws Exception {

		// Found the Line ZT Part 2 in order to change the length.
		// The length format is the same as RLC, so, we will use
		// GenFaults.formatRLC.

		System.out.println("Found 2nd part of the Line. Length = "
				+ FormatValues.formatRLC(FaultConfiguration.length
						- FaultConfiguration.faultPosition));

		atpFileLine = bufferedReader1.readLine();
		
		m_EndNode = atpFileLine.substring(8,13);
		
		FaultConfiguration.m_counter++;
		String newModifiedLine = atpFileLine.substring(0, 44)
				+ FormatValues.formatRLC(FaultConfiguration.length
						- FaultConfiguration.faultPosition)
				+ atpFileLine.substring(50, 80);
		bufferedWriter.write(newModifiedLine);
		bufferedWriter.newLine();

		atpFileLine = bufferedReader1.readLine();
		FaultConfiguration.m_counter++;
		newModifiedLine = atpFileLine.substring(0, 44)
				+ FormatValues.formatRLC(FaultConfiguration.length
						- FaultConfiguration.faultPosition)
				+ atpFileLine.substring(50, 80);
		bufferedWriter.write(newModifiedLine);
	}
}