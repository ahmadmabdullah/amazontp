package amazontp.genfaults;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;

import amazontp.io.LoadATPOutputFile;
import amazontp.io.SaveFile;
import amazontp.io.PrepareMasterFileFromCommandLine;
import amazontp.probability.DiscretePDFSampleGenerator;
import amazontp.probability.GaussianPDF;
import amazontp.util.CountUtils;
import amazontp.util.FormatValues;
import amazontp.util.RunATP;

/**
 * <p>
 * Title: Generate Randomic Line-ZT Shunt Faults 
 * </p>
 * 
 * <p>
 * Description: 
 * 
 * Receives as arguments:
 *  
 * 1) The address of a ATP File containing Line-ZTs
 * 2) Number of events to generate
 * 3) Parameters ranges to sort events (location, time, duration and resistances);
 * 4) Flags that indicates what probabilities are the the parameters using;
 * 5) Window length and Sampling Frequency.
 * 
 * Then sort the events and stores in the ATP folder an ARFF file to Weka simulations and also 
 * stores in ATP Folder/amazonatps the waveforms in ascii files, labeled in labels.txt file.
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: LaPS - UFPA
 * </p>
 * 
 * @author Aldebaro Klautau, Gustavo Guedes, Jefferson Morais, Jos� Borges and Yomara Pires.
 * @version 1.0
 */

 //Usage: 


public class GenRandomicFaults {

	private static Random m_random = new Random(System.currentTimeMillis());

	private static Random m_random2 = new Random(); 
	
	static String oS;

	public static String atpDir;

	static String m_OriginalFileName;

	String batFileName;

	Runtime r = Runtime.getRuntime();

	// Method for generate amazontp.bat file in ATP directory
	private void GenBatFile() throws Exception {

		SaveFile sb = new SaveFile();

		if (oS.indexOf("Windows") != -1) {
			atpDir = System.getenv("WATDIR");
			System.out.println("WATDIR: "+atpDir);
			if (atpDir == null) {
				atpDir = "c:\\Programs";
				System.err
						.println("Couldn't find Enviroment Variable WATDIR, setting c:\\ATP as ATP Folder.");
			} else {
				System.out.println("TPBIG located in: " + atpDir);
			}
		} else {
			atpDir = System.getenv("GNUDIR");
			if (atpDir == null) {
				atpDir = "/home/atplinux";
				System.err
						.println("Couldn't find Enviroment Variable GNUDIR, setting /home/linux as ATP Folder.");
			} else {
				System.out.println("TPBIG located in: " + atpDir);
			}
		}

		if (oS.indexOf("Windows") != -1) {
			batFileName = "amazontp.bat";
		} else {
			batFileName = "amazontp.sh";
		}

		sb.deleteFile(atpDir + batFileName);
		sb.createFile(atpDir + batFileName);
		sb.saveFile(atpDir + batFileName, "c:");
		//sb.saveFile(atpDir + batFileName, "cd " + atpDir);

		if (oS.indexOf("Windows") != -1) {
			sb.saveFile(atpDir + batFileName, "tpbig.exe disk %1 * -r");
		} else {
			sb.saveFile(atpDir + batFileName, "tpbig DISK $1 $2 -R");

			r.exec("chmod a+x " + atpDir + batFileName);
			Thread.sleep(200);
		}
	}

	// Generates Randomic Faults.
	public void genRandomicFaults(String atpFile, int numFaults,
			int numLinesZT, double location_start, double location_end,
			double time_start, double time_end, double duration_start,
			double duration_end, double resistance_start,
			double resistance_end, boolean isLocationUniform,
			boolean isTimeUniform, boolean isDurationUniform,
			boolean isResistanceUniform, int windowLength, double fs,
			boolean isAllLinesSelected, int[] indexesOfWantedLinesZT)
			throws Exception {

		// Number of faults types (AT,BT,CT,AB,AC,BC,ABC,ABT,ACT,BCT,ABCT).
		int numberOfFaultTypes = 11;

		double prob = 1.0 / numberOfFaultTypes;
		double[] pmf = new double[numberOfFaultTypes];

		for (int i = 0; i < numberOfFaultTypes; i++) {
			pmf[i] = prob;
		}

		DiscretePDFSampleGenerator faultDiscretePDFSampleGenerator = new DiscretePDFSampleGenerator(
				pmf, false);

		double[] numLinesZTValues;

		DiscretePDFSampleGenerator numLinesZTValuesDiscretePDFSampleGenerator;

		if (isAllLinesSelected) {

			numLinesZTValues = new double[numLinesZT];

			for (int i = 0; i < numLinesZT; i++) {
				numLinesZTValues[i] = 1.0 / numLinesZT;
			}

			numLinesZTValuesDiscretePDFSampleGenerator = new DiscretePDFSampleGenerator(
					numLinesZTValues, true);

		} else {

			numLinesZTValues = new double[indexesOfWantedLinesZT.length];

			for (int i = 0; i < indexesOfWantedLinesZT.length; i++) {
				numLinesZTValues[i] = 1.0 / indexesOfWantedLinesZT.length;
			}

			numLinesZTValuesDiscretePDFSampleGenerator = new DiscretePDFSampleGenerator(
					numLinesZTValues,true);
			

		}

		String outputARFFFile = "master.arff";
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
				outputARFFFile));

		SaveFile sf = new SaveFile();

		if (oS.indexOf("Windows") != -1) {
			sf.deleteFile("C:/Programs/ATPWatcom/amazonatps/labels.txt");
			sf.createFile("C:/Programs/ATPWatcom/amazonatps/labels.txt");
			//sf.deleteFile("C:/ATP/ATPWatcom/amazonatps/labels.txt");
			//sf.createFile("C:/ATP/ATPWatcom/amazonatps/labels.txt");
		} else {
			sf.deleteFile("labels.txt");
			sf.createFile("labels.txt");
		}

		int numberOfATPFileLines = CountUtils.countNumberOfLinesInFile(atpFile);

		double TStep = 0;
		double TMax = 0;

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				atpFile));

		for (int i = 0; i < numberOfATPFileLines; i++) {

			String atpFileLine = bufferedReader.readLine();

			if (atpFileLine.indexOf("BEGIN") != -1
					|| atpFileLine.substring(0, 1).equals("C") == true
					|| atpFileLine.substring(0, 1).equals("$") == true) {
				// Do nothing, BEGIN NEW DATA CASE, comment ou special cards
				// lines.
			} else {
				TStep = Double.parseDouble(atpFileLine.substring(0, 8));
				TMax = Double.parseDouble(atpFileLine.substring(8, 16));
				System.out.println("Time Step: " + TStep);
				System.out.println("Simulation Time (TMax): " + TMax);
				break;
			}
		}

		if (TMax == 0 || TStep == 0) { // In case of bad ATP files.
			System.out
					.println("Unable to find Time Step and Simulation Time, Bad ATP File, Program will exit!");
			System.exit(1);
		}

		if (time_end == -1) {
			time_end = TMax;
		}

		bufferedReader.close();

		System.out.println("Start location: " + location_start);
		System.out.println("Start time: " + time_start);
		System.out.println("Start duration: " + duration_start);
		System.out.println("Start resistance: " + resistance_start);
		System.out.println("End location: " + location_end);
		System.out.println("End time: " + time_end);
		System.out.println("End duration: " + duration_end);
		System.out.println("End resistance: " + resistance_end);
		System.out.println(" ");

		// Checking user inputs consistency.
		validateValues(TMax, location_start, location_end, time_start,
				time_end, duration_start, duration_end, resistance_start,
				resistance_end, isLocationUniform, isTimeUniform,
				isDurationUniform, isResistanceUniform);

		for (int i = 0; i < numFaults; i++) {
			// Need to reset for next iteration.
			// copyOriginalLines();

			double tclose;
			double topen;
			double faultDuration;

			while (true) {

				if (isDurationUniform == true) {
					// faultDuration = [duration_start, duration_end[
					faultDuration = m_random.nextDouble()
							* (duration_end - duration_start) + duration_start;
				} else {

					// Gaussian Distribution for duration.
					// duration_start = mean
					// duration_end = stdDev

					GaussianPDF gpdf = new GaussianPDF(0, Double.MAX_VALUE,
							duration_start, duration_end);
					faultDuration = gpdf.getNextDouble();

				}

				if (isTimeUniform == true) {
					// tclose [time_start, time_end[
					tclose = m_random.nextDouble() * (time_end - time_start)
							+ time_start;

					topen = tclose + faultDuration;

					// The loop won't break until faultEnd be smaller than TMax
					// and the user specified time_end.
					if (topen < TMax && topen < time_end) {

						System.out.println("Fault Start = " + tclose);
						System.out.println("Fault Duration = " + faultDuration);
						System.out.println("Fault End = " + topen);

						break;
					}
				} else {

					// Gaussian Distribution for time.
					// time_start = mean
					// time_end = stdDev

					GaussianPDF gpdf = new GaussianPDF(0, Double.MAX_VALUE,
							time_start, time_end);

					tclose = gpdf.getNextDouble();

					topen = tclose + faultDuration;

					// The loop won't break until faultEnd be smaller than TMax
					// and the user specified time_end.
					if (topen < TMax && topen < time_end) {

						System.out.println("Fault Start = " + tclose);
						System.out.println("Fault Duration = " + faultDuration);
						System.out.println("Fault End = " + topen);

						break;
					}

				}
			}

			double line_to_line_R;
			double line_to_ground_R;
			if (isResistanceUniform == true) {
				// Resistances [resistance_start,resistance_end[
				line_to_line_R = m_random.nextDouble()
						* (resistance_end - resistance_start)
						+ resistance_start;
				line_to_ground_R = m_random.nextDouble()
						* (resistance_end - resistance_start)
						+ resistance_start;
			} else {
				// Gaussian Distribution for resistance.
				// resistance_start = mean
				// resistance_end = stdDev

				GaussianPDF gpdf = new GaussianPDF(0, Double.MAX_VALUE,
						resistance_start, resistance_end);
				line_to_line_R = gpdf.getNextDouble();
				line_to_ground_R = gpdf.getNextDouble();
			}
			
			int nfaultType = faultDiscretePDFSampleGenerator.getSampleIndex();

			
			
			int nLineZT = numLinesZTValuesDiscretePDFSampleGenerator
					.getSampleIndex();

			String numLineZTInString = null;

			if (!isAllLinesSelected) {
				nLineZT = indexesOfWantedLinesZT[nLineZT];
			}

			if (nLineZT < 10) {
				numLineZTInString = "" + 0 + nLineZT;
			} else {
				numLineZTInString = "" + nLineZT;
			}
			
			double fractionFromBegin;
			if (isLocationUniform == true) {

				fractionFromBegin = m_random.nextDouble()
						* ((location_end - location_start) + location_start)
						/ 100;
			} else {
				// Gaussian Distribution for location.
				// location_start = mean
				// location_end = stdDev

				GaussianPDF gpdf = new GaussianPDF(0, 1, location_start,
						location_end);
				fractionFromBegin = gpdf.getNextDouble();
			}

			// Choose a fault type
			

			String fileName;

			if (oS.indexOf("Windows") != -1) {
				fileName = "C:/Programs/ATPWatcom/amazonatps/simulation_" + i;
			} else {
				fileName = "SIMULATION" + i;
			}

			// In the faultConfiguration method the specific ATP file for the
			// fault is created.
			String label = FaultConfiguration.faultConfiguration(fileName,
					nfaultType, numberOfATPFileLines, atpFile, topen, tclose,
					line_to_line_R, line_to_ground_R, fractionFromBegin,
					numLineZTInString);
			System.out.println("");
			System.out.println("AAA! nLineZT = " + nLineZT + ", label = " + label);
			System.out.println("");
			
		String dataBase = getFileNameFromPath(m_OriginalFileName);
		dataBase = deleteExtension(dataBase);
			
			String title = "Data base = " + dataBase + " ; Fault type = " + label
					+ " ; Fault line index= " + (numLineZTInString)
					+ " ; Fault Started in "
					+ FormatValues.formatSwitchTime(tclose) + " s ; Ended in "
					+ FormatValues.formatSwitchTime(topen)
					+ " s ; The fault happens on "
					+ FormatValues.formatSwitchTime(fractionFromBegin * 100)
					+ "% of line length ; The resistance between lines is "
					+ FormatValues.formatRLC(line_to_line_R)
					+ " ohms and resistance from line to ground is "
					+ FormatValues.formatRLC(line_to_ground_R) + " ohms.";

			if (oS.indexOf("Windows") != -1) {
				sf.saveFile("C:/Programs/ATPWatcom/amazonatps/labels.txt", "simulation_" + i
						+ ": " + title);//C:/ATP/ATPWatcom/amazonatps/
			} else {
				sf.saveFile("labels.txt", "SIMULATION" + i + ": " + title);
			}

			// Generating amazontp.bat
			try {
				GenBatFile();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			// invoke ATP
			String command;

			if (oS.indexOf("Windows") != -1) {
				command = atpDir + batFileName + " " + fileName + ".ATP";
			} else {
				command = atpDir + batFileName + " " + fileName + ".ATP "
						+ fileName + ".lis -R";
			}

			new RunATP(command);

			Thread.sleep(100);

			double decimationFactor = ( Math.floor(((1.0 / fs) / TStep)+.5));			
			if ((1/TStep)%decimationFactor!=0){
				fs=(1/TStep)/decimationFactor;
			}
			
			System.err.println("Decimantion factor " + decimationFactor);
			System.err.println("Fequencia atual: "+fs);

			BufferedReader bufferedReader1 = new BufferedReader(new FileReader(
					fileName + ".txt"));

			String line = bufferedReader1.readLine();

			if (line == null) {
				// ignoredSim ++;
				System.err
						.println("Null ATP Output File! Restarting simulation #"
								+ i);
				i--;
			} else {
				new LoadATPOutputFile(fileName + ".txt", TStep, tclose, topen,
						label, decimationFactor, bufferedWriter, windowLength,
						i);

				if (oS.indexOf("Windows") == -1) {
					r.exec("mv " + fileName + ".ATP " + fileName + ".LIS "
							+ fileName + ".txt " + "./AMAZONATPS");
				}

			}

			bufferedReader1.close();

		} // Fim do for do numero de faltas.

		System.out
				.println("Total: " + numFaults + " randomic faults generated");

		if (oS.indexOf("Windows") == -1) {
			r.exec("mv labels.txt ./AMAZONATPS");
		}

		bufferedWriter.close();

	}
	
	public static String getFileNameFromPath(String pathWhichEndsWithFileName) {

		return (new File(pathWhichEndsWithFileName)).getName();

	}
	
	public static String deleteExtension(String fileName) {

		int ndotIndex = fileName.lastIndexOf(".");

		if (ndotIndex == -1) {

			return fileName;

		} else {

			return fileName.substring(0,ndotIndex);

		}

	}

	private void validateValues(double TMax, double location_start,
			double location_end, double time_start, double time_end,
			double duration_start, double duration_end,
			double resistance_start, double resistance_end,
			boolean isLocationUniform, boolean isTimeUniform,
			boolean isDurationUniform, boolean isResistanceUniform) {

		if (isTimeUniform == true) {

			if (time_start > TMax || time_end > TMax) {
				System.err
						.println("Please change time range, at least one of its maximum value is bigger than TMax");
				System.exit(1);
			}

			if (time_start < 0 || time_end < 0) {
				System.err.println("Negative time entered by user.");
				System.exit(1);
			}

			if (time_start > time_end) {
				System.err.println("Start time must be smaller than end time.");
				System.exit(1);
			}

		} else { // Gaussian consistency for time.

			if (time_start > TMax) {
				System.err.println("Warning! Mean is bigger than TMax");
			}

			if (time_start - time_end > TMax) {
				System.err
						.println("Warning! TMax is smaller than \"Mean - Std. Deviation\" for time");
			}

			if (time_start + time_end > TMax) {
				System.err
						.println("Warning! TMax is smaller than \"Mean + Std. Deviation\" for time");
			}

			if (time_start < 0 || time_end < 0) {
				System.err
						.println("Negative Mean or Std. Deviation entered by user.");
				System.exit(1);
			}
		}

		if (isDurationUniform == true) {

			if (duration_start > TMax || duration_end > TMax) {
				System.err
						.println("Please change duration range, at least one of its maximum value is bigger than TMax");
				System.exit(1);
			}

			if (duration_start < 0 || duration_end < 0) {
				System.err.println("Negative duration entered by user.");
				System.exit(1);
			}

			if (duration_start > duration_end) {
				System.err.println("Duration start must be smaller than end");
				System.exit(1);
			}
		} else { // Gaussian consistency for duration.

			if (duration_start > TMax) {
				System.err
						.println("Warning! Duration Mean is bigger than TMax");
			}

			if (duration_start - duration_end > TMax) {
				System.err
						.println("Warning! TMax is smaller than \"Mean - Std. Deviation\" for duration");
			}

			if (duration_start + duration_end > TMax) {
				System.err
						.println("Warning! TMax is smaller than \"Mean + Std. Deviation\" for duration");
			}

			if (duration_start < 0 || duration_end < 0) {
				System.err
						.println("Negative Mean or Std. Deviation entered by user for duration.");
				System.exit(1);
			}
		}

		if (isLocationUniform == true) {

			if (location_start < 0 || location_start > 100 || location_end < 0
					|| location_end > 100) {
				System.err
						.println("Location values must be between 0% and 100%");
				System.exit(1);
			}

			if (location_start > location_end) {
				System.err
						.println("Location start must be smaller than location end");
				System.exit(1);
			}

		} else { // Gaussian consistency for location.

			if (location_start < 0 || location_start > 100 || location_end < 0
					|| location_end > 100) {
				System.err
						.println("Location values must be between 0% and 100%");
				System.exit(1);
			}
		}

		if (isResistanceUniform == true) {

			if (resistance_start <= 0 || resistance_end <= 0) {
				System.err.println("Resistance values must be positive");
				System.exit(1);
			} else {
				if (resistance_start > resistance_end) {
					System.err
							.println("Resistance start must be smaller than resistance end");
					System.exit(1);
				}

				if ((duration_end - duration_start) > (time_end - time_start)) {
					System.err
							.println("Time of Fault will never be smaller than End Time.");
					System.err.println("Duration end - duration_start = "
							+ (duration_end - duration_start));
					System.err.println("Time end - time_start = "
							+ (time_end - time_start));
					System.exit(1);
				}

			}
		} else { // Gaussian consistency for resistance.
			// Nothing to check.
		}

	}

	public static void main(String[] args) throws Exception {
        //String test[]={"E:/Jose/Circuitos/MARABA.atp", "5", "20.0", "80.0", "0.0", "1.0", "0.0", "0.1", "0.1", "2.0", "true", "true", "true", "true", "5", "5000", "4", "0", "1", "2", "3", 
//};
		boolean isAllLinesSelected;
		if (args.length != 17) {
			// Using all lines
			isAllLinesSelected = false;
		//	System.exit(1);
		} else {
			isAllLinesSelected = true;
		}

		oS = System.getProperty("os.name");
		System.out.println(oS);

		if (oS.indexOf("Windows") != -1) {
			atpDir = System.getenv("WATDIR");
		} else {
			atpDir = System.getenv("GNUDIR");
		}

		m_OriginalFileName = args[0];

		// If numberOfLinesZT is N, the indexes of these lines are [0 , N-1].
		//int numberOfLinesZT = PrepareMasterFile.prepareMasterFile(m_OriginalFileName, atpDir + "master.atp");
		String preparedMasterFile = atpDir + "master.atp";
		int numSimuls = Integer.parseInt(args[1]);

		GenRandomicFaults gf = new GenRandomicFaults();

		double location_start = Double.parseDouble(args[2]);
		double location_end = Double.parseDouble(args[3]);
		double time_start = Double.parseDouble(args[4]);
		double time_end = Double.parseDouble(args[5]);
		double duration_start = Double.parseDouble(args[6]);
		double duration_end = Double.parseDouble(args[7]);
		double resistance_start = Double.parseDouble(args[8]);
		double resistance_end = Double.parseDouble(args[9]);
		boolean isLocationUniform = Boolean.parseBoolean(args[10]);
		boolean isTimeUniform = Boolean.parseBoolean(args[11]);
		boolean isDurationUniform = Boolean.parseBoolean(args[12]);
		boolean isResistanceUniform = Boolean.parseBoolean(args[13]);
		int windowLength = Integer.parseInt(args[14]);
		double fs = Double.parseDouble(args[15]);
		int numberOfLinesZT=Integer.parseInt(args[16]);
		boolean fromCommandLine = Boolean.parseBoolean(args[17]);
		
		if (fromCommandLine) {
			PrepareMasterFileFromCommandLine.prepareMasterFile(
					m_OriginalFileName, atpDir + "master.atp");
		}
		
		int numberOfWantedLinesZT = args.length - 18;
		
		int indexesOfWantedLinesZT[] = new int[numberOfWantedLinesZT];

		if (!isAllLinesSelected) {

			for (int i = 0; i < numberOfWantedLinesZT; i++) {
				
				indexesOfWantedLinesZT[i] = Integer.parseInt(args[17 + i]);
			}

		}

		System.out.println("The Prepared Master File is located in: " + atpDir
				+ "master.atp");

		gf.genRandomicFaults(preparedMasterFile, numSimuls, numberOfLinesZT,
				location_start, location_end, time_start, time_end,
				duration_start, duration_end, resistance_start, resistance_end,
				isLocationUniform, isTimeUniform, isDurationUniform,
				isResistanceUniform, windowLength, fs, !isAllLinesSelected,
				indexesOfWantedLinesZT);

	}
}