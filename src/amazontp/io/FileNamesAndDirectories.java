package amazontp.io;

import java.io.File;
import amazontp.util.*;

/**Methods to deal with file and directory names.
 *
 * @author Aldebaro Klautau
 * @version 2.0 - September 26, 2000.
 */
public class FileNamesAndDirectories {

  /**
   * Replace all occurrences of a given String.
   * It's able to deal with the '\' special character.
   * @param originalString
   * @param toBeReplaced
   * @param replacement
   * @return
   */
  public static String replaceAllOccurrencesOfString(String originalString,
  String toBeReplaced,
  String replacement) {
    String currentOutputString = originalString;
    int nindex = -1;
    do {
      nindex = currentOutputString.lastIndexOf(toBeReplaced);
      if (nindex == -1) {
        return currentOutputString;
      }
      String newString = currentOutputString.substring(0, nindex);
      //newString += replacement + originalString.substring(nindex+toBeReplaced.length(),originalString.length()-1);
      newString += replacement +
          currentOutputString.substring(nindex + toBeReplaced.length(),
                                   currentOutputString.length());
      currentOutputString = newString;
    } while (nindex != -1);
    return currentOutputString;
  }

	public static String replacePartOfString(String originalString,
	String toBeReplaced,
	String replacement) {
		String lowerCaseOriginalString = originalString.toLowerCase();
		toBeReplaced = toBeReplaced.toLowerCase();
		int nindex = lowerCaseOriginalString.lastIndexOf(toBeReplaced);
		if (nindex == -1) {
			return null;
		}
		String newString = originalString.substring(0,nindex);
		//newString += replacement + originalString.substring(nindex+toBeReplaced.length(),originalString.length()-1);
		newString += replacement + originalString.substring(nindex+toBeReplaced.length(),originalString.length());
		forceEndingWithSlash(newString);
		return newString;
	}

	/**Check if file name has proper extension and exit if not using
	 * a case sensitive comparison.
	 */
	public static void checkExtensionWithCaseSensitiveAndExitOnError(String fileNameInTest,
													   String mandatoryExtension) {

		String extensionInTest = getExtension(fileNameInTest);
		if (extensionInTest == null) {
			End.throwError("Problem finding file extension!\nCould not find dot '.' in file name:" + fileNameInTest);
		}
		if (!extensionInTest.equals(mandatoryExtension)) {
			End.throwError(fileNameInTest + " does not have " +
						" mandatory extension = " + mandatoryExtension);
		}
	}

	/**Check if file name has proper extension and exit if not ignoring case.
	 */
	public static void checkExtensionWithCaseIgnoredAndExitOnError(String fileNameInTest,
													 String mandatoryExtension) {

		String extensionInTest = getExtension(fileNameInTest);
		if (extensionInTest == null) {
			End.throwError("Problem finding file extension!\nCould not find dot '.' in file name:" + fileNameInTest);
		}
		if (!extensionInTest.equalsIgnoreCase(mandatoryExtension)) {
			End.throwError(fileNameInTest + " does not have " +
						" mandatory extension = " + mandatoryExtension);
		}
	}

	/**Return true if file name has proper extension based on
	 * a case sensitive comparison.
	 */
	public static boolean checkExtensionWithCaseSensitive(String fileNameInTest,
													   String extension) {

		String extensionInTest = getExtension(fileNameInTest);
		if (extensionInTest == null) {
			return false;
		}
		if (extensionInTest.equals(extension)) {
			return true;
		} else {
			return false;
		}
	}

	/**Return true if file name has proper extension ignoring case.
	 */
	public static boolean checkExtensionWithCaseIgnored(String fileNameInTest,
													 String extension) {

		String extensionInTest = getExtension(fileNameInTest);
		if (extensionInTest == null) {
			return false;
		}
		if (extensionInTest.equalsIgnoreCase(extension)) {
			return true;
		} else {
			return false;
		}
	}

	/**Return extension (string after the dot in file name) or
	 * null if dot was not found.
	 */
	public static String getExtension(String fileName) {
		int ndotIndex = fileName.lastIndexOf(".");
		if (ndotIndex == -1) {
			return null;
		} else {
			return fileName.substring(ndotIndex + 1);
		}
	}

	/**Return file name without extension (string after the dot
	 * in file name) or fileName if dot was not found.
	 */
	public static String deleteExtension(String fileName) {
		int ndotIndex = fileName.lastIndexOf(".");
		if (ndotIndex == -1) {
			return fileName;
		} else {
			return fileName.substring(0,ndotIndex);
		}
	}

	/**Substitute extension (string after the dot in file name) of
	 * a file name.
	 */
	public static String substituteExtension(String fileName,
											 String newExtension) {

		String fileNameWithoutExtension = deleteExtension(fileName);

		if (newExtension.startsWith(".")) {
			return fileNameWithoutExtension + newExtension;
		} else {
			return fileNameWithoutExtension + "." + newExtension;
		}
	}

	/**I think the '\\' confuses Properties.load, so replace it by '/'.
	 */
	public static String replaceBackSlashByForward(String inputFileName) {
		char oldCharacter = '\\';
		char newCharacter = '/';
		return inputFileName.replace(oldCharacter,newCharacter);
	}

	public static String replaceForwardSlashByBack(String inputFileName) {
		char oldCharacter = '/';
		char newCharacter = '\\';
		return inputFileName.replace(oldCharacter,newCharacter);
	}

	/**Make sure directory ends with slash '/'.
	 */
	public static String forceEndingWithSlash(String directory) {
		//makes sure rootDirectory ends properly
		if ( (!directory.endsWith("/")) &
			 (!directory.endsWith("\\")) ) {
			return directory + "/";
		} else {
			return directory;
		}
	}

	/**Replace back-slash by slash and make sure directory ends with slash '/'.
	 */
	public static String replaceAndForceEndingWithSlash(String directory) {
		directory = replaceBackSlashByForward(directory);
		//makes sure rootDirectory ends properly
		if (!directory.endsWith("/")) {
			return directory + "/";
		} else {
			return directory;
		}
	}

	/**Get the absolute path, adding the directory if necessary.
	 */
	public static String getAbsolutePath(String fileName, String directory) {
		File file = new File(fileName);
		if (file.isAbsolute()) {
			return fileName;
		} else {
			//note that
			//fileName can be: ../../somedir/actualFileName or actualFileName
			//deal with both cases
			String onlyFileName = getFileNameFromPath(fileName);
			if (directory != null) {
				forceEndingWithSlash(directory);
				return directory + onlyFileName;
			} else {
				return fileName;
			}
		}
	}

	public static void createDirectoriesIfNecessaryGivenFileName(String fileName) {
		String directory = getPathFromFileName(fileName);
		createDirectoriesIfNecessary(directory);
	}

	public static void createDirectoriesIfNecessary(String directory) {
		if (directory == null) {
			return;
		}

		File file = new File(directory);
		//do nothing if directory already exists
		if(!file.isDirectory()) {
			if (!file.mkdirs()) {
				End.throwError("Writing subdirectories for path = " + directory);
			}
		}
	}

	public static boolean deleteFile(String fullPathFileName) {
		return (new File(fullPathFileName)).delete();
	}

	/**If path = c:\dir1\dir2 it returns c:/dir1/
	 * If path = c:\ it returns c:/
	 * If path = c:\dir1\dir2\someFile.txt it returns c:/dir1/dir2/
	 * If path = someFile.txt it throws an error.
	 */
	public static String getParent(String path) {
		File file = new File(path);
		String parent = file.getParent();
		if (parent != null) {
			return replaceAndForceEndingWithSlash(parent);
		}
		if (file.isFile()) {
			End.throwError("Can not get parent from path: " + path);
			return null;
		} else {
			//can be C:\
			return replaceAndForceEndingWithSlash(path);
		}
	}

	public static String getFileNameFromPath(String pathWhichEndsWithFileName) {
		return (new File(pathWhichEndsWithFileName)).getName();
	}

	public static String getPathFromFileName(String pathWhichEndsWithFileName) {
		String path = (new File(pathWhichEndsWithFileName)).getParent();
		if (path != null) {
		   return replaceAndForceEndingWithSlash(path);
		} else {
			//XXX maybe C:\ ?
			return null;
		}
	}

	public static String concatenateTwoPaths(String firstPath, String lastPath) {
		String first = replaceBackSlashByForward(firstPath);
		String last = replaceBackSlashByForward(lastPath);
		first = forceEndingWithSlash(first);
		if (last.startsWith("/")) {
			//return forceEndingWithSlash(first.concat(last.substring(1)));
			return first.concat(last.substring(1));
		} else {
			//return forceEndingWithSlash(first.concat(last));
			return first.concat(last);
		}
	}

	public static String deleteDriveLetterFromPath(String path) {
		if (path.charAt(1) == ':') {
			return path.substring(2);
		} else {
			return path;
		}
	}

	public static String askSystemForTemporaryFileName() {
		File file = null;
		try {
			file = File.createTempFile("spock",".tmp");
		}
		catch (Exception ex) {
			System.err.println("Couldn't create temporary file!");
			ex.printStackTrace();
			System.exit(1);
		}
		return file.toString();
	}

}
