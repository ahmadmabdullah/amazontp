package  amazontp.io;

import java.io.File;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import amazontp.io.*;
import amazontp.matlab.Cloner;
import amazontp.util.End;
import amazontp.util.Print;

/**
 * Organizes a table that associates an entry number (row) to a group of labels (columns). The table TIMIT39 is based on the paper "Speaker-Independent Phone Recognition Using Hidden Markov Models", K.-F. Lee and H.-W. Hon, IEEE Transactions on ASSP, vol. 37, no. 11, November, 1989. pp. 1641-8. The table TIMIT46 is based on book "Automatic Speech Recognition - the development of the SPHINX system" by Kai-Fu Lee, Kluwer, 1989, page 56. The table TIMIT48 was designed by myself with information collected from Internet. The label ax-h was substituted by axh to avoid confusion with the minus symbol '-' used in triphones.
 * @author   Aldebaro Klautau
 * @version   2.0 - October 22, 2000.
 */


 //Below is not true anymore
 //Note: all TIMIT tables have a "sil" (silence) as the first label
 //of one of its entries. The reason is that the network used
 //for recognition expects the existence of a physical HMM with
 //name "sil".

public final class TableOfLabels implements Serializable {

  public static final String m_FILE_EXTENSION = "TBL";

  /**Type (inner class) of this object.
   */
  private Type m_type;

  /**The line indicates the entry number and column
   * indicates one of its labels.
   */
  protected String[][] m_table; //non rectangular matrix

  private Hashtable m_hashtable;
  //specific to extended types:
  private TableOfLabels m_baseTable = null;
  //specific to triphones (for faster retrieval of physical model indices):
  //the order is central-left-right; may save some memory when all logical
  //map to the same physical as in sp and sil
  public short[][][] m_physicalIndex = null;

  /**Creates a TableOfLabels based on a (possibly non rectangular) matrix
   * of Strings where each line indicates the entry number and
   * columns indicate the labels associated to that entry.
   * Assumes Type.GENERIC.
   */
   //XXX I'm gonna follow convention of not cloning in constructor
  public TableOfLabels(String[][] table) {
    m_type = Type.GENERIC;
    m_table = table;
//		if (table != null) {
//			m_table = new String[table.length][];
//			for (int i=0; i<table.length; i++) {
//				m_table[i] = new String[table[i].length];
//				for (int j=0; j<table[i].length; j++) {
//					m_table[i][j] = table[i][j];
//				}
//			}
//		}
    makeHashtable();
  }

  /**Creates a TableOfLabels based on an array
   * of Strings where each line indicates the entry number.
   * Assumes Type.GENERIC.
   */
  public TableOfLabels(String[] table) {
    m_type = Type.GENERIC;
    if (table != null) {
      m_table = new String[table.length][1];
      for (int i=0; i<table.length; i++) {
        m_table[i][0] = table[i];
      }
      makeHashtable();
    } else {
      End.throwError("Tried to construct TableOfLabels with null object!");
    }
  }

  public TableOfLabels(String firstEntry, boolean oanyValue) {
    m_type = Type.GENERIC;
    m_table = new String[1][1];
    m_table[0][0] = firstEntry;
    makeHashtable();
  }

  public TableOfLabels(Type type) {
    m_type = type;
    if (type == Type.TIMIT61) {
      createTIMIT61LabelsTable();
    } else if (type == Type.TIMIT60) {
      createTIMIT60LabelsTable();
    } else if (type == Type.TIMIT39) {
      createTIMIT39LabelsTable();
    } else if (type == Type.TIMIT47) {
      createTIMIT47LabelsTable();
    } else if (type == Type.TIMIT48) {
      createTIMIT48LabelsTable();
    } else if (type == Type.TIMITVOWELS) {
      createTIMITVOWELSLabelsTable();
    } else if (type == Type.TIDIGITS) {
      createTIDIGITSTable();
    } else if (type == Type.ISIP41) {
      createISIP41LabelsTable();
    } else {
      End.throwError("Table of type " + type.toString() +
            " can not be created automatically by class TableOfLabels.");
    }
    makeHashtable();
  }

  /**
   * This constructor initializes the derived Type of TableOfLabels (like
   * triphones) based on the elements from the baseTable.
   * @param derivedType
   * @param baseTable
   */
  public TableOfLabels( Type derivedType, TableOfLabels baseTable ) {
    if( !derivedType.equals(Type.TRIPHONE) ) {
      End.throwError("Only the TRIPHONE derived type has been defined so far.");
    }
    m_type = derivedType;
    m_baseTable = baseTable;
  }

  //assumes m_table was already created
  public void makeHashtable() {
    float floadFactor = 0.75F; //as recommended in Java tutorial
//    int nnumberOfEntries = m_table.length;
    int nnumberOfEntries = 0;
    for( int i=0; i<m_table.length; i++ ) {
      nnumberOfEntries += m_table[i].length;
    }
    int ninitialCapacity = (int) (0.5F + nnumberOfEntries / floadFactor);
    m_hashtable = new Hashtable(ninitialCapacity,floadFactor);
    for (int i=0; i<m_table.length; i++) {
      Integer nindex = new Integer(i);
      for (int j=0; j<m_table[i].length; j++) {
        m_hashtable.put(m_table[i][j],nindex);
      }
    }
  }

  public void setPhysicalLabels( String[][] table, short[][][] physicalIndex ) {
    if( !m_type.equals(Type.TRIPHONE) ) {
      End.throwError("addPhysicalLabels method should be used only when "+
                     "TableOfLabels represents triphones!");
    }
    m_table = table;
    m_physicalIndex = physicalIndex;
    makeHashtable();
  }

  public void deleteGlottalStopLabelQ() {
    int nindex = getEntry("q");
    if (nindex != -1) {
      //there's q in this table, delete it
      String[] labels = m_table[nindex];
      //check how many labels nindex has
      if (labels.length == 1) {
        //delete entry
        String[][] table = new String[m_table.length-1][];
        for (int i = 0; i < nindex; i++) {
          table[i] = m_table[i];
        }
        for (int i = nindex+1; i < m_table.length; i++) {
          table[i-1] = m_table[i];
        }
        m_table = table;
        //need to make a new one because indices were possibly shifted
        makeHashtable();
      } else {
        //delete q label
        m_table[nindex] = new String[labels.length - 1];
        int j=0;
        for (int i = 0; i < labels.length; i++) {
          if (!labels[i].equals("q")) {
            m_table[nindex][j] = labels[i];
            j++;
          }
        }
        //this seemed to be missing. n.j.
        m_hashtable.remove("q");
      }
    }
  }

  private void addToHashtable(String entryLabel, int entryNumber) {
    m_hashtable.put(entryLabel, new Integer(entryNumber));
  }

  private void addToHashtable(String[] entryLabels, int entryNumber) {
    for (int j=0; j<entryLabels.length; j++) {
      m_hashtable.put(entryLabels[j], new Integer(entryNumber));
    }
  }

  private void removeFromHashtable(int nentryNumber) {
    int nnumberOfEntries = getNumberOfEntries();
    if (nentryNumber < 0 || nentryNumber > nnumberOfEntries -1) {
      End.throwError("Trying to substitute entry # " + nentryNumber +
      " in a table with only " + nnumberOfEntries + " entries.");
    }
    for (int i = 0; i < m_table[nentryNumber].length; i++) {
      m_hashtable.remove(m_table[nentryNumber][i]);
    }
  }

  private void substituteEntry(String[] newEntryLabels, int nentryNumber) {
    //recreate the hash table is inefficient...
    //makeHashtable();
    removeFromHashtable(nentryNumber);
    addToHashtable(newEntryLabels, nentryNumber);
    m_table[nentryNumber] = newEntryLabels;
  }

  public String getPrefferedName(int ntableEntry, String extension) {
    if (m_table == null) {
      End.throwError(
                "Can not call getPrefferedName() with a null TableOfLabels");
    }
    return ntableEntry +
        "_" +
        m_table[ntableEntry][0] +
        "." +
        extension;
  }

  public static boolean isPossibleToCreateTableFromRootDirectory(
                                              String rootDirectory ) {

    if ( rootDirectory == null || rootDirectory.equals("") ) {
      Print.error(
              "Tried to create a table of labels with null or empty String");
      return false;
    }

    File root = new File(rootDirectory);

    //find the subdirectories of rootDirectory, because the user
    //can eventually have some files (that are not subdirectories
    //in rootDirectory. For example, for word Yes, let's say there
    //are 2 subdirectories:
    //c:\Database\Yes\Speaker1\
    //c:\Database\Yes\Speaker2\

    //allRootFiles will contain all files of rootDirectory
    //but without the complete path (without rootDirectory)
    String[] allRootFiles = root.list();
    if (allRootFiles == null || allRootFiles.length == 0) {
      Print.error("Could not find any subdirectory under " + rootDirectory +
      ". Each label (word, phoneme, etc.) should has its own subdirectory." +
      " Failed finding the labels.");
      return false;
    }
    Vector subdirectories = new Vector();
    //loop below can take 5 minutes if directory has around 14,000 files
    for (int i=0; i<allRootFiles.length; i++) {
      String fullPath = FileNamesAndDirectories.concatenateTwoPaths(
                                              rootDirectory,allRootFiles[i] );
      File candidate = new File(fullPath);
      //System.out.println(candidate.toString());
      //String pathSeparator = candidate.pathSeparator;
      if (candidate.isDirectory()) {
        //subdirectories.addElement(candidate.getPath());
        subdirectories.addElement(allRootFiles[i]);
      }
    }

    //subdirectories represents the labels, and now I can
    //try to create a TableOfLabels
    int nnumberOfEntries = subdirectories.size();
    if (nnumberOfEntries < 2) {
      Print.error( "Could not find at least two subdirectories under " +
            rootDirectory + ".\nEach label (word, phoneme, etc.) should have" +
            " its own subdirectory.\nFailed trying to guess the labels based" +
            " on directory structure." );
      return false;
    }
    return true;
  }


  public void createTableFromRootDirectory(String rootDirectory) {

    if (rootDirectory == null || rootDirectory.equals("")) {
      End.throwError(
                "Tried to create a table of labels with null or empty String");
    }

    //Print.dialog(rootDirectory);

    m_type = Type.GENERIC;

    File root = new File(rootDirectory);

    //find the subdirectories of rootDirectory, because the user
    //can eventually have some files (that are not subdirectories
    //in rootDirectory. For example, for word Yes, let's say there
    //are 2 subdirectories:
    //c:\Database\Yes\Speaker1\
    //c:\Database\Yes\Speaker2\

    //allRootFiles will contain all files of rootDirectory
    //but without the complete path (without rootDirectory)
    String[] allRootFiles = root.list();
    if (allRootFiles == null || allRootFiles.length == 0) {
      End.throwError("Could not find any subdirectory under " + rootDirectory +
      ". Each label (word, phoneme, etc.) should has its own subdirectory." +
      " Failed finding the labels.");
    }

    Vector subdirectories = new Vector();
    for (int i=0; i<allRootFiles.length; i++) {
      String fullPath = FileNamesAndDirectories.concatenateTwoPaths(
                                              rootDirectory,allRootFiles[i] );
      File candidate = new File(fullPath);
      //System.out.println(candidate.toString());
      //String pathSeparator = candidate.pathSeparator;
      if (candidate.isDirectory()) {
        //subdirectories.addElement(candidate.getPath());
        subdirectories.addElement(allRootFiles[i]);
      }
    }

    //subdirectories represents the labels, and now I can
    //create a TableOfLabels
    int nnumberOfEntries = subdirectories.size();
    if (nnumberOfEntries < 2) {
      End.throwError("Could not find at least two subdirectories under " +
          rootDirectory + ".\nEach label (word, phoneme, etc.) should have" +
          "  its own subdirectory.\nFailed trying to guess the labels based" +
          " on directory structure.");
    }
    m_table = new String[nnumberOfEntries][1];
    for (int i=0; i<nnumberOfEntries; i++) {
      m_table[i][0] = (String) subdirectories.elementAt(i);
    }
    //System.out.println(this.toString());
  }

  private static String[] getLabels(String stringIn) {
    StringTokenizer stringTokenizer = new StringTokenizer(stringIn," ");
    int nnumberOfTokens = stringTokenizer.countTokens();
    String[] stringsOut = new String[nnumberOfTokens];
    for (int i=0; i<nnumberOfTokens; i++) {
      stringsOut[i] = stringTokenizer.nextToken();
    }
    return stringsOut;
  }

  public void addNewLabelToGivenEntry(int nentryNumber, String newLabel) {
    String[] temp = m_table[nentryNumber];
    int ncurrentNumberOfLabels = temp.length;
    //create space to 1 more label
    m_table[nentryNumber] = new String[ncurrentNumberOfLabels + 1];
    //copy old entries
    for (int j = 0; j < ncurrentNumberOfLabels; j++) {
      m_table[nentryNumber][j] = temp[j];
    }
    //add new entry
    m_table[nentryNumber][ncurrentNumberOfLabels] = newLabel;
    //type should be GENERIC in this case
    m_type = Type.GENERIC;
    addToHashtable(newLabel, nentryNumber);
  }

  public void addNewEntryToEndOfTable(String newEntry) {
    String[] temporaryVector = new String[1];
    temporaryVector[0] = newEntry;
    addNewEntryToEndOfTable(temporaryVector);
  }

  public void addNewEntryToEndOfTable(String[] newEntry) {
    String[][] table = this.getMatrixOfStrings();
    String[][] newTable = new String[table.length + 1][];
    //copy current data
    for (int i=0; i<table.length; i++) {
      newTable[i] = table[i];
      //int nnumberOfLabels = table[i].length;
      //newTable[i] = new String[nnumberOfLabels];
      //for (int j=0; j<nnumberOfLabels; j++) {
      //	newTable[i][j] = table[i][j];
      //}
    }
    //copy new entry as last entry of new table
    //int nnumberOfLabels = newEntry.length;
    //newTable[newTable.length-1] = new String[nnumberOfLabels];
    //for (int j=0; j<nnumberOfLabels; j++) {
    //	newTable[newTable.length-1][j] = newEntry[j];
    //}
    newTable[newTable.length-1] = newEntry;
    m_table = newTable;
    //type should be GENERIC in this case
    m_type = Type.GENERIC;
    addToHashtable(newEntry, newTable.length-1);
  }

  /**Get Type of this table.
   */
  public Type getType() {
    return m_type;
  }

  /**Get matrix of Strings of this table.
   */
  public String[][] getMatrixOfStrings() {
    return Cloner.clone(m_table);
  }

  /**Get all labels of a given entry as a vector of Strings.
   */
  public String[] getLabels(int nentryNumber) {
    if (m_table != null) {
      return m_table[nentryNumber];
    } else {
      return null;
    }
  }

  /**Get all labels of a given entry as one String, with
   * labels separated by blank spaces.
   */
  public String getLabelsAsString(int nentryNumber) {
    if (m_table == null) {
      return null;
    } else {
      int nnumberOfLabels = this.getNumberOfLabels(nentryNumber);
      String labels = "";
      for (int i=0; i < nnumberOfLabels - 1; i++) {
        labels += m_table[nentryNumber][i] + " ";
      }
      //don't add blank space to last label
      labels += m_table[nentryNumber][nnumberOfLabels - 1];
      return labels;
    }
  }

  /**Get all labels of a given entry as one String, with
   * labels separated by underscore '_'.
   */
  public String getLabelsAsStringSeparatedBy_(int nentryNumber) {
    if (m_table == null) {
      return null;
    } else {
      int nnumberOfLabels = this.getNumberOfLabels(nentryNumber);
      String labels = "";
      for (int i=0; i < nnumberOfLabels - 1; i++) {
        labels += m_table[nentryNumber][i] + "_";
      }
      //don't add '_' to last label
      labels += m_table[nentryNumber][nnumberOfLabels - 1];
      return labels;
    }
  }

  public String[] getAllLabels() {
    String[] labels = new String[getTotalNumberOfLabels()];
    int ncurrentLabelIndex = 0;
    for (int i = 0; i < m_table.length; i++) {
      for (int j=0; j < m_table[i].length; j++) {
        labels[ncurrentLabelIndex++] = m_table[i][j];
      }
    }
    return labels;
  }

  public int getTotalNumberOfLabels() {
    int nnumberOfLabels = 0;
    for (int i = 0; i < m_table.length; i++) {
      nnumberOfLabels += m_table[i].length;
    }
    return nnumberOfLabels;
  }

  /**Get first label of a given entry.
   */
  public String getFirstLabel(int nentryNumber) {
    if (m_table != null) {
      return m_table[nentryNumber][0];
    } else {
      return null;
    }
  }

  public String[] getAllFirstLabels() {
    int nnumberOfEntries = m_table.length;
    String[] firstLabels = new String[nnumberOfEntries];
    for (int i = 0; i < firstLabels.length; i++) {
      firstLabels[i] = m_table[i][0];
    }
    return firstLabels;
  }

  public String getAllFirstLabelsAsString() {
    String[] out = getAllFirstLabels();
    StringBuffer stringBuffer = new StringBuffer(out[0]);
    for (int i = 1; i < out.length; i++) {
      stringBuffer.append(" " + out[i]);
    }
    return stringBuffer.toString();
  }

  public int getNumberOfOccurrenceOfCentralPhone(String centralPhone) {
    String[] labels = getAllFirstLabels();
    int nnumberOfOccurrenceOfCentralPhone = 0;
    for (int i = 0; i < labels.length; i++) {
      if (centralPhone.equals(getCentralPhoneIfTriphone(labels[i]))) {
        nnumberOfOccurrenceOfCentralPhone++;
      }
    }
    return nnumberOfOccurrenceOfCentralPhone;
  }

  /**
   * Go through "first labels" only (physical HMMs).
   */
  public int[] getEntriesWithSameCentralPhone(String centralPhone) {
     String[] labels = getAllFirstLabels();
     Vector entriesVector = new Vector();
     for (int i = 0; i < labels.length; i++) {
       if (centralPhone.equals(getCentralPhoneIfTriphone(labels[i]))) {
        entriesVector.addElement(new Integer(i));
       }
     }
     int nnumberOfOccurrenceOfCentralPhone = entriesVector.size();
     int[] entries = new int[nnumberOfOccurrenceOfCentralPhone];
     for (int i = 0; i < entries.length; i++) {
       entries[i] = ((Integer) entriesVector.elementAt(i)).intValue();
     }
     return entries;
  }

  /**Get first label corresponding to a given label.
   */
  public String getFirstLabel(String label) {
    if (m_table != null) {
      int nindex = getEntry(label);
      if (nindex == -1) {
        return null;
      }
      return m_table[nindex][0];
    } else {
      return null;
    }
  }

  /**Returns true if the specified label corresponds to the
   * specified entry or false otherwise.
   */
  public boolean isAMatch(int nentryNumber, String label) {
    for (int j=0; j<m_table[nentryNumber].length; j++) {
      if (label.equals(m_table[nentryNumber][j])) {
        return true;
      }
    }
    return false;
  }

  public boolean isAMatchExclusingFirstLabel(int nentryNumber, String label) {
    for (int j=1; j<m_table[nentryNumber].length; j++) {
      if (label.equals(m_table[nentryNumber][j])) {
        return true;
      }
    }
    return false;
  }

  /**Get entry number of a given label. Returns -1 if unsuccessful.
   */
  public int getEntry(String label) {
    //better not to convert the triphone string to monophones. hashtable is faster than conversion.
//    if( m_type.equals(Type.TRIPHONE) ) {
//      String[] phones = fromTriphoneToPhones( label );
//      if( phones==null ) {
//        int central = this.m_baseTable.getEntry( label );
//        return this.m_physicalIndex[central][0][0];
//      } else {
//        int left = m_baseTable.getEntry( phones[0] );
//        int central = m_baseTable.getEntry( phones[1] );
//        int right = m_baseTable.getEntry( phones[2] );
//        //the order is central-left-right
//        return m_physicalIndex[central][left][right];
//      }
//    } else {
      //find index using Hashtable
      Integer index = (Integer) m_hashtable.get(label);
      if (index == null) {
        return -1;
      }
      return index.intValue();
//    }
  }

  /**
   * This method returns the physical index of the triphone specified with the triplet
   * of monophone indices. If the actual TableOfLabels is not a triphone table, method
   * thows an error and exits.
   * @param left
   * @param central
   * @param right
   * @return
   */
  public int getTriphoneEntry( short left, short central, short right ) {
    if( !m_type.equals(Type.TRIPHONE) ) {
      End.throwError("Asking for triphone physical index from a non triphone table of labels!");
    }
    if( m_physicalIndex[central].length==1 ) {
      return m_physicalIndex[central][0][0];
    } else {
      return m_physicalIndex[central][left][right];
    }
  }

  /**
   * Similar to previous function, this one does the lookup of the special symbol like sp or sil
   * that do not have full triphone context
   * @param central
   * @return
   */
  public int getTriphoneEntry( short central ) {
    if( !m_type.equals(Type.TRIPHONE) ) {
      End.throwError("Asking for triphone physical index from a non triphone table of labels!");
    }
    if( m_physicalIndex[central].length==1 ) {
      return m_physicalIndex[central][0][0];
    } else {
      End.throwError("Full context information required for this phone,");
      return -1;
    }
  }

  public boolean hasAnyTriphone() {
    for (int i=0; i<m_table.length; i++) {
      for (int j=0; j<m_table[i].length; j++) {
        if (fromTriphoneToPhones(m_table[i][j]) != null) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Specific for triphones
   * General idea was (not anymore):
   * Merge two tables f and g, where each entry of g is mapped to
   * one entry of d. The final table has the number
   * of entries of f. The operation is defined only when all
   * entries of g have their first labels corresponding to one
   * label of f.
   */
  public static TableOfLabels createTableForScoringTriphones(
                                    TableOfLabels monophoneScoringTable,
                                    TableOfLabels triphoneTable ) {
    //start with original scoring table, including both physical and logical
    //HMM labels. this allows to use the monophone transcriptions
    String[][] newTable =
                    new String[monophoneScoringTable.getNumberOfEntries()][];
    for (int i=0; i<newTable.length; i++) {
      String[] labels = monophoneScoringTable.getLabels(i);
      newTable[i] = new String[labels.length];
      for (int j=0; j<labels.length; j++) {
        newTable[i][j] = labels[j];
      }
    }
    //now add triphones. Consider possibility of having a monophone
    int nnumberOfEntries = triphoneTable.getNumberOfEntries();
    for (int i=0; i<nnumberOfEntries; i++) {
      String[] newLabels = triphoneTable.getLabels(i);
      String physicalHMMLabel = newLabels[0];
      String centralPhone = getCentralPhone(physicalHMMLabel);
      if (centralPhone == null) {
        //this is a monophone and must already be at monophone table
        int nentry = monophoneScoringTable.getEntry(physicalHMMLabel);
        if (nentry == -1) {
          End.throwError("Could not find " + physicalHMMLabel);
        }
        //if it is not the first entry, then add it as "logical" HMM
        if ( !physicalHMMLabel.equals(
                          monophoneScoringTable.getFirstLabel(nentry)) ) {
          addLabelsToGivenEntry(nentry,newTable,newLabels);
        }
      } else {
        int nentryOfThisPhone = monophoneScoringTable.getEntry(centralPhone);
        addLabelsToGivenEntry(nentryOfThisPhone,newTable,newLabels);
      }
    }
    return new TableOfLabels(newTable);
  }

  private static void addLabelsToGivenEntry( int nentry, String[][] table,
                                             String[] newLabels ) {
    int ntotalNumberOfLabels = newLabels.length + table[nentry].length;
    String[] oldLabels = table[nentry];
    table[nentry] = new String[ntotalNumberOfLabels];
    //important, keep the order: old, then new labels
    for (int i=0; i<oldLabels.length; i++) {
      table[nentry][i] = oldLabels[i];
    }
    for (int i=0; i<newLabels.length; i++) {
      table[nentry][i+oldLabels.length] = newLabels[i];
    }
  }

  private static String[] fromTriphoneToPhones(String triphoneLabel) {
    int nminusIndex = triphoneLabel.indexOf('-');
    int nplusIndex = triphoneLabel.indexOf('+');
    if (nminusIndex == -1 || nplusIndex == -1) {
      return null;
    }
    String[] phones = new String[3];
    //left context
    phones[0] = triphoneLabel.substring( 0, nminusIndex ).trim();
    phones[1] = triphoneLabel.substring( nminusIndex+1, nplusIndex ).trim();
    phones[2] = triphoneLabel.substring( nplusIndex+1,
                                         triphoneLabel.length() ).trim();
    return phones;
  }

  /**
   * If monophone, returns null
   * If triphone, returns central phone
   * otherwise, returns null.
   */
  public static String getCentralPhone(String triphoneLabel) {
    String[] phones = fromTriphoneToPhones(triphoneLabel);
    if (phones != null) {
       return phones[1];
    } else {
      return null;
    }
  }

  /**
   * If triphone, returns central phone
   * If monophone, returns input label
   * otherwise, returns null.
   */
  public static String getCentralPhoneIfTriphone(String possibleTriphoneLabel) {
    String[] phones = fromTriphoneToPhones(possibleTriphoneLabel);
    if (phones != null) {
       return phones[1];
    } else {
      return possibleTriphoneLabel;
    }
  }

  public static String getRightContextPhone(String triphoneLabel) {
    String[] phones = fromTriphoneToPhones(triphoneLabel);
    if (phones != null) {
       return phones[2];
    } else {
      return null;
    }
  }

  public static String getLeftContextPhone (String triphoneLabel) {
    String[] phones = fromTriphoneToPhones(triphoneLabel);
    if (phones != null) {
       return phones[0];
    } else {
      return null;
    }
  }

  public boolean isLabelInTable(String label) {
    Integer index = (Integer) m_hashtable.get(label);
    if (index == null) {
      return false;
    }
    return true;
//		for (int i=0; i<m_table.length; i++) {
//			for (int j=0; j<m_table[i].length; j++) {
//				if (label.equals(m_table[i][j])) {
//					return true;
//				}
//			}
//		}
//		return false;
  }

  /**Get the number of entries of this table.
   */
  public int getNumberOfEntries() {
    if (m_table != null) {
      return m_table.length;
    } else {
      return 0;
    }
  }

  /**Get the number of labels of specified entry.
   */
  public int getNumberOfLabels(int nentry) {
    if (m_table != null) {
      return m_table[nentry].length;
    } else {
      return 0;
    }
  }

  public String toString() {
    if (m_type == null || m_table == null) {
      return null;
    }
    StringBuffer out = new StringBuffer(
          "TableOfLabels.Type = " + m_type.toString() + IO.m_NEW_LINE +
          "TableOfLabels.nnumberOfEntries = " + this.getNumberOfEntries() );

    for (int i=0; i<this.getNumberOfEntries(); i++) {
      out.append(IO.m_NEW_LINE + "TableOfLabels.Entry_" + i + " =");
      for (int j=0; j<m_table[i].length; j++) {
        out.append(" " + m_table[i][j]);
      }
    }
    return out.toString();
  }

  /**Creates a copy of this TableOfLabels.
   */
  public Object clone() {
    TableOfLabels tableOfLabels = new TableOfLabels(m_table);
    //Type.GENERIC was assumed, so correct the type
    tableOfLabels.m_type = this.m_type;
    return tableOfLabels;
  }

  /**Creates the table with TIMIT labels without including glottal stop q,
   * with a total of 60 labels.
   */
  private void createTIMIT60LabelsTable() {
    m_type = Type.TIMIT60;
    m_table = new String[60][];
    for (int i=0; i<60; i++) {
      m_table[i] = new String[1]; //only 1 label per entry in this case
    }
    //m_table[60] = new String[2]; //h#
    //m_table[60][1] = "h#";
    String phonemes[] = {"iy","ih","ix","eh","ae","ax","ah","axh",
      "uw","ux","uh","aa","ao","ey","ay","oy","aw","ow","er","axr",
      "l","el","r","w","y","m","em","n","en","nx","ng","eng","dx",
      "jh","ch","z","s","sh","zh","hh","hv","v","f","dh","th",
      "b","p","d","t","g","k","bcl","pcl","dcl","tcl","gcl","kcl",
      "epi","pau","h#"};

    for (int i=0; i<m_table.length; i++) {
      m_table[i][0] = phonemes[i];
    }
  }

  /**Creates ISIP41 table, used in their wsj tutorial. Should provide
   * baseline for direct comparison of decoders. Contains 41 distinct
   * phones.
   */
  private void createISIP41LabelsTable() {
    m_type = Type.ISIP41;
    m_table = new String[41][];
    for( int i=0; i<41; i++ ) {
      m_table[i] = new String[1];
    }
    String phonemes[] = {"sp","sil","aa","ae","ah","ao","aw","ay",
                   "b","ch","d","dh","eh","er","ey","f","g",
             "hh","ih","iy","jh","k","l","m","n","ng",
             "ow","oy","p","r","s","sh","t","th","uh",
             "uw","v","w","y","z","zh"};

    for( int i=0; i<41; i++ ) {
      m_table[i][0] = phonemes[i];
    }
  }

  /**Creates the table with 16 TIMIT vowels.
   */
  private void createTIMITVOWELSLabelsTable() {
    m_type = Type.TIMITVOWELS;
    m_table = new String[16][1];
    //not included in paper by Zahorian:
    //"ix","ax","axh","axr",
    String phonemes[] = {
      //monopthongs
      "iy","ih","eh","ey","ae","aa","ah",
      "ao","ow","uh","ux","er","uw",
      //dipthongs
      "ay","oy","aw"};
    for (int i=0; i<m_table.length; i++) {
      m_table[i][0] = phonemes[i];
    }
  }

  /**Creates the table with 61 TIMIT labels (includes glottal stop q).
   */
  private void createTIMIT61LabelsTable() {
    m_type = Type.TIMIT61;
    m_table = new String[61][];
    for (int i=0; i<61; i++) {
      m_table[i] = new String[1]; //only 1 label per entry in this case
    }
    //m_table[60] = new String[2]; //h#
    //m_table[60][1] = "h#";
    String phonemes[] = {"iy","ih","ix","eh","ae","ax","ah","axh",
      "uw","ux","uh","aa","ao","ey","ay","oy","aw","ow","er","axr",
      "l","el","r","w","y","m","em","n","en","nx","ng","eng","dx",
      "jh","ch","z","s","sh","zh","hh","hv","v","f","dh","th",
      "b","p","d","t","g","k","bcl","pcl","dcl","tcl","gcl","kcl",
      "q","epi","pau","h#"};

    for (int i=0; i<m_table.length; i++) {
      m_table[i][0] = phonemes[i];
    }

  }

  /**Creates the conventional table with the 61 TIMIT labels
   * collapsed to 39 labels.
   */
  private void createTIMIT39LabelsTable() {
    m_type = Type.TIMIT39;

    //use auxiliar variable because Java sintax doesn't allow
    //to initialize m_table this way
    String[][] table = {{"iy"},
           {"ih","ix"},
           {"eh"},
           {"ae"},
           {"ax","ah","axh"},
           {"uw","ux"},
           {"uh"},
           {"aa","ao"},
           {"ey"},
           {"ay"},
           {"oy"},
           {"aw"},
           {"ow"},
           {"er","axr"},
           {"l","el"},
           {"r"},
           {"w"},
           {"y"},
           {"m","em"},
           {"n","en","nx"},
           {"ng","eng"},
           {"dx"},
           {"jh"},
           {"ch"},
           {"z"},
           {"s"},
           {"sh","zh"},
           {"hh","hv"},
           {"v"},
           {"f"},
           {"dh"},
           {"th"},
           {"b"},
           {"p"},
           {"d"},
           {"t"},
           {"g"},
           {"k"},
           {"h#","bcl","pcl","dcl","tcl","gcl","kcl","q","epi","pau"}};

    m_table = table;
  }

//	/**Creates the conventional table with the 61 TIMIT labels
//	 * collapsed to 46 labels.
//	 */
//	private void createTIMIT46LabelsTable() {
//		m_type = Type.TIMIT46;
//		m_table = new String[46][];
//		for (int i=0; i<m_table.length; i++) {
//			m_table[i] = new String[1];
//		}
//
//		//now increase size according to table:
//		m_table[5] = new String[2]; //ax-h
//		m_table[7] = new String[2]; //ux
//		m_table[16] = new String[2]; //axr
//		m_table[17] = new String[2]; //el
//		m_table[21] = new String[2]; //em
//		m_table[22] = new String[3]; //en,nx
//		m_table[23] = new String[2]; //eng
//		m_table[31] = new String[2]; //hv
//		m_table[42] = new String[5]; //pcl, tcl, kcl, q
//		m_table[43] = new String[4]; //bcl, dcl, gcl
//		m_table[44] = new String[2]; //epi
//		m_table[45] = new String[2]; //h#
//
//		String firstPhoneme[] = {"iy","ih","ix","eh","ae","ah",
//			"ax","uw","uh","aa","ao","ey","ay","oy","aw","ow",
//			"er","l","r","w","y","m","n","ng","dx","jh","ch",
//			"z","s","sh","zh","hh","v","f","dh","th","b","p",
//			"d","t","g","k","cl","vcl","pau","sil"};
//
//		//copy first label of each entry
//		for (int i=0; i<46; i++) {
//			m_table[i][0] = firstPhoneme[i];
//			//Print.dialog(i + " " + firstPhoneme[i]);
//		}
//		//now, complete table
//		m_table[5][1] = "axh";
//		m_table[7][1] = "ux";
//		m_table[17][1] = "el";
//		m_table[16][1] = "axr";
//		m_table[21][1] = "em";
//		m_table[22][1] = "en";
//		m_table[22][2] = "nx";
//		m_table[23][1] = "eng";
//		m_table[31][1] = "hv";
//		m_table[42][1] = "pcl";
//		m_table[42][2] = "tcl";
//		m_table[42][3] = "kcl";
//		m_table[42][4] = "q";
//		m_table[43][1] = "bcl";
//		m_table[43][2] = "dcl";
//		m_table[43][3] = "gcl";
//		m_table[44][1] = "epi";
//		m_table[45][1] = "h#";
//	}

  //old table
//	private void createTIMIT48LabelsTable() {
//		m_type = Type.TIMIT48;
//		m_table = new String[48][];
//		for (int i=0; i<m_table.length; i++) {
//			m_table[i] = new String[1];
//		}
//		//now increase size according to table:
//		m_table[9] = new String[3]; //dcl,gcl
//		m_table[17] = new String[2]; //axr
//		m_table[21] = new String[2]; //hv
//		m_table[28] = new String[2]; //em
//		m_table[29] = new String[2]; //nx
//		m_table[30] = new String[2]; //eng
//		m_table[34] = new String[4]; //epi,h#,#h
//		m_table[35] = new String[3]; //tcl,kcl
//		m_table[42] = new String[2]; //ux
//
//		String phonemes[] = { "aa","ae","ah","ao","aw","ax","axh","axr","ay","b",
//                          "bcl","ch","d","dcl","dh","dx","eh","el","em","en",
//                          "eng","epi","er","ey","f","g","gcl","hh","hv","ih",
//                          "ix","iy","jh","k","kcl","l","m","n","ng","nx","ow",
//                          "oy","p","pau","pcl","q","r","s","sh","t","tcl",
//                          "th","uh","uw","ux","v","w","y","z","zh"};
//
//		String firstPhoneme[] = {"aa","ae","ah","ao","aw","ax",
//								 "axh","ay","b","bcl","ch", //-1
//								 "d","dh","dx","eh","el","en",
//								 /*"epi",*/"er","ey","f","g","hh",//-1
//								 "ih","ix","iy","jh","k","l",
//								 "m","n","ng","ow","oy","p","pau","pcl", //+2
//								 "r","s","sh","t","th","uh",
//								 "uw","v","w","y","z","zh"};
//
//		//copy first label of each entry
//		for (int i=0; i<m_table.length; i++) {
//			m_table[i][0] = firstPhoneme[i];
//		}
//
//		//now, complete table
//		//now increase size according to table:
//		m_table[9][1] = "dcl";
//		m_table[9][2] = "gcl";
//		m_table[17][1] = "axr";
//		m_table[21][1] = "hv";
//		m_table[28][1] = "em";
//		m_table[29][1] = "nx";
//		m_table[30][1] = "eng";
//		m_table[35][1] = "tcl";
//		m_table[35][2] = "kcl";
//		m_table[34][1] = "epi";
//		m_table[34][2] = "h#";
//		m_table[34][3] = "#h";
//		m_table[42][1] = "ux";
//	}


  /**Creates the conventional table with the 61 TIMIT labels
   * collapsed to 47 labels according to S. Sandhu and O. Ghitza,
   * ICASSP'95, "A Comparative study of mel cepstra and EIH for
   * phone classification under adverse conditions".
   */
  private void createTIMIT47LabelsTable() {
    m_type = Type.TIMIT47;
    //use auxiliar variable because Java sintax doesn't allow
    //to initialize m_table this way
    String[][] table = {{"iy"},
           {"ih"},
           {"eh"},
           {"ae"},
           {"ax","axh","ix"},
           {"ah"},
           {"uw","ux"},
           {"uh"},
           {"aa"},
           {"ao"},
           {"ey"},
           {"ay"},
           {"oy"},
           {"aw"},
           {"ow"},
           {"er"},
           {"axr"},
           {"l"},
           {"el"},
           {"r"},
           {"w"},
           {"y"},
           {"m"},
           {"em"},
           {"n"},
           {"nx"},
           {"en"},
           {"ng","eng"},
           {"dx"},
           {"jh"},
           {"ch"},
           {"z"},
           {"s"},
           {"sh"},
           {"zh"},
           {"hh","hv"},
           {"v"},
           {"f"},
           {"dh"},
           {"th"},
           {"b","bcl"},
           {"p","pcl"},
           {"d","dcl"},
           {"t","tcl"},
           {"g","gcl"},
           {"k","kcl"},
           {"h#","epi","pau","q"}}; //

    m_table = table;
  }

  /**Creates the conventional table with the 61 TIMIT labels
   * collapsed to 48 labels.
   */
  private void createTIMIT48LabelsTable() {
    m_type = Type.TIMIT48;
    //use auxiliar variable because Java sintax doesn't allow
    //to initialize m_table this way
    String[][] table = {{"iy"},
           {"ih"},
           {"ix"},
           {"eh"},
           {"ae"},
           {"ax","axh"},
           {"ah"},
           {"uw","ux"},
           {"uh"},
           {"aa"},
           {"ao"},
           {"ey"},
           {"ay"},
           {"oy"},
           {"aw"},
           {"ow"},
           {"er"},
           {"axr"},
           {"l"},
           {"el"},
           {"r"},
           {"w"},
           {"y"},
           {"m","em"},
           {"n","nx"},
           {"en"},
           {"ng","eng"},
           {"dx"},
           {"jh"},
           {"ch"},
           {"z"},
           {"s"},
           {"sh"},
           {"zh"},
           {"hh","hv"},
           {"v"},
           {"f"},
           {"dh"},
           {"th"},
           {"b"},
           {"p"},
           {"d"},
           {"t"},
           {"g"},
           {"k"},
           {"bcl","dcl","gcl"}, //"vcl" voiced
           {"pcl","tcl","kcl"}, //"cl" unvoiced
           {"h#","epi","pau","q"}}; //,"sil"

    m_table = table;
  }

        //this needs to be in sync with TIDigitsPathOrganizer
  //private static final String[][] m_table = {{"z"},{"1"},{"2"},{"3"},
  //	{"4"},{"5"},{"6"},{"7"},{"8"},{"9"},{"o"}};
  private void createTIDIGITSTable() {
    //only add sil, pause, etc after the 11 numeric entries
    //other classes (HTKToolsCaller) rely on this ordering
    String[][] table = {{"zero"},{"one"},{"two"},{"three"},
    {"four"},{"five"},{"six"},{"seven"},{"eight"},{"nine"},{"oh"},
    /*{"sp"},*/{"sil","pau"}};
    m_table = table;
  }


  /**
 * Inner class that represents the type of TableOfLabel's.
 */
  public static class Type implements Serializable {
//  public static class Type extends SuperType implements Serializable {

    protected String m_strName;

    public static final Type	TIMIT39 = new Type("TIMIT39");
    public static final Type	TIMIT47 = new Type("TIMIT47");
    public static final Type	TIMIT48 = new Type("TIMIT48");
    public static final Type	TIMIT61 = new Type("TIMIT61");
    public static final Type	GENERIC = new Type("GENERIC");
    public static final Type	TIMIT60 = new Type("TIMIT60");
    public static final Type	TIMITVOWELS = new Type("TIMITVOWELS");
    public static final Type	TIDIGITS = new Type("TIDIGITS");
    public static final Type    ISIP41 = new Type("ISIP41");
    public static final Type    TRIPHONE = new Type("TRIPHONE");

    /**
	 * @uml.property  name="m_types"
	 * @uml.associationEnd  multiplicity="(0 -1)"
	 */
    protected static Type[] m_types;

    //In case of adding a new Type (above), don't forget to add it below.
    static {
      m_types = new Type[10];
      m_types[0] = GENERIC;
      m_types[1] = TIMIT39;
      m_types[2] = TIMIT47;
      m_types[3] = TIMIT48;
      m_types[4] = TIMIT60;
      m_types[5] = TIMIT61;
      m_types[6] = TIMITVOWELS;
      m_types[7] = TIDIGITS;
      m_types[8] = ISIP41;
      m_types[9] = TRIPHONE;
    }

    //notice the constructor is 'protected', not public.
    protected Type(String strName) {
      //m_strName is defined in superclass
      m_strName = strName;
    }

    /**Return true if the input String is equal
     * (case sensitive) to the String that represents
     * one of the defined Type's.
     */
    public static boolean isValid(String typeIdentifierString) {
      for (int i=0; i<m_types.length; i++) {

        //notice I am using case sensitive comparation
        //System.out.println("typeIdentifierString = "+typeIdentifierString);
        //System.out.println("m_types[i].toString() = "+m_types[i].toString());
        if (typeIdentifierString.equals(m_types[i].toString())) {
          return true;
        }
      }
      return false;
    }

    /**Return the index of m_types element that matches
     * the input String or -1 if there was no match.
     */
    private static int getTypeIndex(String typeIdentifierString) {
      for (int i=0; i<m_types.length; i++) {
        //notice I am using case sensitive comparation
        if (typeIdentifierString.equals(m_types[i].toString())) {
          return i;
        }
      }
      return -1;
    }

    /**Return the Type correspondent to the given indentifier
     * or GENERIC if not found
    */
    //it was not declared in SuperType because I wanted to
    //return this specif Type not a SuperType and didn't know how
    //to do the casting from the superclass... Is that possible ?
    public static final Type getType(String typeIdentifierString) {
      if (isValid(typeIdentifierString)) {
        int nindex = getTypeIndex(typeIdentifierString);
        return m_types[nindex];
      } else {
        //return generic
        int nindex = getTypeIndex("GENERIC");
        return m_types[nindex];
      }
    }

    /**Return the Type correspondent to the given indentifier
    * String or exit in case identifier is not valid.
    */
    //it was not declared in SuperType because I wanted to
    //return this specif Type not a SuperType and didn't know how
    //to do the casting from the superclass... Is that possible ?
//		public static final Type getTypeAndExitOnError(
//                                      String typeIdentifierString) {
//			if (isValid(typeIdentifierString)) {
//				int nindex = getTypeIndex(typeIdentifierString);
//				return m_types[nindex];
//			} else {
//				End.throwError( typeIdentifierString +
//                        " is not a valid TableOfLabels type.");
//				//make compiler happy:
//				return null;
//			}
//		}
  }
} // end of class
