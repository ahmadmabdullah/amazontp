package amazontp.io;

import java.io.*;
import java.util.StringTokenizer;

import amazontp.genfaults.GenRandomicFaults;
import amazontp.util.CountUtils;

/**
 * <p>
 * Title: Load ATP Output File
 * </p>
 * <p>
 * Description: Loads all data from Alternative Transient Program (ATP) ascii
 * output file (pl4 file converted in ascii from the startup file of ATP) to
 * build an equivalent ARFF file (used in Weka).
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: LaPS - UFPA
 * </p>
 * 
 * @author Aldebaro Klautau, Andr�ia Santos, Gustavo Guedes, Jos� Borges and
 *         Yomara Pinheiro, Jefferson Morais
 * @version 1.0
 */

public class LoadATPOutputFile {

	private static final String m_NEW_LINE;

	static {
		m_NEW_LINE = System.getProperties().getProperty("line.separator");
	}

	/**
	 * @todo Jose, vamos usar um metodo static ao inves do construtor aqui. E
	 *       vamos reorganizar a classe, pois agora farei um outro metodo que
	 *       faz algo semelhante ao abaixo
	 */
	public LoadATPOutputFile(String fileName, double T, double tfaultStart,
			double tfaultEnd, String faultLabel, double decimationFactor,
			BufferedWriter bufferedWriter, int windowLength, int simulationIndex)
			throws Exception {

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));
		
		//ak
		System.out.println(fileName);
		
		String line = bufferedReader.readLine();
		if (line == null) {

			System.err.println("Null Line");

			// In case of sync problems.
			Thread.sleep(1000);
			bufferedReader = new BufferedReader(new FileReader(fileName));
			line = bufferedReader.readLine();
		}

		// JB: Get date and time of simulation and, if wanted, some unknown
		// parameter.(O modo como se encontra o Nskip � certeza ???)

		// AK: get date, time, ?, ?, N, ...
		// where N is the number of "numbers" we need to skip after the first
		// blank

		String[] tokens = splitLine(line);
		System.err.println("Date " + tokens[0]);
		System.err.println("Time " + tokens[1]);

		// System.err.println("unknown parameter ?? = " + tokens[2]);
		// System.err.println("unknown parameter ?? = " + tokens[3]);

		// Identifying the first part of the header.
		while ((line = bufferedReader.readLine()) != null) {
			//ak
			System.out.println(line);
			if (line.equals("")) {
				System.err
						.println("Guess I found the end of the first part of the header.");
				break;
			}
		}

		line = bufferedReader.readLine();
		StringTokenizer stringTokenizer = new StringTokenizer(line);
		int N = stringTokenizer.countTokens();

		while ((line = bufferedReader.readLine()).length() > 8
				&& Double.parseDouble(line.substring(0, 8)) != 0) {
			stringTokenizer = new StringTokenizer(line);
			N += stringTokenizer.countTokens();
		}

		if (N > 18) {
			System.err
					.println("Please specify less than 9 signals to output in ATP file.");
			System.exit(1);
		}

		// Will break "while" exactly in first line, so we will count the
		// numberOfSignals.
		stringTokenizer = new StringTokenizer(line);
		int nnumberOfSignals = stringTokenizer.countTokens();

		if (simulationIndex == 0) {

			StringBuffer stringBuffer = new StringBuffer("@relation master");
			stringBuffer.append(m_NEW_LINE);

			for (int j = 0; j < windowLength * (nnumberOfSignals - 1); j++) {
				stringBuffer.append("@attribute P" + (j + 1) + " real"
						+ m_NEW_LINE);
			}
			stringBuffer
					.append("@attribute class {AT,BT,CT,AB,AC,BC,ABC,ABT,ACT,BCT,ABCT}"
							+ m_NEW_LINE);
			stringBuffer.append("@data" + m_NEW_LINE);
			bufferedWriter.write(stringBuffer.toString());

		}

		// "allSamples" is an array that storages all the samples of the
		// simulation, while "samples" is an array that collects only samples of
		// a single line.

		double[] allSamples = new double[nnumberOfSignals];
		int ncurrentSample = 0;
		double[] samples = splitLineAsDoubles(line);

		// Check if the simulation time starts at zero. (Logical Condition)
		if (samples[0] != 0) {
			System.err
					.println("Error in logic, first sample time different than zero, equal to "
							+ samples[0]);
		}

		// Storage "samples" information in "allSamples"
		for (int i = 0; i < samples.length; i++) {
			allSamples[i + ncurrentSample] = samples[i];

		}

		ncurrentSample += samples.length;

		int ln = 1;
		double dcurrentTime = 0;
		int decimatedLine = 0;

		String[] labels = new String[windowLength];

		String tempFile = GenRandomicFaults.atpDir + "temp.txt";

		BufferedWriter tempBufferedWriter = new BufferedWriter(new FileWriter(
				tempFile));

		while ((line = bufferedReader.readLine()) != null) {

			samples = splitLineAsDoubles(line);

			if (ncurrentSample >= nnumberOfSignals) {
				// Already have collected all signals for this specific time
				// location
				if (ln % decimationFactor != 0) {
					dcurrentTime += T;
					ln++;
					continue;
				}

				// A time step of T in which a non-zero percentage of fault
				// happens, the label is classified as a fault.
				if ((dcurrentTime > tfaultStart && dcurrentTime < tfaultEnd)
						|| ((dcurrentTime - T) > tfaultStart && (dcurrentTime - T) < tfaultEnd)) {

					labels[decimatedLine % windowLength] = faultLabel;

				} else {
					labels[decimatedLine % windowLength] = "OK";

				}

				ncurrentSample = 0;

				tempBufferedWriter.write("" + allSamples[1]);

				for (int i = 2; i < allSamples.length; i++) {

					tempBufferedWriter.write("," + allSamples[i]);
				}

				if (decimatedLine % windowLength == windowLength - 1) {

					int numberOfOK = 0;

					for (int i = 0; i < labels.length; i++) {

						if (labels[i].equals("OK")) {
							numberOfOK++;
						}

					}

					if (numberOfOK <= labels.length / 2) {

						tempBufferedWriter.write("," + faultLabel);
					} else {
						tempBufferedWriter.write("," + "OK");
					}

					tempBufferedWriter.newLine();

				} else {

					tempBufferedWriter.write(",");

				}

				if (ln == 2) {
					// second time location
					if (allSamples[0] != T) {
						System.err.println("Error in logic: T = " + T
								+ " is different than " + allSamples[0]);
					}
				}

				dcurrentTime += T;
				ln++;
				decimatedLine++;
			}

			for (int i = 0; i < samples.length; i++) {
				// System.out.println("i = " + i + " , ncurrentSample = " +
				// ncurrentSample);
				allSamples[i + ncurrentSample] = samples[i];
			}

			ncurrentSample += samples.length;

		}

		tempBufferedWriter.close();

		BufferedReader tempBufferedReader = new BufferedReader(new FileReader(
				tempFile));

		int nTempFileLines = CountUtils.countNumberOfLinesInFile(tempFile);

		String tempLine;

		for (int i = 0; i < nTempFileLines; i++) {

			tempLine = tempBufferedReader.readLine();

			if (tempLine.indexOf("OK") == -1) {
				bufferedWriter.write(tempLine);
				bufferedWriter.newLine();
			}

		}

		tempBufferedReader.close();

		System.err.println("Found " + ln + " time locations");
		System.err.println("Total simulation time = " + ((ln - 1) * T));
		System.err.println("Total simulation time = " + dcurrentTime);
		bufferedReader.close();

	}

	private static String[] splitLine(String line) {

		StringTokenizer stringTokenizer = new StringTokenizer(line);

		int N = stringTokenizer.countTokens();
		String[] x = new String[N];
		for (int i = 0; i < N; i++) {
			x[i] = stringTokenizer.nextToken();
		}
		return x;
	}

	private static double[] splitLineAsDoubles(String line) {

		if (line.indexOf("-100000.") != -1) { // Execute only when the line
			// has the -100000. bug

			// System.out.println("Bug in line: = " + line);
			StringTokenizer stringTokenizer = new StringTokenizer(line);
			int N = stringTokenizer.countTokens() + 1;
			double[] x = new double[N];

			for (int i = 0; i < N; i++) {

				String token = stringTokenizer.nextToken();

				if (token.length() > 7) { // The bug time

					String token1 = token.substring(0, token.length() - 8);
					String token2 = token.substring(token.length() - 8, token
							.length() - 1);
					// System.out.println("Bug Solved: TOKEN 1 = " + token1
					// + " TOKEN 2 = " + token2);
					x[i] = Double.parseDouble(token1);
					x[i + 1] = Double.parseDouble(token2);
					i++;
				}

				else {
					x[i] = Double.parseDouble(token);

				}

			}

			return x;

		}

		StringTokenizer stringTokenizer = new StringTokenizer(line);
		int N = stringTokenizer.countTokens();
		double[] x = new double[N];

		for (int i = 0; i < N; i++) {

			String token = stringTokenizer.nextToken();
			x[i] = Double.parseDouble(token);

		}

		return x;
	}

	/**
	 * @todo Organize this class. Notar a coincidencia do abaixo com o
	 *       construtor.
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	// IMPortaNTE: vou assumir que ha apenas 6 sinais e que todos cabem em uma
	// linha. O construtor permite ateh 9 sinais.
	public static long countNumberOfWaveformLines(String fileName)
			throws Exception {

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));

		// Identifying and skiping the first part of the header.
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			if (line.equals("")) {
				// Guess I found the end of the first part of the header.
				break;
			}
		}
		line = bufferedReader.readLine();
		line = bufferedReader.readLine();

		// from now on, all the lines are 7 samples of "waveforms"
		long numberOfLines = 0;
		while ((line = bufferedReader.readLine()) != null) {
			numberOfLines++;
		}
		bufferedReader.close();

		return numberOfLines;
	}

	/**
	 * @todo Organize this class. Notar a coincidencia do abaixo com o
	 *       construtor.
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	// IMPortaNTE: vou assumir que ha apenas 6 sinais e que todos cabem em uma
	// linha. O construtor permite ateh 9 sinais.
	public static float[][] parseATPOutputFile(String fileName)
			throws Exception {

		int numberOfLines = (int) countNumberOfWaveformLines(fileName);
		float[][] output = new float[7][numberOfLines];

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));

		// Identifying and skiping the header.
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			if (line.equals("")) {
				// Guess I found the end of the first part of the header.
				break;
			}
		}
		line = bufferedReader.readLine();
		line = bufferedReader.readLine();

		// from now on, all the lines are 7 samples of "waveforms"
		for (int i = 0; i < numberOfLines; i++) {
			line = bufferedReader.readLine();
			double[] x = splitLineAsDoubles(line);
			for (int j = 0; j < x.length; j++) {
				output[j][i] = (float) x[j];
			}
		}
		return output;
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 4) {
			System.err
					.println("Usage: <file name> <total # of signals (look at the PL4)> <# of first signals want to keep> <sampling frequency>");
			System.exit(1);
		}
	}

}