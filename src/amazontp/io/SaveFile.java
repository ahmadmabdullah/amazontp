package amazontp.io;

import java.io.*;

public class SaveFile {
	public SaveFile() {
	}

	public void createFile(String nome) throws IOException {
		try {
			File file = new File(nome);
			boolean success = file.createNewFile();
			if (success) {
				// System.out.println("Arquivo Criado com Sucesso");
			} else {
				// System.out.println("Arquivo j� Existe");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveFile(String nome, String conteudo) throws IOException {
		try {
			BufferedWriter saida = new BufferedWriter(
					new FileWriter(nome, true));
			saida.write(conteudo);
			saida.newLine();
			saida.close();
		} catch (Exception e) {
			System.out.println("Houve um erro na gera��o do arquivo");
		}
	}
	public void SaveFileContents(String nome, String conteudo) throws IOException {
		try {
			BufferedWriter saida = new BufferedWriter(
					new FileWriter(nome, true));
			String[] line = conteudo.split("\n"); 
			System.err.println("line length: "+line.length);
			for (int i = 0; i < line.length; i++) {
				saida.write(line[0]);
				saida.newLine();				
			}
			saida.close();
		} catch (Exception e) {
			System.out.println("Houve um erro na gera��o do arquivo");
		}
	}
	

	public void deleteFile(String nome) throws IOException {
		try {
			File file = new File(nome);
			file.delete();
		} catch (Exception e) {
			System.out.println("Houve um erro na exclus�o do arquivo");
		}

	}
}
