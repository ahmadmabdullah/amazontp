package amazontp.io;

import java.util.*;
import java.io.*;

/**Constructs a list with the full path of files below
 * a given directory and with specified file extension.
 * Note that the file extension is not case sensitive.
 * The class is very slow when the number of included files
 * is large as, for example, 4000 (TIMIT training sequence)
 * probably because it stores the String's in a Vector.
 */
public final class DirectoryTree {

	private File root;
	private Vector filePaths = new Vector();
	private int curFile = 0;
	private FilenameFilter m_filenameFilter;
	private static String m_pathSeparator;

	static {
		//m_pathSeparator = System.getProperties().getProperty("file.separator");
		//internally, we are going to deal only with "/" as path separator
		m_pathSeparator = "/";
	}

	/**Intitialize DirectoryTree starting from directory provided
	 * and filtering by file_extension.
	 */
	public DirectoryTree(String root_directory, String file_extension) {

		FileNamesAndDirectories.forceEndingWithSlash(root_directory);

		root = new File(root_directory);
		//System.out.println(root_directory);

		m_filenameFilter = new FileExtensionFilter(file_extension);

		compileFileList(root, file_extension);
	}

	/**Intitialize DirectoryTree starting from directory provided
	 * and filtering by file name
	 */
	public DirectoryTree(String root_directory, String fileName, boolean ofilterByFileName) {

		FileNamesAndDirectories.forceEndingWithSlash(root_directory);

		root = new File(root_directory);
		//System.out.println(root_directory);

		m_filenameFilter = new SameNameFilter(fileName);

		compileFileList(root, fileName);
	}

	/**Recursively looks for files skipping subdirectories.
	 */
	public DirectoryTree(String root_directory) {

		FileNamesAndDirectories.forceEndingWithSlash(root_directory);

		root = new File(root_directory);
		//files = dir.list(new DirFilter());
		//System.out.println(root_directory);
		compileFileList(root);
	}

	/**Intitialize DirectoryTree starting from directory provided
	 * and getting only subdirectories of root, without recursion
	 * through the subdirectories.
	 */
	public DirectoryTree(String root_directory, boolean ogetDirectoriesWithoutRecursion) {

		FileNamesAndDirectories.forceEndingWithSlash(root_directory);
		root = new File(root_directory);

		m_filenameFilter = new DirFilter();
		String [] files = root.list(m_filenameFilter);
		if (files != null) {
			for(int i = 0; i < files.length; i++) {
				//System.out.println(files[i]);
				filePaths.addElement(FileNamesAndDirectories.replaceBackSlashByForward(root.getPath()) + m_pathSeparator + files[i]);
			}
		}
	}

	/**Recursively search directory tree and add files with a given
	 * file extension to filePaths Vector.
	 */
	private void compileFileList(File dir, String fileNameOrExtension) {
		String [] files = dir.list(m_filenameFilter);
		if (files != null) {
			for(int i = 0; i < files.length; i++) {
				//System.out.println(files[i]);
				filePaths.addElement(FileNamesAndDirectories.replaceBackSlashByForward(dir.getPath()) + m_pathSeparator + files[i]);
			}
		}

		files = dir.list(new DirFilter());
		if (files != null) {
			for(int i = 0; i < files.length; i++) {
				compileFileList(new File(FileNamesAndDirectories.replaceBackSlashByForward(dir.getPath()) + m_pathSeparator + files[i]), fileNameOrExtension);
			}
		}
	}

	/**Recursively search directory tree and add files to filePaths Vector.
	 */
	private void compileFileList(File dir) {
		//String pathSeparator = System.getProperties().getProperty("file.separator");
		String [] files = dir.list();
		if (files != null) {
			for(int i = 0; i < files.length; i++) {
				String fullPath = FileNamesAndDirectories.replaceBackSlashByForward(dir.getPath()) + m_pathSeparator + files[i];
				//add only files
				if (!new File(fullPath).isDirectory()) {
					//System.out.println(files[i]);
					filePaths.addElement(fullPath);
				}
			}
		}

		files = dir.list(new DirFilter());
		if (files != null) {
			for(int i = 0; i < files.length; i++) {
				compileFileList(new File(FileNamesAndDirectories.replaceBackSlashByForward(dir.getPath()) + m_pathSeparator + files[i]));
			}
		}
	}

	/**Return next file in filePaths, or null if at the end.
	 */
	public String nextFile()
	{
		if(curFile == filePaths.size())
			return null;
		return (String)filePaths.elementAt(curFile++);
	}

	/**Returns a Vector with the list of files as Strings.
	 */
	public Vector getFiles() {
		return (Vector)filePaths.clone();
	}

	public int getNumberOfPaths() {
		if (filePaths == null) {
			return 0;
		}
		return filePaths.size();
	}

	public String[] getFilesAsStrings() {
		int nnumberOfFiles = filePaths.size();
		String[] files = new String[nnumberOfFiles];
		for (int i = 0; i < nnumberOfFiles; i++) {
			files[i] = (String) filePaths.elementAt(i);
		}
		return files;
	}

	/**Writes this DirectoryTree to an ASCII file.
	 */
	public void writeToFile(String filename) {
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename));
			for (int i=0; i<this.filePaths.size(); i++) {
				String s = (String) filePaths.elementAt(i);
				bufferedWriter.write(s);
				bufferedWriter.newLine();
			}
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**Inner class to filter files by their extension (ignore case, ex: WAV,wav,WaV).
	 */
	class FileExtensionFilter implements FilenameFilter {
		String m_extension;

		public FileExtensionFilter(String extension) {
			m_extension = extension.toLowerCase();
		}

		public boolean accept(File dir, String name) {
			return (name.toLowerCase().endsWith("." + m_extension));
		}
	}

	/**Inner class to filter files by their name, ignoring case.
	 */
	class SameNameFilter implements FilenameFilter {
		String m_fileName;

		public SameNameFilter(String fileName) {
			m_fileName = fileName;
		}

		public boolean accept(File dir, String name) {
			return (name.equalsIgnoreCase(m_fileName));
		}
	}

	/**Inner class to filter directories.
	 */
	class DirFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			File temp = new File(dir.getPath() + m_pathSeparator + name);
			return temp.isDirectory();
		}
	}
}