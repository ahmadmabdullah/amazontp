package amazontp.io;

import java.awt.image.BufferStrategy;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;
import java.util.StringTokenizer;

import amazontp.matlab.Cloner;

import amazontp.matlab.*;

public class PreProcessor {

	private static boolean m_odebug = false;

	private static boolean m_onlyEvent = false;

	private static String[] m_tempBuffer = new String[52];

	private static String m_NEW_LINE;

	private static final int m_nwindows = 5;

	private static final int m_nsignals = 6;

	private static double deltaT = 4e4d;

	static {

		m_NEW_LINE = System.getProperties().getProperty("line.separator");

	}

	public static void main(String[] args) throws Exception {
		if (args.length != 6) {

			System.out
					.println("Usage: <list in directory> <list out directory> <RSRdb> <decimate factor> <number of cycles> <true for only fault><true for debug>");

			System.exit(1);

		}

		String directory = args[0];
		int rsrdb = Integer.parseInt(args[1]);
		int dfactor = Integer.parseInt(args[2]);
		int ncycles = Integer.parseInt(args[3]);
		m_onlyEvent = Boolean.parseBoolean(args[4]);
		m_odebug = Boolean.parseBoolean(args[5]);

		directory = FileNamesAndDirectories
				.replaceAndForceEndingWithSlash(directory);

		String labelsFile = directory + "labels.txt";

		if (!new File(labelsFile).exists()) {

			System.out.println(labelsFile + " does not exist!");

			System.exit(1);

		}

		BufferedReader bufferedReader = new BufferedReader(new FileReader(

		labelsFile));

		String line = null;

		int nfileCounter = 0;

		SimulationInfo previousSimulationInfo = new SimulationInfo();

		previousSimulationInfo.m_fileName = "";

		/*
		 * BufferedWriter bufferedwriter = new BufferedWriter(new FileWriter(
		 * directory+"tramoeste2_validacao080207.arff"));
		 */

		while ((line = bufferedReader.readLine()) != null) {

			if (line.equals("")) {

				break;

			}

			System.out.println(line);

			SimulationInfo simulationInfo = parseLine(line);

			if (simulationInfo.m_fileName

			.equals(previousSimulationInfo.m_fileName)) {

				// deal with the bug: delete the previous files and decrement

				// the counter

				nfileCounter--;

				String temp = directory + previousSimulationInfo.toString()
						+ "_" + nfileCounter + ".bin";
				System.out.println("temp: " + temp);
				if (new File(temp).delete()) {

					System.out.println("Deleted " + temp);

				}

			}

			preProcessorOneFile(directory, simulationInfo, nfileCounter,
					dfactor, rsrdb, ncycles);

			// prepare for next iteration

			nfileCounter++;

			previousSimulationInfo = simulationInfo;

		}

		bufferedReader.close();
		// bufferedwriter.close();

		System.out.println(nfileCounter + " files were saved.");
	}

	private static void preProcessorOneFile(String directory,
			SimulationInfo simulationInfo, int nfileCounter, int dfactor,
			int rsrdb, int ncycles) throws Exception {
		String inputFileName = directory + simulationInfo.m_fileName + ".txt";

		double startTime = Double.parseDouble(simulationInfo.m_startTime);

		double endTime = Double.parseDouble(simulationInfo.m_finalTime);

		double[][] signals = null;

		double fs = deltaT / dfactor;

		System.out.println(fs);

		String x = simulationInfo.m_network + simulationInfo.m_faultLineIndex
				+ simulationInfo.m_faultType + nfileCounter;

		x = x.toUpperCase();

		signals = getWholeWaveforms(inputFileName, startTime, endTime, deltaT);
		double[][] signals2 = signals;
		for (int i = 0; i < signals.length; i++) {
			signals[i] = preProcessorWaveform(signals[i], dfactor, rsrdb,
					(x + i));
		}

		// only faults
		if (m_onlyEvent) {
			signals = extractFaultWaveforms(inputFileName, startTime, endTime,

			fs, signals, ncycles);

		}

		String outputFileName = directory + simulationInfo.toString() + "_"

		+ nfileCounter + ".bin";
		// float[][] d = new float[signals.length][signals[0].length];

		/*
		 * BufferedWriter bufferedweriter2 = new BufferedWriter(new
		 * FileWriter(outputFileName));
		 * bufferedweriter2.write(writeToArffFileWithoutClass(signals));
		 * bufferedweriter2.close();
		 * bufferedWriter.write(writeToArffFile(signals, simulationInfo,
		 * nfileCounter));
		 */

		// HTKInterfacer.writePattern(windowingProcess(signals), outputFileName,
		// 2000, (short) 9);
		// HTKInterfacer.getPatternFromFile(outputFileName);
		 IO.writeMatrixtoBinFile(outputFileName, signals);
		// IO.writeMatrixtoASCIIFile(outputFileName, signals);
		// IO.DisplayMatrix(windowingProcess(signals));
		if (m_odebug) {
			String x2;

			MatlabInterfacer.sendArray(signals2, "p");

			String command = "figure(1);plot(p(1,:),'b'); hold on; plot(p(2,:), 'r'); hold on; "
					+ "plot(p(3,:),'g'); hold on; legend('Phase A','Phase B','phase C') ";

			MatlabInterfacer.sendCommand(command);

			MatlabInterfacer.sendArray(signals, "x");
			command = "figure(2);plot(x(1,:),'b'); hold on; plot(x(2,:), 'r'); hold on; "
					+ "plot(x(3,:),'g'); hold on; legend('Phase A','Phase B','phase C') ";
			MatlabInterfacer.sendCommand(command);
			
			MatlabInterfacer.sendArray(signals2, "x");
			command = "figure(3);plot(x(4,:),'b'); hold on; plot(x(5,:), 'r'); hold on; "
					+ "plot(x(6,:),'g'); hold on; legend('Phase A','Phase B','phase C') ";
			MatlabInterfacer.sendCommand(command);
			
			MatlabInterfacer.sendArray(signals, "x");
			command = "figure(4);plot(x(4,:),'b'); hold on; plot(x(5,:), 'r'); hold on; "
					+ "plot(x(6,:),'g'); hold on; legend('Phase A','Phase B','phase C') ";
			MatlabInterfacer.sendCommand(command);
			
			
			
			// MatlabInterfacer.plotArray(getWholeWaveforms(inputFileName)[2],
			// "y",2);
			// MatlabInterfacer.plotArray(signals[2],"x",1);
			// MatlabInterfacer.plotArray(signals2[2],"x",2);
			// MatlabInterfacer.plotArray(signals3[2],"y",3);

			/*
			 * String command = "subplot(3,1,1), plot(p(1,:)),title('Fase A ');" +
			 * "subplot(3,1,2),plot(p(2,:)),title('Fase B '); " +
			 * "subplot(3,1,3),plot(p(3,:)),title('Fase C ');"+ "figure(2);"+
			 * "subplot(3,1,1),plot(p(4,:)),title('Fase A ');"+
			 * "subplot(3,1,2),plot(p(5,:)),title('Fase B ');"+
			 * "subplot(3,1,3),plot(p(6,:)),title('Fase C ');";
			 * MatlabInterfacer.sendCommand(command);
			 */
			// MatlabInterfacer.plotArray(getWholeWaveforms(inputFileName,
			// startTime, endTime, 5000)[2], "y",2);
			String filename = "D:\\Documentos\\BasesQESIA\\amazonatps\\"
					+ "tramoeste\\waveformstramoestevalid\\"
					+ simulationInfo.toString() + "_" + nfileCounter;
			// String command2 = "print('-f1', '-djpeg','" + filename + "');";
			// MatlabInterfacer.sendCommand(command2);
			IO.pause();

		}

		System.out.println("Wrote " + outputFileName);

	}

	private static void preProcessorOneFile(String directory,
			SimulationInfo simulationInfo, int nfileCounter, int dfactor,
			int rsrdb, BufferedWriter bufferedWriter, int ncycles)
			throws Exception {
		String inputFileName = directory + simulationInfo.m_fileName + ".txt";

		double startTime = Double.parseDouble(simulationInfo.m_startTime);

		double endTime = Double.parseDouble(simulationInfo.m_finalTime);

		double fs = deltaT / 8;

		double[][] signals = null;

		double[][] signals2 = null;

		String x = simulationInfo.m_network + simulationInfo.m_faultLineIndex
				+ simulationInfo.m_faultType + nfileCounter;

		x = x.toUpperCase();

		signals = getWholeWaveforms(inputFileName, startTime, endTime, fs);

		for (int i = 0; i < signals.length; i++) {
			signals[i] = preProcessorWaveform(signals[i], dfactor, rsrdb,
					(x + i));
		}

		// only faults
		signals = extractFaultWaveforms(inputFileName, startTime, endTime, fs,
				signals, ncycles);

		String outputFileName = directory + simulationInfo.toString() + "_"

		+ nfileCounter + ".bin";
		// float[][] d = new float[signals.length][signals[0].length];
		float[][] d = new float[1][signals[0].length];
		for (int j = 0; j < d.length; j++) {
			for (int k = 0; k < d[j].length; k++) {
				d[j][k] = (float) signals[j][k];
			}
		}

		BufferedWriter bufferedweriter2 = new BufferedWriter(new FileWriter(
				outputFileName));
		bufferedweriter2.write(writeToArffFileWithoutClass(signals));
		bufferedweriter2.close();
		bufferedWriter.write(writeToArffFile(signals, simulationInfo,
				nfileCounter));

		// HTKInterfacer.writePattern(d,outputFileName, 2000, (short) 9);
		// HTKInterfacer.getPatternFromFile(outputFileName);
		IO.writeMatrixtoBinFile(outputFileName, signals);
		// IO.writeMatrixtoASCIIFile(outputFileName, signals);
		// IO.DisplayMatrix(signals);

		if (m_odebug) {
			String x2;

			MatlabInterfacer.sendArray(signals, x);
			// MatlabInterfacer.plotArray(signals[2],"x",1);
			// MatlabInterfacer.plotArray(signals2[2],"x",2);
			// MatlabInterfacer.plotArray(signals3[2],"y",3);

			String command = "subplot(3,1,1), plot(" + x
					+ "(1,:)),title('Fase A " + x + " ');subplot(3,1,2),plot("
					+ x + "(2,:)),title('Fase B " + x
					+ "'); subplot(3,1,3),plot(" + x + "(3,:)),title('Fase C "
					+ x + " ');";
			MatlabInterfacer.sendCommand(command);
			String filename = "D:\\Documentos\\BasesQESIA\\amazonatps\\"
					+ "tramoeste\\waveformstramoestevalid\\"
					+ simulationInfo.toString() + "_" + nfileCounter;
			// String command2 = "print('-f1', '-djpeg','" + filename + "');";
			// MatlabInterfacer.sendCommand(command2);
			IO.pause();

		}

		System.out.println("Wrote " + outputFileName);

	}

	private static float[][] windowingProcess(double[][] signals) {
		int numberOfSamplesPerFrame = signals[0].length;
		int numberOfParameters = m_nwindows * m_nsignals;
		float[][] d = new float[numberOfParameters][numberOfSamplesPerFrame];
		for (int i = 0; i < numberOfSamplesPerFrame; i++) {
			for (int j = 0; j < numberOfParameters; j += (numberOfParameters / m_nwindows)) {
				d[j][i] = (float) signals[0][i];
				d[j + 1][i] = (float) signals[1][i];
				d[j + 2][i] = (float) signals[2][i];
				d[j + 3][i] = (float) signals[3][i];
				d[j + 4][i] = (float) signals[4][i];
				d[j + 5][i] = (float) signals[5][i];
			}

		}
		return d;

	}

	private static String writeToArffFile(double[][] d,
			SimulationInfo simulationInfo, int nfilecount) {
		StringBuffer stringbuffer = new StringBuffer();
		int numberOfSamplesPerFrame = d[0].length;
		int numberOfParameters = m_nwindows * m_nsignals;
		int countline = 0;
		int count = 0;
		String tempString = "";
		String labelfault = simulationInfo.m_faultType;
		if (nfilecount == 0) {

			// first iteration
			stringbuffer.append(getARFFHeader(numberOfParameters));
			countline = numberOfParameters + 2;
			// stringbuffer.append(m_NEW_LINE);
			// bufferedwriter.write(getARFFHeader(numberOfParameters));

		}
		for (int j = 0; j < numberOfSamplesPerFrame; j++) {
			// stringbuffer.append(d[j][k] + ",");

			tempString += IO.format(d[0][j]) + ",";
			tempString += IO.format(d[1][j]) + ",";
			tempString += IO.format(d[2][j]) + ",";
			tempString += IO.format(d[3][j]) + ",";
			tempString += IO.format(d[4][j]) + ",";
			tempString += IO.format(d[5][j]) + ",";

			// bufferedwriter.write(d[j][k] + ",");
			// System.out.print(x[j][k] + ",");

			// stringbuffer.append(tempString);

			count++;

			if (count % m_nwindows == 0) {
				// stringbuffer.append(labelfault);
				// stringbuffer.append(m_NEW_LINE);
				tempString += labelfault;
				tempString += m_NEW_LINE;
				countline++;
				// tempString = tempString.substring(0,tempString.length()-1)+
				// m_NEW_LINE;
				countline++;
				// bufferedwriter.write(fault);
				// System.out.println(fault);
				stringbuffer.append(tempString);
				tempString = "";
			}

		}

		// System.out.println(fileNames[i] + " " + fault + " " + x.length +

		// " " + x[0].length);
		// bufferedwriter.close();
		return stringbuffer.toString();

	}

	private static String writeToArffFileWithoutClass(double[][] d) {
		StringBuffer stringbuffer = new StringBuffer();
		int numberOfSamplesPerFrame = d[0].length;
		int countline = 0;
		int count = 0;
		String tempString = "";
		for (int j = 0; j < numberOfSamplesPerFrame; j++) {
			// stringbuffer.append(d[j][k] + ",");

			tempString += IO.format(d[0][j]) + ",";
			tempString += IO.format(d[1][j]) + ",";
			tempString += IO.format(d[2][j]) + ",";
			tempString += IO.format(d[3][j]) + ",";
			tempString += IO.format(d[4][j]) + ",";
			tempString += IO.format(d[5][j]) + ",";

			// bufferedwriter.write(d[j][k] + ",");
			// System.out.print(x[j][k] + ",");

			// stringbuffer.append(tempString);

			count++;

			if (count % m_nwindows == 0) {
				// stringbuffer.append(labelfault);
				// stringbuffer.append(m_NEW_LINE);
				// tempString += labelfault;
				tempString = tempString.substring(0, tempString.length() - 1)
						+ m_NEW_LINE;
				countline++;
				// bufferedwriter.write(fault);
				// System.out.println(fault);
				stringbuffer.append(tempString);
				tempString = "";
			}

		}

		// System.out.println(fileNames[i] + " " + fault + " " + x.length +

		// " " + x[0].length);
		// bufferedwriter.close();
		return stringbuffer.toString();

	}

	private static double[] preProcessorWaveform(double[] signal, int dfactor,
			int rsrdb, String varname) {
		// double[] y = decimateSignalFromMatlab(signal, dfactor, varname);
		double[] y = decimateSignal(signal, dfactor);
		double[] z = insertNoise(rsrdb, y);
		return y;
	}

	private static double[] insertNoise(int rsrdb, double[] y) {
		double pnoise = noisePower(signalPower(y), rsrdb);
		double[] n = new double[y.length];
		double[] z = new double[y.length];
		double noisefactor = Math.sqrt(pnoise);
		Random r = new Random();
		for (int i = 0; i < n.length; i++) {
			n[i] = noisefactor * r.nextGaussian();
		}
		for (int i = 0; i < z.length; i++) {
			z[i] = y[i] + n[i];
		}
		return z;
	}

	private static double[] decimateSignalFromMatlab(double[] signal,
			int dfactor, String varName) {
		double[] y = null;
		MatlabInterfacer.sendArray(signal, varName);
		String command = "y = decimate(" + varName + "," + dfactor + ",'fir')";
		MatlabInterfacer.sendCommand(command);
		y = MatlabInterfacer.getVector("y");
		MatlabInterfacer.sendCommand("clear y");
		MatlabInterfacer.sendCommand("clear " + varName);

		// MatlabInterfacer.close();
		return y;

	}

	private static double[] decimateSignal(double[] signal, int dfactor) {
		int newNumberOfColumns = signal.length / dfactor;
		double[] y = new double[newNumberOfColumns];
		for (int j = 0; j < y.length; j++) {
			y[j] = signal[j * dfactor];
		}
		return y;
	}

	private static double signalPower(double[] signal) {
		double psignal = 0;
		int nsamples = signal.length;
		double sum = 0;
		for (int i = 0; i < signal.length; i++) {
			sum += signal[i] * signal[i];
		}
		psignal = sum / nsamples;
		return psignal;
	}

	private static double noisePower(double psignal, int rsrdb) {
		double pnoise = psignal / Math.pow(10, (0.1 * rsrdb));
		return pnoise;
	}

	/**
	 * 
	 * Organizes the info in a line such as: simulation_0: Data base = MARABA ;
	 * 
	 * Fault type = ABCT ; Fault line index= 03 ; Fault Started in 4.21287E-1 s ;
	 * 
	 * Ended in 8.12037E-1 s ; The fault happens on 7.25127E1% of line length ;
	 * 
	 * The resistance between lines is 7.24E1 ohms and resistance from line to
	 * 
	 * ground is 3.95E1 ohms.
	 * 
	 */

	private static class SimulationInfo {

		protected String m_fileName;

		protected String m_network;

		protected String m_faultType;

		protected String m_faultLineIndex;

		protected String m_startTime;

		protected String m_finalTime;

		protected String m_faultPosition;

		protected String m_linesResistance;

		protected String m_lineGroundResistance;

		public String toString() {

			return (m_network + "_" + m_faultLineIndex + "_" + m_faultType)

			.toLowerCase();

		}

	}

	public static SimulationInfo parseLine(String line) throws Exception {

		StringTokenizer stringTokenizer = new StringTokenizer(line);

		int n = stringTokenizer.countTokens();

		if (n != 52) {

			System.err.println("Error in logic while parsing line: \n" + line);

			throw new Exception();

		}

		// store in temporary variable

		for (int i = 0; i < n; i++) {

			String token = stringTokenizer.nextToken();

			// System.out.println(i + " " + token);

			m_tempBuffer[i] = token;

		}

		SimulationInfo simulationInfo = new SimulationInfo();

		// take out the ":" from fileName:

		simulationInfo.m_fileName = m_tempBuffer[0].substring(0,

		m_tempBuffer[0].length() - 1);

		simulationInfo.m_network = m_tempBuffer[4];

		simulationInfo.m_faultType = m_tempBuffer[9];

		simulationInfo.m_faultLineIndex = m_tempBuffer[14];

		simulationInfo.m_startTime = m_tempBuffer[19];

		simulationInfo.m_finalTime = m_tempBuffer[24];

		simulationInfo.m_faultPosition = m_tempBuffer[31];

		simulationInfo.m_linesResistance = m_tempBuffer[41];

		simulationInfo.m_lineGroundResistance = m_tempBuffer[50];

		return simulationInfo;

	}

	private static double[][] normalizeLinearMatrix(double[][] d) {
		double max = 0;
		double min = d[0][0];
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				if (d[i][j] >= max)
					max = d[i][j];
				if (d[i][j] <= min)
					min = d[i][j];
			}
		}
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = (d[i][j] - min) / (max - min);
			}
		}
		return d;
	}

	private static double[][] normalizeMaxMinMatrix(double[][] d, int newmax,
			int newmin) {
		double max = 0;
		double min = d[0][0];
		double[][] v = d;		
        //normalize voltage
		for (int i = 0; i < d.length/2 ; i++) {
			for (int j = 0; j < d[i].length; j++) {
				if (d[i][j] >= max)
					max = d[i][j];
				if (d[i][j] <= min)
					min = d[i][j];
			}
		}
		for (int i = 0; i < d.length/2; i++) {
			for (int j = 0; j < d[i].length; j++) {
				v[i][j] = ((d[i][j] - min) / (max - min)) * (newmax - newmin)
						+ newmin;
				/*if (d[i][j] < 0)
					v[i][j] = -v[i][j];
				else
					v[i][j] = (-1) * v[i][j];*/
			}
		}
		
//		normalize current
		max = 0;
		min = d[d.length/2][0];
		
		for (int i = d.length/2; i < d.length ; i++) {
			for (int j = 0; j < d[i].length; j++) {
				if (d[i][j] >= max)
					max = d[i][j];
				if (d[i][j] <= min)
					min = d[i][j];
			}
		}
		for (int i = d.length/2; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				v[i][j] = ((d[i][j] - min) / (max - min)) * (newmax - newmin)
						+ newmin;
				/*if (d[i][j] < 0)
					v[i][j] = -v[i][j];
				else
					v[i][j] = (-1) * v[i][j];*/
			}
		}
		
		return v;

	}

	private static double[] normalizeMaxMinVector(double[] d, int newmax,
			int newmin) {
		double max = 0;
		double min = d[0];
		double[] v = d;
		for (int i = 0; i < d.length; i++) {
			if (d[i] >= max)
				max = d[i];
			if (d[i] <= min)
				min = d[i];

		}
		for (int i = 0; i < d.length; i++) {

			v[i] = ((d[i] - min) / (max - min)) * (newmax - newmin) + newmin;
			if (d[i] < 0)
				v[i] = -v[i];
			else
				v[i] = (-1) * v[i];

		}
		return v;

	}

	private static double[][] getWholeWaveforms(String fileName,
			double startTime, double endTime, double fs)

	throws Exception {

		// read the whole waveform

		float[][] signals = LoadATPOutputFile.parseATPOutputFile(fileName);
		// deltaT = (double) signals[0][1];

		double max = 0;

		int noutputSamples = signals[0].length - 1;

		/*
		 * System.out.println(startTime + " " + T + " " + endTime " " +
		 * 
		 * noutputSamples);
		 */

		// assume there are 6 signals of interest
		double[][] output = new double[6][noutputSamples];
		double[] maxsofVoltageAndCurrent = maximumOfVoltageAndCurrent(
				startTime, endTime, fs, signals);
		double maxvaluevoltage = maxsofVoltageAndCurrent[0];
		System.out.println("maxvaluevoltage: " + maxvaluevoltage);
		double maxvaluecurrent = maxsofVoltageAndCurrent[1];
		System.out.println("maxvaluecurrent: " + maxvaluecurrent);
		// take the samples at the begin of the line
		for (int i = 0; i < noutputSamples; i++) {

			// will not consider the "time" waveform

			// output[0][i] = signals[0][i];

			output[0][i] =  signals[1][i];
            
			output[1][i] = signals[2][i];
			
			output[2][i] = signals[3][i];

			output[3][i] = signals[4][i];
			
			output[4][i] = signals[5][i];

			output[5][i] = signals[6][i];
			
			
		}		
		output = normalizeMaxMinMatrix(output, 1, -1);

		return output;

	}

	private static double[] maximumOfVoltageAndCurrent(double startTime,
			double endTime, double fs, float[][] signals) {

		double[] maxs = new double[2];
		int startIndice = (int) (startTime * fs);
		System.out.println("startIndice: " + startIndice);
		int endIndice = (int) (0.5 + (endTime * fs));

		double[][] startwaveforms = new double[6][startIndice + 1];

		// maxvaluevoltage
		maxs[0] = 0;

		// maxvaluecurrent
		maxs[1] = 0;

		// take the samples at the begin of waveforms
		for (int i = 0; i < startIndice + 1; i++) {
			for (int j = 0; j < startwaveforms.length; j++) {
				startwaveforms[j][i] = (double) signals[j][i];
			}
		}

		// max value of voltage
		for (int i = 0; i < startIndice + 1; i++) {
			for (int j = 0; j < 3; j++) {
				if (startwaveforms[j][i] >= maxs[0])
					maxs[0] = startwaveforms[j][i];
			}
		}

		// max value of current
		for (int i = 0; i < startIndice + 1; i++) {
			for (int j = 3; j < 6; j++) {
				if (startwaveforms[j][i] >= maxs[1])
					maxs[1] = startwaveforms[j][i];
			}
		}
		return maxs;
	}

	private static double[][] extractFaultWaveforms(String fileName,

	double startTime, double endTime, double fs, double[][] signals, int ncycles)
			throws Exception {

		// read the whole waveform

		// float[][] signals = LoadATPOutputFile.parseATPOutputFile(fileName);

		// get the sampling period (inverse of sampling frequency)

		// T = signals[0][1];

		// System.out.println("T: " + T);

		// 6 cycles before and after
		System.out.println("ncycles: " + ncycles);
		int nsamples = (int) (ncycles * Math.floor(0.5 + (fs / 60.0)));
		System.out.println("nsamples: " + nsamples);

		// fault duration
		double duration = endTime - startTime;
		System.out.println("duration: " + duration);
		int sampleduration = (int) (0.5 + (duration * fs));
		System.out.println("sampleduration: " + sampleduration);
		// cut out the fault

		int startIndice = (int) (startTime * fs);
		System.out.println("startIndice: " + startIndice);

		int endIndice = (int) (0.5 + (endTime * fs));
		System.out.println("endIndice: " + endIndice);

		int startIndice2 = startIndice;
		startIndice = startIndice - nsamples;

		endIndice = endIndice + nsamples;

		if (startIndice == endIndice) {

			endIndice++;

		}

		if (endIndice > signals[0].length - 1) {

			// cannot extrapolate the number of samples

			endIndice = signals[0].length - 1;

		}

		// ak

		if (m_odebug) {

			MatlabInterfacer.sendArray(Cloner.clone(signals), "all");

			// System.out.println("Fault start = " + (startIndice * T) + " end =
			// "

			// + (endIndice * T));

		}

		int noutputSamples = endIndice - startIndice + 1;

		// System.out.println("startTime: " + startTime + "sample period " + T
		// + "endTime: " + endTime + "noutputSamples: " + noutputSamples);

		// assume there are 6 signals of interest

		double[][] output = new double[6][noutputSamples];

		// System.out.println("Max Value of voltage: " + maxvaluevoltage);
		// System.out.println("Max Value of current: " + maxvaluecurrent);

		// take the samples at the begin of the line

		for (int i = 0; i < noutputSamples; i++) {

			// will consider the "time" waveform

			// output[0][i] = signals[0][startIndice + i];

			output[0][i] = signals[0][startIndice + i];

			output[1][i] = signals[1][startIndice + i];

			output[2][i] = signals[2][startIndice + i];

			output[3][i] = signals[3][startIndice + i];

			output[4][i] = signals[4][startIndice + i];

			output[5][i] = signals[5][startIndice + i];

		}

		// System.out.println("Writing " + noutputSamples + " samples");

		return output;

	}

	private static String getARFFHeader(int numberOfAttributes) {

		StringBuffer stringBuffer = new StringBuffer("@relation master");

		stringBuffer.append(m_NEW_LINE);

		for (int j = 0; j < numberOfAttributes; j++) {

			stringBuffer

			.append("@attribute P" + (j + 1) + " real" + m_NEW_LINE);

		}

		stringBuffer

		.append("@attribute class {AT,BT,CT,AB,AC,BC,ABC,ABT,ACT,BCT,ABCT}"

		+ m_NEW_LINE);

		stringBuffer.append("@data" + m_NEW_LINE);

		return (stringBuffer.toString());

	}

}
