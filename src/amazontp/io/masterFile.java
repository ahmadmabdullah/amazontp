package amazontp.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Iterator;
import java.util.Locale;

import amazontp.atpcomponents.LineZT;


public class masterFile {
	
	int cont=0;
	String letra []={"A","B","C"};
	Iterator m_iLineSelected,xx; // i = Iterator das linhas ZT
	BufferedReader bufferedReader;
	BufferedWriter bufferedWriter;
	
	String newPart1Line = "";
	LineZT m_lineZTSelected;
	
	double length = 0;
	int numberOfPhase = 0; // A = 0 , B = 1 and C = 2;
	String fileName,outFileName;
	ArrayList<LineZT> m_lineSelected;
	Collection <String> fase = new ArrayList<String>();
	Collection <String> reseach = new ArrayList<String>();// linhas que n�o estao em ordem
	String[][] switchCardLines = new String[100][4];
	boolean test=false;
	
	
	private static DecimalFormat m_rlcScientificNumberFormat100;

	private static DecimalFormat m_rlcScientificNumberFormat10;

	private static DecimalFormat m_rlcScientificNumberFormat1;

	private static DecimalFormat m_rlcScientificNumberFormat0;

	private static DecimalFormat m_rlcScientificNumberFormatMinus10;

	private static DecimalFormat m_rlcScientificNumberFormatMinus1;

	private static DecimalFormat m_switchTimeScientificNumberFormat;

	private static  String m_NEW_LINE;

	// private static final String m_SLASH;

	private static double m_dfactorToConvertToLogBase10;

	//private static String[] m_originalLines;

	//private static String[] m_modifiedLines;

	//private static Random m_random = new Random(System.currentTimeMillis());

	// private static Random m_random = new Random(1L);

	public masterFile(String fileName,String outFileName, ArrayList<LineZT> lineSelected) {
	  
	  this.fileName=fileName;
	  this.outFileName=outFileName;
	  m_iLineSelected=lineSelected.iterator();
	  this.m_lineSelected=lineSelected;
	  System.out.println("N� de linhas selecionadas: "+m_lineSelected.size());
	 

	  
	  m_dfactorToConvertToLogBase10 = 1.0 / Math.log(10);

		Locale.setDefault(Locale.US);

		m_NEW_LINE = System.getProperties().getProperty("line.separator");

		System.getProperties().getProperty("file.separator");

		// want to use this locale, otherwise can get confused with , instead of

		// .

		NumberFormat.getInstance(Locale.US);

		m_switchTimeScientificNumberFormat = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat0 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat1 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat10 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat100 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormatMinus10 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormatMinus1 = (DecimalFormat) NumberFormat

		.getInstance();

		// each range of numbers require a special format:

		// maybe there's some smarter way...

		m_rlcScientificNumberFormat100.applyPattern("0.E000");

		m_rlcScientificNumberFormat100.setMaximumFractionDigits(0);

		m_rlcScientificNumberFormat10.applyPattern("0.#E00");

		m_rlcScientificNumberFormat1.applyPattern("0.##E0");

		m_rlcScientificNumberFormat0.applyPattern("0.#E0");

		m_rlcScientificNumberFormatMinus10.applyPattern("0.E00");

		// ak not sure about the one below:

		m_rlcScientificNumberFormatMinus1.applyPattern("0.#E0");

		m_switchTimeScientificNumberFormat.applyPattern("0.###E0##");
		
		
	}
	
	
	private static String format(double x) {

		if (x < 0) {

			System.err.println("Error: " + x + " is negative!");

			return "Error: " + x + " is negative!";

		}

		String s = Double.toString(x);

		// ATP restricts the RLC values to be represented by 6 characteres and

		// requires a dot .

		int n = s.length();

		boolean oisDotThere = (s.indexOf(".") == -1) ? false : true;

		if (oisDotThere && n <= 6) {

			// string is ok

			return forceNCharacters(s, x, 6);

		}

		if (n <= 5) { // and oisDotThere = false otherwise the if above would

			// be true

			// it must be an integer number, so add . in the end

			return forceNCharacters(s + ".", x, 6);

		}

		// otherwise we need to use scientific notation

		// positive number

		if (x < 1.0E-99) {

			System.err.println("ERROR parsing x=" + x + " too small");

			return "ERROR parsing x=" + x + " too small";

		}

		// find the exponent:

		int exponent = (int) (Math.log(x) * m_dfactorToConvertToLogBase10);

		// this way is elegant but slower:

		// int exponentDigits = Integer.toString(exponent).length();

		if (exponent < -10) {

			s = m_rlcScientificNumberFormatMinus10.format(x);

		} else if (exponent < 0) {

			s = m_rlcScientificNumberFormatMinus1.format(x);

		} else if (exponent < 1) {

			s = m_rlcScientificNumberFormat0.format(x);

		} else if (exponent < 10) {

			s = m_rlcScientificNumberFormat1.format(x);

		} else if (exponent < 100) {

			s = m_rlcScientificNumberFormat10.format(x);

		} else if (exponent < 1000) {

			s = m_rlcScientificNumberFormat100.format(x);

			if (s.indexOf(".") == -1) {

				if (s.length() < 6) {

					n = s.indexOf("E");

					s = s.substring(0, n) + "." + s.substring(n);

				}

			}

		} else {

			// should never happen because the maximum double is around 1e300

			System.err.println("ERROR: " + x + " is too large!");

			return "ERROR: " + x + " is too large!";

		}

		return forceNCharacters(s, x, 6);

	}

	// Used to force numbers in order to respect ATP File Specifications.
	private static String forceNCharacters(String s, double x, int N) {

		int n = s.length();

		if (n > N) {

			System.err.println("ERROR: " + x + " was wrongly parsed as " + s);

			return "ERROR: " + x + " was wrongly parsed as " + s;

		}

		if (n == N) {

			return s;

		}

		// C 1 2 3 4 5 6 7 8

		// C

		// 345678901234567890123456789012345678901234567890123456789012345678901234567890

		String fillIn = "                                                                                ";

		return fillIn.substring(0, N - n) + s;

	}
	
	public int prepareMasterFile()
	throws Exception {
		
         bufferedReader = new BufferedReader(new FileReader(fileName));
		 bufferedWriter = new BufferedWriter(new FileWriter(this.outFileName));
		 ArrayList<String>medidor=new ArrayList<String>();
		 String line;
		 int numberOfLineZT = 0;
		 int countLineZT=0;
		 
	//	 line = bufferedReader.readLine();

		 String[] newPart2Lines = new String[3];
		 
		 boolean verif=true;
		 String x=null;
		 
		 m_lineZTSelected= (LineZT)  m_iLineSelected.next();// iterator das linhas ZT setadas pelo usuario
		 
			while ((line = bufferedReader.readLine())!=null) {
				
				

				if (line.substring(0, 1).equals("-")) {
					
					//verify if line selected is equal line of the archive
					

					// The first two collums of a LineZT (Phases A and B)
					if (numberOfPhase == 0 || numberOfPhase == 1) {
						
						
						
						if(line.equals(m_lineZTSelected.getFaseA())||line.equals(m_lineZTSelected.getFaseB())){
							System.err.println("FASE: "+m_lineZTSelected.getFase());
							if(!fase.contains(m_lineZTSelected.getFase())){
							fase.add(m_lineZTSelected.getFase());
							}
						
						if (numberOfPhase == 0) {
							bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 1\r\n");
						}

						// numberOfLineZT could be larger then 10, so, we need to
						// create this condition in order to concat the name of the
						// line ZT correctly in the collums.
						
						// Dividing line in two parts of (length - 10) and 10. 
						
						if (numberOfLineZT < 10) {
							
							length = Double.parseDouble(line.substring(44, 50).trim());

							newPart1Line = line.substring(0, 8) + "NEW0" + numberOfLineZT + line.substring(13, 44)+ format(length-10) + line.substring(50, 80);
							bufferedWriter.write(newPart1Line + "\r\n");

							newPart2Lines[numberOfPhase] = line.substring(0, 2) + "NEW0" + numberOfLineZT + line.substring(7, 44)+ format(10) + line.substring(50, 80);

							numberOfPhase++;
						}

						else {
							length = Double.parseDouble(line.substring(44, 50).trim());
							
							// Dividing line in two parts of (length - 10) and 10. 

							newPart1Line = line.substring(0, 8) + "NEW" + numberOfLineZT + line.substring(13, 44) + format(length - 10) + line.substring(50, 80);
							bufferedWriter.write(newPart1Line + "\r\n");

							newPart2Lines[numberOfPhase] = line.substring(0, 2) + "NEW" + numberOfLineZT + line.substring(7, 44) + format(10) + line.substring(50, 80);

							numberOfPhase++;
						}
					}
						else {
							
							bufferedWriter.write(line+"\r\n");
						}
						

					} else {

						if (numberOfPhase == 2) { // Phase C
							this.otherLine();

							if (numberOfLineZT < 10) {
								
								
								newPart1Line = line.substring(0, 8) + "NEW0" + numberOfLineZT + line.substring(13, 80);
								bufferedWriter.write(newPart1Line + "\r\n");

								newPart2Lines[numberOfPhase] = line.substring(0,2) + "NEW0" + numberOfLineZT + line.substring(7, 80);

								// Writing new Lines
								bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 2\r\n");
								bufferedWriter.write(newPart2Lines[0] + "\r\n");
								bufferedWriter.write(newPart2Lines[1] + "\r\n");
								bufferedWriter.write(newPart2Lines[2] + "\r\n");

								// Writting fault resistances.
								bufferedWriter.write("  MIDC0" + numberOfLineZT + "END_0" + numberOfLineZT + "            " + format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  MIDB0"
												+ numberOfLineZT
												+ "END_0"
												+ numberOfLineZT
												+ "            "
												+ format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  MIDA0"
												+ numberOfLineZT
												+ "END_0"
												+ numberOfLineZT
												+ "            "
												+ format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  STG_0"
												+ numberOfLineZT
												+ "                  "
												+ format(1.E3)
												+ "                                               0\r\n");

								switchCardLines[numberOfLineZT][0] = "  NEW0"
										+ numberOfLineZT
										+ "CMIDC0"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";

								switchCardLines[numberOfLineZT][1] = "  NEW0"
										+ numberOfLineZT
										+ "BMIDB0"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";

								switchCardLines[numberOfLineZT][2] = "  NEW0"
										+ numberOfLineZT
										+ "AMIDA0"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";

								switchCardLines[numberOfLineZT][3] = "  END_0"
										+ numberOfLineZT
										+ "STG_0"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";

								numberOfPhase = 0;
								numberOfLineZT++;

							} else { // numberOfLineZT > 10
								
								
								if (numberOfPhase == 0) {
									bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 1\r\n");
								}
								
								newPart1Line = line.substring(0, 8) + "NEW"
										+ numberOfLineZT + line.substring(13, 80);
								bufferedWriter.write(newPart1Line + "\r\n");
								
								newPart2Lines[numberOfPhase] = line.substring(0,
										2)
										+ "NEW"
										+ numberOfLineZT
										+ line.substring(7, 80);

								// Writing new Lines
								bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 2\r\n");
								bufferedWriter.write(newPart2Lines[0] + "\r\n");
								bufferedWriter.write(newPart2Lines[1] + "\r\n");
								bufferedWriter.write(newPart2Lines[2] + "\r\n");

								// Writting fault resistances.
								bufferedWriter
										.write("  MIDC"
												+ numberOfLineZT
												+ "END_"
												+ numberOfLineZT
												+ "            "
												+ format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  MIDB"
												+ numberOfLineZT
												+ "END_"
												+ numberOfLineZT
												+ "            "
												+ format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  MIDA"
												+ numberOfLineZT
												+ "END_"
												+ numberOfLineZT
												+ "            "
												+ format(1.E3)
												+ "                                               0\r\n");
								bufferedWriter
										.write("  STG_"
												+ numberOfLineZT
												+ "                  "
												+ format(1.E3)
												+ "                                               0\r\n");

								// Collection SwitchCardLines in order to write when
								// Switch Card have been found.

								switchCardLines[numberOfLineZT][0] = "  NEW"
										+ numberOfLineZT
										+ "CMIDC"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";
								switchCardLines[numberOfLineZT][1] = "  NEW"
										+ numberOfLineZT
										+ "BMIDB"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";
								switchCardLines[numberOfLineZT][2] = "  NEW"
										+ numberOfLineZT
										+ "AMIDA"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";
								switchCardLines[numberOfLineZT][3] = "  END_"
										+ numberOfLineZT
										+ "STG_"
										+ numberOfLineZT
										+ "       10.       -1.                                             0\r\n";

								numberOfPhase = 0;
								//numberOfLineZT++;
							}

						}

					}

				 
					}// fim do if
				
				
				
				
				
				else {

					if (line.indexOf("/SWITCH") != -1) {
						
						test=true;
						bufferedWriter.write(line + "\r\n");
						for (int m = 0; m < numberOfLineZT; m++) {
							bufferedWriter.write(switchCardLines[m][0]);
							bufferedWriter.write(switchCardLines[m][1]);
							bufferedWriter.write(switchCardLines[m][2]);
							bufferedWriter.write(switchCardLines[m][3]);
						}
					
						this.setXx(fase.iterator());
						
						
					
					} else if (test){
						
					    if(verif){
					    		
						 x=(line.contains("/SOURCE")?"Fim do cart�o":this.research(line.substring(8,13)));
						
						 verif=(x.equals("fase n�o esta entre as selecionadas")?true:false);
					    }
					 
						if(x!=null && line.contains(x)){
							
						
							String number=(countLineZT<10?"0"+countLineZT:String.valueOf(countLineZT));
							
							 
							
						//	System.out.println("NEWC"+x.substring(4));
						//	System.out.println("NEWC"+line.substring(12,14)+line.substring(8,14));
							//System.out.println("NEC"+number+"A"+line.substring(8,14));
							bufferedWriter.write(line.replace(x,"NEC"+number)+"\r\n");
							medidor.add(("  "+"NEC"+number+"A")+line.substring(8,14)+"                                        MEASURING                0");
							line = bufferedReader.readLine();
							
							bufferedWriter.write(line.replace(x,"NEC"+number)+"\r\n");
							medidor.add(("  "+"NEC"+number+"B")+line.substring(8,14)+"                                        MEASURING                0");
							line = bufferedReader.readLine();
							bufferedWriter.write(line.replace(x,"NEC"+number)+"\r\n");
							medidor.add(("  "+"NEC"+number+"C")+line.substring(8,14)+"                                        MEASURING                0");
							verif=true;
							countLineZT++;
							
						}
						
						
						
						else if(line.equals("/SOURCE")){
							Iterator jj= medidor.iterator();
							while(jj.hasNext()){
								bufferedWriter.write((String)jj.next()+"\r\n");	
							}
							bufferedWriter.write("/SOURCE"+"\r\n");
							test=false;
						}
						
						
					    
						else{
							bufferedWriter.write(line + "\r\n");
						}
					}
					
					else {
						
						bufferedWriter.write(line + "\r\n");
					}

				}

				

			}// fim do while
			
			bufferedReader.close();
			bufferedWriter.close();
			System.out.println("DONE! New file created: " + outFileName);
			
		
			
			

			return numberOfLineZT;
		 
		

	}
	
   public void setXx(Iterator xx){
	   this.xx=xx;
   }
   
   public String research(String fase){
	   System.out.println(fase.toString());
	   if(this.fase.contains(fase)){
		   return fase;
	   }
	   else{
		   return "fase n�o esta entre as selecionadas";
	   }
   }
	
	public void otherLine(){
		if(m_iLineSelected.hasNext()){
			m_lineZTSelected=(LineZT) m_iLineSelected.next();
		}
		
	}

}
