package amazontp.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;





public class FaultLines{

	private static String[] m_tempBuffer = new String[52];
	
	public static void main(String[] args) throws Exception{
		if (args.length != 2) {

			System.out
					.println("Usage: <list in directory><extension>");

			System.exit(1);

		}
		
		String dir = args[0];

		String extension = args[1];
		
		DirectoryTree directoryTree = new DirectoryTree(dir, extension);

		String[] fileNames = directoryTree.getFilesAsStrings();

		//System.out.println("fileNames: "+fileNames[0]);
		System.out.println();
		if (fileNames == null) {

			System.err.println("Couldn't find files with extension "

			+ extension);

			System.exit(1);

		}
		
		for (int i = 0; i < fileNames.length; i++) {
			System.out.println(toString(fileNames[i]));			
		}						

	}
	
	public static String toString(String path){
		String fileName = FileNamesAndDirectories.getFileNameFromPath(path);		
		StringTokenizer token = new StringTokenizer(fileName,"_");
		String circuit = token.nextToken();
		String lineIndex = token.nextToken();
		String fault = token.nextToken().toUpperCase();
		return fault + " "+circuit+" "+lineIndex;
	}
	
	

}
