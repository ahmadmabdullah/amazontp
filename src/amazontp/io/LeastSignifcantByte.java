package amazontp.io;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LeastSignifcantByte extends FilterOutputStream {

	public LeastSignifcantByte(OutputStream out) {
		super(out);
	}

	public void writeShort(int s) throws EOFException, IOException {
		out.write(s & 0xFF);
		out.write((s >> 8) & 0xFF);
	}

	public void writeInt(int i) throws EOFException, IOException {

		// out.write(i & 0xFF | (i >> 8) & 0xFF | (i >> 16) & 0xFF |(i >> 24) &
		// 0xFF);

		out.write(i & 0xFF);
		out.write((i >> 8) & 0xFF);
		out.write((i >> 16) & 0xFF);
		out.write((i >> 24) & 0xFF);

	}

	public void writeShortAsBits(int s) throws EOFException, IOException {

		out.write(s & 0x01);
		out.write((s << 1) & 0x01);
		out.write((s << 2) & 0x01);
		out.write((s << 3) & 0x01);
		out.write((s << 4) & 0x01);
		out.write((s << 5) & 0x01);
		out.write((s << 6) & 0x01);
		out.write((s << 7) & 0x01);
		out.write(s & 0x01);
		out.write((s << 1) & 0x01);
		out.write((s << 2) & 0x01);
		out.write((s << 3) & 0x01);
		out.write((s << 4) & 0x01);
		out.write((s << 5) & 0x01);
		out.write((s << 6) & 0x01);
		out.write((s << 7) & 0x01);
	}

}
