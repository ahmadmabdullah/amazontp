package amazontp.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

import amazontp.atpcomponents.LineZT;
import amazontp.util.CountUtils;

public class PrepareMasterFile {

	/**
	 * @param args
	 */

	// private static NumberFormat m_rlcNumberFormat;
	// private static float m_fminimumNumber;
	// private static float m_fmaximumNumber;
	private static DecimalFormat m_rlcScientificNumberFormat100;

	private static DecimalFormat m_rlcScientificNumberFormat10;

	private static DecimalFormat m_rlcScientificNumberFormat1;

	private static DecimalFormat m_rlcScientificNumberFormat0;

	private static DecimalFormat m_rlcScientificNumberFormatMinus10;

	private static DecimalFormat m_rlcScientificNumberFormatMinus1;

	private static DecimalFormat m_switchTimeScientificNumberFormat;

	private static final String m_NEW_LINE;

	// private static final String m_SLASH;

	private static final double m_dfactorToConvertToLogBase10;

	private static String[] m_originalLines;

	private static String[] m_modifiedLines;

	private static Random m_random = new Random(System.currentTimeMillis());

	// private static Random m_random = new Random(1L);

	static {

		m_dfactorToConvertToLogBase10 = 1.0 / Math.log(10);

		Locale.setDefault(Locale.US);

		m_NEW_LINE = System.getProperties().getProperty("line.separator");

		System.getProperties().getProperty("file.separator");

		// want to use this locale, otherwise can get confused with , instead of

		// .

		NumberFormat.getInstance(Locale.US);

		m_switchTimeScientificNumberFormat = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat0 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat1 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat10 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormat100 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormatMinus10 = (DecimalFormat) NumberFormat

		.getInstance();

		m_rlcScientificNumberFormatMinus1 = (DecimalFormat) NumberFormat

		.getInstance();

		// each range of numbers require a special format:

		// maybe there's some smarter way...

		m_rlcScientificNumberFormat100.applyPattern("0.E000");

		m_rlcScientificNumberFormat100.setMaximumFractionDigits(0);

		m_rlcScientificNumberFormat10.applyPattern("0.#E00");

		m_rlcScientificNumberFormat1.applyPattern("0.##E0");

		m_rlcScientificNumberFormat0.applyPattern("0.#E0");

		m_rlcScientificNumberFormatMinus10.applyPattern("0.E00");

		// ak not sure about the one below:

		m_rlcScientificNumberFormatMinus1.applyPattern("0.#E0");

		m_switchTimeScientificNumberFormat.applyPattern("0.###E0##");

	}

	// Formats the number into ATP File Notation. (By AK)
	private static String format(double x) {

		if (x < 0) {

			System.err.println("Error: " + x + " is negative!");

			return "Error: " + x + " is negative!";

		}

		String s = Double.toString(x);

		// ATP restricts the RLC values to be represented by 6 characteres and

		// requires a dot .

		int n = s.length();

		boolean oisDotThere = (s.indexOf(".") == -1) ? false : true;

		if (oisDotThere && n <= 6) {

			// string is ok

			return forceNCharacters(s, x, 6);

		}

		if (n <= 5) { // and oisDotThere = false otherwise the if above would

			// be true

			// it must be an integer number, so add . in the end

			return forceNCharacters(s + ".", x, 6);

		}

		// otherwise we need to use scientific notation

		// positive number

		if (x < 1.0E-99) {

			System.err.println("ERROR parsing x=" + x + " too small");

			return "ERROR parsing x=" + x + " too small";

		}

		// find the exponent:

		int exponent = (int) (Math.log(x) * m_dfactorToConvertToLogBase10);

		// this way is elegant but slower:

		// int exponentDigits = Integer.toString(exponent).length();

		if (exponent < -10) {

			s = m_rlcScientificNumberFormatMinus10.format(x);

		} else if (exponent < 0) {

			s = m_rlcScientificNumberFormatMinus1.format(x);

		} else if (exponent < 1) {

			s = m_rlcScientificNumberFormat0.format(x);

		} else if (exponent < 10) {

			s = m_rlcScientificNumberFormat1.format(x);

		} else if (exponent < 100) {

			s = m_rlcScientificNumberFormat10.format(x);

		} else if (exponent < 1000) {

			s = m_rlcScientificNumberFormat100.format(x);

			if (s.indexOf(".") == -1) {

				if (s.length() < 6) {

					n = s.indexOf("E");

					s = s.substring(0, n) + "." + s.substring(n);

				}

			}

		} else {

			// should never happen because the maximum double is around 1e300

			System.err.println("ERROR: " + x + " is too large!");

			return "ERROR: " + x + " is too large!";

		}

		return forceNCharacters(s, x, 6);

	}

	// Used to force numbers in order to respect ATP File Specifications.
	private static String forceNCharacters(String s, double x, int N) {

		int n = s.length();

		if (n > N) {

			System.err.println("ERROR: " + x + " was wrongly parsed as " + s);

			return "ERROR: " + x + " was wrongly parsed as " + s;

		}

		if (n == N) {

			return s;

		}

		// C 1 2 3 4 5 6 7 8

		// C

		// 345678901234567890123456789012345678901234567890123456789012345678901234567890

		String fillIn = "                                                                                ";

		return fillIn.substring(0, N - n) + s;

	}
	
	

	// Could identify and divide till 100 lines ZT in ATP File.

public static int prepareMasterFile(String fileName, String outputFileName, ArrayList<LineZT> h2)
			throws Exception {

		// TODO Auto-generated method stub
	  
		String line;
		String newPart1Line = "";
		
		

		int numberOfLineZT = 0;

		String[][] switchCardLines = new String[100][4];

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
				outputFileName));

		double length = 0;

		int nnumLines = CountUtils.countNumberOfLinesInFile(fileName);
		// System.out.println("Number of Lines = " + nnumLines);
		
		

		line = bufferedReader.readLine();

		String[] newPart2Lines = new String[3];

		int numberOfPhase = 0; // A = 0 , B = 1 and C = 2;
		
		
	//	for(Iterator u=h2.iterator();u.hasNext();){
		//	w=(LineZT) u.next();
		
		
			
				

		for (int i = 0; i < nnumLines; i++) {

			if (line.substring(0, 1).equals("-")) {
				
				//verify if line selected is equal line of the archive
				

				// The first two collums of a LineZT (Phases A and B)
				if (numberOfPhase == 0 || numberOfPhase == 1) {
					
					if (numberOfPhase == 0) {
						bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 1\r\n");
					}

					// numberOfLineZT could be larger then 10, so, we need to
					// create this condition in order to concat the name of the
					// line ZT correctly in the collums.
					
					// Dividing line in two parts of (length - 10) and 10. 
					
					if (numberOfLineZT < 10) {
						length = Double.parseDouble(line.substring(44, 50).trim());

						newPart1Line = line.substring(0, 8) + "NEW0" + numberOfLineZT + line.substring(13, 44)+ format(length-10) + line.substring(50, 80);
						bufferedWriter.write(newPart1Line + "\r\n");

						newPart2Lines[numberOfPhase] = line.substring(0, 2) + "NEW0" + numberOfLineZT + line.substring(7, 44)+ format(10) + line.substring(50, 80);

						numberOfPhase++;
					}

					else {
						length = Double.parseDouble(line.substring(44, 50).trim());
						
						// Dividing line in two parts of (length - 10) and 10. 

						newPart1Line = line.substring(0, 8) + "NEW" + numberOfLineZT + line.substring(13, 44) + format(length - 10) + line.substring(50, 80);
						bufferedWriter.write(newPart1Line + "\r\n");

						newPart2Lines[numberOfPhase] = line.substring(0, 2) + "NEW" + numberOfLineZT + line.substring(7, 44) + format(10) + line.substring(50, 80);

						numberOfPhase++;
					}

				} else {

					if (numberOfPhase == 2) { // Phase C

						if (numberOfLineZT < 10) {
							
							
							newPart1Line = line.substring(0, 8) + "NEW0" + numberOfLineZT + line.substring(13, 80);
							bufferedWriter.write(newPart1Line + "\r\n");

							newPart2Lines[numberOfPhase] = line.substring(0,2) + "NEW0" + numberOfLineZT + line.substring(7, 80);

							// Writing new Lines
							bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 2\r\n");
							bufferedWriter.write(newPart2Lines[0] + "\r\n");
							bufferedWriter.write(newPart2Lines[1] + "\r\n");
							bufferedWriter.write(newPart2Lines[2] + "\r\n");

							// Writting fault resistances.
							bufferedWriter.write("  MIDC0" + numberOfLineZT + "END_0" + numberOfLineZT + "            " + format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  MIDB0"
											+ numberOfLineZT
											+ "END_0"
											+ numberOfLineZT
											+ "            "
											+ format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  MIDA0"
											+ numberOfLineZT
											+ "END_0"
											+ numberOfLineZT
											+ "            "
											+ format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  STG_0"
											+ numberOfLineZT
											+ "                  "
											+ format(1.E3)
											+ "                                               0\r\n");

							switchCardLines[numberOfLineZT][0] = "  NEW0"
									+ numberOfLineZT
									+ "CMIDC0"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";

							switchCardLines[numberOfLineZT][1] = "  NEW0"
									+ numberOfLineZT
									+ "BMIDB0"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";

							switchCardLines[numberOfLineZT][2] = "  NEW0"
									+ numberOfLineZT
									+ "AMIDA0"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";

							switchCardLines[numberOfLineZT][3] = "  END_0"
									+ numberOfLineZT
									+ "STG_0"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";

							numberOfPhase = 0;
							numberOfLineZT++;

						} else { // numberOfLineZT > 10
							
							
							if (numberOfPhase == 0) {
								bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 1\r\n");
							}
							
							newPart1Line = line.substring(0, 8) + "NEW"
									+ numberOfLineZT + line.substring(13, 80);
							bufferedWriter.write(newPart1Line + "\r\n");
							
							newPart2Lines[numberOfPhase] = line.substring(0,
									2)
									+ "NEW"
									+ numberOfLineZT
									+ line.substring(7, 80);

							// Writing new Lines
							bufferedWriter.write("C AMAZONTP - Line " + numberOfLineZT + " - Part 2\r\n");
							bufferedWriter.write(newPart2Lines[0] + "\r\n");
							bufferedWriter.write(newPart2Lines[1] + "\r\n");
							bufferedWriter.write(newPart2Lines[2] + "\r\n");

							// Writting fault resistances.
							bufferedWriter
									.write("  MIDC"
											+ numberOfLineZT
											+ "END_"
											+ numberOfLineZT
											+ "            "
											+ format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  MIDB"
											+ numberOfLineZT
											+ "END_"
											+ numberOfLineZT
											+ "            "
											+ format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  MIDA"
											+ numberOfLineZT
											+ "END_"
											+ numberOfLineZT
											+ "            "
											+ format(1.E3)
											+ "                                               0\r\n");
							bufferedWriter
									.write("  STG_"
											+ numberOfLineZT
											+ "                  "
											+ format(1.E3)
											+ "                                               0\r\n");

							// Collection SwitchCardLines in order to write when
							// Switch Card have been found.

							switchCardLines[numberOfLineZT][0] = "  NEW"
									+ numberOfLineZT
									+ "CMIDC"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";
							switchCardLines[numberOfLineZT][1] = "  NEW"
									+ numberOfLineZT
									+ "BMIDB"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";
							switchCardLines[numberOfLineZT][2] = "  NEW"
									+ numberOfLineZT
									+ "AMIDA"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";
							switchCardLines[numberOfLineZT][3] = "  END_"
									+ numberOfLineZT
									+ "STG_"
									+ numberOfLineZT
									+ "       10.       -1.                                             0\r\n";

							numberOfPhase = 0;
							numberOfLineZT++;
						}

					}

				}

			 
				}else {

				if (line.indexOf("/SWITCH") != -1) {
					bufferedWriter.write(line + "\r\n");
					for (int m = 0; m < numberOfLineZT; m++) {
						bufferedWriter.write(switchCardLines[m][0]);
						bufferedWriter.write(switchCardLines[m][1]);
						bufferedWriter.write(switchCardLines[m][2]);
						bufferedWriter.write(switchCardLines[m][3]);
					}
				} else {
					bufferedWriter.write(line + "\r\n");
				}

			}

			line = bufferedReader.readLine();

		}
		
        

		bufferedReader.close();
		bufferedWriter.close();
		System.out.println("DONE! New file created: " + outputFileName);

		return numberOfLineZT;
	}	public static void main(String[] args) throws Exception {
	//	prepareMasterFile("C:\\Programs\\ATPWatcom\\TRAMOESTEvo.atp",
		//		"C:\\Programs\\ATPWatcom\\Teste.atp");
	}

}
