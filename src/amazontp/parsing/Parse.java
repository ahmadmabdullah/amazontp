package amazontp.parsing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.Source;

/**
 * 
 * @author claudomir
 * 
 *This class is responsible for the parse of archive
 */
public class Parse {
	
	private String file;
	
	private BufferedReader bufferedReader;
	
	StringTokenizer stringTokenizer ;
	
	ArrayList <Component>branch;
	
	ArrayList <Component>swit;
	
	ArrayList <Source>source;
	
	
	
	public Parse(String file){
		this.file=file;
		this.parseArchive();
	}
	
	public void parseArchive(){
		String []line={new String()};
		try {
			this.bufferedReader=new BufferedReader(new FileReader(
					this.file));
			while((line[0]=bufferedReader.readLine())!=null){
				
				
				
				if(line[0].equalsIgnoreCase("/BRANCH")){
					
				//	 System.out.println("================Um /BRANCH RECONHECIDO=====================");
				//	 System.out.println("=======================================================");
					
					 branch= BranchParsing.parse(bufferedReader,line);
									Iterator i;
				//	for (i = branch.iterator(); i.hasNext();) {
					//	System.out.println("\n" + i.next().toString());
						
				//	}
					
					
				}
				 
			     if(line[0].equalsIgnoreCase("/SWITCH")){
			   //	 System.out.println("================Um /SWITCH RECONHECIDO=====================");
				//	 System.out.println("=======================================================");
					 swit= SWITCHparsing.parse(bufferedReader,line);
				//	 Iterator i;
					//	for (i = swit.iterator(); i.hasNext();) {
						//	System.out.println("\nSource: \n" + i.next().toString());	
					//	}
				
					
				}
				
				 if(line[0].equalsIgnoreCase("/SOURCE")){
				//	 System.out.println("===================SOURCE========================");
					//	System.out.println("=======================================================");
				
					source= SOURCEparsing.parse(bufferedReader,line);
			//		 Iterator i;
				//		for (i = source.iterator(); i.hasNext();) {
					//		System.out.println("\nSource: \n" + i.next().toString());	
					//	}
						
				}
				 if (line[0].equalsIgnoreCase("BLANK")){
			//		System.out.println("Um BLANK RECONHECIDO");
				//	System.out.println(line[0]);
				}
				
				
			}// end of while
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo n�o encontrado");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Falha na opera��o de IO");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList getBranch(){
		return this.branch;
	}
	
	public ArrayList getSwitch(){
		return this.swit;
	}
	
	public ArrayList getSource(){
		return this.source;
	}
	
	public static void main(String args[]){
		String s ="C:\\MARABA.atp";
        new Parse(s);
	}

}
