package amazontp.parsing;


import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.Source;

/**
 * @author  
 */
public class SOURCEparsing extends Parsing {

	private static Source source;
	private static boolean test=false;
	private static ArrayList s;

	// Method to do the parsing.
	public static ArrayList parse(BufferedReader bufferedReader,String []line) throws Exception {
		//String line = null;

	//	BufferedReader bufferedReader = new BufferedReader(new FileReader(
		//		fileName));
		 s = new ArrayList<Component>(); 
		do {
			//line = bufferedReader.readLine();

			// Exits in blank line.
			exitAtBlankLine(line[0]);

			// Possible ends of the SOURCE card.
			if (isBlankCard(line[0]) || isOutputCard(line[0])) {
				break;
			}

			StringTokenizer stringTokenizer = new StringTokenizer(line[0]);

			// If a SOURCE is recognized, start its parsing.
			if (isSourceToken(stringTokenizer.nextToken())) {
				do {
					line[0] = bufferedReader.readLine();

				//	System.out.println("Linha: " + line[0]);
					stringTokenizer = new StringTokenizer(line[0]);

					if (isBlankCard(line[0]) || isOutputCard(line[0])) {
						test=true;
						break;
					}

					// Printing the comment lines.
					if (isComment(stringTokenizer.nextToken())) {

					//	System.out.println("	Comment line!");
					//	System.out.println(" ");
					}

					else {

						// JB: Consegui fazer pular os PARAMETER FITTING, agora
						// s� falta
						// faz�-los armazenar nas Collecions essas linhas
						// tamb�m!
						// O mesmo desafio � necess�rio nos Transformadores na
						// classe RLCparsing
						if (line[0].length() >= 17
								&& line[0].substring(0, 17).equals(
										"PARAMETER FITTING")) {
					//		System.out.println("PARAMETER FITTING detected!");
							for (int i = 0; i < 8; i++) {
								line[0] = bufferedReader.readLine();
						//		System.out.println(" ");
							//	System.out
								//		.println("This line belongs to a PARAMETER FITTING!!");
							//	System.out.println("Line: " + line[0]);
							//	System.out.println(" ");
							}

						}

						else {

							// C 1 2 3 4 5 6 7 8
							// C
							// 345678901234567890123456789012345678901234567890123456789012345678901234567890
							// C < n 1><>< Ampl. >< Freq. ><Phase/T0>< A1 >< T1
							// ><
							// TSTART >< TSTOP >

							source = new Source();
							// Collecting Type information.
							if (line[0].length() > 0) {
									source.setType(line[0].substring(0, 2));
							}

							if (line[0].length() > 2) {
								// Collecting Node 1 name.
									source.setNode1(line[0].substring(2, 8));
							}

							if (line[0].length() > 8) {
								// Collecting Initial information.
									source.setInitial(line[0].substring(8, 10));
							}

							if (line[0].length() > 10) {
								// Collecting Amplitude information.
									source.setAmplitude(line[0]
											.substring(10, 20));
							}

							if (line[0].length() > 20) {
								// Collecting Frequency information.
									source.setFrequency(line[0]
											.substring(20, 30));
							}

							if (line[0].length() > 30) {
								// Collecting Phase/TO information.
									source.setPhaseT0(line[0]
											.substring(30, 40));
							}

							if (line[0].length() > 40) {
								// Collecting A1 information.
									source.setA1(line[0].substring(40, 50));
							}

							if (line[0].length() > 50) {
								// Collecting T1 information.
									source.setT1(line[0].substring(50, 60));
							}

							if (line[0].length() > 60) {
								// Collecting TSTART information.
									source.setTStart(line[0]
											.substring(60, 70));
							}

							if (line[0].length() > 70) {
								// Collecting TSTOP information.
									source.setTStop(line[0]
											.substring(70, 80));
							}
							s.add(source);
						}
					}

				} while (true);
				if(test==true)
					break;

			}
			line[0] = bufferedReader.readLine();
		} while (true);

		return s;
	}
	
	public static void main(String[] args) {
		try {
			String file = "C:\\Programs\\ATPWatcom\\teste.atp";
			//parse(file);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
