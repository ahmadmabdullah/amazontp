package amazontp.parsing;

import java.util.ArrayList;
import java.util.Iterator;


import amazontp.atpcomponents.LineZT;


public class BreakLineZTAndInsertSwitches {
	private ArrayList<LineZT> lzt;

	//private LineZTParsing lztparsing;
	
	private Parse parse;

	public BreakLineZTAndInsertSwitches(String filename) {
		lzt=new ArrayList<LineZT>();
		Iterator i;
		LineZT x;
		//lztparsing = new LineZTParsing(filename);
		parse = new Parse(filename);
		
		for(i=parse.getBranch().iterator();i.hasNext();){
			//System.out.println("\nSource: \n" + i.next().toString());
			try{
				x=(LineZT)i.next();
				lzt.add(x);	
			}
			catch(Exception e){
				System.err.println("Selected device is not a line");
			}
						
		}

		

	}

	public String[] getNewBranches() {
		String[] s = new String[lzt.size()];

		for (int i = 0; i < s.length; i++) {

			double length = lzt.get(i).getLength();

			// create the new lines
			LineZT lzt1, lzt2;

			// copy original parameters and define the new length values
			lzt1 = new LineZT(lzt.get(i).getGenNodeA(), getBreakNodeName(i, 'A'),
					lzt.get(i).getGenNodeB(), getBreakNodeName(i, 'B'), lzt.get(i)
							.getGenNodeC(), getBreakNodeName(i, 'C'),
					((length - 10.0 >= 0) ? length - 10 : 0), lzt.get(i)
							.getZeroSeqResistance(), lzt.get(i)
							.getZeroSeqInductance(), lzt.get(i)
							.getZeroSeqCapacitance(), lzt.get(i)
							.getPosSeqResistance(), lzt.get(i)
							.getPosSeqInductance(), lzt.get(i)
							.getPosSeqCapacitance());

			lzt2 = new LineZT(getBreakNodeName(i, 'A'), lzt.get(i).getEndNodeA(),
					getBreakNodeName(i, 'B'), lzt.get(i).getEndNodeB(),
					getBreakNodeName(i, 'C'), lzt.get(i).getEndNodeC(), (10.0),
					lzt.get(i).getZeroSeqResistance(), lzt.get(i)
							.getZeroSeqInductance(), lzt.get(i)
							.getZeroSeqCapacitance(), lzt.get(i)
							.getPosSeqResistance(), lzt.get(i)
							.getPosSeqInductance(), lzt.get(i)
							.getPosSeqCapacitance());

			// write branches
			s[i] = "C AMAZONTP - Starting to break Line " + i
					+ " and to insert the switches.\r\n";
			s[i] += "C Part 1:\r\n" + lzt1.getBranchCard() + "C Part 2:\r\n"
					+ lzt2.getBranchCard();
			s[i] += "C Switches:\r\n";
			s[i] += "  "
					+ getMiddleNodeName(i, 'A')
					+ getEndNodeName(i)
					+ "            1000.0                                               0\r\n";
			s[i] += "  "
					+ getMiddleNodeName(i, 'B')
					+ getEndNodeName(i)
					+ "            1000.0                                               0\r\n";
			s[i] += "  "
					+ getMiddleNodeName(i, 'C')
					+ getEndNodeName(i)
					+ "            1000.0                                               0\r\n";
			s[i] += "  "
					+ getStgNodeName(i)
					+ "                  1000.0                                               0\r\n";
			s[i] += "C AMAZONTP - End of Line " + i
				+ " manipulation.\r\n";
		}
		return s;
	}

	private String getBreakNodeName(int lineNumber, char phase) {
		// format the node names
		if (lineNumber < 10) {
			return "NEW0" + lineNumber + phase;
		} else {
			return "NEW" + lineNumber + phase;
		}
	}

	private String getMiddleNodeName(int lineNumber, char phase) {
		// format the node names
		if (lineNumber < 10) {
			return "MID" + phase + "0" + lineNumber;
		} else {
			return "MID" + phase + lineNumber;
		}
	}

	private String getEndNodeName(int lineNumber) {
		// format the node names
		if (lineNumber < 10) {
			return "END_0" + lineNumber;
		} else {
			return "END_" + lineNumber;
		}
	}

	private String getStgNodeName(int lineNumber) {
		// format the node names
		if (lineNumber < 10) {
			return "STG_0" + lineNumber;
		} else {
			return "STG_" + lineNumber;
		}
	}

	public ArrayList<LineZT> getLzt() {
		return lzt;
	}

	public static void main(String[] args) {
		String t= "E:/Jose/Circuitos/MARABA.atp";
		BreakLineZTAndInsertSwitches blzt = new BreakLineZTAndInsertSwitches(t);
		String s[]= blzt.getNewBranches();
		
		for (int i = 0; i < s.length; i++) {
			
				System.out.println(s[i]);
				System.err.println("--------------------------");
		}
	}
}
