package amazontp.parsing;

public abstract class Parsing {

	// Method to find comment lines.
	public static boolean isComment(String comment) {
		return comment.equalsIgnoreCase("C");
	}

	// Exits in blank line.
	protected static void exitAtBlankLine(String line) {
		if (line.trim().equals("") || line.trim().equals(null)) {
			System.out.println("Found a blank line. Master File is wrong.");
			System.exit(1);
		}
	}

	// Find the BRANCH card
	protected static boolean isBranchCard(String s) {
		return (s.equalsIgnoreCase("/BRANCH"));
	}

	// Find the BLANK card
	protected static boolean isBlankCard(String s) {
		return s.substring(0, 5).equals("BLANK");
	}

	// Find the SWITCH card
	protected static boolean isSwitchCard(String s) {
		return s.substring(0, 7).equals("/SWITCH");
	}
	
	protected static boolean isSwitchToken(String s) {
		return s.equals("/SWITCH");
	}

	// Find the SOURCE card
	protected static boolean isSourceCard(String s) {
		return s.substring(0, 7).equals("/SOURCE");
	}
	
	protected static boolean isSourceToken(String s) {
		return s.equals("/SOURCE");
	}

	// Find the OUTPUT card
	protected static boolean isOutputCard(String s) {
		return s.substring(0, 7).equals("/OUTPUT");
	}
}
