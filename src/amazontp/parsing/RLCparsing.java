package amazontp.parsing;

import amazontp.atpcomponents.RLC;
import amazontp.atpcomponents.Transformer;

import java.io.*;
import java.util.*;

/**
 * @author  Jefferson
 */
public class RLCparsing extends Parsing {

	private static RLC rlc;

	// Method to do the parsing.
	public static Object parse(String fileName) throws Exception {
		String line = null;

		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));
		do {
			line = bufferedReader.readLine();

			// Exits in blank line.
			exitAtBlankLine(line);

			// Possible ends of the BRANCH card.
			if (isBlankCard(line) || isSwitchCard(line) || isSourceCard(line)
					|| isOutputCard(line)) {
				break;
			}

			StringTokenizer stringTokenizer = new StringTokenizer(line);

			// If a BRANCH is reconized, start its parsing.
			if (isBranchCard(stringTokenizer.nextToken())) {
				ArrayList<Transformer> transformers = new ArrayList<Transformer>();  
				do {
					line = bufferedReader.readLine();

					// Prints the line.
					System.out.println("Line: " + line);
					stringTokenizer = new StringTokenizer(line);

					// Possible ends of the BRANCH card.

					if (isBlankCard(line) || isSwitchCard(line) || isSourceCard(line)
							|| isOutputCard(line)) {
						break;
					}

					// Printing the comment lines.
					if (isComment(stringTokenizer.nextToken())) {

						System.out.println("	Comment Line!");
						System.out.println(" ");
					}

					else {

						// Discovering transformers.

						// A transformer will be called "Undefined Transformer"
						// if it is totally
						// declared in the master file. It will be called
						// "Already-Defined" if
						// it is just a "copy" of another transformer.

						// Challenge: A method to identify how many widings a
						// tranformer has.

						if (line.length() >= 13
								&& line.substring(2, 13).equals("TRANSFORMER")) {
							System.out.println("Transformer detected!");
							// Identifying Undefined Transformers.
							Transformer tf = new Transformer();							
							tf.setBustop(line.substring(38,44));
						    ArrayList<Double> flux = tf.getFlux();
						    ArrayList<Double> current = tf.getCurrent();
							System.out.println("This is an UNDEFINED TRANSFORMER.");
							System.out.println("Bustop: "+tf.getBustop());
							
							if (line.length() >= 21
									&& line.substring(15, 21).equals("      ")) {
								do {
									line = bufferedReader.readLine();									
									System.out.println("Line: " + line);							
									flux.add(Double.parseDouble(line.substring(3,14)));
									current.add(Double.parseDouble(line.substring(19,30)));
								} while (line.length() >= 17
										&& line.substring(0, 17).equals(
												"            9999") == false);

								// JB: Como no master.atp todos os
								// transformadores tem 2 widings, ent�o depois
								// eu acho um jeito pra tentar detectar os casos
								// de 3 widings.

								int nWidings = 2; // Realize that this number
								// will
								// never be larger than 3.

								for (int i = 0; i < nWidings; i++) {
									line = bufferedReader.readLine();
									System.out.println(" ");
									System.out
											.println("This is an UNDEFINED TRANSFORMER.");
									System.out.println("Line: " + line);
									System.out.println(" ");
								}

							}

							// Identifying Already-Defined Transformers.
							else {
								// Ainda viciando no master.atp
								int nWidingsAD = 2;

								for (int i = 0; i < nWidingsAD; i++) {
									line = bufferedReader.readLine();
									System.out.println(" ");
									System.out.println("This is an ALREADY-DEFINED TRANSFORMER.");
									System.out.println("Line: " + line);
									System.out.println(" ");
								}
							}
							transformers.add(tf);

						}

						else {

							rlc = new RLC();
							if (line.length() > 0) {
								// Collecting iType information.
									rlc.setIType(line.substring(0, 2));
							}
							if (line.length() > 2) {
								// Collecting Node 1 name.
									rlc.setNode1(line.substring(2, 8));
							}

							if (line.length() > 8) {
								// Collecting Node 2 name.
									rlc.setNode2(line.substring(8, 14));
							}

							if (line.length() > 26) {
								// Collecting resistance.
									rlc.setResistance(line
											.substring(26, 32));
							}

							if (line.length() > 32) {
								// Collecting inductance.
									rlc.setInductance(line
											.substring(32, 38));
							}

							if (line.length() > 38) {
								// Collecting capacitance.
									rlc.setCapacitance(line
											.substring(38, 44));
							}

							if (line.length() > 44) {
								// Collecting Length.
									rlc.setLength(line
											.substring(44, 50));
							}

							if (line.length() > 80) {
								// Collecting Output.
									rlc.setOutput(line
											.substring(79));
							}

						}

					}

				} while (true);

			}

		} while (true);
		return rlc;

	}
	public static void main(String[] args) {
		//RLCparsing rlcparsing = new RLCparsing();
		try {
			String file = "C:\\Programs\\ATPWatcom\\teste.atp";
			parse(file);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}

}
