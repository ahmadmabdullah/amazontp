package amazontp.parsing.ParsingComponents;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.LineZ;
import amazontp.atpcomponents.LineZT;
import amazontp.atpcomponents.LineZ_T;
import amazontp.parsing.Parsing;

/**
 * 
 * @author Jota_Erre
 * 
 */

public class LineParsing {

	private static ArrayList lines;

	public static ArrayList line(BufferedReader bufferedReader, String[] line)
			throws IOException {
		ArrayList<String> typeLine = new ArrayList<String>();
		LineParsing.lines = new ArrayList<Component>();
		while (line[0].contains("-")&&!Parsing.isComment(line[0])) {
			if (line[0].contains("-1")) {
				typeLine.add(line[0]);
				line[0] = bufferedReader.readLine();
				if (line[0].contains("-2") && !Parsing.isComment(line[0])) {
					typeLine.add(line[0]);
					line[0] = bufferedReader.readLine();
					if (line[0].contains("-3") && !Parsing.isComment(line[0])) {
						
						typeLine.add(line[0]);
						LineParsing.lines.add(LineParsing.lineThreePhase(typeLine));
						typeLine.clear();
						line[0] = bufferedReader.readLine();
					} else {
						LineParsing.lines.add(LineParsing.lineTwoPhase(typeLine));
					}
				} else {
					LineParsing.lines.add(LineParsing.lineOnePhase(typeLine));
				}
			}

		}// end while

		return LineParsing.lines;
	}

	public static LineZ lineOnePhase(ArrayList<String> line) {
		LineZ z = new LineZ();
		String aux;

		for (Iterator i = line.iterator(); i.hasNext();) {

			aux = (String) i.next();

			z.setGenNodeA(aux.substring(3, 8));
			z.setEndNodeA(aux.substring(9, 14));
			z.setResistance(aux.substring(28, 32));
			z.setZ(aux.substring(34, 38));
			z.setV(aux.substring(40, 44));
			z.setLength(aux.substring(46, 50));
			z.setIline(aux.substring(51, 52));
			z.setOutput(aux.substring(79, 80));

		}

		return z;

	}

	public static LineZ_T lineTwoPhase(ArrayList<String> line) {
		LineZ_T z = new LineZ_T();
		String aux;
		int cont = 1;

		for (Iterator i = line.iterator(); i.hasNext();) {
			aux = (String) i.next();
			if (cont == 1) {
				z.setGenNodeA(aux.substring(3, 8));
				z.setEndNodeA(aux.substring(9, 14));
				z.setEndResistance(aux.substring(28, 32));
				z.setEndZ(aux.substring(34, 38));
				z.setEndV(aux.substring(40, 44));
				z.setLength(aux.substring(46, 50));
				z.setIline(aux.substring(51, 52));
				z.setOutput(aux.substring(79, 80));
				cont++;
			} else {
				z.setGenNodeB(aux.substring(3, 8));
				z.setEndNodeB(aux.substring(9, 14));
				z.setGenResisstance(aux.substring(28, 32));
				z.setGenZ(aux.substring(34, 38));
				z.setGenV(aux.substring(40, 44));

			}

		}

		return z;

	}

	public static LineZT lineThreePhase(ArrayList<String> line) {
		
		LineZT z = new LineZT();
		String aux;
		int cont = 1;

		for (Iterator i = line.iterator(); i.hasNext();) {
			aux = (String) i.next();
//			 reads node names of phase A, zeroSeq and length values
			if (cont == 1) {
				z.setFase(aux.substring(2,7));
				z.setFaseA(aux);
				z.setGenNodeA(aux.substring(2, 8));
				z.setEndNodeA(aux.substring(9, 14));
				//z.setEndResistance(aux.substring(28, 32));
				z.setZeroSecResistance(Double.parseDouble(aux.substring(26, 32)));
				//z.setEndZ(aux.substring(34, 38));
				z.setZeroSecInductance(Double.parseDouble(aux.substring(32, 38)));
				//z.setEndV(aux.substring(40, 44));
				z.setZeroSecCapacitance(Double.parseDouble(aux.substring(38, 44)));
				//z.setLength(aux.substring(46, 50));
				z.setLength(Double.parseDouble(aux.substring(44, 50)));
				z.setIline(aux.substring(51, 52));
				z.setOutput(aux.substring(79, 80));
				cont++;
//				 reads node names of phase B and posSeq values
			} else if (cont==2) {
				z.setFaseB(aux);
				z.setGenNodeB(aux.substring(2, 8));
				z.setEndNodeB(aux.substring(8, 14));
				//z.setGenResisstance(aux.substring(28, 32));
				z.setPosSecResistance(Double.parseDouble(aux.substring(26, 32)));
				//z.setGenZ(aux.substring(34, 38));
				z.setPosSecInductance(Double.parseDouble(aux.substring(32, 38)));
				//z.setGenV(aux.substring(40, 44));
				z.setPosSecCapacitance(Double.parseDouble(aux.substring(38, 44)));
                cont++;
			}
//			 reads node names of phase C
			else{
				z.setFaseC(aux);
				z.setGenNodeC(aux.substring(2, 8));
				z.setEndNodeC(aux.substring(8, 14));
			}

		}

		return z;

	}

}// end class
