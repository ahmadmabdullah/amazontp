package amazontp.parsing.ParsingComponents;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.Transformer;

/**
 * 
 * @author claudomir junior
 *
 */

public class TransformerParsing {
	
	private static ArrayList trans;
	
	public static ArrayList transformer(BufferedReader bufferedReader, String[] line) throws IOException{
		
		TransformerParsing.trans=new ArrayList<Component>();
		
		Transformer tf = new Transformer();							
		tf.setBustop(line[0].substring(38,44));
	    ArrayList<Double> flux = tf.getFlux();
	    ArrayList<Double> current = tf.getCurrent();
	    
	    if (line[0].length() >= 21
				&& line[0].substring(15, 21).equals("      ")) {
			do {
				line[0] = bufferedReader.readLine();									
		//		System.out.println("Line: " + line[0]);							
				flux.add(Double.parseDouble(line[0].substring(3,14)));
				//System.out.println("comprimento="+line.length());
				//current.add(Double.parseDouble(line.substring(19,30)));
			} while (line[0].length() >= 17
					&& line[0].substring(0, 17).equals(
							"            9999") == false);

			// JB: Como no master.atp todos os
			// transformadores tem 2 widings, ent�o depois
			// eu acho um jeito pra tentar detectar os casos
			// de 3 widings.

			int nWidings = 2; // Realize that this number
			// will
			// never be larger than 3.

			for (int i = 0; i < nWidings; i++) {
				line[0] = bufferedReader.readLine();
//				System.out.println(" ");
//				System.out
//						.println("This is an UNDEFINED TRANSFORMER.");
//				System.out.println("Line: " + line[0]);
//				System.out.println(" ");
			}

		}

		// Identifying Already-Defined Transformers.
		else {
			// Ainda viciando no master.atp
			int nWidingsAD = 2;

			for (int i = 0; i < nWidingsAD; i++) {
				line[0] = bufferedReader.readLine();
//				System.out.println(" ");
//				System.out.println("This is an ALREADY-DEFINED TRANSFORMER.");
//				System.out.println("Line: " + line[0]);
//				System.out.println(" ");
			}
		}
	    TransformerParsing.trans.add(tf);
		
		return TransformerParsing.trans;
	}

}
