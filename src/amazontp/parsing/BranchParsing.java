package amazontp.parsing;

import java.io.BufferedReader;
import java.util.ArrayList;

import amazontp.atpcomponents.Component;
import amazontp.parsing.ParsingComponents.LineParsing;
import amazontp.parsing.ParsingComponents.TransformerParsing;

/**
 * 
 * @author Claudomir Junior
 * 
 */

public class BranchParsing extends Parsing {

	private static boolean test = false;

	private static ArrayList branch;
	
	private static BufferedReader bufferedReader;

	/**
	 * 
	 * @param bufferedReader
	 * @param line
	 * @return returns all elements from the branch card
	 * @throws Exception
	 */

	public static ArrayList parse(BufferedReader bufferedReader, String[] line)
			throws Exception {
		BranchParsing.bufferedReader= bufferedReader;
		BranchParsing.branch=new ArrayList<Component>();
		do {
			
			line[0] = BranchParsing.bufferedReader.readLine();
			
			// Exits in blank line.
			exitAtBlankLine(line[0]);
            
			// Possible ends of the BRANCH card.
        	if (isBlankCard(line[0]) || isSwitchCard(line[0])
					|| isSourceCard(line[0]) || isOutputCard(line[0])) {
                test=true;
				break;
			}
        	// discovering lines
        	if(line[0].contains("-")&& !Parsing.isComment(line[0])){
        		BranchParsing.branch.addAll(LineParsing.line(BranchParsing.bufferedReader,line));
        	}
        	
        	if (line[0].length() >= 13 && line[0].substring(2, 13).equals("TRANSFORMER")){
        		BranchParsing.branch.addAll(TransformerParsing.transformer(BranchParsing.bufferedReader,line));
        	}
        	
		} while (true);

		return BranchParsing.branch;

	}
	


}
