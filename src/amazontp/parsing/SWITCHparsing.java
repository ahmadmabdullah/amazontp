package amazontp.parsing;


import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import amazontp.atpcomponents.Component;
import amazontp.atpcomponents.Switch;

/**
 * @author  Jefferson
 */
public class SWITCHparsing extends Parsing {
	
	
	
	private static ArrayList sw;

	private static Switch swiTch;
	
	private static boolean test=false;

	// Method to do the parsing.
	public static ArrayList parse(BufferedReader bufferedReader,String []line) throws Exception {
		//String line = null;
		// String[] vcomment = null;
		//BufferedReader bufferedReader = new BufferedReader(new FileReader(
			//	fileName));
		SWITCHparsing.sw = new ArrayList<Component>(); 
		do {

			

			// Exits in blank line.
			exitAtBlankLine(line[0]);

			// Possible ends of the SWITCH card.
			if (isBlankCard(line[0]) || isSourceCard(line[0]) || isOutputCard(line[0])) {
				break;
			}

			StringTokenizer stringTokenizer = new StringTokenizer(line[0]);

			// If a SWITCH is recognized, start its parsing.
			if (isSwitchToken(stringTokenizer.nextToken())) {
				do {
					line[0] = bufferedReader.readLine();

				//	System.out.println("Line: " + line[0] );
					stringTokenizer = new StringTokenizer(line[0]);

					if (isBlankCard(line[0]) || isSourceCard(line[0])
							|| isOutputCard(line[0])) {
						test=true;
						break;
					}

					// Printing the comment lines.
					if (isComment(stringTokenizer.nextToken())) {

		//				System.out.println("	Comment line!");
		//				System.out.println(" ");
					}

					else {

						// C 1 2 3 4 5 6 7 8
						// C
						// 345678901234567890123456789012345678901234567890123456789012345678901234567890
						// C < n 1>< n 2>< Tclose ><Top/Tde >< Ie ><Vf/CLOP ><
						// type >

						swiTch = new Switch();
						if (line[0].length() > 0) {
							// Collecting iType information.
							swiTch.setIType(line[0].substring(0, 2));
						}

						if (line[0].length() > 2) {
							// Collecting Node 1 name.
							swiTch.setNode1(line[0].substring(2, 8));
						}

						if (line[0].length() > 8) {
							// Collecting Node 2 name.
							swiTch.setNode2(line[0].substring(8, 14));
						}

						if (line[0].length() > 14) {
							// Collecting TClose information.
							swiTch.setTClose(line[0].substring(14, 24));

						}

						if (line[0].length() > 24) {
							// Collecting Top/Tde information.
							swiTch.setTopTde(line[0].substring(24, 34));
						}

						if (line[0].length() > 34) {
							// Collecting Ie information.
							swiTch.setIe(line[0].substring(34, 44));
						}

						if (line[0].length() > 44) {
							// Collecting VfClop information.
							swiTch.setVfCLOP(line[0].substring(44, 54));
						}

						if (line[0].length() > 54) {
							// Collecting Type information.
							swiTch.setType(line[0].substring(54, 64));
						}

						if (line[0].length() == 80) {
							// Collecting Output information.

							swiTch.setOutput(line[0].substring(79));

						}
						sw.add(swiTch);
					}

				} while (true);
				if (test==true)
					break;

			}
			line[0] = bufferedReader.readLine();
		} while (true);
		return sw;
	}
}
