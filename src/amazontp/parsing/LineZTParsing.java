package amazontp.parsing;

import java.io.*;
import java.util.ArrayList;

import javax.swing.JFrame;

import amazontp.atpcomponents.LineZT;

/**
 * @author Gustavo Guedes
 * 
 */
public class LineZTParsing {

	// can read up to MAX lines
	//private final int MAX = 100;

	private ArrayList<LineZT> lztArray =  new ArrayList<LineZT>();
	

	public LineZTParsing(String fileName) {
		//initializeLZTsArray();

		try {
			parseLines(fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*private void initializeLZTsArray() {
		for (int i = 0; i < lztArray.length; i++) {
			lztArray[i] = new LineZT();
		}
	}*/

	private void parseLines(String fileName) throws Exception {

		// link the file to the BufferedReader object
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				fileName));

		String line;
		int i = 0;
		while (true) {

			line = bufferedReader.readLine();
			// end of file?
			if (line == null)
				break;

			// if the first char of line is "-", it's a Line branch card
			if (line.charAt(0) == '-') {
				LineZT lzt = new LineZT();   
				// reads node names of phase A, zeroSeq and length values
				lzt.setGenNodeA(line.substring(2, 8));
				lzt.setEndNodeA(line.substring(8, 14));
				lzt.setZeroSecResistance(Double.parseDouble(line.substring(26, 32)));
				lzt.setZeroSecInductance(Double.parseDouble(line.substring(32, 38)));
				lzt.setZeroSecCapacitance(Double.parseDouble(line.substring(38, 44)));
                lzt.setLength(Double.parseDouble(line.substring(44, 50)));

				// reads node names of phase B and posSeq values
				line = bufferedReader.readLine();
				lzt.setGenNodeB(line.substring(2, 8));
				lzt.setEndNodeB(line.substring(8, 14));
				lzt.setPosSecResistance(Double.parseDouble(line.substring(26, 32)));
				lzt.setPosSecInductance(Double.parseDouble(line.substring(32, 38)));
				lzt.setPosSecCapacitance(Double.parseDouble(line.substring(38, 44)));

				// reads node names of phase C
				line = bufferedReader.readLine();
				lzt.setGenNodeC(line.substring(2, 8));
				lzt.setEndNodeC(line.substring(8, 14));

				// ok, we can now look for the next LineZT's
				i++;
				lztArray.add(lzt);
			}
		}
	}
	

	public ArrayList<LineZT> getLztArray() {
		return lztArray;
	}

	public static void main(String[] args) {
		LineZTParsing lztparsing = new LineZTParsing(
				"C:\\Programs\\ATPWatcom\\master.atp");
		ArrayList<LineZT> lztarray = lztparsing.getLztArray();
		
		for (int i = 0; i < lztarray.size(); i++) {
			System.out.print("Line " + i + ":\n" + lztarray.get(i).toString());
			System.out.print("Branch cards:\n" + lztarray.get(i).getBranchCard());
			JFrame jf = new JFrame();
			jf.getContentPane().add(lztarray.get(i).getLineZTPropertiesPanel());
			jf.setSize(jf.getContentPane().getPreferredSize());
			jf.setVisible(true);
		}
	}

}